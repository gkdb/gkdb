* Can I use **None** in an IDS?
  * No it's not possible, if you don't want to specify a value you have to use the default values indicated in IMAS specifications
    Please note that the IDS in the *idspy_dictionaries* python package already use by default the official IMAS default values
  
* Can I make my own class to after insert in the GKDB or should I use the one in the *idspy_dictionaries" package?
  * Due to the type checking in the insertion process, it will not work or have undetermined behaviour and fail in all cases

* I didn't know about the default IMAS default values so I used my own can I continue to use them?
  * Sorry but no, only the official IMAS default values are considered as default values

* Is there any type checking for ids members or can I put any kind of values?
  * There is like "hard" type checking for values in ids members so it's highly probable that an exception will be thrown if your data does not have the right type.
* Is there any exception concerning typing?
  * Only **2** currently :
    * if the expected type is a decimal number and you use an integer (but not the opposite)
    * if you use a boolean value and the expected type is an int (in that case *True* is converted to *1* and *False* to *0*)
* In the case of a data array are the column size and order have to follow IMAS documentations?
  * Yes it's mandatory otherwise the IDS will be indicated as not valid. Please note that in the IMAS documentation, the first coordinate is also the first coordinate of the array etc...
  
* If I don't know what value I should put for a member?
  * If you are not sure, keep the default value.
* In IMAS philosophy no IDS or IDS members are mandatory, is it also the case here?
  * No some IDS members or IDS class are mandatory, a full list will be given later in the development process

* Is an array with shape (N,) equivalent to an array with shape (N,1)? 
  * No it's not the case in python+numpy you have to follows IMAS recommandations

* My array as just 1 element, can I store it as a float/int?
  * No you cannot it has to be store as an array with just 1 element in.


* What are the mandatory elements in a gyrokinetic IDS?
  * Code : name and commit or name and release
  * Species : everything
  * model : everything
  * input_species_global : everything
  * flux_surface : everything
  * non_linear : optional
  * linear : optional
  * collisions : mandatory except if simulation is collisionless