from ldap3 import Server, Connection, ALL
import inspect
from ldaphelper import ldaphelper
import sys
import os
root = os.path.dirname(os.path.realpath(__file__))
for subpath in ['ssha']:
    path = os.path.join(root, subpath)
    if not path in sys.path:
        sys.path.append(path)

from ssha.openldap_passwd import *
from base64 import b64encode

def connect(server_name, username, password):
    # the following is the user_dn format provided by the ldap server
    admin_dn = "cn="+username+","+base_dn
    # adjust this to your base dn for searching
    serv = Server(server_name, get_info=ALL)
    conn = Connection(serv, user=admin_dn, password=password)
    if not conn.bind():
        raise Exception('error in bind', c.result)
    return conn

base_dn = "dc=gkdb,dc=org"
ldap_server="gkdb.org"
username = os.environ['LDAP_ADMIN_USER']
password = os.environ['LDAP_ADMIN_PASSWORD']
conn = connect(ldap_server, username, password)

POSIX_TO_SQL = {'sql_readonly': 'read_only',
                'sql_write': 'write_to_sandbox',
                'sql_admin': 'developer'
                }
class LdapGroup:
    def to_addModlist(self):
        attrs = inspect.getmembers(self, lambda a:not(inspect.isroutine(a)))
        modlist = []
        for attr, val in attrs:
            if not attr.startswith('_') and val is not None:
                if val.__class__ in [str, list]:
                    pass
                else:
                    val = str(val)
                modlist.append((attr, val))
        return modlist

    def to_server(self, server_dn=base_dn):
        conn.add('cn=' + self.cn + ',' + server_dn, self.to_addModlist())

class PosixGroup(LdapGroup):
    objectclass = ['top', 'posixGroup']
    def __init__(self, name, gid, description=None, memberUid=None, userPassword=None, autopush=True):
        self.cn = name
        self.gidNumber = gid

        self.description = description
        self.memberUid = memberUid
        self.userPassword = userPassword
        if autopush:
            self.to_server()


class PosixAccount(LdapGroup):
    objectclass = ['top', 'inetOrgPerson', 'posixAccount']
    def __init__(self, first_name, last_name, primary_gid, description=None, gecos=None, loginShell=None, userPassword=None, autopush=True):
        self.sn = last_name
        self.gn = first_name
        self.gidNumber = primary_gid
        self.uid = ''.join([first_name[0].lower()] + last_name.lower().split())
        self.cn = self.uid
        self.homeDirectory = ''.join(['/home/users/', self.uid])
        if get_highest_uid() == -1:
            self.uidNumber = 1000
        else:
            self.uidNumber = get_highest_uid() + 1
        self.description = description
        self.gecos = gecos
        self.loginShell = loginShell
        if userPassword is None:
            random_bytes = os.urandom(64)
            password = b64encode(random_bytes).decode('utf-8')
            userPassword = make_secret(b64encode(random_bytes).decode('utf-8'))
        self.userPassword = userPassword
        if autopush:
            self.to_server()
        print('created user: ', self.uid, ' with password: ' + password)

def get_all_users():
    search_filter = '(&(objectClass=posixAccount)(uid=*))'
    result = conn.search(base_dn,search_filter,attributes=['uid', 'uidNumber', 'gidNumber'])
    if not result:
        raise Exception('Error getting all users')

def get_all_groups():
    search_filter = '(&(objectClass=posixGroup)(gidNumber=*))'
    result = conn.search(base_dn,search_filter, attributes=['gidNumber', 'cn'])
    if not result:
        raise Exception('Error getting all groups')

def get_highest_uid():
    get_all_users()
    uid_numbers = [e.uidNumber.value for e in conn.entries]
    return max(uid_numbers)

def get_gid_name_map():
    get_all_groups()
    return {e.gidNumber.value: e.cn.value for e in conn.entries}

def get_user_sqlgroup_map():
    get_all_users()
    uids = {e.uid.value: e.gidNumber.value for e in conn.entries}
    gid_name_map = get_gid_name_map()
    uid_sql_grp_map = set((uid, POSIX_TO_SQL[gid_name_map[gid]]) for uid, gid in uids.items())
    return uid_sql_grp_map
