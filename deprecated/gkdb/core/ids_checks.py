from itertools import chain
import json

import numpy as np
import scipy as sc
from scipy.interpolate import interp1d
from IPython import embed

from gkdb.core.equilibrium import get_values_min_max_consistency_check, calculate_a_N, check_r_minor_norm_shape_consistency

allowed_codes = ['GKW', 'GENE', 'test']
error_msg = lambda errors: 'Entry does not meet GKDB definition: {!s}'.format(errors)

def check_wrapper(check_function, ids, errors, *args, on_disallowance=None, **kwargs):
    allow_entry = check_function(ids, errors, *args, **kwargs)
    if on_disallowance == 'raise_immediately' and allow_entry is not True:
        raise Exception(error_msg(errors))
    return allow_entry

def check_json(json_path, only_input, on_disallowance='raise_at_end'):
    with open(json_path) as file:
        ids = json.load(file)
    allow_entry = check_ids_entry(ids, only_input, on_disallowance=on_disallowance)
    return allow_entry

def check_ids_entry(ids, only_input, on_disallowance='raise_at_end'):
    on_disallowance = 'raise_at_end'
    if on_disallowance not in ['raise_immediately', 'raise_at_end', 'print_at_end']:
        raise Exception
    allow_entry = True
    errors = []
    allow_entry &= check_ids_input_fields(ids, errors, on_disallowance=on_disallowance)
    if not only_input:
        allow_entry &= check_ids_output_fields(ids, errors, on_disallowance=on_disallowance)
    if not allow_entry:
        if on_disallowance == 'raise_at_end':
            raise Exception(error_msg(errors))
        elif on_disallowance == 'print_at_end':
            print(error_msg(errors))
    return allow_entry

def check_ids_input_fields(ids, errors, on_disallowance='raise_at_end'):
    check_input_fields_exist(ids)
    allow_entry = True
    allow_entry &= check_wrapper(check_electron_definition, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_quasineutrality, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_centrifugal, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_magnetic_flutter, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_magnetic_compression, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_include_full_curvature_drift, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_collisions_shape, ids, errors, on_disallowance=on_disallowance)
    return allow_entry

def check_ids_output_fields(ids, errors, on_disallowance='raise_at_end'):
    allow_entry = True
    allow_entry &= check_wrapper(check_code_allowed, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_number_of_modes, ids, errors, on_disallowance=on_disallowance)
    #allow_entry &= check_wrapper(check_growth_rate_tolerance, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_poloidal_angle_grid_bounds, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_poloidal_angle_grid_lengths, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_phi_rotation, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_field_normalization, ids, errors, on_disallowance=on_disallowance)
    allow_entry &= check_wrapper(check_r_minor_norm_shape_consistency, ids, errors, on_disallowance=on_disallowance)
    if not allow_entry:
        if on_disallowance == 'raise_at_end':
            raise Exception(error_msg(errors))
        elif on_disallowance == 'print_at_end':
            print(error_msg(errors))
    return allow_entry

def check_code_allowed(ids, errors):
    allow_entry = True
    if not ids['code']['name'] in allowed_codes:
        allow_entry = False
        errors.append("Code '{!s}' not in allowed codes {!s}"
                      .format(ids['code']['name'], allowed_codes))
    return allow_entry

input_fields = [
    'ids_properties',
    'model',
    'species_all',
    'model',
    'flux_surface',
    'wavevector',
    'species',
    'collisions',
]

def check_input_fields_exist(ids):
    missing_fields = []
    for field in input_fields:
        if field not in ids:
            allow_entry = False
            missing_fields.append(field)
    if missing_fields:
        raise Exception('Fields {!s} missing from ids!'.format(missing_fields))

electron_mandatory = {'mass_norm': 2.724437108e-4,
                      'temperature_norm': 1,
                      'density_norm': 1}

def check_electron_definition(ids, errors):
    allow_entry = True
    for spec in ids['species']:
        if spec['charge_norm'] == -1:
            electron = spec
    if electron is None:
        allow_entry = False
        errors.append('Electron species not found')
    else:
        for field, val in electron_mandatory.items():
            if not np.isclose(electron[field], val,1e-3,0):
                allow_entry = False
                errors.append("Invalid value for electron species field '{!s}'".format(field))
    return allow_entry

def check_quasineutrality(ids, errors):
    allow_entry = True
    Zs = [spec['charge_norm'] for spec in ids['species']]
    ns = [spec['density_norm'] for spec in ids['species']]
    RLns = [spec['density_log_gradient_norm'] for spec in ids['species']]
    quasi = np.isclose(sum([Z * n for Z, n in zip(Zs, ns)]), 0)
    quasi_grad = np.isclose(sum([Z * n * RLn for Z, n, RLn in zip(Zs, ns, RLns)]), 0, atol=1E-6)
    if not quasi:
        allow_entry = False
        errors.append("Entry is not quasineutral! Zn = {!s} and ns = {!s}".format(Zs, ns))
    if not quasi_grad:
        allow_entry = False
        errors.append("Entry is not quasineutral for gradients! Zn = {!s}, ns = {!s} and RLns = {!s}".format(Zs, ns, RLns))

    return allow_entry

def check_centrifugal(ids, errors):
    allow_entry = True
    u_N = ids['species_all']['velocity_tor_norm']
    ms = [spec['mass_norm'] for spec in ids['species']]
    Ts = [spec['temperature_norm'] for spec in ids['species']]
    Mach = u_N * np.sqrt([m / T for m, T in zip(ms, Ts)])
    Mach_bound = 0.2
    if any(Mach > Mach_bound) and not ids['model']['include_centrifugal_effects']:
        allow_entry = False
        errors.append('Species with Mach > {!s} and include_centrifugal_effects is False.'.format(Mach_bound))
    return allow_entry

def check_magnetic_flutter(ids, errors):
    allow_entry = True
    if ids['species_all']['beta_reference'] > 0:
        if not ids['model']['include_a_field_parallel']:
            allow_entry = False
            errors.append('include_a_field_parallel should be true if beta_reference > 0')
    return allow_entry

def check_magnetic_compression(ids, errors):
    allow_entry = True
    if ids['species_all']['beta_reference'] > 0.5:
        if not ids['model']['include_b_field_parallel']:
            allow_entry = False
            errors.append('include_b_field_parallel should be true if beta_reference > 0.5')
    return allow_entry

def check_number_of_modes(ids, errors):
    allow_entry = True
    non_linear_run = ids['model']['non_linear_run']
    initial_value_run = ids['model']['initial_value_run']
    if not non_linear_run:
        for ii, wv in enumerate(ids['wavevector']):
            num_eigenmodes = len(wv['eigenmode'])
            if initial_value_run:
                if num_eigenmodes != 1:
                    allow_entry = False
                    errors.append('For an initial value run, the number of eigenmodes per wavevector should be 1, wavevector {!s} has {!s} eigenmodes'.format(ii, num_eigenmodes))
            else:
                if num_eigenmodes < 1:
                    allow_entry = False
                    errors.append('For an eigenvalue run, the number of eigenmodes per wavevector should be at least 1, wavevector {!s} has {!s} eigenmodes'.format(ii, num_eigenmodes))
    return allow_entry

def check_growth_rate_tolerance(ids, errors):
    growth_rate_tolerance_bound = 10
    allow_entry = True
    for ii, wv in enumerate(ids['wavevector']):
        for jj, eig in enumerate(wv['eigenmode']):
            if eig['growth_rate_tolerance'] > growth_rate_tolerance_bound:
                allow_entry = False
                errors.append('Growth rate tolerance has to be under {!s}%. Is {!s} for wavevector {!s} eigenmode {!s}'.format(growth_rate_tolerance_bound, eig['growth_rate_tolerance'], ii, jj))
    return allow_entry

def is_monotonic(array):
    return all(np.diff(array) > 0)

def check_moment_rotation(poloidal_grid, phi_potential, phi_theta_0_bound):
    phi_max_ind = np.argmax(np.abs(phi_potential))
    phi_max = phi_potential[phi_max_ind]

    if phi_max.imag < phi_theta_0_bound and phi_max.real >= 0:
        rotation_okay = True
    else:
        rotation_okay = False
    return rotation_okay

def check_monoticity(ids, errors):
    allow_entry = True
    for ii, wv in enumerate(ids['wavevector']):
        for jj, eig in enumerate(wv['eigenmode']):
            grid = eig['poloidal_angle']
            if not is_monotonic(grid):
                allow_entry = False
                errors.append('Poloidal angle grid should be monotonically increasing. For wavevector {!s} eigenmode {!s} it is not'.format(ii, jj))
    return allow_entry

def check_poloidal_angle_grid_bounds(ids, errors):
    allow_entry = True
    non_linear_run = ids['model']['non_linear_run']
    max_poloidal_angle_shift = np.pi/8.
    for ii, wv in enumerate(ids['wavevector']):
        for jj, eig in enumerate(wv['eigenmode']):
            if not non_linear_run:
                grid = eig['poloidal_angle']
                poloidal_turns = wv['poloidal_turns']
                if not all([(el >= -poloidal_turns * np.pi - max_poloidal_angle_shift) and
                            el <= poloidal_turns * np.pi + max_poloidal_angle_shift for el in grid]):
                    allow_entry = False
                    errors.append('Poloidal grid out of bounds! Should be between [-Np * pi - pi/8, Np * pi + pi/8]. For wavevector {!s} eigenmode {!s} it is not'.format(ii, jj))
    return allow_entry

def make_time_dependent(lst):
    depends_on_time = not np.isscalar(lst[0])
    var = np.array(lst)
    if not depends_on_time:
        var = np.expand_dims(var, 1)
    return var

def check_phi_rotation(ids, errors):
    allow_entry = True
    for ii, wv in enumerate(ids['wavevector']):
        for jj, eig in enumerate(wv['eigenmode']):
            grid = eig['poloidal_angle']
            phi = make_time_dependent(eig['phi_potential_perturbed_norm_real']) + make_time_dependent( eig['phi_potential_perturbed_norm_imaginary']) * 1j
            for kk, phi_vec, in enumerate(phi.T):
                if not check_moment_rotation(grid, phi_vec, 1e-3):
                    allow_entry = False
                    errors.append('Poloidal grid not rotated correctly! Im(phi(theta(max(|phi|)))) != 0 for wavevector {!s} eigenmode {!s} time {!s}'.format(ii, jj, kk))
    return allow_entry

def check_field_normalization(ids, errors):
    allow_entry = True
    fields_perturbed_considered = ['phi_potential_perturbed_norm']
    if ids['model']['include_a_field_parallel']:
        fields_perturbed_considered += ['a_field_parallel_perturbed_norm']
    if ids['model']['include_b_field_parallel']:
        fields_perturbed_considered += ['b_field_parallel_perturbed_norm']

    for ii, wv in enumerate(ids['wavevector']):
        for jj, eig in enumerate(wv['eigenmode']):
            considered_fields = {}
            for field in fields_perturbed_considered:
                for suff in ['_real', '_imaginary']:
                    considered_fields[field + suff] = make_time_dependent(eig[field + suff])
            grid = eig['poloidal_angle']
            for tt in range(considered_fields[fields_perturbed_considered[0] + '_real'].shape[1]):
                A_f = 0.0
                for field in fields_perturbed_considered:
                    A_f += np.trapz(np.abs(considered_fields[field+'_real'][:,tt])**2+np.abs(considered_fields[field+'_imaginary'][:,tt])**2,grid)
                A_f = np.sqrt(A_f/(2.0*np.pi))
                if not np.abs(A_f-1.0) < 1E-6:
                    allow_entry = False
                    errors.append('Normalization amplitude A_f = {!s} should be 1.0 for wavevector {!s} eigenmode {!s} time {!s}'.format(A_f, ii, jj, tt))
    return allow_entry

def check_poloidal_angle_grid_lengths(ids, errors):
    allow_entry = True
    non_linear_run = ids['model']['non_linear_run']
    for ii, wv in enumerate(ids['wavevector']):
        for jj, eig in enumerate(wv['eigenmode']):
            grid = eig['poloidal_angle']
            tgrid = eig['time_norm']
            check_arrays_in = eig.items()
            if 'moments_norm_rotating_frame' in eig:
                check_arrays_in = chain(check_arrays_in, *[mom.items() for mom in eig['moments_norm_rotating_frame']])
            for field, val in check_arrays_in:
                if isinstance(val, list):
                    if field not in ['moments_norm_rotating_frame', 'fluxes_norm', 'time_norm'] and len(val) != len(grid):
                        allow_entry = False
                        errors.append('Field {!s} for wavevector {!s} eigenmode {!s} same length as poloidal_grid'.format(field, ii, jj))
                    depends_on_time = not np.isscalar(val[0])
                    if field not in ['moments_norm_rotating_frame', 'fluxes_norm', 'time_norm'] and depends_on_time and len(val[0]) != len(tgrid):
                        allow_entry = False
                        errors.append('Field {!s} for wavevector {!s} eigenmode {!s} same length as time_norm'.format(field, ii, jj))
    return allow_entry

def check_include_full_curvature_drift(ids, errors):
    allow_entry = True
    if ids['model']['include_b_field_parallel'] is True:
        if ids['model']['include_full_curvature_drift'] is False:
            allow_entry = False
            errors.append('include_full_curvature_drift must be True if include_b_field_parallel is True.')
    return allow_entry

def check_collisions_shape(ids, errors):
    allow_entry = True
    num_spec = len(ids['species'])
    coll_norm = np.array(ids['collisions']['collisionality_norm'])
    if coll_norm.shape != (num_spec, num_spec):
        allow_entry = False
        errors.append('collisionality_norm should be (num_species x num_species), is {!s}.'.format(coll_norm.shape))
    return allow_entry
