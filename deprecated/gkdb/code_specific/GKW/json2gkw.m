% Generate GKW input file(s) from a GKDB JSON file
% 
% 
%
% Inputs:
%  fl_json      input GKDB json file (file path+name)
%  fl_gkw       reference GKW input file (file path+name)
%  from_param   if 1: check if the JSON file is from a GKW run, 
%                              if yes, use only the 'parameters' key/value table 
%                              otherwise use the GKDB inputs
%               if 0 (default): use the GKDB inputs to generate the GKW input file
%  proj         project name for the created input files 
%  flroot       filename root for the created input files
%
% Outputs:
%
%
% Remarks: 1) The grids, dissipation, time step, parallelisation, etc. of the reference input file are not modified by this script
%          2) The number of poloidal turns (NPERIOD) is changed keeping the number of points per poloidal turn constant
%
% YC - 15.02.2019


function []=json2gkw(fl_json,fl_gkw,from_param,proj,flroot)


if ~exist('from_param')||isempty(from_param)
 from_param=0;
end

if from_param==1
 disp('Generating GKW input files from the parameters table of a GKDB JSON is not implemented yet')
 return
end


% load the json file
out=readjson(fl_json);

if ~out.model.initial_value_run
 disp('Set up of eigenvalue runs not implemented yet')
 return
end
if out.model.non_linear_run
 disp('Set up of non-linear runs not implemented yet')
 return
end



% get number of species
nsp=size(out.species,2);

% get the wavevector values 
nk=size(out.wavevector,2);
[kx,ky]=deal(zeros(nk,1));
for ii=1:nk
 kx(ii)=out.wavevector{ii}.radial_component_norm;
 ky(ii)=out.wavevector{ii}.binormal_component_norm;
end

% determine the type of scan required to cover the wavevector range
% scan_type = 0 -> single wavevector, 
%             1 -> 1D scan, 
%             2 -> 2D scan, 
%            -2 -> coupled 1D scan
kx_unique=unique(kx);
ky_unique=unique(ky);
scan_type=-2; % coupled 1D scan
if length(kx_unique)*length(ky_unique)==1
 scan_type=0;  % single wavevector
elseif length(kx_unique)==1|length(ky_unique)==1
 scan_type=1;  % 1D scan
elseif length(kx_unique).*length(ky_unique)==length(kx) % possibly a 2D scan
 kdum1=repmat(ky_unique,[1 length(kx_unique)]) + i.*repmat(transpose(kx_unique),[length(ky_unique) 1]);
 kdum1=sort(kdum1(:));
 kdum2 = ky + i.*kx;
 kdum2 = sort(kdum2(:));
 if kdum1==kdum2 % 2D scan
  scan_type=2;
 end
end


% load the GKW reference file 
[GKWref,sss_in]=read_gkwinput(fl_gkw,''); 

% strip out comments
sss_in=regexprep(sss_in,'![^\n]*',''); 
sss_in=[sprintf('/\n') sss_in];
sss_in=regexprep(sss_in,'/\s[^&]*&','/\n&');
sss_in(1:2)=[];
% remove white spaces
sss_in=regexprep(sss_in,'\n\s+(?<tok1>[^\n])','\n$<tok1>');

% add/remove species if needed
sp_namelists=regexp(sss_in,'&SPECIES(.*?)/','match');
if length(sp_namelists)~=nsp
 strrep = '';
 for ii=1:nsp
  strrep=[strrep sp_namelists{1} '\n'];
 end
 sss_in=regexprep(sss_in,'(&SPECIES.*?/\s*)+',strrep);
end

true_false={'T','F'};

% Use GKDB reference quantities for normalisation:
Rrat=1;  % Miller convention
Brat=1;  % Miller convention
qrat=1;  % qref=e
mrat=1;  % mref=mD
Trat=1;  % Warning: implies Tref=Te, unusual in GKW
nrat=1;  % nref=ne(s=0)
vthrat=1;
rhorat=1;

% model 
check_var(GKWref.CONTROL,{'spectral_radius','non_linear','flux_tube','method'});
check_var(GKWref.CONTROL,{'nlphi','nlapar','nlbpar'});
check_var(GKWref.SPCGENERAL,{'adiabatic_electrons'});

sss_in=modify_var(sss_in,'spectral_radius','T');
sss_in=modify_var(sss_in,'non_linear','F');
sss_in=modify_var(sss_in,'flux_tube','T');
sss_in=modify_var(sss_in,'method','EXP');
sss_in=modify_var(sss_in,'nlphi','T');
sss_in=modify_var(sss_in,'nlapar',true_false{find(out.model.include_a_field_parallel==[true false])});
sss_in=modify_var(sss_in,'nlbpar',true_false{find(out.model.include_b_field_parallel==[true false])});
sss_in=modify_var(sss_in,'adiabatic_electrons','F');


% magnetic equilibrium
c=out.flux_surface.shape_coefficients_c;
s=out.flux_surface.shape_coefficients_s;
dcdr=out.flux_surface.dc_dr_minor_norm;
dsdr=out.flux_surface.ds_dr_minor_norm;
[R_FS,Z_FS]=genshape2rz(c,s,dcdr,dsdr);

% Call rz2miller with the 'gkdb' switch to use the same zmil definition as in the GKDB and have s=0 coincide with theta=0
[r,Rmil,Zmil,k,d,z,dRmildr,dZmildr,sk,sd,sz,Rout,Zout,chi2]=rz2miller(R_FS,Z_FS,'gkdb',0); 

if chi2(2)>0.03
 disp('Warning, Miller parameterisation is poor')
end
if abs(r(2)-out.flux_surface.r_minor_norm)>1e-3
 disp('Problem with the Miller parametrisation: can not retrieve the original FS minor radius')
 return
end

check_var(GKWref.GEOM,{'eps','q','shat','eps_type','geom_type','signb','signj','r0_loc'});
check_var(GKWref.GEOM,{'kappa','skappa','delta','sdelta','square','ssquare','zmil','drmil','dzmil','gradp_type','gradp'});
check_var(GKWref.SPCGENERAL,{'betaprime_type','betaprime_ref'})


sss_in=modify_var(sss_in,'eps',out.flux_surface.r_minor_norm./Rrat);
sss_in=modify_var(sss_in,'q',abs(out.flux_surface.q));
sss_in=modify_var(sss_in,'shat',out.flux_surface.magnetic_shear_r_minor);
sss_in=modify_var(sss_in,'eps_type',int8(1));
sss_in=modify_var(sss_in,'geom_type','miller');
sss_in=modify_var(sss_in,'signb',int8(-out.flux_surface.b_field_tor_sign));
sss_in=modify_var(sss_in,'signj',int8(-out.flux_surface.ip_sign));
sss_in=modify_var(sss_in,'r0_loc','LFS');
sss_in=modify_var(sss_in,'kappa',k(2));
sss_in=modify_var(sss_in,'skappa',sk(2));
sss_in=modify_var(sss_in,'delta',d(2));
sss_in=modify_var(sss_in,'sdelta',sd(2));
sss_in=modify_var(sss_in,'square',z(2));
sss_in=modify_var(sss_in,'ssquare',sz(2));
sss_in=modify_var(sss_in,'zmil',Zmil(2));
sss_in=modify_var(sss_in,'drmil',dRmildr(2));
sss_in=modify_var(sss_in,'dzmil',dZmildr(2));

if out.model.include_full_curvature_drift
  sss_in=modify_var(sss_in,'gradp_type','beta_prime');
  sss_in=modify_var(sss_in,'betaprime_type','ref');
  sss_in=modify_var(sss_in,'betaprime_ref',-out.flux_surface.pressure_gradient_norm.*Rrat./Brat.^2);
else
  sss_in=modify_var(sss_in,'gradp_type','beta_prime_input');
  sss_in=modify_var(sss_in,'gradp',-out.flux_surface.pressure_gradient_norm.*Rrat./Brat.^2);  
  sss_in=modify_var(sss_in,'betaprime_type','ref');
  sss_in=modify_var(sss_in,'betaprime_ref',0);  
end


% species
check_var(GKWref.SPECIES,{'mass','z','temp','dens','rlt','rln','uprim'});
for ii=1:nsp
 sss_in=modify_var(sss_in,'z',int32(out.species{ii}.charge_norm/qrat),ii);
 sss_in=modify_var(sss_in,'mass',out.species{ii}.mass_norm/mrat,ii);
 sss_in=modify_var(sss_in,'dens',out.species{ii}.density_norm/nrat,ii); % theta=0 coincides with s=0
 sss_in=modify_var(sss_in,'temp',out.species{ii}.temperature_norm/Trat,ii); 
 sss_in=modify_var(sss_in,'rln',out.species{ii}.density_log_gradient_norm*Rrat,ii); 
 sss_in=modify_var(sss_in,'rlt',out.species{ii}.temperature_log_gradient_norm*Rrat,ii); 
 sss_in=modify_var(sss_in,'uprim',-out.flux_surface.b_field_tor_sign*out.species{ii}.velocity_tor_gradient_norm*Rrat.^2/vthrat,ii); 
end


% species_all & rotation
check_var(GKWref.SPCGENERAL,{'beta_type','beta_ref'})
sss_in=modify_var(sss_in,'beta_type','ref');
sss_in=modify_var(sss_in,'beta_ref',out.species_all.beta_reference./(nrat*Trat*Brat.^2));
if out.species_all.debye_length_reference>0
 disp('Warning, finite Debye length effects not implemented in GKW')
end

check_var(GKWref.ROTATION,{'vcor','coriolis','cf_drift','cf_trap','cf_upphi','cf_upsrc'});
sss_in=modify_var(sss_in,'vcor',-out.flux_surface.b_field_tor_sign*out.species_all.velocity_tor_norm*Rrat/vthrat);
sss_in=modify_var(sss_in,'coriolis','T');
sss_in=modify_var(sss_in,'cf_drift',true_false{find(out.model.include_centrifugal_effects==[true false])});
sss_in=modify_var(sss_in,'cf_trap',true_false{find(out.model.include_centrifugal_effects==[true false])});
sss_in=modify_var(sss_in,'cf_upphi',true_false{find(out.model.include_centrifugal_effects==[true false])});
sss_in=modify_var(sss_in,'cf_upsrc',true_false{find(out.model.include_centrifugal_effects==[true false])});


% collisions
check_var(GKWref.CONTROL,'collisions');
check_var(GKWref.COLLISIONS,{'rref','tref','nref','zeff','pitch_angle','en_scatter','friction_coll'});
check_var(GKWref.COLLISIONS,{'mom_conservation','ene_conservation','mass_conserve','freq_override','cons_type'});

disp('Warning, collisions not treated now')
sss_in=modify_var(sss_in,'collisions','F');
sss_in=modify_var(sss_in,'pitch_angle','F');
sss_in=modify_var(sss_in,'en_scatter','F');
sss_in=modify_var(sss_in,'friction_coll','F');


% wavevectors -> loop
check_var(GKWref.MODE,{'kthrho','kr_type','krrho'});
check_var(GKWref.GRIDSIZE,{'nperiod','n_s_grid'});
sss_in=modify_var(sss_in,'kr_type','kr');
ns_per_turn = floor(GKWref.GRIDSIZE.n_s_grid./(GKWref.GRIDSIZE.nperiod.*2-1));
ok_write=1;

if scan_type==1
 % find which variable is scanned
 if length(kx_unique)>1
  var_name{1}='krrho';
 else
  var_name{1}='kthrho';
 end
 nbvar=1;
 nbval(1)=nk;
 indx=zeros(nk,1);
 spcindx{1}=0;
 coupled=0;
 for ii=1:nk
   % modify wavevectors valuse
   sss_in=modify_var(sss_in,'krrho',out.wavevector{ii}.radial_component_norm.*rhorat);
   sss_in=modify_var(sss_in,'kthrho',out.wavevector{ii}.binormal_component_norm.*rhorat);
   sss_in=modify_var(sss_in,'nperiod',int32((out.wavevector{ii}.poloidal_turns+1)/2));
   sss_in=modify_var(sss_in,'n_s_grid',int32(out.wavevector{ii}.poloidal_turns.*ns_per_turn));
   if length(kx_unique)>1
    var_val{1}(ii)=out.wavevector{ii}.radial_component_norm.*rhorat;
   else
    var_val{1}(ii)=out.wavevector{ii}.binormal_component_norm.*rhorat;
   end
  
   % write file
   file=[gkwpath('input',proj) flroot '_' num2str(ii)];
   if unix(['test -e ' file])==0 & ok_write~=2
     disp(['Warning, file ' file ' already exist.'])
     ok_write=input('Overwrite (0: no, 1: yes, 2: yes to all) ?');
   end
   if ok_write==1|ok_write==2,
     fid=fopen(file,'w');
     fprintf(fid,'%s',sss_in);
     fclose(fid);
   else
     disp(['File ' file 'not created'])
     ok_write=1;
   end
   sinfo.files{ii}=file;
   indx(ii,1)=ii;
 end
end

if nk>1

 % creation of a scan description file 
 sinfo.date=date;
 sinfo.comments='';
 sinfo.nbfiles=nk;
 sinfo.nbvar=nbvar;
 sinfo.indx=indx;
 for ii=1:nbvar,
	sinfo.var.name{ii}=var_name{ii};
	sinfo.var.val{ii}=var_val{ii};
	sinfo.var.nbval(ii)=nbval(ii);
	sinfo.var.spc{ii}=spcindx{ii};
	sinfo.var.coupled(ii)=coupled(ii);
	if coupled(ii)>0
	  for jj=1:coupled(ii)
		sinfo.var.cpl(ii).name{jj}=varcpl_name{ii,jj};
		sinfo.var.cpl(ii).val{jj}=varcpl_val{ii,jj};
		sinfo.var.cpl(ii).spc{jj}=spcindxcpl{ii,jj};
	  end
	else
		sinfo.var.cpl(ii).name='no coupled variable';
		sinfo.var.cpl(ii).val=[];
		sinfo.var.cpl(ii).spc=[];
	end	
 end

 save([gkwpath('scan',proj) flroot],'sinfo')
 disp(['Description of the scan saved in ' gkwpath('scan',proj) flroot '.mat'])

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% local functions %%%%%%%%%%%%%%%%%%%%%%5
function str_out=modify_var(str_in,varname,varvalue,spcindx)

if ~exist('spcindx')||isempty(spcindx)
 spcindx=0;
end
str_out=str_in;

if isstr(varvalue)
  if strcmp(varvalue,'F')|strcmp(varvalue,'T')
    rep=varvalue;
  else
    rep=['"' varvalue '"'];
  end
else
  if isinteger(varvalue)
   rep=sprintf('%i',varvalue);
  else
   rep=sprintf('%e',varvalue);
  end
end

if spcindx~=0
  for ii=1:length(spcindx)
    str_out=regexprep(str_in,['(?<tok1>' varname '[\s]*=[\s'']*)(?<tok2>[^\n!,;'']*)'],['$<tok1>' rep],spcindx(ii),'ignorecase');
  end
else
    str_out=regexprep(str_in,['(?<tok1>' varname '[\s]*=[\s]*)(?<tok2>[^\n!,;]*)'],['$<tok1>' rep],'once','ignorecase');
end


%%%%%%%%%%%%%%%%%%
function check_var(Sin,varnames)
 
testfield=~isfield(Sin,varnames);
I=find(testfield);
if ~isempty(I)
err_msg='';
 for ii=1:length(I)
  err_msg=[err_msg 'Variable ''' varnames{I(ii)} ''' not found in the reference input file' char(10)];
 end
 error([err_msg 'Update the reference GKW input file'])
end
