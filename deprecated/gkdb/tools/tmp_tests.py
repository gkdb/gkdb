# Tests of get_metric.py
# Tests mostly ok. Some discrepancies with GKW for grad_r close to theta=0. Not sure this is an issue in the computation of the metric
# coefficients or in the shape parametrisation. To check once Fourier parametrisation is implemented in GKW
import matplotlib.pyplot as plt
import json
import scipy.io
cd /home/yann/projects/gkdb/gkdb_gitlab/gkdb/tools
import get_metric

json_path='/home/yann/runs1/gkw/GKDB_TEST/json/case1_m1_1.json' #sb=1,sj=1 circular
json_path='/home/yann/runs1/gkw/TCVREV_1/json/p1real_kth_1.json' #sb=-1,sj=-1 shaped
json_path='/home/yann/runs1/gkw/JET_75225/json/jneq33_emcvr_kth_1.json'#sb=-1,sj=-1, high betaprime
with open(json_path) as file:
  ids=json.load(file)
th_in=np.asarray(ids['wavevector'][0]['eigenmode'][0]['poloidal_angle'])

(th_out,amin,R,Z,J_r,dVdr,grad_r,dpsidr,bt,bp,grr,grth,gthth)=get_metric.get_metric(ids,th_in)

scipy.io.savemat('/home/yann/projects/gkdb/tmp/test_metric.mat', dict(th_out=th_out,amin=amin,R=R,Z=Z,J_r=J_r,dVdr=dVdr,grad_r=grad_r,dpsidr=dpsidr,bt=bt,bp=bp,grr=grr,grth=grth,gthth=gthth,Gq=Gq))  

fig,ax=plt.subplots()
ax.plot(R,Z)
plt.show()
