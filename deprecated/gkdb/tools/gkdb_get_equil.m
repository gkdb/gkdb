% Compute magnetic equilibrium related quantities from the content of a GKDB JSON file
%
%       [geom]=gkdb_get_equil(GKin,theta_grid)
%
%  Inputs:
%    GKin           matlab structure with the content of a GKDB JSON file (as produced by readjson.m)
%    theta_grid     poloidal angle grid on which to compute the metric quantities (optional)
%                     default:  theta_grid=linspace(-pi,pi,101);
%
%  Outputs:
%    geom           matlab structure with geometry related quantities (all normalised with GKDB conventions)
%      .amin          flux surface minor radius (on theta_grid)
%      .R             flux surface horizontal coordinates
%      .Z             flux surface vertical coordinates
%      .J_r           (r,theta,phi) Jacobian J_r=1/(grad_r x grad_theta . grad_phi)
%      .dVdr          radial derivative of the plasma volume
%      .grad_r        |grad_r|
%      .dpsidr        radial derivative of the poloidal magnetic flux
%      .bt            toroidal magnetic field (normalised to Bref)
%      .bp            poloidal magnetic field (normalised to Bref)
%
% See Candy Plasma Phys. Control. Fusion 51, 105009 (2009) for more details on the calculations
%
% YC - 18.02.2020



function [geom]=gkdb_get_equil(GKin)


if nargin<1
 disp('Not enough input arguments')
 return
end

if ~exist('theta_grid')||isempty(theta_grid)
  Nth=101;
  theta_grid=linspace(-pi,pi,Nth);
else
  Nth=length(theta_grid)
end
theta_grid=transpose(theta_grid(:));
keyboard

% flux surface
c=GKin.flux_surface.shape_coefficients_c;
dcdr=GKin.flux_surface.dc_dr_minor_norm;
s=GKin.flux_surface.shape_coefficients_s;
dsdr=GKin.flux_surface.ds_dr_minor_norm;
n=transpose([0:1:length(c)-1]);
THETA=repmat(theta_grid,[length(c) 1]);
C=repmat(c,[1 Nth]);
DCDR=repmat(dcdr,[1 Nth]);
S=repmat(s,[1 Nth]);
DSDR=repmat(dsdr,[1 Nth]);
N=repmat(n,[1 Nth]);

amin = sum(C.*cos(N.*THETA)+S.*sin(N.*THETA));
R=1+amin.*cos(theta_grid);
Z=-amin.*sin(theta_grid);


% (r,theta,phi) Jacobian
damindr=sum(DCDR.*cos(N.*THETA)+DSDR.*sin(N.*THETA));
dRdr=damindr.*cos(theta_grid);
dZdr=-damindr.*sin(theta_grid);
damindth=sum(-N.*C.*sin(N.*THETA)+N.*S.*cos(N.*THETA));
dRdth=damindth.*cos(theta_grid)-amin.*sin(theta_grid);
dZdth=-damindth.*sin(theta_grid)-amin.*cos(theta_grid);

J_r=-R.*(dRdr.*dZdth-dRdth.*dZdr);

% dVdr 
dVdr=2*pi.*trapz(theta_grid,J_r);

% |grad_r|
grad_r=-sqrt((dRdth).^2+(dZdth).^2)./(dRdr.*dZdth-dRdth.*dZdr);

% dpsidr
dpsidr=1./(2*pi.*GKin.flux_surface.q).*trapz(theta_grid,J_r./R.^2);

% magnetic field components
bt=GKin.flux_surface.b_field_tor_sign.*1./R;
bp=dpsidr.*grad_r./R;

% outputs
geom.theta_grid=theta_grid;
geom.amin=amin;
geom.R=R;
geom.Z=Z;
geom.J_r=J_r;
geom.dVdr=dVdr;
geom.grad_r=grad_r;
geom.dpsidr=dpsidr;
geom.bt=bt;
geom.bp=bp;
