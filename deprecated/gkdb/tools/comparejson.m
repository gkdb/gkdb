% Script to compare two JSON files (difference listed on screen)
% 
%   [out1,out2]=comparejson(flnm1,flnm2,switches,tol)
%
% Inputs:
%   flnm1       path+filename to the first JSON file
%   flnm2       path+filename to the second JSON file
%   switches    array of 5 logicals to select the content to be compared
%                 inputs / growth rate / fluxes / mode structure / moments
%                 default: [1 1 0 0 0]
%   tol         tolerance used for the comparison of two values
%                 var1 is equal to var2 if abs(var1-var2)<=tol.*abs(var1)
% 
% Outputs:
%   out1        matlab structure with the content of the first JSON file
%   out2        matlab structure with the content of the second JSON file
%
%
% Note: file 1 is used as reference for fields existence, i.e. all fields present in file 1 are compared with file 2
%
% TO DO: output the file differences in a variable + switch to turn off difference no screen
% 
% YC - 09.05.2019



function [out1,out2]=comparejson(flnm1,flnm2,switches,tol)

if nargin<2
 disp('Error: at least two input files are required')
end

if ~exist('switches')||isempty(switches)
 switches=[1 1 0 0 0];
end

if ~exist('tol')||isempty(tol)
 tol=1e-3;
end

out{1}=readjson(flnm1);
out{2}=readjson(flnm2);

out1=out{1};
out2=out{2};
%%%%%%%%%%%%%%%%%
% INPUTS
%%%%%%%%%%%%%%%%%
% Take the first file as a reference and checks that the others match the inputs
% display the difference in case it does not

if switches(1)

tables={'model','flux_surface','species','species_all','collisions'};

for ii=1:length(tables)
 if iscell(out{1}.(tables{ii}))
  for kk=1:length(out{1}.(tables{ii}))
   fields=fieldnames(out{1}.(tables{ii}){kk});
   test_fields=isfield(out{2}.(tables{ii}){kk},fields);
   for jj=1:length(fields)
    if test_fields(jj)
      var1=out{1}.(tables{ii}){kk}.(fields{jj});
      var2=out{2}.(tables{ii}){kk}.(fields{jj});
      [is_equal,str1,str2,str3]=compare_values(var1,var2,tol);
      if ~is_equal
        disp(['Species ' num2str(kk) ', ' fields{jj} '   file1: '  str1]);
        disp(['Species ' num2str(kk) ', ' fields{jj} '   file2: '  str2]);
        if ~isempty(str3)
          disp(['Species ' num2str(kk) ', ' fields{jj} '     rel. diff.: '  str3]);
        end
      end
    else
     disp(['Field ' fields{jj} ' missing in file 2'])
    end
   end
  end
 else
  fields=fieldnames(out{1}.(tables{ii}));
  test_fields=isfield(out{2}.(tables{ii}),fields);
  for jj=1:length(fields)
   if test_fields(jj)
    var1=out{1}.(tables{ii}).(fields{jj});
    var2=out{2}.(tables{ii}).(fields{jj});
    [is_equal,str1,str2,str3]=compare_values(var1,var2,tol);
    if ~is_equal
      disp([fields{jj} '   file1: '  str1]);
      disp([fields{jj} '   file2: '  str2]);
      if ~isempty(str3)
        disp([fields{jj} '     rel. diff.: '  str3]);
      end
    end
   else
     disp(['Field ' fields{jj} ' missing in file 2'])
   end
  end
 end
end

end

%%%%%%%%%%%%%%%%%
% OUTPUTS
%%%%%%%%%%%%%%%%%
dum1=cell2mat(out{1}.wavevector);
kx1=[dum1(:).radial_component_norm];
ky1=[dum1(:).binormal_component_norm];
dum2=cell2mat(out{2}.wavevector);
kx2=[dum2(:).radial_component_norm];
ky2=[dum2(:).binormal_component_norm];
N=min(length(ky1),length(ky2))-1;
nsp=length(out{1}.wavevector{1}.eigenmode{1}.fluxes_moments);

if switches(2)
% growth rate and frequency spectra
fields={'growth_rate_norm','frequency_norm'};
dum1=NaN.*ky1;
dum2=NaN.*ky2;
for jj=1:length(fields)
 for ii=1:length(out{1}.wavevector)
   dum1(ii)=out{1}.wavevector{ii}.eigenmode{1}.(fields{jj});
 end
 for ii=1:length(out{2}.wavevector)
   dum2(ii)=out{2}.wavevector{ii}.eigenmode{1}.(fields{jj});
 end
 figure
 plot(ky1(1:N),dum1(1:N),'b+')
 hold on
 plot(ky2(1:N),dum2(1:N),'or')
 xlabel('ky')
 title(fields{jj})
end
end

if switches(3)
% fluxes spectra
fields=fieldnames(out{1}.wavevector{1}.eigenmode{1}.fluxes_moments{1}.fluxes_norm);
dum1=NaN.*ky1;
dum2=NaN.*ky2;
for kk=1:nsp
for jj=1:length(fields)
 for ii=1:length(out{1}.wavevector)
   if strcmp(out{1}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.fluxes_norm.(fields{jj}),'null')
    dum1(ii)=NaN;
   else
    dum1(ii)=out{1}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.fluxes_norm.(fields{jj});
   end
 end
 for ii=1:length(out{2}.wavevector)
   if strcmp(out{2}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.fluxes_norm.(fields{jj}),'null')
    dum2(ii)=NaN;
   else
    dum2(ii)=out{2}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.fluxes_norm.(fields{jj});
   end
 end
 figure
 plot(ky1(1:N),dum1(1:N),'b+')
 hold on
 plot(ky2(1:N),dum2(1:N),'or')
 xlabel('ky')
 title(['Species ' num2str(kk) ' ' fields{jj}])
end
end
end

if switches(4)
% mode structure
fields={'phi_potential','a_field_parallel','b_field_parallel'};
for jj=1:length(fields)
 for ii=1:N
   th1=out{1}.wavevector{ii}.eigenmode{1}.poloidal_angle;
   th2=out{2}.wavevector{ii}.eigenmode{1}.poloidal_angle;
   dum1_real=out{1}.wavevector{ii}.eigenmode{1}.([fields{jj} '_perturbed_norm_real']);
   if strcmp(dum1_real,'null')
    dum1_real=NaN.*th1;
   end
   dum1_imag=out{1}.wavevector{ii}.eigenmode{1}.([fields{jj} '_perturbed_norm_imaginary']);
   if strcmp(dum1_imag,'null')
    dum1_imag=NaN.*th1;
   end
   dum2_real=out{2}.wavevector{ii}.eigenmode{1}.([fields{jj} '_perturbed_norm_real']);
   if strcmp(dum2_real,'null')
    dum2_real=NaN.*th2;
   end
   dum2_imag=out{2}.wavevector{ii}.eigenmode{1}.([fields{jj} '_perturbed_norm_imaginary']);
   if strcmp(dum2_imag,'null')
    dum2_imag=NaN.*th2;
   end
   figure
   plot(th1,dum1_real,'b')
   hold on
   plot(th1,dum1_imag,'b--')
   plot(th2,dum2_real,'r')
   plot(th2,dum2_imag,'r--')
   xlabel('\theta')
   title(['ky=' num2str(ky1(ii)) ',  ' fields{jj}]) 
 end
end
end

if switches(5)
% moments
fields={'density','velocity_parallel','temperature_parallel','temperature_perpendicular'};
%fields={'temperature_parallel','temperature_perpendicular'};
fields={'density'};
%fields={'velocity_parallel'};
%fields={'temperature_perpendicular'};
for kk=1:nsp
for jj=1:length(fields)
 for ii=1:N
   th1=out{1}.wavevector{ii}.eigenmode{1}.poloidal_angle;
   th2=out{2}.wavevector{ii}.eigenmode{1}.poloidal_angle;
   dum1_real=out{1}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.moments_norm_rotating_frame.([fields{jj} '_real']);
   if strcmp(dum1_real,'null')
    dum1_real=NaN.*th1;
   end
   dum1_imag=out{1}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.moments_norm_rotating_frame.([fields{jj} '_imaginary']);
   if strcmp(dum1_imag,'null')
    dum1_imag=NaN.*th1;
   end
   dum2_real=out{2}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.moments_norm_rotating_frame.([fields{jj} '_gyroaveraged_real']);
   if strcmp(dum2_real,'null')
    dum2_real=NaN.*th2;
   end
   dum2_imag=out{2}.wavevector{ii}.eigenmode{1}.fluxes_moments{kk}.moments_norm_rotating_frame.([fields{jj} '_gyroaveraged_imaginary']);
   if strcmp(dum2_imag,'null')
    dum2_imag=NaN.*th2;
   end
   figure
   plot(th1,dum1_real,'b')
   hold on
   plot(th1,dum1_imag,'b--')
   plot(th2,dum2_real,'r')
   plot(th2,dum2_imag,'r--')
   xlabel('\theta')
   title(['Species ' num2str(kk) ', ky=' num2str(ky1(ii)) ',  ' fields{jj}]) 
 end
end
end
end







%%%%% subfunctions %%%%

function [is_equal,str1,str2,str3]=compare_values(var1,var2,tol)

is_equal=1;
str1='';
str2='';
str3='';

if ~isequal(var1,var2)

  if strcmp(var1,'null')|strcmp(var2,'null')|islogical(var1)|islogical(var2)

    str1=num2str(var1);
    str2=num2str(var2);
    is_equal=0;

  elseif isnumeric(var1)&isnumeric(var2) 

    N=min(length(var1(:)),length(var2(:)));
    for ii=1:N
      str1=[str1 ' ' num2str(var1(ii),'%10.8e')];
      str2=[str2 ' ' num2str(var2(ii),'%10.8e')];
%      str3=[str3 ' ' num2str(2*abs(var1(ii)-var2(ii))./(abs(var1(ii))+abs(var2(ii))),'%10.8e')];
      str3=[str3 ' ' num2str(var1(ii)./abs(var2(ii)),'%10.8e')];

      if abs(var1(ii)-var2(ii))>tol.*abs(var1(ii))
        is_equal=0;
      end
    end

  else

    str1=['Inconsistent data types'];
    is_equal=0;

  end

end

%%%%%%%%%%%%%%%%%

