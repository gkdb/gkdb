CREATE SCHEMA IF NOT EXISTS gkdb_development;
SET SCHEMA 'gkdb_development';
CREATE EXTENSION IF NOT EXISTS pg_uuidv7;

    DO $$ BEGIN 
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'complex') THEN
        CREATE TYPE complex AS (
            r real,
            i real
        );
    END IF;
    CREATE TABLE dummy_table (dummy_column complex);
    DROP TABLE dummy_table;
    END $$;
    
create table if not exists tag_table
(
    id          serial
        primary key,
    tagid       varchar(32) not null,
    categorie   varchar(32) not null
        constraint tag_table_categorie_check
            check ((categorie)::text = ANY
                   ((ARRAY ['simulation'::character varying, 'experiment'::character varying, 'doi'::character varying, 'conference'::character varying, 'tutorial'::character varying, 'ML_training'::character varying, 'ML_set'::character varying, 'other'::character varying, 'reference_case'::character varying, 'zenodo'::character varying, 'equivalents'::character varying, 'gkw'::character varying, 'gene'::character varying, 'test'::character varying, 'qualikiz'::character varying, 'tglf'::character varying])::text[])),
    description text,
    unique (tagid, categorie)
);



create table if not exists gyrokinetics_table
(
    id   bigserial
        primary key,
    uuid uuid default gkdb_development.uuid_generate_v7() not null
        unique,
    time real                                             not null
);



create table if not exists ids_tags_table
(
    gk_id  bigint  not null
        references gyrokinetics_table,
    tag_id integer not null
        references tag_table,
    primary key (gk_id, tag_id)
);

create table if not exists file_entry_table
(
    id       serial
        primary key,
    gk_id    bigint       not null
        references gyrokinetics_table,
    hash     varchar(64)  not null
        unique,
    filename varchar(256) not null
        unique,
    storage  varchar(256) not null,
    unique (hash, storage)
);



create table if not exists flux_surface_table
(
    id                         serial
        primary key,
    gk_id                      bigint not null
        references gyrokinetics_table,
    r_minor_norm               real   not null,
    elongation                 real   not null,
    delongation_dr_minor_norm  real   not null,
    dgeometric_axis_r_dr_minor real   not null,
    dgeometric_axis_z_dr_minor real   not null,
    q                          real   not null,
    magnetic_shear_r_minor     real   not null,
    pressure_gradient_norm     real   not null,
    ip_sign                    real   not null,
    b_field_tor_sign           real   not null,
    shape_coefficients_c       bytea,
    dc_dr_minor_norm           bytea,
    shape_coefficients_s       bytea,
    ds_dr_minor_norm           bytea
);



create table if not exists input_normalizing_table
(
    id          serial
        primary key,
    gk_id       bigint not null
        references gyrokinetics_table,
    t_e         real   not null,
    n_e         real   not null,
    r           real   not null,
    b_field_tor real   not null
);



create table if not exists species_all_table
(
    id                     serial
        primary key,
    gk_id                  bigint not null
        references gyrokinetics_table,
    beta_reference         real   not null,
    velocity_tor_norm      real   not null,
    zeff                   real   not null,
    debye_length_reference real   not null,
    shearing_rate_norm     real   not null
);



create table if not exists model_table
(
    id                               serial
        primary key,
    gk_id                            bigint  not null
        references gyrokinetics_table,
    include_centrifugal_effects      boolean not null,
    include_a_field_parallel         boolean not null,
    include_b_field_parallel         boolean not null,
    include_full_curvature_drift     boolean not null,
    collisions_pitch_only            boolean not null,
    collisions_momentum_conservation boolean not null,
    collisions_energy_conservation   boolean not null,
    collisions_finite_larmor_radius  boolean not null,
    non_linear_run                   boolean not null,
    time_interval_norm               bytea
);



create table if not exists species_table
(
    id                            serial
        primary key,
    gk_id                         bigint not null
        references gyrokinetics_table,
    charge_norm                   real   not null,
    mass_norm                     real   not null,
    density_norm                  real   not null,
    density_log_gradient_norm     real   not null,
    temperature_norm              real   not null,
    temperature_log_gradient_norm real   not null,
    velocity_tor_gradient_norm    real   not null
);



create table if not exists ids_properties_table
(
    id               serial
        primary key,
    gk_id            bigint       not null
        references gyrokinetics_table,
    comment          text         not null,
    homogeneous_time integer      not null,
    provider         varchar(512) not null,
    creation_date    varchar(512) not null
);



create table if not exists code_table
(
    id          serial
        primary key,
    gk_id       bigint       not null
        references gyrokinetics_table,
    name        varchar(64)  not null,
    commit      varchar(512) not null,
    version     varchar(32)  not null,
    repository  varchar(256) not null,
    parameters  jsonb,
    output_flag bytea        not null
);


create table if not exists wavevector_table
(
    id                      bigserial
        primary key,
    gk_id                   bigint not null
        references gyrokinetics_table,
    radial_component_norm   real   not null,
    binormal_component_norm real   not null
);



create table if not exists collisions_table
(
    id                  serial
        primary key,
    gk_id               bigint  not null
        references gyrokinetics_table,
    collisionality_norm real    not null,
    specie_one          integer not null
        references species_table,
    specie_two          integer not null
        references species_table
);


create table if not exists ids_provenance_node_table
(
    id                serial
        primary key,
    path              varchar(512)   not null,
    sources           varchar(256)[] not null,
    ids_properties_id integer        not null
        references ids_properties_table
);

create table if not exists library_table
(
    id         serial
        primary key,
    code_id    integer      not null
        references code_table,
    name       varchar(64)  not null,
    commit     varchar(512) not null,
    version    varchar(32)  not null,
    repository varchar(256) not null,
    parameters jsonb        not null
);

create table if not exists eigenmode_table
(
    id                                bigserial  primary key,
    wavevector_id                     bigint  not null
        references wavevector_table,
    gk_id                             bigint  not null
        references gyrokinetics_table,
    code_parameters                   jsonb,
    poloidal_turns                    integer,
    growth_rate_norm                  real,
    frequency_norm                    real,
    growth_rate_tolerance             real,
    phi_potential_perturbed_weight    bytea,
    phi_potential_perturbed_parity    bytea,
    a_field_parallel_perturbed_weight bytea,
    a_field_parallel_perturbed_parity bytea,
    b_field_parallel_perturbed_weight bytea,
    b_field_parallel_perturbed_parity bytea,
    poloidal_angle                    bytea,
    phi_potential_perturbed_norm      bytea,
    a_field_parallel_perturbed_norm   bytea,
    b_field_parallel_perturbed_norm   bytea,
    time_norm                         bytea,
    initial_value_run                 boolean
);



    
    
create table if not exists fluxes_table
(
    id                                          serial
        primary key,
    gk_id                                       bigint      not null
        references gyrokinetics_table,
    species_id                                  integer
        references species_table,
    eigenmode_id                                bigint
        references eigenmode_table,
    frame                                       varchar(16) not null,
    ftype                                       varchar(16) not null,
    uuid                                        uuid        not null,
    particles_phi_potential                     real        not null,
    particles_a_field_parallel                  real        not null,
    particles_b_field_parallel                  real        not null,
    energy_phi_potential                        real        not null,
    energy_a_field_parallel                     real        not null,
    energy_b_field_parallel                     real        not null,
    momentum_tor_parallel_phi_potential         real        not null,
    momentum_tor_parallel_a_field_parallel      real        not null,
    momentum_tor_parallel_b_field_parallel      real        not null,
    momentum_tor_perpendicular_phi_potential    real        not null,
    momentum_tor_perpendicular_a_field_parallel real        not null,
    momentum_tor_perpendicular_b_field_parallel real        not null
);


create table if not exists moments_table
(
    id                              serial
        primary key,
    gk_id                           bigint      not null
        references gyrokinetics_table,
    species_id                      integer
        references species_table,
    eigenmode_id                    bigint
        references eigenmode_table,
    mtype                           varchar(16) not null,
    uuid                            uuid        not null,
    norm                            boolean,
    density                         bytea       not null,
    j_parallel                      bytea       not null,
    pressure_parallel               bytea       not null,
    pressure_perpendicular          bytea       not null,
    heat_flux_parallel              bytea       not null,
    v_parallel_energy_perpendicular bytea       not null,
    v_perpendicular_square_energy   bytea       not null
);


create index if not exists ix_gkdb_development_tag_table_categorie
    on tag_table (categorie);
    
create index if not exists ix_gkdb_development_gyrokinetics_table_uuid
    on gyrokinetics_table (uuid);
    
create index if not exists ix_gkdb_development_file_entry_table_gk_id
    on file_entry_table (gk_id);
    
create index if not exists ix_gkdb_development_flux_surface_table_gk_id
    on flux_surface_table (gk_id);
    
create index if not exists ix_gkdb_development_input_normalizing_table_gk_id
    on input_normalizing_table (gk_id);
    
create index if not exists ix_gkdb_development_species_all_table_gk_id
    on species_all_table (gk_id);
    
create index if not exists ix_gkdb_development_model_table_gk_id
    on model_table (gk_id);
    
create index if not exists ix_gkdb_development_species_table_gk_id
    on species_table (gk_id);
    
create index if not exists ix_gkdb_development_ids_properties_table_gk_id
    on ids_properties_table (gk_id);
    
create index if not exists ix_gkdb_development_code_table_gk_id
    on code_table (gk_id);
    
create index if not exists ix_gkdb_development_code_table_name
    on code_table (name);
    
create index if not exists ix_gkdb_development_wavevector_table_gk_id
    on wavevector_table (gk_id);
    
create index if not exists ix_gkdb_development_collisions_table_gk_id
    on collisions_table (gk_id);
    
create index if not exists ix_gkdb_development_eigenmode_table_wavevector_id
    on eigenmode_table (wavevector_id);
    
create index if not exists ix_gkdb_development_eigenmode_table_gk_id
    on eigenmode_table (gk_id);
    
create index if not exists ix_gkdb_development_eigenmode_table_growth_rate_norm
    on eigenmode_table (growth_rate_norm);
    
create index if not exists ix_gkdb_development_eigenmode_table_frequency_norm
    on eigenmode_table (frequency_norm);
    
create index if not exists ix_gkdb_development_fluxes_table_eigenmode_id
    on fluxes_table (eigenmode_id);

create index if not exists ix_gkdb_development_fluxes_table_species_id
    on fluxes_table (species_id);

create index if not exists ix_gkdb_development_fluxes_table_gk_id
    on fluxes_table (gk_id);

create index if not exists ix_gkdb_development_fluxes_table_uuid
    on fluxes_table (uuid);
    


create index if not exists ix_gkdb_development_moments_table_uuid
    on moments_table (uuid);

create index if not exists ix_gkdb_development_moments_table_eigenmode_id
    on moments_table (eigenmode_id);

create index if not exists ix_gkdb_development_fluxes_table_energy_phi_potential
    on fluxes_table (energy_phi_potential);
    
create index if not exists ix_gkdb_development_species_table_charge_norm
    on species_table (charge_norm);

create index if not exists ix_gkdb_development_species_table_density_norm
    on species_table (density_norm);


-- PSQL functions
CREATE OR REPLACE FUNCTION get_column_sizes(p_schema text, p_table text)
RETURNS TABLE(column_name text, total_size bigint) AS $$
DECLARE 
_column text;
BEGIN
FOR _column IN 
SELECT information_schema.columns.column_name
FROM information_schema.columns
WHERE table_schema = p_schema AND table_name = p_table
LOOP
RETURN QUERY EXECUTE format(
'SELECT %L as column_name, (avg(pg_column_size(%I)) * count(%I))::bigint as total_size FROM %I.%I',
_column, _column, _column, p_schema, p_table
);
END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_table_counts_and_size(p_schema text)
RETURNS TABLE(table_name text, row_count bigint, table_size text) AS $$
DECLARE 
_tbl text;
_fqn_tbl text;
BEGIN 
FOR _tbl IN 
SELECT tablename
FROM   pg_tables 
WHERE  schemaname = p_schema
LOOP
_fqn_tbl := p_schema || '.' || _tbl;
RETURN QUERY EXECUTE format(
'SELECT %L::text, count(*) as entries, 
pg_size_pretty(pg_total_relation_size(%L::regclass)) FROM %s', 
_tbl, _fqn_tbl, _fqn_tbl);
END LOOP;  
END;
$$ LANGUAGE PLPGSQL;
