set schema 'gkdb_development';

CREATE EXTENSION pg_uuidv7;

CREATE TYPE complex AS (
        r       real,
        i       real
    );


CREATE TABLE gkdb_development.tag_table (
	id SERIAL NOT NULL, 
	tagid VARCHAR(32) NOT NULL, 
	categorie VARCHAR(32) NOT NULL CHECK (categorie IN ('simulation', 'experiment', 'doi', 'conference', 'tutorial', 'ML_training', 'ML_set', 'other', 'reference_case', 'zenodo', 'equivalents', 'gkw', 'gene', 'test', 'qualikiz', 'tglf')), 
	description TEXT, 
	PRIMARY KEY (id), 
	UNIQUE (tagid, categorie)
)


CREATE TABLE gkdb_development.moments_table (
	id SERIAL NOT NULL, 
	type VARCHAR(16) NOT NULL, 
	uuid UUID NOT NULL, 
	fluxesmoments_id INTEGER NOT NULL, 
	density complex[][] NOT NULL, 
	j_parallel complex[][] NOT NULL, 
	pressure_parallel complex[][] NOT NULL, 
	pressure_perpendicular complex[][] NOT NULL, 
	heat_flux_parallel complex[][] NOT NULL, 
	v_parallel_energy_perpendicular complex[][] NOT NULL, 
	v_perpendicular_square_energy complex[][] NOT NULL, 
	PRIMARY KEY (id)
)


CREATE TABLE gkdb_development.fluxes_moments_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	eigenmode_id INTEGER NOT NULL, 
	specie_id INTEGER NOT NULL, 
	moments_particle_id INTEGER NOT NULL, 
	moments_id INTEGER NOT NULL, 
	moments_gyroav_id INTEGER NOT NULL, 
	moments_norm_gyrocenter_id INTEGER, 
	moments_norm_particle_id INTEGER, 
	fluxes_norm_gyrocenter_id INTEGER, 
	fluxes_norm_gyrocenter_rotating_frame_id INTEGER, 
	fluxes_norm_particle_id INTEGER, 
	fluxes_norm_particle_rotating_frame_id INTEGER, 
	PRIMARY KEY (id)
)


CREATE TABLE gkdb_development.gyrokinetics_table (
	id BIGSERIAL NOT NULL, 
	uuid UUID DEFAULT uuid_generate_v7() NOT NULL, 
	time FLOAT NOT NULL, 
	PRIMARY KEY (id), 
	UNIQUE (uuid)
)



CREATE INDEX ix_gkdb_development_gyrokinetics_table_uuid ON gkdb_development.gyrokinetics_table (uuid)
CREATE TABLE gkdb_development.ids_tags_table (
	gk_id bigint NOT NULL, 
	tag_id INTEGER NOT NULL, 
	PRIMARY KEY (gk_id, tag_id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id), 
	FOREIGN KEY(tag_id) REFERENCES gkdb_development.tag_table (id)
)


CREATE TABLE gkdb_development.file_entry_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	hash VARCHAR(64) NOT NULL, 
	filename VARCHAR(256) NOT NULL, 
	storage VARCHAR(256) NOT NULL, 
	PRIMARY KEY (id), 
	UNIQUE (hash, storage), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id), 
	UNIQUE (hash), 
	UNIQUE (filename)
)


CREATE TABLE gkdb_development.flux_surface_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	r_minor_norm FLOAT NOT NULL, 
	elongation FLOAT NOT NULL, 
	delongation_dr_minor_norm FLOAT NOT NULL, 
	dgeometric_axis_r_dr_minor FLOAT NOT NULL, 
	dgeometric_axis_z_dr_minor FLOAT NOT NULL, 
	q FLOAT NOT NULL, 
	magnetic_shear_r_minor FLOAT NOT NULL, 
	pressure_gradient_norm FLOAT NOT NULL, 
	ip_sign FLOAT NOT NULL, 
	b_field_tor_sign FLOAT NOT NULL, 
	shape_coefficients_c FLOAT[] NOT NULL, 
	dc_dr_minor_norm FLOAT[] NOT NULL, 
	shape_coefficients_s FLOAT[] NOT NULL, 
	ds_dr_minor_norm FLOAT[] NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id), 
	UNIQUE (shape_coefficients_c)
)


CREATE TABLE gkdb_development.input_normalizing_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	t_e FLOAT NOT NULL, 
	n_e FLOAT NOT NULL, 
	r FLOAT NOT NULL, 
	b_field_tor FLOAT NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
)


CREATE TABLE gkdb_development.species_all_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	beta_reference FLOAT NOT NULL, 
	velocity_tor_norm FLOAT NOT NULL, 
	zeff FLOAT NOT NULL, 
	debye_length_reference FLOAT NOT NULL, 
	shearing_rate_norm FLOAT NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
)


CREATE TABLE gkdb_development.model_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	include_centrifugal_effects BOOLEAN NOT NULL, 
	include_a_field_parallel BOOLEAN NOT NULL, 
	include_b_field_parallel BOOLEAN NOT NULL, 
	include_full_curvature_drift BOOLEAN NOT NULL, 
	collisions_pitch_only BOOLEAN NOT NULL, 
	collisions_momentum_conservation BOOLEAN NOT NULL, 
	collisions_energy_conservation BOOLEAN NOT NULL, 
	collisions_finite_larmor_radius BOOLEAN NOT NULL, 
	non_linear_run BOOLEAN NOT NULL, 
	time_interval_norm FLOAT[], 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
)


CREATE TABLE gkdb_development.species_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	charge_norm FLOAT NOT NULL, 
	mass_norm FLOAT NOT NULL, 
	density_norm FLOAT NOT NULL, 
	density_log_gradient_norm FLOAT NOT NULL, 
	temperature_norm FLOAT NOT NULL, 
	temperature_log_gradient_norm FLOAT NOT NULL, 
	velocity_tor_gradient_norm FLOAT NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
)


CREATE TABLE gkdb_development.ids_properties_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	comment TEXT NOT NULL, 
	homogeneous_time INTEGER NOT NULL, 
	provider VARCHAR(512) NOT NULL, 
	creation_date VARCHAR(512) NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
)


CREATE TABLE gkdb_development.code_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	name VARCHAR(64) NOT NULL, 
	commit VARCHAR(512) NOT NULL, 
	version VARCHAR(32) NOT NULL, 
	repository VARCHAR(256) NOT NULL, 
	parameters JSONB, 
	output_flag INTEGER[] NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id), 
	UNIQUE (output_flag)
)


CREATE TABLE gkdb_development.wavevector_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	radial_component_norm FLOAT NOT NULL, 
	binormal_component_norm FLOAT NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
)


CREATE TABLE gkdb_development.collisions_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	collisionality_norm FLOAT NOT NULL, 
	specie_one INTEGER NOT NULL, 
	specie_two INTEGER NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id), 
	FOREIGN KEY(specie_one) REFERENCES gkdb_development.species_table (id), 
	FOREIGN KEY(specie_two) REFERENCES gkdb_development.species_table (id)
)


CREATE TABLE gkdb_development.ids_provenance_node_table (
	id SERIAL NOT NULL, 
	path VARCHAR(512) NOT NULL, 
	sources VARCHAR(256)[] NOT NULL, 
	ids_properties_id INTEGER NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(ids_properties_id) REFERENCES gkdb_development.ids_properties_table (id)
)


CREATE TABLE gkdb_development.library_table (
	id SERIAL NOT NULL, 
	code_id INTEGER NOT NULL, 
	name VARCHAR(64) NOT NULL, 
	commit VARCHAR(512) NOT NULL, 
	version VARCHAR(32) NOT NULL, 
	repository VARCHAR(256) NOT NULL, 
	parameters JSONB NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(code_id) REFERENCES gkdb_development.code_table (id)
)


CREATE TABLE gkdb_development.eigenmode_table (
	id SERIAL NOT NULL, 
	wavevector_id INTEGER NOT NULL, 
	poloidal_turns INTEGER NOT NULL, 
	growth_rate_norm FLOAT NOT NULL, 
	frequency_norm FLOAT NOT NULL, 
	growth_rate_tolerance FLOAT NOT NULL, 
	phi_potential_perturbed_weight FLOAT[], 
	phi_potential_perturbed_parity FLOAT[], 
	a_field_parallel_perturbed_weight FLOAT[], 
	a_field_parallel_perturbed_parity FLOAT[], 
	b_field_parallel_perturbed_weight FLOAT[], 
	b_field_parallel_perturbed_parity FLOAT[], 
	poloidal_angle FLOAT[], 
	phi_potential_perturbed_norm FLOAT[], 
	a_field_parallel_perturbed_norm FLOAT[], 
	b_field_parallel_perturbed_norm FLOAT[], 
	time_norm FLOAT[], 
	initial_value_run BOOLEAN NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(wavevector_id) REFERENCES gkdb_development.wavevector_table (id)
)


CREATE TABLE gkdb_development.code_partial_constant_table (
	id SERIAL NOT NULL, 
	eigenmode_id INTEGER NOT NULL, 
	code_id INTEGER NOT NULL, 
	parameters JSONB NOT NULL, 
	output_flag INTEGER[], 
	PRIMARY KEY (id), 
	FOREIGN KEY(eigenmode_id) REFERENCES gkdb_development.eigenmode_table (id), 
	FOREIGN KEY(code_id) REFERENCES gkdb_development.code_table (id)
)


CREATE TABLE gkdb_development.fluxes_table (
	id SERIAL NOT NULL, 
	gk_id bigint NOT NULL, 
	species_id INTEGER NOT NULL, 
	eigenmode_id INTEGER, 
	frame VARCHAR(16) NOT NULL, 
	type VARCHAR(16) NOT NULL, 
	particles_phi_potential FLOAT NOT NULL, 
	particles_a_field_parallel FLOAT NOT NULL, 
	particles_b_field_parallel FLOAT NOT NULL, 
	energy_phi_potential FLOAT NOT NULL, 
	energy_a_field_parallel FLOAT NOT NULL, 
	energy_b_field_parallel FLOAT NOT NULL, 
	momentum_tor_parallel_phi_potential FLOAT NOT NULL, 
	momentum_tor_parallel_a_field_parallel FLOAT NOT NULL, 
	momentum_tor_parallel_b_field_parallel FLOAT NOT NULL, 
	momentum_tor_perpendicular_phi_potential FLOAT NOT NULL, 
	momentum_tor_perpendicular_a_field_parallel FLOAT NOT NULL, 
	momentum_tor_perpendicular_b_field_parallel FLOAT NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id), 
	FOREIGN KEY(species_id) REFERENCES gkdb_development.species_table (id), 
	FOREIGN KEY(eigenmode_id) REFERENCES gkdb_development.eigenmode_table (id), 
	UNIQUE (frame), 
	UNIQUE (type)
)


ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(moments_norm_particle_id) REFERENCES gkdb_development.moments_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(fluxes_norm_particle_rotating_frame_id) REFERENCES gkdb_development.fluxes_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(moments_id) REFERENCES gkdb_development.moments_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(moments_norm_gyrocenter_id) REFERENCES gkdb_development.moments_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(moments_gyroav_id) REFERENCES gkdb_development.moments_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(fluxes_norm_gyrocenter_id) REFERENCES gkdb_development.fluxes_table (id)
ALTER TABLE gkdb_development.moments_table ADD FOREIGN KEY(fluxesmoments_id) REFERENCES gkdb_development.fluxes_moments_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(gk_id) REFERENCES gkdb_development.gyrokinetics_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(specie_id) REFERENCES gkdb_development.species_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(fluxes_norm_gyrocenter_rotating_frame_id) REFERENCES gkdb_development.fluxes_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(eigenmode_id) REFERENCES gkdb_development.eigenmode_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(moments_particle_id) REFERENCES gkdb_development.moments_table (id)
ALTER TABLE gkdb_development.fluxes_moments_table ADD FOREIGN KEY(fluxes_norm_particle_id) REFERENCES gkdb_development.fluxes_table (id)
