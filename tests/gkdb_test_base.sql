-- basic SQL test

--\connect "pytest";

CREATE SCHEMA IF NOT EXISTS "gkdb_development";

set schema 'gkdb_development';
CREATE EXTENSION pg_uuidv7;

CREATE SEQUENCE code_partial_constant_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "gkdb_development"."code_partial_constant_table" (
    "id" integer DEFAULT nextval('code_partial_constant_table_id_seq') NOT NULL,
 --   "eigenmode_id" integer NOT NULL,
    "parameters" jsonb ,
    "output_flag" integer ,
    CONSTRAINT "code_partial_constant_table_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE code_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "gkdb_development"."code_table" (
    "id" integer DEFAULT nextval('code_table_id_seq') NOT NULL,
    "gk_id" integer NOT NULL,
    "name" character varying(64) NOT NULL,
    "commit" character varying(512) NOT NULL,
    "version" character varying(32) NOT NULL,
    "repository" character varying(256) NOT NULL,
    "parameters" jsonb,
    "output_flag" integer[] NOT NULL,
    CONSTRAINT "code_table_output_flag_key" UNIQUE ("output_flag"),
    CONSTRAINT "code_table_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE SEQUENCE gyrokinetics_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "gkdb_development"."gyrokinetics_table" (
    "id" integer DEFAULT nextval('gyrokinetics_table_id_seq') NOT NULL,
    "uuid" uuid DEFAULT uuid_generate_v7() NOT NULL,
    "time" double precision NOT NULL,
    CONSTRAINT "gyrokinetics_table_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "gyrokinetics_table_uuid_key" UNIQUE ("uuid")
) WITH (oids = false);

CREATE INDEX "ix_gkdb_development_gyrokinetics_table_uuid" ON "gkdb_development"."gyrokinetics_table" USING btree ("uuid");

CREATE TABLE "gkdb_development"."ids_tag_table" (
    "gk_id" integer NOT NULL,
    "tag_id" integer NOT NULL,
    CONSTRAINT "ids_tag_table_pkey" PRIMARY KEY ("gk_id", "tag_id")
) WITH (oids = false);

CREATE SEQUENCE library_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "gkdb_development"."library_table" (
    "id" integer DEFAULT nextval('library_table_id_seq') NOT NULL,
    "code_id" integer NOT NULL,
    "name" character varying(64) NOT NULL,
    "commit" character varying(512) NOT NULL,
    "version" character varying(32) NOT NULL,
    "repository" character varying(256) NOT NULL,
    "parameters" jsonb NOT NULL,
    CONSTRAINT "library_table_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE SEQUENCE tag_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "gkdb_development"."tag_table" (
    "id" integer DEFAULT nextval('tag_table_id_seq') NOT NULL,
    "tagid" character varying(32) NOT NULL,
    "categorie" character varying(32) NOT NULL CHECK( categorie IN ('test','simulation','doi')) ,
    "description" text,
    CONSTRAINT "tag_table_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "tag_table_tagid_categorie_key" UNIQUE ("tagid", "categorie")
) WITH (oids = false);


CREATE SEQUENCE wavevector_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "gkdb_development"."wavevector_table" (
    "id" integer DEFAULT nextval('wavevector_table_id_seq') NOT NULL,
    "gk_id" integer ,
    "radial_component_norm" double precision NOT NULL,
    "binormal_component_norm" double precision NOT NULL,
    CONSTRAINT "wavevector_table_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


-- ALTER TABLE ONLY "gkdb_development"."code_partial_constant_table" ADD CONSTRAINT "code_partial_constant_table_eigenmode_id_fkey" FOREIGN KEY (eigenmode_id) REFERENCES eigenmode_table(id) NOT DEFERRABLE;

ALTER TABLE ONLY "gkdb_development"."code_table" ADD CONSTRAINT "code_table_gks_id_fkey" FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table(id) NOT DEFERRABLE;

ALTER TABLE ONLY "gkdb_development"."ids_tag_table" ADD CONSTRAINT "ids_tag_table_gk_id_fkey" FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table(id) NOT DEFERRABLE;
ALTER TABLE ONLY "gkdb_development"."ids_tag_table" ADD CONSTRAINT "ids_tag_table_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES tag_table(id) NOT DEFERRABLE;

ALTER TABLE ONLY "gkdb_development"."library_table" ADD CONSTRAINT "library_table_code_id_fkey" FOREIGN KEY (code_id) REFERENCES code_table(id) NOT DEFERRABLE;

ALTER TABLE ONLY "gkdb_development"."wavevector_table" ADD CONSTRAINT "wavevector_table_gks_id_fkey" FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table(id) NOT DEFERRABLE;


-- adding data to tag table
INSERT INTO "gkdb_development"."tag_table" ("tagid", "categorie", "description") VALUES ('#1234', 'test', 'a db test');
INSERT INTO "gkdb_development"."tag_table" ("tagid", "categorie", "description") VALUES ('#5678', 'test', 'another test');

INSERT INTO "gkdb_development"."wavevector_table" ("radial_component_norm", "binormal_component_norm") VALUES ('1.1', '2.2');
INSERT INTO "gkdb_development"."wavevector_table" ("radial_component_norm", "binormal_component_norm") VALUES ('3.3', '4.4');

INSERT INTO "gkdb_development"."gyrokinetics_table" ("time") VALUES (11.11);
INSERT INTO "gkdb_development"."gyrokinetics_table" ("time") VALUES (22.22);
INSERT INTO "gkdb_development"."gyrokinetics_table" ("time") VALUES (33.33);
-- end of basic SQL test