CREATE SCHEMA IF NOT EXISTS "gkdb_development";

set schema 'gkdb_development';

CREATE EXTENSION IF NOT EXISTS pg_uuidv7;

--         CREATE TYPE IF NOT EXISTS complex AS (
--             r float4,
--             i float4
--         );

DO $$ BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'complex') THEN
        CREATE TYPE complex AS (
            r float4,
            i float4
        );
    END IF;
    CREATE TABLE dummy_table (dummy_column complex);
    DROP TABLE dummy_table;
END $$;


CREATE TABLE IF NOT EXISTS tag_table (
	id SERIAL NOT NULL, 
	tagid VARCHAR(32) NOT NULL, 
	categorie VARCHAR(32) NOT NULL CHECK (categorie IN ('simulation', 'experiment', 'doi', 'conference', 'tutorial', 'ML_training', 'ML_set', 'other', 'reference_case', 'zenodo', 'equivalents', 'gkw', 'gene', 'test', 'qualikiz', 'tglf')), 
	description TEXT, 
	PRIMARY KEY (id), 
	UNIQUE (tagid, categorie)
);





CREATE TABLE IF NOT EXISTS gyrokinetics_table
(
    id   BIGSERIAL                       NOT NULL,
    uuid UUID DEFAULT uuid_generate_v7() NOT NULL,
    time float4                           NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (uuid)
);


-- CREATE INDEX ix_gkdb_development_gyrokinetics_table_uuid ON gkdb_development.gyrokinetics_table (uuid);

CREATE TABLE IF NOT EXISTS ids_tags_table
(
    gk_id  BIGINT  NOT NULL,
    tag_id INTEGER NOT NULL,
    PRIMARY KEY (gk_id, tag_id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id),
    FOREIGN KEY (tag_id) REFERENCES tag_table (id)
);


CREATE TABLE IF NOT EXISTS file_entry_table
(
    id       SERIAL       NOT NULL,
    gk_id    BIGINT       NOT NULL,
    hash     VARCHAR(64)  NOT NULL,
    filename VARCHAR(256) NOT NULL,
    storage  VARCHAR(256) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (hash, storage),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id),
    UNIQUE (hash),
    UNIQUE (filename)
);


CREATE TABLE IF NOT EXISTS flux_surface_table
(
    id                         SERIAL  NOT NULL,
    gk_id                      BIGINT  NOT NULL,
    r_minor_norm               float4   NOT NULL,
    elongation                 float4   NOT NULL,
    delongation_dr_minor_norm  float4   NOT NULL,
    dgeometric_axis_r_dr_minor float4   NOT NULL,
    dgeometric_axis_z_dr_minor float4   NOT NULL,
    q                          float4   NOT NULL,
    magnetic_shear_r_minor     float4   NOT NULL,
    pressure_gradient_norm     float4   NOT NULL,
    ip_sign                    float4   NOT NULL,
    b_field_tor_sign           float4   NOT NULL,
    shape_coefficients_c       float4[] NOT NULL,
    dc_dr_minor_norm           float4[] NOT NULL,
    shape_coefficients_s       float4[] NOT NULL,
    ds_dr_minor_norm           float4[] NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id),
    UNIQUE (shape_coefficients_c)
);


CREATE TABLE IF NOT EXISTS input_normalizing_table
(
    id          SERIAL NOT NULL,
    gk_id       BIGINT NOT NULL,
    t_e         float4  NOT NULL,
    n_e         float4  NOT NULL,
    r           float4  NOT NULL,
    b_field_tor float4  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS species_all_table
(
    id                     SERIAL NOT NULL,
    gk_id                  BIGINT NOT NULL,
    beta_reference         float4  NOT NULL,
    velocity_tor_norm      float4  NOT NULL,
    zeff                   float4  NOT NULL,
    debye_length_reference float4  NOT NULL,
    shearing_rate_norm     float4  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS model_table
(
    id                               SERIAL  NOT NULL,
    gk_id                            BIGINT  NOT NULL,
    include_centrifugal_effects      BOOLEAN NOT NULL,
    include_a_field_parallel         BOOLEAN NOT NULL,
    include_b_field_parallel         BOOLEAN NOT NULL,
    include_full_curvature_drift     BOOLEAN NOT NULL,
    collisions_pitch_only            BOOLEAN NOT NULL,
    collisions_momentum_conservation BOOLEAN NOT NULL,
    collisions_energy_conservation   BOOLEAN NOT NULL,
    collisions_finite_larmor_radius  BOOLEAN NOT NULL,
    non_linear_run                   BOOLEAN NOT NULL,
    time_interval_norm               float4[],
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS species_table
(
    id                            SERIAL NOT NULL,
    gk_id                         BIGINT NOT NULL,
    charge_norm                   float4  NOT NULL,
    mass_norm                     float4  NOT NULL,
    density_norm                  float4  NOT NULL,
    density_log_gradient_norm     float4  NOT NULL,
    temperature_norm              float4  NOT NULL,
    temperature_log_gradient_norm float4  NOT NULL,
    velocity_tor_gradient_norm    float4  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS ids_properties_table
(
    id               SERIAL       NOT NULL,
    gk_id            BIGINT       NOT NULL,
    comment          TEXT         NOT NULL,
    homogeneous_time INTEGER      NOT NULL,
    provider         VARCHAR(512) NOT NULL,
    creation_date    VARCHAR(512) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS code_table
(
    id          SERIAL       NOT NULL,
    gk_id       BIGINT       NOT NULL,
    name        VARCHAR(64)  NOT NULL,
    commit      VARCHAR(512) NOT NULL,
    version     VARCHAR(32)  NOT NULL,
    repository  VARCHAR(256) NOT NULL,
    parameters  JSONB,
    output_flag INTEGER[]    NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS wavevector_table
(
    id                      SERIAL NOT NULL,
    gk_id                   BIGINT NOT NULL,
    radial_component_norm   float4  NOT NULL,
    binormal_component_norm float4  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);


CREATE TABLE IF NOT EXISTS collisions_table
(
    id                  SERIAL  NOT NULL,
    gk_id               BIGINT  NOT NULL,
    collisionality_norm float4   NOT NULL,
    specie_one          INTEGER NOT NULL,
    specie_two          INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id),
    FOREIGN KEY (specie_one) REFERENCES species_table (id),
    FOREIGN KEY (specie_two) REFERENCES species_table (id)
);


CREATE TABLE IF NOT EXISTS ids_provenance_node_table
(
    id                SERIAL         NOT NULL,
    path              VARCHAR(512)   NOT NULL,
    sources           VARCHAR(256)[] NOT NULL,
    ids_properties_id INTEGER        NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (ids_properties_id) REFERENCES ids_properties_table (id)
);


CREATE TABLE IF NOT EXISTS library_table
(
    id         SERIAL       NOT NULL,
    code_id    INTEGER      NOT NULL,
    name       VARCHAR(64)  NOT NULL,
    commit     VARCHAR(512) NOT NULL,
    version    VARCHAR(32)  NOT NULL,
    repository VARCHAR(256) NOT NULL,
    parameters JSONB        NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (code_id) REFERENCES code_table (id)
);


CREATE TABLE IF NOT EXISTS eigenmode_table
(
    id                                SERIAL  NOT NULL,
    wavevector_id                     INTEGER ,
    poloidal_turns                    INTEGER NOT NULL,
    growth_rate_norm                  float4   NOT NULL,
    frequency_norm                    float4   NOT NULL,
    growth_rate_tolerance             float4   NOT NULL,
    phi_potential_perturbed_weight    float4[],
    phi_potential_perturbed_parity    float4[],
    a_field_parallel_perturbed_weight float4[],
    a_field_parallel_perturbed_parity float4[],
    b_field_parallel_perturbed_weight float4[],
    b_field_parallel_perturbed_parity float4[],
    poloidal_angle                    float4[],
    phi_potential_perturbed_norm      complex[][],
    a_field_parallel_perturbed_norm   complex[][],
    b_field_parallel_perturbed_norm   complex[][],
    time_norm                         float4[],
    initial_value_run                 BOOLEAN NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (wavevector_id) REFERENCES wavevector_table (id)
);



CREATE TABLE IF NOT EXISTS code_partial_constant_table
(
    id           SERIAL  NOT NULL,
    eigenmode_id INTEGER NOT NULL,
    code_id      INTEGER NOT NULL,
    parameters   JSONB   NOT NULL,
    output_flag  INTEGER,
    PRIMARY KEY (id),
    FOREIGN KEY (eigenmode_id) REFERENCES eigenmode_table (id),
    FOREIGN KEY (code_id) REFERENCES code_table (id)
);


CREATE TABLE IF NOT EXISTS fluxes_table
(
    id                                          SERIAL      NOT NULL,
    gk_id                                       BIGINT      NOT NULL,
    species_id                                  INTEGER     NOT NULL,
    eigenmode_id                                INTEGER,
    frame                                       VARCHAR(16) NOT NULL,
    ftype                                       VARCHAR(16) NOT NULL,
    particles_phi_potential                     float4       NOT NULL,
    particles_a_field_parallel                  float4       NOT NULL,
    particles_b_field_parallel                  float4       NOT NULL,
    energy_phi_potential                        float4       NOT NULL,
    energy_a_field_parallel                     float4       NOT NULL,
    energy_b_field_parallel                     float4       NOT NULL,
    momentum_tor_parallel_phi_potential         float4       NOT NULL,
    momentum_tor_parallel_a_field_parallel      float4       NOT NULL,
    momentum_tor_parallel_b_field_parallel      float4       NOT NULL,
    momentum_tor_perpendicular_phi_potential    float4       NOT NULL,
    momentum_tor_perpendicular_a_field_parallel float4       NOT NULL,
    momentum_tor_perpendicular_b_field_parallel float4       NOT NULL,
    uuid                            UUID        NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id),
    FOREIGN KEY (species_id) REFERENCES species_table (id),
    FOREIGN KEY (eigenmode_id) REFERENCES eigenmode_table (id)
);

CREATE TABLE IF NOT EXISTS moments_table
(
    id                              SERIAL      NOT NULL,
    species_id                      INTEGER     NOT NULL,
    eigenmode_id                    INTEGER,
    gk_id                           BIGINT      NOT NULL,
    norm                            BOOLEAN,
    mtype                           VARCHAR(16) NOT NULL,
    uuid                            UUID        NOT NULL,
    density                         complex[][] NOT NULL,
    j_parallel                      complex[][] NOT NULL,
    pressure_parallel               complex[][] NOT NULL,
    pressure_perpendicular          complex[][] NOT NULL,
    heat_flux_parallel              complex[][] NOT NULL,
    v_parallel_energy_perpendicular complex[][] NOT NULL,
    v_perpendicular_square_energy   complex[][] NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (species_id) REFERENCES species_table (id),
    FOREIGN KEY (eigenmode_id) REFERENCES eigenmode_table (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id)
);

CREATE TABLE IF NOT EXISTS fluxes_moments_table
(
    id                                       SERIAL  NOT NULL,
    gk_id                                    BIGINT  NOT NULL,
    eigenmode_id                             INTEGER NOT NULL,
    specie_id                                INTEGER NOT NULL,
    moments_particle_id                      INTEGER NOT NULL,
    moments_id                               INTEGER NOT NULL,
    moments_gyroav_id                        INTEGER NOT NULL,
    moments_norm_gyrocenter_id               INTEGER,
    moments_norm_particle_id                 INTEGER,
    fluxes_norm_gyrocenter_id                INTEGER,
    fluxes_norm_gyrocenter_rotating_frame_id INTEGER,
    fluxes_norm_particle_id                  INTEGER,
    fluxes_norm_particle_rotating_frame_id   INTEGER,
    PRIMARY KEY (id),
    FOREIGN KEY (gk_id) REFERENCES gyrokinetics_table (id),
    FOREIGN KEY (eigenmode_id) REFERENCES eigenmode_table (id),
    FOREIGN KEY (specie_id) REFERENCES species_table (id),
    FOREIGN KEY (moments_particle_id) REFERENCES moments_table (id),
    FOREIGN KEY (moments_id) REFERENCES moments_table (id),
    FOREIGN KEY (moments_gyroav_id) REFERENCES moments_table (id),
    FOREIGN KEY (moments_norm_gyrocenter_id) REFERENCES moments_table (id),
    FOREIGN KEY (moments_norm_particle_id) REFERENCES moments_table (id),
    FOREIGN KEY (fluxes_norm_gyrocenter_id) REFERENCES fluxes_table (id),
    FOREIGN KEY (fluxes_norm_gyrocenter_rotating_frame_id) REFERENCES fluxes_table (id),
    FOREIGN KEY (fluxes_norm_particle_id) REFERENCES fluxes_table (id),
    FOREIGN KEY (fluxes_norm_particle_rotating_frame_id) REFERENCES fluxes_table (id)
);

INSERT INTO "gyrokinetics_table" ("time") VALUES (11.11);
INSERT INTO "gyrokinetics_table" ("time") VALUES (22.22);
INSERT INTO "gyrokinetics_table" ("time") VALUES (33.33);