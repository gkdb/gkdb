READMD GKDB
===========



Installation the easy way
- install docker (www.docker.com)
- clone the gkdb repository
 - ``git clone https://gitlab.com/gkdb/gkdb.git``
- open a terminal and go in your newly downloaded gkdb/docker folder
- create an environment variable for your PostgreSQL Admin user named GKDB_ROOTUSER
``>> export GKDB_ROOTUSER=my_secret_root_username``
- create an environment variable for your PostgreSQL Admin password named GKDB_ROOTPWD
``>> export GKDB_ROOTPWD=my_very_secret_root_password``
- start your newly obtained container using the command :
``docker compose up``
- The PostgreSQL server can be reach on port : 
``6543``
- To facilitate database administration, webservice adminer is accessible at the following URL :
`` http://localhost:9192 ``
It's not recommanded at all to allow connections from internet/other computers using this configuration. In the case you use linux+ufw as firewall please be noticed that ufw doesn't work for docker ports and you must use iptables in that case.

In the case where you want to use your own PostgreSQL server, you must install the pg_uuidv7 extension : https://github.com/fboulnois/pg_uuidv7.git

Even in that case, please define the *2* environment variable defined before since they are used by all the python scripts to connect to the PostgreSQL Database. In particular you need admin rights to be allowed to create the corresponding databases, schemas and tables.
If you want to use a remote PostgreSQL server, please define the *2* environment variable FASTER_DB_HOST and FASTER_DB_PORT



** FAQ **
* Can I use another kind of database as MySQL or MariaDB for example?
Since GKDB uses possibilities only defined in PostgreSQL (Array type, JSONBinary and Composite Datatype) it's not possible to use and DB.

* What is the minimal PostgreSQL version supported?
 GKDB should be compatible with PostgreSQL >=11. Please note that versions 11 to 14 are supported as best effort.

* I prefer to use DBeaver/pgMyAdmin to manage my PostgreSQL server, is it possible?
  Feel free to do it, adminer is used by default mainly because it's light  and enough in most cases.
