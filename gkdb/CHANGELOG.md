CHANGELOG GKDB
==============

Please note that it's still alpha stage and major changes are possible for each release.
* v 0.1.0-alpha **it's still an alpha version, use it at your onw risk**
  * redesign of schema based on IMAS Data Dictionaries v 3.40
  * remove of unused time in gyrokinetics_local table
  * suppression of idsproperties and child tables in the database
  * more logging and explicit error messages
  

** [Previous versions history, deprecated]**

* v 0.0.4
  * missing ids in ids extraction from DB
  * add query for Qe/Qi
  
* v 0.0.3
  * gkdb database
    * add a FK to gyrokinetics in eigenmode_table table
    * add index to eigenmode_table.growth_rate_norm and frequency_norm
    * remove codepartialconstant and add parameters (JsonB) to eigenmode_table
    * bytearray stored as zstandard compressed numpy bytearray
  * docker compose:
    * increase postgresql shmem size
  * gkdb client
    * add possibility to get IDS based on uuid from DB
    * get DB statistics like number of records
    * find all entries with a growth rate in a given range
    * 
    
* v 0.0.2
  * gkdb package
    * added version number
  * gkdb database/model
    * no more complex composite datatype, all arrays stored as bytearray 
    * correct output_flag entry for code table
    * correct array dimensions for the eigenmode table
    * added uuid to moments table and a moment_type column also
    * added uuid to fluxes table
    * added FK to Code for CodePartialConstant
    * rename gks_id to gk_id
    * add FK for species_id in fluxes table
    * add FK for eigenmode_id in fluxes table
    * add columns frame and type in fluxes_table
    * collisions.collisionality_norm from array to float
    * codepartialconsant.output_flag as int and not int[]
    * missing CHECK CONSTRAINT in the tag table
    * add index on code.name, tag.categorie
    * convert double precision to simple precision
    * correct type from float to complex for eigenmode.*perturbed_norm
    * remove the unique flag for code.output_flag
    * add a norm(bool) for the moments table
    * add index on moments.uuid
    * add index on fluxes.uuid
    * add FK to specie and eigenmode for moments
    * primary keys for wavevector/eigenmode from int to bigint
  * gkdb client
    * initial release
    * add support for float32 and complex type
  * pipeline : 
    * added a generator to make IDS from a qualikiz dataset and insert it in the DB
  * docker compose:
    * add volume for the DB
    * add logging options


Bug :
  * adding tag if tag already exists
  * rollback pipeline not fully working

TODO :
  * re-use existing tag under progress
