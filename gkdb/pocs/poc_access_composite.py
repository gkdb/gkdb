from sqlalchemy import create_engine, Integer, String, DateTime, Text, ForeignKey, \
    Float, Boolean, inspect, Table, Column, SmallInteger, text
from sqlalchemy.orm import relationship, sessionmaker
from datetime import datetime
from sqlalchemy import DateTime, String, Text, UniqueConstraint, CheckConstraint
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from typing import Optional
from sqlalchemy.dialects.postgresql import UUID as pgUUID
from sqlalchemy.dialects.postgresql import ARRAY
from dataclasses import dataclass, field
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError, ProgrammingError, DatabaseError, SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound
from psycopg.errors import DuplicateObject

from sqlalchemy.orm import mapped_column, composite, Mapped
from sqlalchemy.schema import CreateTable

import sqlalchemy as sa


# from sqla_pg_composite import CompositeType

class IdsBaseClass(DeclarativeBase):
    pass

#


class COMPLEX(sa.sql.sqltypes.TypeEngine):
    __visit_name__ = "complex"

    def __init__(self, x=None, y=None):
        # e.g. 'PointZ'
        self.x = x
        # e.g. '4326'
        self.y = y



sa.dialects.postgresql.base.ischema_names['complex'] = COMPLEX

from sqlalchemy import TypeDecorator, String
from sqlalchemy.exc import ArgumentError
import json

#class Complex:
# Your complex class implementation

from sqlalchemy import TypeDecorator, String
from sqlalchemy.dialects.postgresql import FLOAT
from sqlalchemy.types import UserDefinedType
import cmath


class PGComplexWrapper(UserDefinedType):
    def get_col_spec(self):
        return "complex"

    def bind_processor(self, dialect):
        def process(value):
            if value is not None:
                return (value.real, value.imag)
            return None
        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is not None:
                return complex(*(map(float, value[1:-1].split(','))))
            return None
        return process


class CplxTable(IdsBaseClass):
    __tablename__ = 'CplxTable'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    growth_rate_norm: Mapped[complex] = mapped_column(PGComplexWrapper, nullable=True, unique=False, doc='Mode growth rate (only defined for linear)')
    frequency_norm: Mapped[float] = mapped_column(Float, nullable=True, unique=False, doc='Mode frequency (only defined for linear)')


if __name__ == "__main__":
    import psycopg

    # engine = create_engine("sqlite:///:memory:")


try:
    engine = create_engine("postgresql+psycopg://faster:faster@localhost:5432/composite_poc")
    Session = sessionmaker(bind=engine)

except OperationalError:
    print("OperationalError: Unable to connect to the Database.")

except ProgrammingError:
    print("ProgrammingError: Invalid table or schema used.")

except DatabaseError:
    print("DatabaseError: General database error.")

try:
    inspector = inspect(engine)
    table_names = inspector.get_table_names()

except ProgrammingError:
    print("ProgrammingError: No such table exists.")

except SQLAlchemyError as e:
    print(f"An SQLAlchemy error occurred: {e}")


with engine.connect() as con:
    try:
        con.execute(text("CREATE EXTENSION pg_uuidv7;"))
    except sa.exc.ProgrammingError:
        con.rollback()
        con.commit()
    try:
        con.execute(text("CREATE TYPE complex AS (\
    real       double precision,\
    imag       double precision\
);"))
    except sa.exc.ProgrammingError:
        con.rollback()
        con.commit()
    con.commit()
IdsBaseClass.metadata.create_all(engine)

for table_name in table_names:
    print(f"Table:{table_name}")
    column_items = inspector.get_columns(table_name)
    print('\t'.join(n for n in column_items[0]))
    for c in column_items:
        assert len(c) == len(column_items[0])
        print('\t'.join(str(c[n]) for n in c))
        print('\t'.join(str(type(c[n])) for n in c))

meta_data = sa.MetaData()
meta_data.reflect(bind=engine)

FORECAST = meta_data.tables['forecast']

stmt = sa.select(FORECAST)

print(stmt)
with engine.connect() as conn:
    for row in conn.execute(stmt):
        print(row)

Session = sessionmaker(bind=engine)
session = Session()
new_row = CplxTable(
                    growth_rate_norm=complex(1.0, 2.0),
                    frequency_norm=1.23)

# Add the record to the session object
session.add(new_row)

# Commit the record in the session object to the database
session.commit()
try:
    # Query the database to fetch all rows in CplxTable
    for row in session.query(CplxTable).all():
        print(f"id: {row.id}, growth_rate_norm: {row.growth_rate_norm}, frequency_norm: {row.frequency_norm}")
except NoResultFound:
    print("No record found.")
finally:
    # Don't forget to close the session after all queries are executed
    session.close()

print(CreateTable(CplxTable.__table__))