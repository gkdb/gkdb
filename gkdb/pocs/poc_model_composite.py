from sqlalchemy import create_engine, Integer, String, DateTime, Text, ForeignKey, \
    Float, Boolean, inspect, Table, Column, SmallInteger, text
from sqlalchemy.orm import relationship, sessionmaker
from datetime import datetime
from sqlalchemy import DateTime, String, Text, UniqueConstraint, CheckConstraint
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from typing import Optional
from sqlalchemy.dialects.postgresql import UUID as pgUUID
from sqlalchemy.dialects.postgresql import ARRAY
from dataclasses import dataclass
import constants
from psycopg.errors import DuplicateObject

from sqlalchemy.orm import mapped_column, composite, Mapped
from sqlalchemy.schema import CreateTable

import sqlalchemy as sa
#from sqla_pg_composite import CompositeType

class IdsBaseClass(DeclarativeBase):
    pass

@dataclass
class Complex:
    r: float
    i: float



class Vertex(IdsBaseClass):
    __tablename__ = "vertices"

    id: Mapped[int] = mapped_column(primary_key=True)
    x1: Mapped[int]
    y1: Mapped[int]
    x2: Mapped[int]
    y2: Mapped[int]

    start: Mapped[Complex] = composite("x1", "y1")
    end: Mapped[Complex] = composite("x2", "y2")

if __name__ == "__main__":
    import psycopg
    #engine = create_engine("sqlite:///:memory:")
    engine = create_engine("postgresql+psycopg://faster:faster@localhost:5432/gkdb")

    print(CreateTable(Vertex.__table__))
    with engine.connect() as con:
        try:
            con.execute(text("CREATE EXTENSION pg_uuidv7;"))
        except sa.exc.ProgrammingError:
            con.rollback()
            con.commit()
        try:
            con.execute(text("CREATE TYPE complex AS (\
        r       double precision,\
        i       double precision\
    );"))
        except sa.exc.ProgrammingError:
            con.rollback()
            con.commit()
        con.commit()
    IdsBaseClass.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)

    inspector = inspect(engine)
    table_names = inspector.get_table_names()

    for table_name in table_names:
        print(f"Table:{table_name}")
        column_items = inspector.get_columns(table_name)
        print('\t'.join(n for n in column_items[0]))
        for c in column_items:
            assert len(c) == len(column_items[0])
            print('\t'.join(str(c[n]) for n in c))