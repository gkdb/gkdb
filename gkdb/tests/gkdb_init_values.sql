set schema 'gkdb_development';

-- adding data to tag table
INSERT INTO "gkdb_development"."tag_table" ("tagid", "categorie", "description")
                                            VALUES ('#1234', 'test', 'a db test'),
                                                     ('#5678', 'test', 'another test');


INSERT INTO "gkdb_development"."gyrokinetics_local_table" ("id") VALUES ('1'), ('2'), ('3');

INSERT INTO "gkdb_development"."linear_table" ("radial_wavevector_norm", "binormal_wavevector_norm", "gk_id")
                                                VALUES ('1.1', '2.2', '1'),
                                                       ('3.3', '4.4', '1');

INSERT INTO "gkdb_development"."code_version_table" ("name", "commit", "release_version", "repository", "type")
                                                    VALUES ('gkw', '123456789', '0.0.1', 'rep://here', '0');

INSERT INTO "gkdb_development"."code_table" ("version_id", "gk_id") VALUES ('1', '1');

INSERT INTO "gkdb_development"."species_table" ("gk_id", "charge_norm", "mass_norm", "density_norm",
                                                "density_log_gradient_norm",
                                                "temperature_norm",
                                                "temperature_log_gradient_norm",
                                                "velocity_tor_gradient_norm",
                                                "potential_energy_gradient_norm",
                                                "potential_energy_norm")
                                        VALUES ('1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
                                               ('1', '2', '2', '2', '2', '2', '2', '2', '2', '2');

