CREATE OR REPLACE FUNCTION reset_sequence(schema_name TEXT, table_name TEXT, column_name TEXT)
RETURNS VOID AS $$
DECLARE
    seq_name TEXT;
BEGIN
    -- Get the name of the associated sequence
    BEGIN
        EXECUTE format('SELECT pg_get_serial_sequence(%L, %L)', schema_name || '.' || table_name, column_name) INTO seq_name;
    EXCEPTION
        WHEN undefined_table THEN
            RAISE NOTICE 'Sequence for table %.% does not exist', schema_name, table_name;
            RETURN;
    END;
    -- Restart the sequence with 1 if the sequence name is not null
    IF seq_name IS NOT NULL THEN
        EXECUTE format('ALTER SEQUENCE %I RESTART WITH 1', seq_name);
    END IF;
END;
$$ LANGUAGE plpgsql;


truncate table "gkdb_development".tag_table restart identity cascade;
truncate table "gkdb_development".author_table restart identity cascade;
truncate table "gkdb_development".database_statistics_table restart identity cascade;
truncate table "gkdb_development".code_version_table restart identity cascade;
truncate table "gkdb_development".gyrokinetics_local_table restart identity cascade;
truncate table "gkdb_development".ids_tags_table restart identity cascade;
truncate table "gkdb_development".ids_authors_table restart identity cascade;
truncate table "gkdb_development".restrictions_table restart identity cascade;
truncate table "gkdb_development".ids_simulation_details_table restart identity cascade;
truncate table "gkdb_development".file_storage_table restart identity cascade;
truncate table "gkdb_development".flux_surface_table restart identity cascade;
truncate table "gkdb_development".input_normalizing_table restart identity cascade;
truncate table "gkdb_development".input_species_global_table restart identity cascade;
truncate table "gkdb_development".model_table restart identity cascade;
truncate table "gkdb_development".species_table restart identity cascade;
truncate table "gkdb_development".code_table restart identity cascade;
truncate table "gkdb_development".non_linear_table restart identity cascade;
truncate table gkdb_development.linear_table restart identity cascade;
truncate table gkdb_development.ids_revision_history_table restart identity cascade;
truncate table gkdb_development.collisions_table restart identity cascade;
truncate table gkdb_development.fields_table restart identity cascade;
truncate table gkdb_development.fluxes_1d_table restart identity cascade;
truncate table gkdb_development.fluxes_2d_table restart identity cascade;
truncate table gkdb_development.library_table restart identity cascade;
truncate table gkdb_development.eigenmode_table restart identity cascade;
truncate table gkdb_development.eigenmode_fields_table restart identity cascade;
truncate table gkdb_development.linear_weights_table restart identity cascade;
--
-- SELECT reset_sequence('gkdb_development', 'linear_weights_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'tag_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'author_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'database_statistics_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'code_version_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'gyrokinetics_local_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'ids_tags_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'ids_authors_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'restrictions_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'ids_simulation_details_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'file_storage_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'flux_surface_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'input_normalizing_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'input_species_global_table' , 'id');
-- SELECT reset_sequence('gkdb_development', 'model_table' , 'id');
-- SELECT reset_sequence('gkdb_development', 'species_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'code_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'non_linear_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'linear_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'ids_revision_history_table', 'id');
-- SELECT reset_sequence('gkdb_development', 'collisions_table', 'id');
-- SELECT reset_sequence('gkdb_development','fields_table', 'id');
-- SELECT reset_sequence('gkdb_development','fluxes_1d_table', 'id');
-- SELECT reset_sequence('gkdb_development','fluxes_2d_table', 'id');
-- SELECT reset_sequence('gkdb_development','library_table', 'id');
-- SELECT reset_sequence('gkdb_development','eigenmode_table', 'id');
-- SELECT reset_sequence('gkdb_development','eigenmode_fields_table', 'id');
-- SELECT reset_sequence('gkdb_development','linear_weights_table', 'id');
