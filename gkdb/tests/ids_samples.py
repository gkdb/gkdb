from dataclasses import dataclass
import copy

@dataclass
class Library:
    name: str = ''
    commit: str = ''
    version: str = ''
    repository: str = ''
    parameters: dict = None


@dataclass
class Code:
    name: str = ''
    commit: str = ''
    version: str = ''
    repository: str = ''
    parameters: dict = None
    output_flag: list = None
    library: list = None


@dataclass
class Tag:
    tagid: str = ''
    categorie: str = ''
    description: str = ''


@dataclass
class Gyrokinetics:
    uuid: str = ''
    time: float = 0.0
    wavevector: list = None
    code: Code = None
    tag: list = None


@dataclass
class Wavevector:
    radial_component_norm: float = 0.0
    binormal_component_norm: float = 0.0


TEST_DEFAULT_DICT = {"wavevector": Wavevector(-4.2, 4.2),
                     "tag": Tag("#12345", "test", "an UT and useless description"),
                     "library": Library("mylibrary", "first", "9.9.9", "",
                                  parameters={"a": 1, "b": 2}),
                     "code": Code("mycode", "last", "0.0.0", "",
                                  parameters={"a": 3, "b": 4}, output_flag=[4, 2],
                                  library=[])}
TEST_DEFAULT_DICT["code"].library = [TEST_DEFAULT_DICT["library"],]

