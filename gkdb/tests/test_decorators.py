from __future__ import annotations

import pytest
try:
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
except ModuleNotFoundError:
    import sys
    sys.path.append('../')
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids

import gkdb.core.decorators as decor


MAIN_TABLE_NAME = "gyrokinetics_local_table"
MAIN_SCHEMA_NAME = "gkdb_development"

def test_decorator_fk_exists(postgresql_mok_full):
    @decor.foreign_key_exists(MAIN_TABLE_NAME)
    def to_be_decorated(cursor, foreign_key_values: int | list | tuple, schema: str, *args, **kwargs):
        return True

    cursor = postgresql_mok_full.cursor()
    assert to_be_decorated(cursor, (1,), MAIN_SCHEMA_NAME)

def test_decorator_fk_exists_multiple(postgresql_mok_full):
    @decor.foreign_key_exists(MAIN_TABLE_NAME)
    def to_be_decorated(cursor, foreign_key_values: int | list | tuple, schema: str, *args, **kwargs):
        return True

    cursor = postgresql_mok_full.cursor()
    assert to_be_decorated(cursor, (1,2,), MAIN_SCHEMA_NAME)


def test_decorator_fk_not_exists(postgresql_mok_full):
        @decor.foreign_key_exists(MAIN_TABLE_NAME)
        def to_be_decorated(cursor, foreign_key_values: int | list | tuple, schema: str, *args, **kwargs):
            return True

        cursor = postgresql_mok_full.cursor()
        with pytest.raises(ValueError):
            to_be_decorated(cursor, (9999,), MAIN_SCHEMA_NAME)


def test_decorator_fk_table_not_exists(postgresql_mok_full):
    @decor.foreign_key_exists("unknown_table")
    def to_be_decorated(cursor, foreign_key_values: int | list | tuple, schema: str, *args, **kwargs):
        return True

    cursor = postgresql_mok_full.cursor()
    with pytest.raises(AttributeError):
        to_be_decorated(cursor, (1,), MAIN_SCHEMA_NAME)

# def test_decorator_fk_value_exists(postgresql_mok_full):
#     @using_email_address
#     def to_be_decorated(**kwargs):
#         assert kwargs.pop('from_email') == 'management@ecorp.com'
#         assert kwargs.pop('user') == manager
#
#     to_be_decorated(user=manager)