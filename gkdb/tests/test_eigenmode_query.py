import psycopg
import pytest
from pytest_postgresql import factories
from psycopg.rows import dict_row
import numpy as np
from dataclasses import fields

try:
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
    from ids_samples import Tag, TEST_DEFAULT_DICT
    import gkdb.client.client as gkclient
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
    import gkdb.client.client as gkclient
    from ids_samples import Tag, TEST_DEFAULT_DICT

from idspy_toolkit.exceptions import IdsTypeError, IdsForbiddenValue, IdsDimensionError
from gkdb.core.exceptions import IdsNotValid
import gkdb.core.idstogkdb_insert as gkclient
import gkdb.tools.fakedata as fdata
from gkdb.core.constants import IMAS_DEFAULT_FLOAT

def get_ids_info(connect_db_full):
    with connect_db_full.connection() as conn:
        code_id = conn.execute("SELECT id FROM gkdb_development.code_table LIMIT 1;").fetchone()[0]
        gk_id, gk_uuid = conn.execute(
            "SELECT id,uuid FROM gkdb_development.gyrokinetics_local_table LIMIT 1;").fetchone()
        species_id = \
            conn.execute("SELECT id FROM gkdb_development.species_table as s WHERE s.gk_id=%s ;",
                         (str(gk_id),)).fetchall()
        species_id = [x[0] for x in species_id]
    return gk_id, gk_uuid, species_id, code_id


def get_all_tables_size(conn):
    with conn.cursor(row_factory=dict_row) as cur:
        initial_number_id = cur.execute("SELECT * FROM count_rows_in_tables('gkdb_development');").fetchall()
    table_dict = {}
    for x in initial_number_id:
        table_dict.update({x['local_table_name']: int(x['row_count'])})
    assert len(initial_number_id) == len(table_dict.keys())
    return table_dict


def test_insert_wavevector(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with conn.cursor() as cur:
            ret_wv = gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                                 species_id, gk_uuid)
            conn.commit()

    assert ret_wv is True

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            number_id = conn.execute("SELECT count(id) FROM gkdb_development.linear_weights_table;").fetchone()[0]
        conn.commit()

    assert number_id == fake_data.neigenmode * fake_data.nspecies * 2

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            number_id = conn.execute(
                "SELECT count(id) FROM gkdb_development.linear_weights_table as linear"
                " where frame='rotating' and gk_id=%s;", (gk_id,)).fetchone()[0]
        conn.commit()
    number_id = int(number_id)
    assert number_id == fake_data.neigenmode * fake_data.nspecies

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            number_id = conn.execute(
                "SELECT count(id) FROM gkdb_development.linear_weights_table as linear"
                " where frame='laboratory' and gk_id=%s;", (gk_id,)).fetchone()[0]
        conn.commit()
    number_id = int(number_id)
    assert number_id == fake_data.neigenmode * fake_data.nspecies


# assert len(ret_wv[1]) == 2


def test_insert_fluxes_wrong_size(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        tables_size_init = get_all_tables_size(conn)

    for idx in range(len(wv.eigenmode)):
        for f in fields(wv.eigenmode[idx].linear_weights):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights, f.name,
                        getattr(wv.eigenmode[idx].linear_weights, f.name)[:-2]
                        )

        for f in fields(wv.eigenmode[idx].linear_weights_rotating_frame):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name,
                        getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name)[:-2]
                        )

    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with pytest.raises(IdsDimensionError):
            with conn.cursor() as cur:
                gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                            species_id, gk_uuid)
                conn.commit()

    with connect_db_full.connection() as conn:
        tables_size = get_all_tables_size(conn)

    assert tables_size['eigenmode_table'] == tables_size_init['eigenmode_table']
    assert tables_size_init['linear_weights_table'] == tables_size['linear_weights_table'], \
        f"entries added in linear_weights table : {tables_size['linear_weights_table'] - tables_size_init['linear_weights_table']}"


def test_insert_fluxes_imas_default_in(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        tables_size_init = get_all_tables_size(conn)

    for idx in range(len(wv.eigenmode)):
        for f in fields(wv.eigenmode[idx].linear_weights):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights, f.name,
                        [IMAS_DEFAULT_FLOAT for _ in range(len(getattr(wv.eigenmode[idx].linear_weights, f.name)))]
                        )

        for f in fields(wv.eigenmode[idx].linear_weights_rotating_frame):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name,
                        [IMAS_DEFAULT_FLOAT for _ in range(len(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name)))]
                        )

    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with pytest.raises(IdsForbiddenValue):
            with conn.cursor() as cur:
                #with pytest.raises(ValueError) as excinfo:
                gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                            species_id, gk_uuid)
                conn.commit()

    with connect_db_full.connection() as conn:
        tables_size = get_all_tables_size(conn)

    assert tables_size['eigenmode_table'] == tables_size_init['eigenmode_table']
    assert tables_size_init['linear_weights_table'] == tables_size['linear_weights_table'], \
        f"entries added in linear_weights table : {tables_size['linear_weights_table'] - tables_size_init['linear_weights_table']}"


def test_insert_fluxes_all_none(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        tables_size_init = get_all_tables_size(conn)

    for idx in range(len(wv.eigenmode)):
        for f in fields(wv.eigenmode[idx].linear_weights):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights, f.name,
                        [None for _ in range(len(getattr(wv.eigenmode[idx].linear_weights, f.name)))]
                        )

        for f in fields(wv.eigenmode[idx].linear_weights_rotating_frame):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name,
                        [None for _ in range(len(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name)))]
                        )

    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with pytest.raises(IdsTypeError):
            with conn.cursor() as cur:
                #with pytest.raises(ValueError) as excinfo:
                gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                            species_id, gk_uuid)
                conn.commit()

    with connect_db_full.connection() as conn:
        tables_size = get_all_tables_size(conn)

    assert tables_size['eigenmode_table'] == tables_size_init['eigenmode_table']
    assert tables_size_init['linear_weights_table'] == tables_size['linear_weights_table'], \
        f"entries added in linear_weights table : {tables_size['linear_weights_table'] - tables_size_init['linear_weights_table']}"


def test_insert_fluxes_float(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        tables_size_init = get_all_tables_size(conn)

    for idx in range(len(wv.eigenmode)):
        for f in fields(wv.eigenmode[idx].linear_weights):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights, f.name,
                        getattr(wv.eigenmode[idx].linear_weights, f.name)[0]
                        )

        for f in fields(wv.eigenmode[idx].linear_weights_rotating_frame):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name,
                        getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name)[0]
                        )

    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with conn.cursor() as cur:
            #with pytest.raises(ValueError) as excinfo:
            ret_wv = gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                                 species_id, gk_uuid)
            conn.commit()
        #assert excinfo.value.args[0] == "linear weights are not array of species"
        assert ret_wv is True

    with connect_db_full.connection() as conn:
        tables_size = get_all_tables_size(conn)

    assert tables_size['eigenmode_table'] - tables_size_init['eigenmode_table'] == fake_data.neigenmode, \
        f"entries added in eigenmode table : {tables_size['eigenmode_table'] - tables_size_init['eigenmode_table']}"
    assert tables_size_init['linear_weights_table'] == tables_size['linear_weights_table'], \
        f"entries added in linear_weights table : {tables_size['linear_weights_table'] - tables_size_init['linear_weights_table']}"


def test_insert_no_linear_weights(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        tables_size_init = get_all_tables_size(conn)

    for idx in range(len(wv.eigenmode)):
        wv.eigenmode[idx].linear_weights = None
        wv.eigenmode[idx].linear_weights_rotating_frame = None

    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with conn.cursor() as cur:
            ret_wv = gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                                 species_id, gk_uuid)
            conn.commit()
        #assert excinfo.value.args[0] == "linear weights are not array of species"
        assert ret_wv is True

    with connect_db_full.connection() as conn:
        tables_size = get_all_tables_size(conn)

    assert tables_size['eigenmode_table'] - tables_size_init['eigenmode_table'] == fake_data.neigenmode, \
        f"entries added in eigenmode table : {tables_size['eigenmode_table'] - tables_size_init['eigenmode_table']}"
    assert tables_size_init['linear_weights_table'] == tables_size['linear_weights_table'], \
        f"entries added in linear_weights table : {tables_size['linear_weights_table'] - tables_size_init['linear_weights_table']}"


def test_insert_none_linear_weights_rotating(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            initial_number_id = conn.execute("SELECT count(id) FROM gkdb_development.linear_weights_table;").fetchone()[
                0]
        conn.commit()

    for idx in range(len(wv.eigenmode)):
        for f in fields(wv.eigenmode[idx].linear_weights_rotating_frame):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name,
                        None)
    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with pytest.raises(IdsTypeError):
            with conn.cursor() as cur:
                ret_wv = gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                                     species_id, gk_uuid)

        conn.commit()
    assert ret_wv is None


def test_insert_no_linear_weights_rotating(connect_db_full):
    number_species = 2
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    wv = fdata.generate_fake_wavevector(fake_data)

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            initial_number_id = conn.execute("SELECT count(id) FROM gkdb_development.linear_weights_table;").fetchone()[
                0]
        conn.commit()

    for idx in range(len(wv.eigenmode)):
        for f in fields(wv.eigenmode[idx].linear_weights_rotating_frame):
            if isinstance(getattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name), (list, np.ndarray)):
                setattr(wv.eigenmode[idx].linear_weights_rotating_frame, f.name,
                        [])
    gk_id, gk_uuid, species_id, code_id = get_ids_info(connect_db_full)

    ret_wv = None
    with connect_db_full.connection() as conn:
        assert len(species_id) == number_species, \
            f"number of species in the DB {len(species_id)} is not equal to number of species expected"
        with conn.cursor() as cur:
            ret_wv = gkclient._insert_wavevector(cur, ((gk_id,), (code_id,)), (wv,),
                                                 species_id, gk_uuid)

        conn.commit()
    assert ret_wv is True
