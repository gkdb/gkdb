import pytest
import simplejson as js
import xmltodict
import numpy as np
from psycopg import sql
from psycopg.types.json import Jsonb

from unittest.mock import patch
from dataclasses import dataclass

try:
    from gkdb.core.utils import dict2pgjsonb, int2bool, list2pglist, dataclass_to_list, \
        list2pgbytea  # replace with your module name
    from gkdb.core.constants import IMAS_DEFAULT_INT
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from gkdb.core.utils import dict2pgjsonb, int2bool, list2pglist, dataclass_to_list, \
        list2pgbytea  # replace with your module name
    from gkdb.core.constants import IMAS_DEFAULT_INT


def mock_dump(in_dict):
    return '{"person": {"name": "John", "age": "30"}}'


def mock_parse(in_str):
    return {'person': {'name': 'John', 'age': '30'}}


#
# def test_dict2json_with_dict(monkeypatch):
#     #monkeypatch.setattr(js, "dumps", mock_dump)
#     input_dict = {'person': {'name': 'John', 'age': '30'}}
#     assert dict2pgjsonb(input_dict) == Jsonb({"person": {"name": "John", "age": "30"}})
#
#
# def test_dict2json_with_xml_string(monkeypatch):
# #    monkeypatch.setattr(xmltodict, "parse", mock_parse)
# #    monkeypatch.setattr(js, "dumps", mock_dump)
#     input_string = '<person><name>John</name><age>30</age></person>'
#     assert dict2pgjsonb(input_string) == Jsonb({"person": {"name": "John", "age": "30"}})
#

def test_dict2json_with_non_dict_or_xml_string():
    input_string = "not a dict or an xml"
    with pytest.raises(AttributeError):
        dict2pgjsonb(input_string)


def test_int2bool():
    with pytest.raises(ValueError):
        int2bool(None)
    assert int2bool(0) is False, "Expected 'False' for an input of 0"
    with pytest.raises(ValueError):
        int2bool(IMAS_DEFAULT_INT)
    assert int2bool(1) is True, "Expected 'True' for an input of 1"


def test_nparray2pgarray_int():
    int_array = [1, 2, 3]
    result = list2pglist(int_array)
    assert result == [1, 2, 3], "Function did not properly convert simple integer list to PostgreSQL array"


def test_nparray2pgarray_not_array():
    result = list2pglist(1)
    assert result is None, "Function did not properly convert simple integer list to PostgreSQL array"


def test_nparray2pgarray_npint():
    np_array_int = np.array([1, 2, 3])
    result = list2pglist(np_array_int)
    assert result == [1, 2, 3], "Function did not properly convert numpy integer array to PostgreSQL array"


def test_nparray2pgarray_float():
    float_array = [1.0, 2.0, 3.0]
    result = list2pglist(float_array)
    assert result == [1.0, 2.0, 3.0], "Function did not properly convert float list to PostgreSQL array"


def test_nparray2pgarray_complex():
    complex_array = [1 + 2j, 3 + 4j]
    result = list2pglist(complex_array)
    assert result == [np.complex64(complex(1.0, 2.0)), np.complex64(
        complex(3.0, 4.0))], "Function did not properly convert complex list to PostgreSQL array"


@dataclass
class SimpleDataClass:
    attr1: str
    attr2: int
    attr3: list


def test_with_dict():
    with patch.object(sql, 'Identifier') as mock_identifier, patch.object(sql, 'Literal') as mock_literal:
        testing_dict = {'key': 'value', 'another_key': 'another_value'}
        keys, values = dataclass_to_list(testing_dict)
        mock_identifier.assert_any_call('key')
        mock_identifier.assert_any_call('another_key')
        mock_literal.assert_any_call('value')
        mock_literal.assert_any_call('another_value')


def test_with_dataclass():
    with patch.object(sql, 'Identifier') as mock_identifier, patch.object(sql, 'Literal') as mock_literal:
        simpledataclass1 = SimpleDataClass('test', 12, [1, 2])
        keys, values = dataclass_to_list(simpledataclass1)
        mock_identifier.assert_any_call('attr1')
        mock_identifier.assert_any_call('attr2')
        mock_identifier.assert_any_call('attr3')
        mock_literal.assert_any_call('test')
        mock_literal.assert_any_call(12)
        # mock_literal.assert_any_call([1, 2])

        # Ensure that the list argument was converted to a string
        assert isinstance(mock_literal.call_args_list[2][0][0], bytes)

        assert mock_literal.call_args_list[2][0][0] == list2pgbytea([1, 2])


def test_with_non_dict_non_dataclass():
    with pytest.raises(TypeError):
        keys, values = dataclass_to_list(123)


@dataclass
class NestedDataClass:
    attr1: str
    attr2: SimpleDataClass


def test_with_nested_dataclass():
    with patch.object(sql, 'Identifier') as mock_identifier, patch.object(sql, 'Literal') as mock_literal:
        nested_dataclass = NestedDataClass('test', SimpleDataClass('nested_test', 13, [3, 4]))
        keys, values = dataclass_to_list(nested_dataclass)
        mock_identifier.assert_any_call('attr1')
        # attr2 is another dataclass, so its value should not be in the val_list, hence its Identifier should not be in the key_list
        assert 'attr2' not in [call[0][0] for call in mock_identifier.call_args_list]
        mock_literal.assert_any_call('test')
