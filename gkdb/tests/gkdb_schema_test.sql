CREATE SCHEMA IF NOT EXISTS gkdb_development;
set schema 'gkdb_development';

CREATE EXTENSION if not exists pg_uuidv7;

DO $$ BEGIN
    IF to_regtype('complex') IS NULL THEN
        CREATE TYPE  complex AS (
            r float4,
            i float4
        );
    END IF;
END $$;


create table if not exists tag_table
(
    id          serial,
    tagid       varchar(32) not null,
    categorie   varchar(32) not null,
    description text,
    primary key (id),
    unique (tagid, categorie),
    constraint tag_table_categorie_check
        check ((categorie)::text = ANY
               ((ARRAY ['simulation'::character varying, 'experiment'::character varying, 'doi'::character varying, 'conference'::character varying, 'tutorial'::character varying, 'ML_training'::character varying, 'ML_set'::character varying, 'other'::character varying, 'reference_case'::character varying, 'zenodo'::character varying, 'equivalents'::character varying, 'gkw'::character varying, 'gene'::character varying, 'test'::character varying, 'qualikiz'::character varying, 'tglf'::character varying, 'cgyro'::character varying])::text[]))
);

create index if not exists ix_gkdb_development_tag_table_categorie
    on tag_table (categorie);

create table if not exists author_table
(
    id            serial,
    has_ids       boolean                 not null,
    firstname     varchar                 not null,
    lastname      varchar                 not null,
    email         varchar                 not null,
    institute     varchar,
    is_public     boolean                 not null,
    creation_date timestamp default now() not null,
    primary key (id),
    unique (firstname, lastname, email)
);

create table if not exists database_statistics_table
(
    id                serial,
    number_runs       bigint       not null,
    linear_runs       bigint       not null,
    quasi_linear_runs bigint       not null,
    non_linear_runs   bigint       not null,
    cpu_time_str      varchar(256) not null,
    cpu_time          bigint       not null,
    storage           integer      not null,
    storage_str       varchar(16)  not null,
    primary key (id),
    unique (number_runs),
    unique (linear_runs),
    unique (quasi_linear_runs),
    unique (non_linear_runs),
    unique (cpu_time_str),
    unique (cpu_time),
    unique (storage),
    unique (storage_str),
    constraint at_least_one_field_not_null
        check ((number_runs IS NOT NULL) OR (linear_runs IS NOT NULL) OR (quasi_linear_runs IS NOT NULL) OR
               (non_linear_runs IS NOT NULL) OR (cpu_time_str IS NOT NULL) OR (cpu_time IS NOT NULL) OR
               (storage IS NOT NULL) OR (storage_str IS NOT NULL))
);

create table if not exists code_version_table
(
    id                    bigserial,
    description           varchar(512),
    name                  varchar(64) not null,
    commit                varchar(512),
    release_version       varchar(16) not null,
    repository            varchar(512),
    compilation_arguments jsonb,
    type                  integer     not null,
    primary key (id),
    constraint uq_name_release_version_lower
        unique (name, release_version, commit)
);

create index if not exists idx_name_release_version
    on code_version_table (name, release_version, commit);

create table if not exists library_table
(
    id         serial,
    code_id    bigint not null,
    version_id bigint not null,
    parameters jsonb,
    primary key (id),
    foreign key (version_id) references code_version_table
);

create table if not exists gyrokinetics_local_table
(
    id   bigserial,
    uuid uuid default gkdb_development.uuid_generate_v7() not null,
    primary key (id),
    unique (uuid)
);

create table if not exists code_table
(
    id         bigserial,
    gk_id      bigint not null,
    version_id bigint not null,
    library_id integer,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (library_id) references library_table,
    foreign key (version_id) references code_version_table
);

alter table library_table
    add foreign key (code_id) references code_table;

create index if not exists ix_gkdb_development_gyrokinetics_local_table_uuid
    on gyrokinetics_local_table (uuid);

create table if not exists ids_tags_table
(
    gk_id  bigint  not null,
    tag_id integer not null,
    primary key (gk_id, tag_id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (tag_id) references tag_table
);

create table if not exists ids_authors_table
(
    gk_id     bigint  not null,
    author_id integer not null,
    primary key (gk_id, author_id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (author_id) references author_table
);

create table if not exists restrictions_table
(
    id               serial,
    gk_id            bigint                                 not null,
    restriction_type varchar                                not null,
    has_restrictions boolean                                not null,
    time_limited     boolean                                not null,
    end_time         timestamp with time zone default now() not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_restrictions_table_gk_id
    on restrictions_table (gk_id);

create table if not exists ids_simulation_details_table
(
    id                            bigserial,
    gk_id                         bigint  not null,
    uuid                          uuid    not null,
    has_file_storage              boolean not null,
    number_species                integer not null,
    number_wavevectors_lin        integer,
    number_wavevectors_non_lin    integer,
    is_collisionless              boolean not null,
    has_fluxes_1d                 boolean not null,
    has_fluxes_2d                 boolean not null,
    has_fluxes_3d                 boolean not null,
    has_fluxes_4d                 boolean not null,
    has_fluxes_5d                 boolean not null,
    has_moments_1d                boolean not null,
    has_moments_2d                boolean not null,
    has_moments_3d                boolean not null,
    has_moments_4d                boolean not null,
    has_moments_5d                boolean not null,
    has_fields_1d                 boolean not null,
    has_fields_2d                 boolean not null,
    has_fields_3d                 boolean not null,
    has_fields_4d                 boolean not null,
    has_fields_5d                 boolean not null,
    input_hash_full               varchar,
    input_hash_without_parameters varchar,
    has_linear                    boolean not null,
    has_quasi_linear              boolean not null,
    has_non_linear                boolean not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    constraint at_least_one_field_not_null
        check ((uuid IS NOT NULL) OR (has_file_storage IS NOT NULL) OR (number_species IS NOT NULL) OR
               (has_fluxes_1d IS NOT NULL) OR (has_fluxes_2d IS NOT NULL) OR (has_fluxes_3d IS NOT NULL) OR
               (has_fluxes_4d IS NOT NULL) OR (has_fluxes_5d IS NOT NULL) OR (has_moments_1d IS NOT NULL) OR
               (has_moments_2d IS NOT NULL) OR (has_moments_3d IS NOT NULL) OR (has_moments_4d IS NOT NULL) OR
               (has_moments_5d IS NOT NULL) OR (input_hash_full IS NOT NULL) OR
               (input_hash_without_parameters IS NOT NULL) OR (has_linear IS NOT NULL) OR
               (has_quasi_linear IS NOT NULL) OR (has_non_linear IS NOT NULL))
);

create index if not exists ix_gkdb_development_ids_simulation_details_table_gk_id
    on ids_simulation_details_table (gk_id);

create index if not exists ix_gkdb_development_ids_simulation_details_table_uuid
    on ids_simulation_details_table (uuid);

create table if not exists file_storage_table
(
    id          bigserial,
    gk_id       bigint       not null,
    hash        varchar(128) not null,
    filename    varchar(256) not null,
    filetype    varchar(16)  not null,
    storage     varchar(512) not null,
    plugin_id   bigint,
    ids_version varchar(256) not null,
    primary key (id),
    unique (hash, storage),
    unique (hash),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (plugin_id) references code_version_table
);

create index if not exists ix_gkdb_development_file_storage_table_gk_id
    on file_storage_table (gk_id);

create table if not exists flux_surface_table
(
    id                         bigserial,
    gk_id                      bigint not null,
    r_minor_norm               real   not null,
    elongation                 real   not null,
    delongation_dr_minor_norm  real   not null,
    dgeometric_axis_r_dr_minor real   not null,
    dgeometric_axis_z_dr_minor real   not null,
    q                          real   not null,
    magnetic_shear_r_minor     real   not null,
    pressure_gradient_norm     real   not null,
    ip_sign                    real   not null,
    b_field_tor_sign           real   not null,
    shape_coefficients_c       bytea  not null,
    dc_dr_minor_norm           bytea  not null,
    shape_coefficients_s       bytea  not null,
    ds_dr_minor_norm           bytea  not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_flux_surface_table_gk_id
    on flux_surface_table (gk_id);

create table if not exists input_normalizing_table
(
    id          serial,
    gk_id       bigint not null,
    t_e         real,
    n_e         real,
    r           real,
    b_field_tor real,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    constraint at_least_one_quantity_not_null
        check ((t_e IS NOT NULL) OR (n_e IS NOT NULL) OR (r IS NOT NULL) OR (b_field_tor IS NOT NULL))
);

create index if not exists ix_gkdb_development_input_normalizing_table_gk_id
    on input_normalizing_table (gk_id);

create table if not exists input_species_global_table
(
    id                     serial,
    gk_id                  bigint not null,
    beta_reference         real   not null,
    velocity_tor_norm      real   not null,
    debye_length_reference real   not null,
    shearing_rate_norm     real   not null,
    angle_pol              bytea  not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_input_species_global_table_gk_id
    on input_species_global_table (gk_id);

create table if not exists model_table
(
    id                               serial,
    gk_id                            bigint  not null,
    include_coriolis_drift           boolean not null,
    include_centrifugal_effects      boolean not null,
    include_a_field_parallel         boolean not null,
    include_b_field_parallel         boolean not null,
    include_full_curvature_drift     boolean not null,
    collisions_pitch_only            boolean not null,
    collisions_momentum_conservation boolean not null,
    collisions_energy_conservation   boolean not null,
    collisions_finite_larmor_radius  boolean not null,
    adiabatic_electrons              boolean not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_model_table_gk_id
    on model_table (gk_id);

create table if not exists species_table
(
    id                             serial,
    gk_id                          bigint not null,
    charge_norm                    real   not null,
    mass_norm                      real   not null,
    density_norm                   real   not null,
    density_log_gradient_norm      real   not null,
    temperature_norm               real   not null,
    temperature_log_gradient_norm  real   not null,
    velocity_tor_gradient_norm     real   not null,
    potential_energy_norm          bytea  not null,
    potential_energy_gradient_norm bytea  not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_species_table_gk_id
    on species_table (gk_id);

create table if not exists non_linear_table
(
    id                       bigserial,
    gk_id                    bigint  not null,
    binormal_wavevector_norm bytea,
    radial_wavevector_norm   bytea,
    angle_pol                bytea,
    time_norm                bytea,
    time_interval_norm       bytea,
    quasi_linear             boolean not null,
    code_parameters          jsonb,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_non_linear_table_gk_id
    on non_linear_table (gk_id);

create table if not exists linear_table
(
    id                       bigserial,
    gk_id                    bigint not null,
    radial_wavevector_norm   real   not null,
    binormal_wavevector_norm real   not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_linear_table_gk_id
    on linear_table (gk_id);

create table if not exists ids_revision_history_table
(
    id                    serial,
    author_id             integer                 not null,
    simulation_details_id bigint                  not null,
    is_deletion           boolean                 not null,
    is_update             boolean                 not null,
    is_creation           boolean                 not null,
    revision_date         timestamp default now() not null,
    primary key (id),
    foreign key (author_id) references author_table,
    foreign key (simulation_details_id) references ids_simulation_details_table,
    constraint only_one_condition_true
        check (((is_update IS TRUE) AND (NOT is_deletion) AND (NOT is_creation)) OR
               ((is_deletion IS TRUE) AND (NOT is_update) AND (NOT is_creation)) OR
               ((is_creation IS TRUE) AND (NOT is_update) AND (NOT is_deletion)))
);

create index if not exists ix_gkdb_development_ids_revision_history_table_simulati_b844
    on ids_revision_history_table (simulation_details_id);

create index if not exists ix_gkdb_development_ids_revision_history_table_author_id
    on ids_revision_history_table (author_id);

create table if not exists collisions_table
(
    id                  serial,
    collisionality_norm real    not null,
    species_one         integer not null,
    species_two         integer not null,
    primary key (id),
    foreign key (species_one) references species_table,
    foreign key (species_two) references species_table
);

create table if not exists fields_table
(
    id                              bigserial,
    field_type                      varchar(32) not null,
    field_dim                       integer     not null,
    field_value                     bytea       not null,
    gk_id                           bigint      not null,
    non_linear_run_id               bigint      not null,
    phi_potential_perturbed_norm    bytea,
    a_field_parallel_perturbed_norm bytea,
    b_field_parallel_perturbed_norm bytea,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (non_linear_run_id) references non_linear_table
);

create index if not exists ix_gkdb_development_fields_table_gk_id
    on fields_table (gk_id);

create index if not exists ix_gkdb_development_fields_table_non_linear_run_id
    on fields_table (non_linear_run_id);

create table if not exists fluxes_1d_table
(
    id                                          serial,
    gk_id                                       bigint      not null,
    species_id                                  integer,
    non_linear_run_id                           bigint      not null,
    frame                                       varchar(16) not null,
    uuid                                        uuid        not null,
    particles_phi_potential                     real        not null,
    particles_a_field_parallel                  real        not null,
    particles_b_field_parallel                  real        not null,
    energy_phi_potential                        real        not null,
    energy_a_field_parallel                     real        not null,
    energy_b_field_parallel                     real        not null,
    momentum_tor_parallel_phi_potential         real        not null,
    momentum_tor_parallel_a_field_parallel      real        not null,
    momentum_tor_parallel_b_field_parallel      real        not null,
    momentum_tor_perpendicular_phi_potential    real        not null,
    momentum_tor_perpendicular_a_field_parallel real        not null,
    momentum_tor_perpendicular_b_field_parallel real        not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (species_id) references species_table,
    foreign key (non_linear_run_id) references non_linear_table
);

create index if not exists ix_gkdb_development_fluxes_1d_table_non_linear_run_id
    on fluxes_1d_table (non_linear_run_id);

create index if not exists ix_gkdb_development_fluxes_1d_table_uuid
    on fluxes_1d_table (uuid);

create index if not exists ix_gkdb_development_fluxes_1d_table_species_id
    on fluxes_1d_table (species_id);

create index if not exists ix_gkdb_development_fluxes_1d_table_gk_id
    on fluxes_1d_table (gk_id);

create table if not exists fluxes_2d_table
(
    id                                          serial,
    gk_id                                       bigint      not null,
    species_id                                  integer,
    non_linear_run_id                           bigint      not null,
    ftype                                       varchar(16) not null,
    uuid                                        uuid        not null,
    particles_phi_potential                     bytea       not null,
    particles_a_field_parallel                  bytea       not null,
    particles_b_field_parallel                  bytea       not null,
    energy_phi_potential                        bytea       not null,
    energy_a_field_parallel                     bytea       not null,
    energy_b_field_parallel                     bytea       not null,
    momentum_tor_parallel_phi_potential         bytea       not null,
    momentum_tor_parallel_a_field_parallel      bytea       not null,
    momentum_tor_parallel_b_field_parallel      bytea       not null,
    momentum_tor_perpendicular_phi_potential    bytea       not null,
    momentum_tor_perpendicular_a_field_parallel bytea       not null,
    momentum_tor_perpendicular_b_field_parallel bytea       not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (species_id) references species_table,
    foreign key (non_linear_run_id) references non_linear_table
);

create index if not exists ix_gkdb_development_fluxes_2d_table_gk_id
    on fluxes_2d_table (gk_id);

create index if not exists ix_gkdb_development_fluxes_2d_table_non_linear_run_id
    on fluxes_2d_table (non_linear_run_id);

create index if not exists ix_gkdb_development_fluxes_2d_table_uuid
    on fluxes_2d_table (uuid);

create index if not exists ix_gkdb_development_fluxes_2d_table_species_id
    on fluxes_2d_table (species_id);

create table if not exists eigenmode_table
(
    id                    bigserial,
    linear_id             bigint  not null,
    gk_id                 bigint  not null,
    code_parameters       jsonb,
    poloidal_turns        integer not null,
    growth_rate_norm      real,
    frequency_norm        real,
    growth_rate_tolerance real,
    angle_pol             bytea,
    time_norm             bytea,
    initial_value_run     boolean not null,
    primary key (id),
    foreign key (linear_id) references linear_table,
    foreign key (gk_id) references gyrokinetics_local_table
);

create index if not exists ix_gkdb_development_eigenmode_table_growth_rate_norm
    on eigenmode_table (growth_rate_norm);

create index if not exists ix_gkdb_development_eigenmode_table_frequency_norm
    on eigenmode_table (frequency_norm);

create index if not exists ix_gkdb_development_eigenmode_table_linear_id
    on eigenmode_table (linear_id);

create index if not exists ix_gkdb_development_eigenmode_table_gk_id
    on eigenmode_table (gk_id);

create table if not exists eigenmode_fields_table
(
    id                                bigserial,
    gk_id                             bigint not null,
    eigenmode_id                      bigint not null,
    phi_potential_perturbed_weight    bytea  not null,
    phi_potential_perturbed_parity    bytea  not null,
    a_field_parallel_perturbed_weight bytea  not null,
    a_field_parallel_perturbed_parity bytea  not null,
    b_field_parallel_perturbed_weight bytea  not null,
    b_field_parallel_perturbed_parity bytea  not null,
    phi_potential_perturbed_norm      bytea  not null,
    a_field_parallel_perturbed_norm   bytea  not null,
    b_field_parallel_perturbed_norm   bytea  not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (eigenmode_id) references eigenmode_table
);

create index if not exists ix_gkdb_development_eigenmode_fields_table_gk_id
    on eigenmode_fields_table (gk_id);

create table if not exists linear_weights_table
(
    id                                          serial,
    gk_id                                       bigint      not null,
    species_id                                  integer,
    eigenmode_id                                bigint,
    frame                                       varchar(16) not null,
    particles_phi_potential                     real,
    particles_a_field_parallel                  real,
    particles_b_field_parallel                  real,
    energy_phi_potential                        real,
    energy_a_field_parallel                     real,
    energy_b_field_parallel                     real,
    momentum_tor_parallel_phi_potential         real,
    momentum_tor_parallel_a_field_parallel      real,
    momentum_tor_parallel_b_field_parallel      real,
    momentum_tor_perpendicular_phi_potential    real,
    momentum_tor_perpendicular_a_field_parallel real,
    momentum_tor_perpendicular_b_field_parallel real,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (species_id) references species_table,
    foreign key (eigenmode_id) references eigenmode_table,
    constraint at_least_one_weight_not_null
        check ((particles_phi_potential IS NOT NULL) OR (particles_a_field_parallel IS NOT NULL) OR
               (particles_b_field_parallel IS NOT NULL) OR (energy_phi_potential IS NOT NULL) OR
               (energy_a_field_parallel IS NOT NULL) OR (energy_b_field_parallel IS NOT NULL) OR
               (momentum_tor_parallel_phi_potential IS NOT NULL) OR
               (momentum_tor_parallel_a_field_parallel IS NOT NULL) OR
               (momentum_tor_parallel_b_field_parallel IS NOT NULL) OR
               (momentum_tor_perpendicular_phi_potential IS NOT NULL) OR
               (momentum_tor_perpendicular_a_field_parallel IS NOT NULL) OR
               (momentum_tor_perpendicular_b_field_parallel IS NOT NULL))
);

create index if not exists ix_gkdb_development_linear_weights_table_eigenmode_id
    on linear_weights_table (eigenmode_id);

create index if not exists ix_gkdb_development_linear_weights_table_species_id
    on linear_weights_table (species_id);

create index if not exists ix_gkdb_development_linear_weights_table_gk_id
    on linear_weights_table (gk_id);

create table if not exists moments_linear_table
(
    id                              serial,
    gk_id                           bigint      not null,
    species_id                      integer,
    eigenmode_id                    bigint,
    mtype                           varchar(16) not null,
    uuid                            uuid        not null,
    norm                            boolean,
    density                         bytea       not null,
    j_parallel                      bytea       not null,
    pressure_parallel               bytea       not null,
    pressure_perpendicular          bytea       not null,
    heat_flux_parallel              bytea       not null,
    v_parallel_energy_perpendicular bytea       not null,
    v_perpendicular_square_energy   bytea       not null,
    primary key (id),
    foreign key (gk_id) references gyrokinetics_local_table,
    foreign key (species_id) references species_table,
    foreign key (eigenmode_id) references eigenmode_table
);

create index if not exists ix_gkdb_development_moments_linear_table_eigenmode_id
    on moments_linear_table (eigenmode_id);

create index if not exists ix_gkdb_development_moments_linear_table_uuid
    on moments_linear_table (uuid);

create index if not exists ix_gkdb_development_moments_linear_table_species_id
    on moments_linear_table (species_id);



create or replace function get_table_counts_and_size(p_schema text)
    returns TABLE(table_name text, row_count bigint, table_size text)
    language plpgsql
as
$$
DECLARE
    _tbl text;
BEGIN
    FOR _tbl IN
      SELECT tablename
      FROM   pg_tables
      WHERE  schemaname = p_schema
    LOOP
        RETURN QUERY EXECUTE format(
            'SELECT %L::text, count(*), pg_size_pretty(pg_total_relation_size(%L::regclass)) FROM %I.%I',
            _tbl, _tbl, p_schema, _tbl);
    END LOOP;
END;
$$;


CREATE OR REPLACE FUNCTION count_rows_in_tables(schema_name TEXT)
RETURNS TABLE(local_table_name TEXT, row_count BIGINT) AS $$
BEGIN
    FOR local_table_name IN
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema = schema_name AND table_type = 'BASE TABLE'
    LOOP
        EXECUTE format('SELECT COUNT(*) FROM %I.%I', schema_name, local_table_name) INTO row_count;
        RETURN NEXT;
    END LOOP;
END;
$$ LANGUAGE plpgsql;