import pytest

try:
    from ids_samples import Tag, TEST_DEFAULT_DICT
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from ids_samples import Tag, TEST_DEFAULT_DICT

from gkdb.ids_checks import unit_checks as unit_checks
from idspy_dictionaries import ids_gyrokinetics_local as gkids
from idspy_toolkit.exceptions import IdsTypeError, IdsRequiredMissing


def test_check_ids_default():
    newids = gkids.GyrokineticsLocal()
    assert len(unit_checks.get_not_default_ids_members(newids)) == 0

    newids = gkids.Model()
    assert len(unit_checks.get_not_default_ids_members(newids)) == 0

    newids = gkids.GyrokineticsLocal(time=2)
    assert len(unit_checks.get_not_default_ids_members(newids)) == 1


def test_check_ids_has_none():
    with pytest.raises(IdsTypeError):
        unit_checks.check_no_none([3, None])

    newids = gkids.GyrokineticsLocal()
    assert unit_checks.check_no_none(newids) is True

    newids = gkids.GyrokineticsLocal(time=None)
    assert unit_checks.check_no_none(newids) is False

    newids = gkids.GyrokineticsLocal(time=[1, None, 2])
    assert unit_checks.check_no_none(newids) is False

    newids = gkids.GyrokineticsLocal(time=[1, 3, 2])
    assert unit_checks.check_no_none(newids) is True


def test_check_ids_array_size():
    newids = gkids.GyrokineticsLocal()
    assert unit_checks.check_array_size(newids, "time", 0) is True

    newids = gkids.GyrokineticsLocal(time=1)
    assert unit_checks.check_array_size(newids, "time", 0) is False

    newids = gkids.GyrokineticsLocal(time=[1, 2, 3])
    assert unit_checks.check_array_size(newids, "time", 0) is False

    newids = gkids.GyrokineticsLocal(time=[1, 2, 3])
    assert unit_checks.check_array_size(newids, "time", 3) is True
    assert unit_checks.check_array_size(newids, "time", (3,)) is True
    assert unit_checks.check_array_size(newids, "time", (4,)) is False

    newids = gkids.GyrokineticsLocal(time=[[1, 2, ], [3, 4]])
    assert unit_checks.check_array_size(newids, "time", (4,)) is False
    assert unit_checks.check_array_size(newids, "time", 4) is False


def test_contains_all_required():
    # Test case where all required elements are present
    required_list = [1, 2, 3]
    given_list = [1, 2, 3, 4, 5]
    assert unit_checks.contains_all_required(required_list, given_list) == (True, [])

    # Test case where some required elements are missing
    required_list = [1, 2, 3, 4]
    given_list = [1, 2, 3]
    assert unit_checks.contains_all_required(required_list, given_list) == (False, [4])

    # Test case where all required elements are missing
    required_list = [1, 2, 3, 4]
    given_list = [5, 6, 7]
    assert unit_checks.contains_all_required(required_list, given_list) == (False,
                                                                            [1, 2, 3, 4])

    # Test case where required list is empty
    required_list = []
    given_list = [1, 2, 3]
    assert unit_checks.contains_all_required(required_list, given_list) == (True, [])

    # Test case where given list is empty
    required_list = [1, 2, 3]
    given_list = []
    assert unit_checks.contains_all_required(required_list, given_list) == (False,
                                                                            [1, 2, 3])


def test_check_ids_pipeline():
    with pytest.raises(IdsTypeError):
        unit_checks.ids_check_pipeline(3) is False

    newids = gkids.GyrokineticsLocal()
    assert unit_checks.ids_check_pipeline(newids) is False

    newids = gkids.GyrokineticsLocal(time=None)
    with pytest.raises(IdsTypeError):
        unit_checks.ids_check_pipeline(newids)

    newids = gkids.GyrokineticsLocal(time=None)
    with pytest.raises(AttributeError):
        unit_checks.ids_check_pipeline(newids, required_members="time")


    newids = gkids.GyrokineticsLocal(time=None)
    with pytest.raises(AttributeError):
        unit_checks.ids_check_pipeline(newids, require_all=False, required_members="time")

    with pytest.raises(IdsTypeError):
        unit_checks.ids_check_pipeline(newids, require_all=False, required_members=("time",))

    newids = gkids.GyrokineticsLocal(time=[2, 3])
    with pytest.raises(AttributeError):
        unit_checks.ids_check_pipeline(newids, require_all=False, required_members="time")
    assert unit_checks.ids_check_pipeline(newids, require_all=False, required_members=("time",)) is True

    with pytest.raises(IdsRequiredMissing):
        unit_checks.ids_check_pipeline(newids,require_all=False,  required_members=("time", "code"))

    newids = gkids.GyrokineticsLocal()
    try:
        unit_checks.ids_check_pipeline(newids,require_all=False,  required_members=("time",))
    except AttributeError as exc:
        assert False, f"'ids_check_pipeline' raised an exception {exc}"

    assert unit_checks.ids_check_pipeline(newids, require_all=False, required_members=("time",)) is False


