import psycopg
import pytest
from pytest_postgresql import factories

try:
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
    from ids_samples import Tag, TEST_DEFAULT_DICT
    import gkdb.client.client as gkclient
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
    import gkdb.client.client as gkclient
    from ids_samples import Tag, TEST_DEFAULT_DICT

from gkdb.core.decorators import check_foreign_key_exists


def test_postgres_docker(postgresql_mok):
    """Run test."""
    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(*) FROM gkdb_development.tag_table;").fetchone()[0]
    # number = cur.execute("SELECT * FROM pg_catalog.pg_tables;")
    # number = [x for x in number.fetchall()]
    postgresql_mok.commit()
    cur.close()
    assert number == 2


def test_insert_simple(postgresql_mok):
    cur = postgresql_mok.cursor()
    test_tag = Tag(tagid="#123456", categorie="test", description="why")
    request = query_insert_simple_ids(test_tag, "tag_table", "gkdb_development", None)
    cur.execute(request)
    check = cur.execute("SELECT (tagid, categorie) FROM gkdb_development.tag_table;").fetchall()
    check_id = [x[0][0] for x in check]

    check_cat = [x[0][1] for x in check]
    postgresql_mok.commit()
    cur.close()
    assert check_id == ['#1234', '#5678', test_tag.tagid, ]
    assert check_cat == ['test', 'test', test_tag.categorie, ]


def test_manytomany_tag(postgresql_mok):
    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(*) FROM gkdb_development.tag_table;").fetchone()[0]
    # number = cur.execute("SELECT * FROM pg_catalog.pg_tables;")
    # number = [x for x in number.fetchall()]
    postgresql_mok.commit()
    cur.close()
    assert number == 2

    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table;").fetchone()[0]
    pk_list = cur.execute("SELECT id FROM gkdb_development.gyrokinetics_local_table;").fetchall()
    pk_list = [x[0] for x in pk_list]
    postgresql_mok.commit()
    cur.close()
    assert number == 3
    assert pk_list == [1, 2, 3]


def test_insert_tag_same(postgresql_mok):
    new_valid_tag = Tag("#abcdef", "test", "an UT and useless description")
    cur = postgresql_mok.cursor()
    ret = gkclient._insert_tag(cur, (1,), new_valid_tag)
    cur.close()
    assert ret[0] == 1
    assert len(ret[1]) == 1
    assert ret[1][0] >= 1
    old_tag = ret[1][0]
    cur.connection.commit()

    new_duplicate_tag = Tag("#abcdef", "test", "same tag/cat")
    cur = postgresql_mok.cursor()
    ret = gkclient._insert_tag(cur, (1,), new_duplicate_tag)
    cur.close()
    assert ret[0] == 1
    assert ret[1][0] == old_tag, str(ret)


def test_insert_tag_multiple(postgresql_mok):
    number_of_tag = 5
    cur = postgresql_mok.cursor()
    new_valid_tag = Tag("#abcdef", "test", "an UT and useless description")
    for i in range(number_of_tag):
        new_valid_tag.tagid = new_valid_tag.tagid + str(i)
        ret = gkclient._insert_tag(cur,(1,), new_valid_tag)
        assert ret[0] > 0
    postgresql_mok.commit()
    cur.close()

    # check last tag is linked only to one gk.id in the jointtable
    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table AS gk "
                         " INNER JOIN gkdb_development.ids_tags_table as link "
                         " ON gk.id = link.gk_id"
                         " WHERE gk.id = %s"
                         " ;", (1,)).fetchone()[0]
    postgresql_mok.commit()
    cur.close()
    assert number == number_of_tag


def test_insert_tag_wrong_type(postgresql_mok):
    new_invalid_tag = Tag("#abcdef", "invalid_tag", "an UT and useless description")
    cur = postgresql_mok.cursor()
    ret = gkclient._insert_tag(cur,(1,), new_invalid_tag)
    postgresql_mok.commit()
    cur.close()
    assert ret[0] == -1
    assert ret[1] is None


def test_insert_tag(postgresql_mok):
    cur = postgresql_mok.cursor()
    number_entry_start = cur.execute("SELECT count(gk_id) FROM gkdb_development.ids_tags_table ;").fetchone()[0]
    postgresql_mok.commit()
    cur.close()

    new_valid_tag = Tag("#abcdef", "test", "an UT and useless description")
    cur = postgresql_mok.cursor()
    ret = gkclient._insert_tag(cur, (1,),new_valid_tag)
    postgresql_mok.commit()
    cur.close()
    assert ret is not None
    assert len(ret) == 2
    assert ret[0] == 1
    assert len(ret[1]) > 0

    cur = postgresql_mok.cursor()
    number_entry_end = cur.execute("SELECT count(gk_id) FROM gkdb_development.ids_tags_table ;").fetchone()[0]
    postgresql_mok.commit()
    cur.close()
    assert number_entry_end - number_entry_start == 1

    cur = postgresql_mok.cursor()
    # check total number of tags
    number = cur.execute("SELECT count(*) FROM gkdb_development.tag_table;").fetchone()[0]
    postgresql_mok.commit()
    cur.close()
    assert number == 3

    # check last tag is linked only to one gk.id in the jointtable
    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(*) FROM gkdb_development.tag_table AS tag "
                         " INNER JOIN gkdb_development.ids_tags_table as link "
                         " ON tag.id = link.tag_id"
                         " WHERE tag.id = %s"
                         " ;", (ret[1][0],)).fetchone()[0]
    postgresql_mok.commit()
    cur.close()
    assert number == 1

    # check number of elements in the joint table
    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(gk_id) FROM gkdb_development.ids_tags_table ;").fetchone()[0]
    postgresql_mok.commit()
    cur.close()
    assert number == 1

    cur = postgresql_mok.cursor()
    number = cur.execute("SELECT count(tag_id) FROM gkdb_development.ids_tags_table ;").fetchone()[0]
    postgresql_mok.commit()
    cur.close()
    assert number == 1


# def test_insert_code(postgresql_mok):
#     code = TEST_DEFAULT_DICT["code"]
#     cur = postgresql_mok.cursor()
#     gkclient._insert_code(cur, (1,), code)
#     number = cur.execute("SELECT count(*) FROM gkdb_development.code_table;").fetchone()[0]
#     postgresql_mok.commit()
#     cur.close()
#     assert number == 1
#
#     # check FK relation
#     cur = postgresql_mok.cursor()
#     number = cur.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table AS gk "
#                          "INNER JOIN gkdb_development.code_table AS code "
#                          "ON gk.id = code.gk_id "
#                          "WHERE code.gk_id=1;").fetchone()[0]
#     postgresql_mok.commit()
#     cur.close()
#     assert number == 1
#
#     # check number of libraries
#     # check FK relation
#     cur = postgresql_mok.cursor()
#     number = cur.execute("SELECT count(*) FROM gkdb_development.library_table AS lib "
#                          "INNER JOIN gkdb_development.code_table AS code "
#                          "ON lib.code_id = code.id "
#                          "WHERE code.id=1;").fetchone()[0]
#     postgresql_mok.commit()
#     cur.close()
#     assert number == 1


def test_check_foreign_key(postgresql_mok):
    cur = postgresql_mok.cursor()

    pk_list = cur.execute("SELECT id FROM gkdb_development.gyrokinetics_local_table;").fetchall()
    pk_list = [x[0] for x in pk_list]

    assert check_foreign_key_exists(cur, 1,
                                    "id",
                                    "gyrokinetics_local_table",
                                    "gkdb_development") is True
    assert check_foreign_key_exists(cur, [1, ],
                                    "id",
                                    "gyrokinetics_local_table",
                                    "gkdb_development") is True
    assert check_foreign_key_exists(cur, 2,
                                    "id",
                                    "gyrokinetics_local_table",
                                    "gkdb_development") is True
    assert check_foreign_key_exists(cur, pk_list,
                                    "id",
                                    "gyrokinetics_local_table",
                                    "gkdb_development") is True

    assert check_foreign_key_exists(cur, pk_list[::2],
                                    "id",
                                    "gyrokinetics_local_table",
                                    "gkdb_development") is True

    assert check_foreign_key_exists(cur, pk_list + [max(pk_list) + 1, ],
                                    "id",
                                    "gyrokinetics_local_table",
                                    "gkdb_development") is False
