import pytest

try:
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids, query_insert_eigenmode
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids, query_insert_eigenmode

import gkdb.core.idstogkdb_insert as gkclient
import gkdb.tools.fakedata as fdata


def test_postgres_docker_init_size(connect_db_full):
    """Run test."""
    with connect_db_full.connection() as conn:
        # cur = postgresql_mok_full.cursor()
        number = conn.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table;").fetchone()[0]
        # number = cur.execute("SELECT * FROM pg_catalog.pg_tables;")
        # number = [x for x in number.fetchall()]
        conn.commit()
    # cur.close()
    assert number == 3



def test_insert_species(connect_db_full):
    with connect_db_full.connection() as conn:
        initial_species_number = conn.execute("SELECT count(*) FROM gkdb_development.species_table;").fetchone()[0]
        conn.commit()

    fake_data = fdata.generate_fake_params()
    species = fdata.generate_fake_species()
    species_2 = fdata.generate_fake_species()
    with pytest.raises(AttributeError):
        with connect_db_full.connection() as conn:
            with conn.cursor() as cur:
                ret = gkclient._insert_species(cur, (1,), species)
            conn.commit()


    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            ret = gkclient._insert_species(cur, (1,), (species,))
        conn.commit()
    assert ret[0] == 1
    assert len(ret[1]) == 1

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            ret = gkclient._insert_species(cur, (1,), (species_2,))

    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(*) FROM gkdb_development.species_table;").fetchone()[0]
        conn.commit()
    assert number == initial_species_number+2

    # check FK relation
    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table AS gk "
                              "INNER JOIN gkdb_development.species_table AS species "
                              "ON gk.id = species.gk_id "
                              "WHERE gk.id=1;").fetchone()[0]
        conn.commit()

    assert number == initial_species_number+2
    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table AS gk "
                              "INNER JOIN gkdb_development.species_table AS specie "
                              "ON gk.id = specie.gk_id "
                              "WHERE specie.gk_id=1;").fetchone()[0]
        conn.commit()
    assert number == initial_species_number+2


def test_insert_eigenmode(connect_db_full):
    # get number of species for first gk entry (gk_id==1)
    with connect_db_full.connection() as conn:
        number_species = conn.execute("SELECT count(*) FROM gkdb_development.gyrokinetics_local_table AS gk "
                              "INNER JOIN gkdb_development.species_table AS species "
                              "ON gk.id = species.gk_id "
                              "WHERE gk.id=1;").fetchone()[0]
        conn.commit()

    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = number_species
    eig = fdata.generate_fake_eigenmode(fake_data)
    with connect_db_full.connection() as conn:
        number_eig = conn.execute("SELECT count(*) FROM gkdb_development.eigenmode_table;").fetchone()[0]
        conn.commit()
    assert number_eig == 0, "DB should have no eigenmode"
    with connect_db_full.connection() as conn:
        number_wv = conn.execute("SELECT count(*) FROM gkdb_development.linear_table;").fetchone()[0]
        conn.commit()

    with pytest.raises(ValueError):
        with connect_db_full.connection() as conn:
            query_eig = query_insert_eigenmode(eig, None, schema="gkdb_development")
            conn.execute(query_eig)
            conn.commit()

    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(*) FROM gkdb_development.eigenmode_table;").fetchone()[0]
        conn.commit()
    assert number == 0

    wavev = fdata.generate_fake_wavevector(fake_data)
    basecode = fdata.generate_fake_code(fake_params=fake_data)

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            code_id = gkclient._insert_code(cur, (1,), basecode)
            assert code_id is not None

            gk_id, gk_uuid = conn.execute("SELECT id,uuid FROM gkdb_development.gyrokinetics_local_table LIMIT 1;").fetchone()

            species_id = conn.execute("SELECT id FROM gkdb_development.species_table AS s WHERE s.gk_id=%s;",
                                      (str(gk_id),)).fetchall()
            species_id = [x[0] for x in species_id]
            ret_val = gkclient._insert_wavevector(cur,
                                                            ((1,), (code_id,)),
                                                            (wavev,),
                                                            species_id,
                                                            gk_uuid)
        conn.commit()
    assert ret_val is True

    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(*) FROM gkdb_development.linear_table;").fetchone()[0]
        conn.commit()

    assert number == 1+number_wv

    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(*) FROM gkdb_development.eigenmode_table;").fetchone()[0]
        conn.commit()
    assert number == number_eig + fake_data.neigenmode

    # with connect_db_full.connection() as conn:
    #     number = conn.execute("SELECT count(id) FROM gkdb_development.code_version_table").fetchone()[0]
    #     conn.commit()
    # assert number == 1




# def test_insert_tag(connect_db_full):
#     fake_data = fdata.generate_fake_params(12)
#     fdata.generate_fake_lib()
#     code_ids = fdata.generate_fake_code(fake_data)
#     with connect_db_full.connection() as conn:
#         with conn.cursor() as cur:
#             id_code = gkclient._insert_code(cur, (1,), code_ids)
#         conn.commit()
#     assert id_code is not None
#     assert id_code >= 1



# def test_insert_ids_properties(connect_db_full):
#
#     fake_data = fdata.generate_fake_params(12)
#     prop_ids = fdata.generate_fake_ids_properties()
#     with connect_db_full.connection() as conn:
#         with conn.cursor() as cur:
#             id_ids = gkclient._insert_ids_properties(cur, (1,), prop_ids)
#         conn.commit()
#     assert id_ids is not None
#     assert id_ids >= 1


def test_insert_flux_surfaces(connect_db_full):
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = 2
    fs_ids = fdata.generate_fake_flux_surface()
    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            id_fs = gkclient._insert_flux_surface(cur, (1,), fs_ids)
        conn.commit()
    assert id_fs is not None
    assert id_fs == 1


def test_insert_collisions(connect_db_full):
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = 5
    ids_species = [fdata.generate_fake_species() for _ in range(fake_data.nspecies)]
    coll_ids = fdata.generate_fake_collision(fake_data.nspecies)
    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            ret = gkclient._insert_species(cur, (1,), ids_species)
        conn.commit()
    assert len(ret[1]) == 5

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            id_collision = gkclient._insert_collisions(cur, ( ret[1], ), coll_ids)
        conn.commit()
    assert id_collision is not None
    assert id_collision is True

    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(id) FROM gkdb_development.collisions_table as c").fetchone()[0]
        conn.commit()
    assert int(number) == len(ids_species) ** 2

def test_insert_collisions_single(connect_db_full):
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = 1
    ids_species = [fdata.generate_fake_species() for _ in range(fake_data.nspecies)]
    coll_ids = fdata.generate_fake_collision(fake_data.nspecies)
    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            ret = gkclient._insert_species(cur, (1,), ids_species)
        conn.commit()
    assert len(ret[1]) == 1

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            id_collision = gkclient._insert_collisions(cur, ( ret[1], ), coll_ids)
        conn.commit()
    assert id_collision is not None
    assert id_collision is True

    with connect_db_full.connection() as conn:
        number = conn.execute("SELECT count(id) FROM gkdb_development.collisions_table as c").fetchone()[0]
        conn.commit()
    assert int(number) == len(ids_species) ** 2