import pytest
from pytest_postgresql import factories
from psycopg_pool import ConnectionPool
from psycopg import sql
import os.path as opath
import logging
try:
    from gkdb.config import DBConfig
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from gkdb.config import DBConfig

from gkdb.core.psycopg_adapters import complex_type_register

postgresql_in_docker = factories.postgresql_noproc(user = DBConfig().user, password=DBConfig().password)
postgresql_mok = factories.postgresql("postgresql_in_docker",
                                       dbname="gkdb_1", load=["./gkdb_schema_test.sql",
                                                              "./gkdb_init_values.sql", ])
postgresql_mok_full = factories.postgresql("postgresql_in_docker", dbname="gkdb_2",
                                           load=["./gkdb_schema_test.sql",
                                                 "./gkdb_init_values.sql", ])


def load_sql(sql_filename: str, conn) -> None:
    """Database loader for sql files."""
    if not opath.isfile(sql_filename):
        if "tests" not in sql_filename:
            sql_filename = opath.join("tests", sql_filename)
    with conn.cursor() as cur:
        try:
            cur.execute(open(sql_filename, "r").read())
        except FileNotFoundError:
            import os
            raise FileNotFoundError(f"File not found : {os.getcwd()}/{sql_filename}")
           # raise
    conn.commit()

@pytest.fixture
def postgresql_mok_func():
    return postgresql_mok

@pytest.fixture
def postgresql_mok_full_func():
    return postgresql_mok_full

@pytest.fixture(scope="function")
def connect_db_full():
    dbinfo = DBConfig(database="gkdb_tests", host="localhost")
    conninfo = f"postgresql://{dbinfo.user}:{dbinfo.password}@{dbinfo.host}:{dbinfo.port}/{dbinfo.database}"
    logging.warning("create current test database")
    pool = ConnectionPool(conninfo, configure=complex_type_register, name="pool_test", open=False)
    #pool = ConnectionPool(conninfo,  name="pool_test", open=False)
    try:
        pool.open(wait=True)
    except :
        raise TimeoutError(f"invalid open connection {conninfo}")
    with pool.connection() as conn:
        conn.autocommit = True
        load_sql("gkdb_schema_test.sql", conn)
        load_sql("reset_test_complete_db.sql", conn)
        load_sql("gkdb_init_values.sql", conn)
    print("data written into DB")
    yield pool
    logging.warning("delete current test database")
    with pool.connection() as conn:
        conn.autocommit = True
        load_sql("reset_test_complete_db.sql", conn)


@pytest.fixture(scope="session")
def connect_db_light():
    dbinfo = DBConfig(database="gkdb_tests")
    logging.warn("create current test database light")
    conninfo = f"postgresql://{dbinfo.user}:{dbinfo.password}@{dbinfo.host}:{dbinfo.port}/{dbinfo.database}"
    pool = ConnectionPool(conninfo, configure=complex_type_register, open=True)
    with pool.connection() as conn:
        load_sql("gkdb_schema_test.sql", conn)
        load_sql("gkdb_init_values.sql", conn)
    yield pool
    logging.warn("delete current test database")
    with pool.connection() as conn:
        conn.autocommit = True
        with conn.cursor() as cur:
            cur.execute("DROP DATABASE gkdb_tests; CREATE DATABASE gkdb_tests")
            cur.execute("CREATE TYPE complex AS ( r float4, i float4);")
        #cur.commit()
    pool.close()