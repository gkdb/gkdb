import psycopg
import pytest
from pytest_postgresql import factories

try:
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
    from ids_samples import Tag, TEST_DEFAULT_DICT
    import gkdb.client.client as gkclient
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from gkdb.core.idstogkdb_queries import query_insert_simple_ids
    import gkdb.client.client as gkclient
    from ids_samples import Tag, TEST_DEFAULT_DICT

import gkdb.core.idstogkdb_insert as gkclient
import gkdb.tools.fakedata as fdata


def test_insert_code_simple(connect_db_full):
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = 2
    code_ids = fdata.generate_fake_code(fake_data)
    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            id_code = gkclient._insert_code(cur, (1,), code_ids)
        conn.commit()
    assert id_code is not None
    assert id_code >= 1

def test_insert_code_same(connect_db_full):
    fake_data = fdata.generate_fake_params(12)
    fake_data.nspecies = 2
    code_ids = fdata.generate_fake_code(fake_data)
    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            id_code = gkclient._insert_code(cur, (1,), code_ids)
        conn.commit()

    with connect_db_full.connection() as conn:
        list_code = conn.execute("SELECT id, name, commit FROM gkdb_development.code_version_table where type=0 and name=%s and commit=%s;",
                                 (code_ids.name, code_ids.commit)).fetchall()
        conn.commit()
    assert len(list_code) == 1

    with connect_db_full.connection() as conn:
        list_code = conn.execute("SELECT id, version_id FROM gkdb_development.code_table where version_id=%s;",
                                 (list_code[0][0],)).fetchall()
        conn.commit()

    assert len(list_code) == 1
    real_code_id = list_code[0][0]
    assert id_code == real_code_id

    with connect_db_full.connection() as conn:
        with conn.cursor() as cur:
            id_code_new = gkclient._insert_code(cur, (2,), code_ids)
        conn.commit()

    with connect_db_full.connection() as conn:
        list_code = conn.execute("SELECT name, commit FROM gkdb_development.code_version_table where type=0 and name=%s and commit=%s;", (code_ids.name, code_ids.commit)).fetchall()
        conn.commit()

    assert len(list_code) == 1


    assert id_code_new is not None
    assert id_code != id_code_new