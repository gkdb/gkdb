import dataclasses

import pytest

try:
    from ids_samples import Tag, TEST_DEFAULT_DICT
except ModuleNotFoundError:
    import sys

    sys.path.append('../')
    from ids_samples import Tag, TEST_DEFAULT_DICT

from gkdb.ids_checks import gyrokinetics_ids_check as ids_check
from idspy_dictionaries import ids_gyrokinetics_local as gkids
from idspy_toolkit.exceptions import IdsTypeError, IdsRequiredMissing, IdsForbiddenValue, IdsDimensionError
from gkdb.tools import fakedata as fakedata


def test_code_ids():
    ids_code = gkids.Code()
    assert ids_check.check_code(ids_code) is False

    ids_code = gkids.Code(name="toto")
    with pytest.raises(IdsRequiredMissing):
        ids_check.check_code(ids_code)

    ids_code = gkids.Code(name="toto", commit="1234")
    assert ids_check.check_code(ids_code) is True

    ids_code = gkids.Code(commit="1234")
    with pytest.raises(IdsRequiredMissing):
        ids_check.check_code(ids_code)


def test_flux_surface_check():
    ids_fs = gkids.FluxSurface()
    assert ids_check.check_flux_surface(ids_fs) is False

    fake_fs = fakedata.generate_fake_flux_surface()
    assert ids_check.check_flux_surface(fake_fs) is True

    fake_fs.shape_coefficients_c = fake_fs.shape_coefficients_c[:-1]
    assert ids_check.check_flux_surface(fake_fs) is False

    fake_fs = fakedata.generate_fake_flux_surface()
    fake_fs.shape_coefficients_s = fake_fs.shape_coefficients_s[:-2]
    assert ids_check.check_flux_surface(fake_fs) is False

# def gyrokinetic_ids_check():
#     raise NotImplementedError
#
def test_model_check():
    ids_model = gkids.Model()
    assert ids_check.check_model(ids_model) is False

    ids_model = gkids.Model(include_b_field_parallel=False )
    with pytest.raises(IdsRequiredMissing):
        ids_check.check_model(ids_model)

    ids_model = fakedata.generate_fake_model()
    assert ids_check.check_model(ids_model) is True

def test_species_check():
    ids_species = gkids.Species()
    assert ids_check.check_species(ids_species) is False

    ids_species = gkids.Species(charge_norm=2)
    with pytest.raises(IdsRequiredMissing):
        ids_check.check_species(ids_species)

    ids_species = fakedata.generate_fake_species()
    assert ids_check.check_species(ids_species) is True


def test_linear_check():
    ids_linear = gkids.GyrokineticsLinear()
    assert ids_check.check_linear_structure(ids_linear, 0) == (False, None, None)

    fake_params = fakedata.generate_fake_params(2)

    ids_linear = gkids.GyrokineticsLinear(wavevector=[fakedata.generate_fake_wavevector(fake_params)
                                                      for _ in range(2)])
    with pytest.raises(IdsForbiddenValue):
        ids_check.check_linear_structure(ids_linear, 0)
    assert ids_check.check_linear_structure(ids_linear, fake_params.nspecies) == (True, True, True)

    with pytest.raises(IdsDimensionError):
        ids_check.check_linear_structure(ids_linear, fake_params.nspecies+1)

    ids_linear = gkids.GyrokineticsLinear(wavevector = None)

    with pytest.raises(IdsTypeError):
        ids_check.check_linear_structure(ids_linear, 1)

def test_non_linear_check_default():
    ids_non_linear = gkids.GyrokineticsNonLinear()
    assert ids_check.check_non_linear_structure(ids_non_linear, 0) is False

def test_non_linear_check():
    fake_params = fakedata.generate_fake_params(2)
    ids_non_linear = fakedata.generate_fake_non_linear(fake_params)
    assert ids_non_linear.code is not None
    assert getattr(ids_non_linear, 'code') is not None
    assert dataclasses.is_dataclass(getattr(ids_non_linear, 'code')) is True
    assert ids_check.check_non_linear_structure(ids_non_linear, fake_params.nspecies) is True

    with pytest.raises(IdsForbiddenValue):

        ids_check.check_non_linear_structure(ids_non_linear, 0)


    with pytest.raises(IdsDimensionError):
        ids_check.check_non_linear_structure(ids_non_linear, fake_params.nspecies+1)

    ids_non_linear = gkids.GyrokineticsNonLinear(angle_pol = None)

    with pytest.raises(IdsTypeError):
        ids_check.check_non_linear_structure(ids_non_linear, 1)
