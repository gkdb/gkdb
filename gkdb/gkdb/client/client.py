from __future__ import annotations

# for line_profiler, might be removed later
import builtins

try:
    profile = builtins.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func):
        return func

from typing import Type, Any, Optional

import psycopg
import numpy as np
from psycopg_pool import ConnectionPool
from psycopg import sql
from idspy_dictionaries import ids_gyrokinetics_local

from gkdb.config import DBConfig
import gkdb.core.idstogkdb_queries as ids2gkdb
from gkdb.core.utils import dict2pgjsonb, int2bool, list2pgbytea, shorten_imas_default, compressed_nparray
from gkdb.core.internal_dataclasses import ModifiedFluxes1D, ModifiedFluxes2D, ExtendedMomentsLinear
from gkdb.core.psycopg_adapters import complex_type_register
from gkdb.core.utils import format_value_pg
from gkdb.core.constants_gkdb import (COLUMNS_EIGENMODE_TABLE, COLUMNS_FLUXES_TABLE,
                                      COLUMNS_MOMENTS_TABLE, COLUMNS_WAVEVECTOR_TABLE, GKDB_CFG)
import gkdb.core.idstogkdb_insert as ids_insert

READ_WRITE_DELETE_ROLE = "readwritedelete"
READ_WRITE_ROLE = "readwrite"
READ_ONLY_ROLE = "readonly"


def _compress_with_type(value: Any):
    if isinstance(value, np.ndarray):
        return compressed_nparray(value)
    elif isinstance(value, dict):
        return dict2pgjsonb(value)
    else:
        return shorten_imas_default(value)


def _insert_integrated_fluxes(cur, fk_list: tuple[int | tuple[int]],
                              ids_fluxes: list | tuple | None,
                              id_species: tuple[int],
                              uuid_gkid) -> bool | None:
    gk_id_fk = fk_list[0][0]
    if ids_fluxes is None:
        return None

    assert len(id_species) == len(ids_fluxes)
    int_fluxes_flat = []

    for specie_id, integrated_flux in zip(id_species, ids_fluxes):
        current_fluxes = _fluxes_as_pgflux(integrated_flux, uuid_gkid, "integrated", "laboratory")

        int_fluxes_flat.append(
            (
                gk_id_fk, specie_id,
                *[getattr(current_fluxes, name) for name in
                  COLUMNS_FLUXES_TABLE]
            )
        )

    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", "species_id",
                                     *COLUMNS_FLUXES_TABLE)],
        int_fluxes_flat,
        GKDB_CFG.schema,
        "fluxes_table")
    cur.execute(query)
    return True


# @foreign_key_exists("gyrokinetics_table")
def _insert_tag(cursor, fk_list: tuple[int | tuple[int]] | None, ids_tag) -> tuple[int, list[int] | None]:
    gk_id_fk = fk_list[0]
    tag_id = None
    ret_val = 0
    if ids_tag is None:
        return ret_val, None
    if not isinstance(ids_tag, (list, tuple)):
        ids_tag = (ids_tag,)
    tag_id_list = []
    ret_val = 1
    for t in ids_tag:
        tag_id: int | None = None
        query = ids2gkdb.query_insert_tag(t, GKDB_CFG.schema)
        try:
            cur = cursor.execute(query)
        except psycopg.errors.CheckViolation:
            print(f"tag {t} uses an invalid category : {t.categorie}")
            ret_val = -1
            continue
        except psycopg.errors.UniqueViolation:
            cursor.connection.rollback()
            tag_id_loc = cursor.execute(
                sql.SQL("""SELECT id FROM {}.{} as t WHERE t.tagid={} AND t.categorie={}""").format(
                    sql.Identifier(GKDB_CFG.schema),
                    sql.Identifier("tag_table"),
                    sql.Literal(t.tagid),
                    sql.Literal(t.categorie)
                )
            ).fetchone()

            assert tag_id_loc is not None, "tag_id should not be None"
            tag_id = tag_id_loc[0]
        tag_id = tag_id or cur.fetchone()[0]
        try:
            query_link = ids2gkdb.query_tag_link_gkid(tag_id, gk_id_fk,
                                                      GKDB_CFG.schema)
            cursor.execute(query_link)
        except psycopg.errors.UniqueViolation:
            pass
        tag_id_list.append(tag_id)
    return ret_val, tag_id_list or None


def _insert_fluxes_1d(conn, fk_list: tuple[int],
                      ids_fluxes: ids_gyrokinetics_local.FluxesNl1D, uuid, frame: str) -> int | None:
    """

    Inserts 1D non-linear fluxes data into the database.
    Since species is now a foreign key and not any more a coordinate, ids_fluxes contains only float and not anymore np.array
    Parameters:
    - cur: The database connection object.
    - fk_list: A tuple containing foreign key values or tuples of foreign key values.
    The first element corresponds to the gyrokinetics table foreign key.
    the second and thirds correspond to the non_linear_id and specie_id foreign keys respectively.
    - ids_fluxes: An object containing fluxes data.
    - uuid: The UUID of the fluxes data.
    - frame: The frame of the fluxes data (can be laboratory or rotating).

    Returns:
    - The ID of the inserted fluxes data if the insertion was successful.
    - None if ids_fluxes is None or the insertion failed.

    Example usage:
    cur = ConnectionPool().getconn()
    fk_list = (1, (2, 3))
    ids_fluxes = ExtendedFluxes(...)
    uuid = "12345"
    ftype = "fluxes"
    frame = "global"

    result = _insert_fluxes_1d(cur, fk_list, ids_fluxes, uuid, ftype, frame)
    """
    assert len(fk_list) == 3
    gk_id_fk = fk_list[0][0]
    species_id_fk = fk_list[2][0]
    non_linear_id_fk = fk_list[1][0]
    if ids_fluxes is None:
        return None

    ids_flux_ex = ModifiedFluxes1D(
        particles_phi_potential=shorten_imas_default(ids_fluxes.particles_phi_potential),
        particles_a_field_parallel=shorten_imas_default(ids_fluxes.particles_a_field_parallel),
        particles_b_field_parallel=shorten_imas_default(ids_fluxes.particles_b_field_parallel),
        energy_phi_potential=shorten_imas_default(ids_fluxes.energy_phi_potential),
        energy_a_field_parallel=shorten_imas_default(ids_fluxes.energy_a_field_parallel),
        energy_b_field_parallel=shorten_imas_default(ids_fluxes.energy_b_field_parallel),
        momentum_tor_parallel_phi_potential=shorten_imas_default(ids_fluxes.momentum_tor_parallel_phi_potential),
        momentum_tor_parallel_a_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_a_field_parallel),
        momentum_tor_parallel_b_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_b_field_parallel),
        momentum_tor_perpendicular_phi_potential=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_phi_potential),
        momentum_tor_perpendicular_a_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_a_field_parallel),
        momentum_tor_perpendicular_b_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_b_field_parallel),
        uuid=uuid,
        frame=frame
    )
    query = ids2gkdb.query_insert_fluxes_non_linear_1d(ids_flux_ex,
                                                       gk_id_fk,
                                                       non_linear_run_id=non_linear_id_fk,
                                                       species_id=species_id_fk,
                                                       schema=GKDB_CFG.schema)
    return conn.execute(query).fetchone()[0]


# @foreign_key_exists("gyrokinetics_table")
def _insert_moment(conn, fk_list: tuple[int | tuple[int]] | None,
                   ids_moment_ex: ExtendedMomentsLinear) -> int | None:
    gk_id_fk = fk_list[0][0]
    if ids_moment_ex is None:
        return None

    eigenmode_id_fk = fk_list[1][0]
    specie_id_fk = fk_list[2][0]

    query = ids2gkdb.query_insert_moments(ids_moment_ex,
                                          gk_id_fk,
                                          eigenmode_id=eigenmode_id_fk,
                                          specie_id=specie_id_fk,
                                          schema=GKDB_CFG.schema)
    return conn.execute(query).fetchone()[0]


def _fluxes_as_pgflux(
        ids_fluxes, uuid, ftype: str, frame: str) -> ExtendedFluxes:
    ids_flux_ex = ExtendedFluxes(
        particles_phi_potential=shorten_imas_default(ids_fluxes.particles_phi_potential),
        particles_a_field_parallel=shorten_imas_default(ids_fluxes.particles_a_field_parallel),
        particles_b_field_parallel=shorten_imas_default(ids_fluxes.particles_b_field_parallel),
        energy_phi_potential=shorten_imas_default(ids_fluxes.energy_phi_potential),
        energy_a_field_parallel=shorten_imas_default(ids_fluxes.energy_a_field_parallel),
        energy_b_field_parallel=shorten_imas_default(ids_fluxes.energy_b_field_parallel),
        momentum_tor_parallel_phi_potential=shorten_imas_default(ids_fluxes.momentum_tor_parallel_phi_potential),
        momentum_tor_parallel_a_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_a_field_parallel),
        momentum_tor_parallel_b_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_b_field_parallel),
        momentum_tor_perpendicular_phi_potential=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_phi_potential),
        momentum_tor_perpendicular_a_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_a_field_parallel),
        momentum_tor_perpendicular_b_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_b_field_parallel),
        uuid=uuid,
        ftype=ftype,
        frame=frame
    )
    return ids_flux_ex


def __fluxes_moment_as_list(
        ids_fluxes_moment, uuid, ) -> tuple[list, list]:
    list_moments = []
    if GKDB_CFG.write_moments is True:
        ids_moments_ex = ExtendedMomentsLinear(
            density=ids_fluxes_moment.moments_norm_particle.density,
            j_parallel=ids_fluxes_moment.moments_norm_particle.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_particle.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_particle.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_particle.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_particle.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_particle.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="particle",
            norm=True

        )

        list_moments.append(ids_moments_ex)

        ids_moments_ex = ExtendedMomentsLinear(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="default",
            norm=True
        )

        list_moments.append(ids_moments_ex)

        ids_moments_ex = ExtendedMomentsLinear(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density_gyroav,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel_gyroav,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel_gyroav,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular_gyroav,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel_gyroav,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular_gyroav,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy_gyroav,
            uuid=uuid,
            mtype="gyroav",
            norm=True
        )
        # new_id = _insert_moment(cursor, fk_list, ids_moments_ex)
        list_moments.append(ids_moments_ex)
    else:
        list_moments.append(*[None, None, None])

    list_fluxes = []

    list_fluxes.append(_fluxes_as_pgflux(ids_fluxes_moment.fluxes_norm_gyrocenter,
                                         uuid,
                                         "gyro",
                                         frame="lab"))
    list_fluxes.append(_fluxes_as_pgflux(ids_fluxes_moment.fluxes_norm_gyrocenter_rotating_frame,
                                         uuid,
                                         "particle",
                                         frame="lab"))
    list_fluxes.append(_fluxes_as_pgflux(ids_fluxes_moment.fluxes_norm_particle_rotating_frame,
                                         uuid,
                                         "particle",
                                         frame="rotating"))

    return list_moments, list_fluxes


# @foreign_key_exists("gyrokinetics_table")
def _insert_fluxes_moment(conn, fk_list: tuple[int | tuple[int]] | None,
                          ids_fluxes_moment, uuid, ) -> tuple[int | None]:
    """
    Inserts fluxes and moments data into the database.

    Parameters:
        conn (psycopg2.extensions.connection): The connection to the database.
        fk_list (tuple[int | tuple[int]] | None): The foreign key list.
        ids_fluxes_moment (Any): The fluxes and moments data.
        uuid (str): The UUID of the data.

    fk_list must follow the format ((gk_id,),(eig_id_0, eig_id_1,...),(specie_id_0, specie_id_1,...))
    Returns:
        tuple[int|None]: The IDs of the inserted moments and fluxes.
    """
    list_moments = []
    if GKDB_CFG.write_moments is True:
        ids_moments_ex = ExtendedMomentsLinear(
            density=ids_fluxes_moment.moments_norm_particle.density,
            j_parallel=ids_fluxes_moment.moments_norm_particle.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_particle.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_particle.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_particle.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_particle.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_particle.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="particle",
            norm=True

        )
        new_id = _insert_moment(conn, fk_list, ids_moments_ex)
        list_moments.append(new_id)

        ids_moments_ex = ExtendedMomentsLinear(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="default",
            norm=True
        )
        new_id = _insert_moment(conn, fk_list, ids_moments_ex)
        list_moments.append(new_id)

        ids_moments_ex = ExtendedMomentsLinear(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density_gyroav,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel_gyroav,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel_gyroav,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular_gyroav,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel_gyroav,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular_gyroav,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy_gyroav,
            uuid=uuid,
            mtype="gyroav",
            norm=True
        )
        new_id = _insert_moment(conn, fk_list, ids_moments_ex)
        list_moments.append(new_id)

    list_fluxes = []
    # fluxes_norm_gyrocenter: Optional[Fluxes]
    # fluxes_norm_gyrocenter_rotating_frame: Optional[Fluxes]
    # fluxes_norm_particle: Optional[Fluxes]
    # fluxes_norm_particle_rotating_frame: Optional[Fluxes]
    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_gyrocenter, uuid, "gyro", frame="lab")
    list_fluxes.append(new_id)

    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_gyrocenter_rotating_frame, uuid, "gyro",
                            frame="rotating")
    list_fluxes.append(new_id)

    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_particle, uuid, "particle", frame="lab")
    list_fluxes.append(new_id)

    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_particle_rotating_frame, uuid, "particle",
                            frame="rotating")
    list_fluxes.append(new_id)

    return tuple(list_moments), tuple(list_fluxes)


class GkdbClient:
    dbinfo = None
    pool: ConnectionPool = None
    has_read_permission: bool = False
    has_write_permission: bool = False
    has_del_permission: bool = False
    conninfo: Optional[str] = None

    def __establish_pool(self):
        self.pool = ConnectionPool(self.conninfo, configure=complex_type_register, name="pool_gkdb", open=False)
        try:
            self.pool.open(wait=True)
            print(f"pool opened {self.conninfo}")
        except:
            raise TimeoutError(f"invalid open connection {self.conninfo}")

    def __init__(self, dbinfo):
        self.dbinfo = dbinfo

        self.conninfo = f"postgresql://{dbinfo.user}:{dbinfo.password}@{dbinfo.host}:{dbinfo.port}/{dbinfo.database}"
        self.__establish_pool()
        self._get_user_permissions()

    def _get_user_permissions(self):
        """
        Get user permissions.

        :return: A tuple containing the user's read, write, and delete permissions.
        :rtype: tuple

        """
        with self.pool.connection() as conn:
            permissions = conn.execute(
                r" SELECT rolname FROM pg_roles WHERE pg_has_role( CURRENT_USER, oid, 'member');").fetchall()
        permissions = tuple(x[0] for x in permissions)

        if READ_WRITE_DELETE_ROLE in permissions:
            self.has_read_permission = True
            self.has_write_permission = True
            self.has_del_permission = True
        elif READ_WRITE_ROLE in permissions:
            self.has_read_permission = True
            self.has_write_permission = True
            self.has_del_permission = False
        elif READ_ONLY_ROLE in permissions:
            self.has_read_permission = True
            self.has_write_permission = False
            self.has_del_permission = False
        elif "pg_write_all_data" in permissions:
            self.has_read_permission = True
            self.has_write_permission = True
            self.has_del_permission = True
        return self.has_read_permission, self.has_write_permission, self.has_del_permission

    def get_number_records(self):
        """Get the total number of records in the gyrokinetics table.

        Returns:
            int: The total number of records in the gyrokinetics table.

        Raises:
            psycopg.errors.UndefinedTable: If the gyrokinetics table does not exist in the database.

        Example:
            gkdb_client = GkdbClient()
            num_records = gkdb_client.get_number_records()
        """
        try:
            with self.pool.connection() as conn:
                records = conn.execute(
                    sql.SQL(r"SELECT count(uuid) FROM {}.gyrokinetics_local_table;").format(
                        sql.Identifier(self.dbinfo.schema))).fetchone()[0]

        except psycopg.errors.UndefinedTable:
            print("not a GKDB database")
            records = None
        return records

    def get_number_records_per_type(self):
        pass

    def get_number_records_per_code(self):
        pass

    @profile
    def add_new_ids(self, ids: Type[ids_gyrokinetics_local.GyrokineticsLocal]) -> tuple[int, str | Any]:
        # TODO : check backpropagation
        if self.has_write_permission is False:
            raise PermissionError("Current User does not have write permission")

        def __insert_gyrokinetic_table() -> str:
            request = sql.SQL("INSERT into {}.{} VALUES (DEFAULT) RETURNING (id, uuid)").format(
                sql.Identifier(self.dbinfo.schema),
                sql.Identifier("gyrokinetics_local_table")
            )
            return request

        request_gk_table = __insert_gyrokinetic_table()
        with self.pool.connection() as conn:
            # get id, uuid for new IDS
            with conn.cursor() as cur:
                cur.execute(request_gk_table)
                gk_id, gk_uuid = cur.fetchone()[0]

            # with self.pool.connection() as cur:
            #     with cur.cursor() as cur:
            #         request = sql.SQL("SELECT id from {}.{}").format(
            #             sql.Identifier(self.db_info.schema),
            #             sql.Identifier("gyrokinetics_table"),
            #         )
            #         cur.execute(request)
            #
            # #   cur.commit()
            # print(f"adding ids with id {gk_id} and uuid [{gk_uuid}]")
            # with self.pool.connection() as cur:

            with conn.cursor() as cur:

                # tag: list[EntryTag]
                # TODO : debug when tag already exists
                # TODO : to be reimplemented
                # with cur.cursor() as cur:
                # _insert_tag(cur, (gk_id,), ids.tag)

                # normalizing_quantities: Optional[InputNormalizing]
                if ids.normalizing_quantities is not None:
                    query = ids2gkdb.query_insert_input_normalizing(ids.normalizing_quantities, gk_id, GKDB_CFG.schema)
                    cur.execute(query)
                # flux_surface: Optional[FluxSurface]
                # with cur.cursor() as cur:
                ids_insert._insert_flux_surface(cur, (gk_id,), ids.flux_surface)

                # model: Optional[Model]
                # with cur.cursor() as cur:

                ids_insert._insert_model(cur, (gk_id,), ids.model)

                # species_all: Optional[InputSpeciesGlobal]
                if ids.species_all is not None:
                    query = ids2gkdb.query_insert_input_species(ids.species_all, gk_id, GKDB_CFG.schema)
                    cur.execute(query)

                # species: list[Species]
                # with cur.cursor() as cur:
                ret_species, id_species = ids_insert._insert_species(cur, (gk_id,), ids.species)
                # collisions: Optional[Collisions]
                # with cur.cursor() as cur:

                ids_insert._insert_collisions(cur, (id_species,), ids.collisions)
                # code: Optional[Code]
                # with cur.cursor() as cur:
                code_id = ids_insert._insert_code(cur, (gk_id,), ids.code)

                # wavevector: list[Wavevector]
                # with cur.cursor() as cur:
                if ids.linear is not None:
                    ids_insert._insert_linear(cur, ((gk_id,), (code_id,)), ids.linear,
                                              id_species, gk_uuid)

                # time: list[ndarray[(int,), float]]

                # TODO : check non_linear is false when only default values
                # non linear
                # if ids.non_linear is not None:
                #     ids_insert._insert_non_linear(cur, ((gk_id,), (code_id,)), ids.non_linear,
                #                                   id_species, gk_uuid)
            conn.commit()

        return gk_id, gk_uuid

    def get_ids_by_uuid(self) -> str:
        pass


if __name__ == "__main__":
    test = GkdbClient(GKDB_CFG)
    print("number of records in the database : {0}".format(test.get_number_records()))
    from gkdb.tools.fakedata import generate_fake_params, generate_rnd_ids_gk
    #
    import time

    #
    start = time.perf_counter()
    for _ in range(100):
        new_params = generate_fake_params()
        new_ids = generate_rnd_ids_gk(new_params)
        gkid, gkuuid = test.add_new_ids(new_ids)
        print(gkid, gkuuid)
    print(time.perf_counter() - start)
    # print("number of records in the database : {0}".format(test.get_number_records()))
