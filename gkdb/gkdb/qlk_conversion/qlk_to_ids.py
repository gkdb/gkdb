import qlk2ids_opt
import os
import pickle
from idspy_dictionaries import ids_gyrokinetics as gkids
import copy
import numpy as np
import xarray as xr
import time

from gkdb.client.client import GkdbClient, GKDB_CFG


def convert_QLK_to_IDS(filepath: str):
    '''

    :param filepath: path to qualikiz.nc file to convert to ids_to_check
    :param output_folder_path: path to folder where to save the output
    :param name: name of the file to save (without extension)
    :param overwrite: whether to overwrite existing file or not
    :param check_output: checks for debugging
    :param error_check: checks for errors in conversion
    :param type_check: checks correct type for conversion
    :param method: int (0-2) dictating which method to use.
                    0: split by dimx then convert and output an IDS file. working but slow for large files
                    1: convert dataset then loop over dimx and output and IDS file. wip but hopefully faster
                    2: for internal use only (EDGE10D), convert as in 1 but only the values required for AI training
    :return:
    '''
    # open file in float 64 format using xarray

    full_ds64 = xr.open_dataset(filepath, engine='netcdf4')

    # convert dims to int
    # Original coordinate values as float64 and convert dimensions to int values
    for coord in ("ntheta", "nions", "dimn", "dimx", "numicoefs", "ecoefs"):
        # Original coordinate values as float64
        try:
            original_coords = full_ds64[coord].values

            # Convert the original float64 coordinates to int32
            modified_coords = original_coords.astype('int64')

            # Create a new coordinate with the modified dtype
            new_coord = xr.DataArray(modified_coords, dims=coord, coords={coord: modified_coords})

            # Update the dataset's coordinate with the new coordinate
            full_ds64[coord] = new_coord
        except KeyError:
            return

    # get number of simulations in file to iterate over
    base_dim_x = full_ds64.dimx.values
    ndimx = len(base_dim_x)
    dim_x_max = base_dim_x.max()

    # iterate over each simulation

    tile_size = 1024
    idx_file = 0

    # constants used for all sub datasets
    Ai0_array = np.float64(full_ds64.isel(nions=0).Ai.values)

    # calculate ids_to_check members common to all entries
    ids_properties: gkids.IdsProperties = gkids.IdsProperties(
        # # provider
        provider='Matisse Lanzarone',
        # # creation_date
        creation_date=time.time(),
        # # comment
        comment='Edge10D converted to IMAS format',
    )

    # sub IDS common to all ds
    code = gkids.Code(name="QuaLiKiz", output_flag=(0,))

    model: gkids.Model = gkids.Model(non_linear_run=False,
                                     include_a_field_parallel=False,
                                     include_b_field_parallel=False,
                                     include_full_curvature_drift=True,
                                     include_centrifugal_effects=False,
                                     collisions_pitch_only=True,
                                     collisions_momentum_conservation=False,
                                     collisions_energy_conservation=False,
                                     collisions_finite_larmor_radius=False,
                                     )

    flux_surface: gkids.FluxSurface = gkids.FluxSurface(
        # ip_sign
        ip_sign=qlk2ids_opt.ip_sign(),
        # b_field_tor_sign
        b_field_tor_sign=qlk2ids_opt.b_field_tor_sign(),

        # dgeometric_axis_z_dr_minor
        dgeometric_axis_z_dr_minor=qlk2ids_opt.dgeometric_axis_z_dr_minor(),
        # elongation
        elongation=qlk2ids_opt.elongation(),
        # delongation_dr_minor_norm
        delongation_dr_minor_norm=qlk2ids_opt.delongation_dr_minor_norm(),
        # shape_coefficients_c
        shape_coefficients_c=qlk2ids_opt.shape_coefficients_c(),
        # shape_coefficients_s
        shape_coefficients_s=qlk2ids_opt.shape_coefficients_s(),
        # dc_dr_minor_norm
        dc_dr_minor_norm=qlk2ids_opt.dc_dr_minor_norm(),
        # ds_dr_minor_norm
        ds_dr_minor_norm=qlk2ids_opt.ds_dr_minor_norm(),
    )

    # to avoid issue if dimx does not start at 0
    for ix in base_dim_x[::tile_size]:
        ds_buffer = full_ds64.isel(dimx=full_ds64['dimx'].isin(range(ix, min(ix + tile_size, dim_x_max - 1))))
        len_max = len(ds_buffer.dimx.values)

        for subdims in range(len_max):
            ds = ds_buffer[{'dimx': subdims}]
            # dimensions to iterate over
            ndimn = len(ds.dimn.values)
            nnumsols = len(ds.numsols.values)
            nnions = len(ds.nions.values)

            # # load all the required variables as float 64 to circumvent the issues with loading as float64 initially and avoid rounding errors especially for back conversion
            Rmin = ds.Rmin.values.astype('float64')[()]
            x = ds.x.values.astype('float64')[()]
            Ro = ds.Ro.values.astype('float64')[()]
            Bo = ds.Bo.values.astype('float64')[()]
            q = ds.q.values.astype('float64')[()]
            smag = ds.smag.values.astype('float64')[()]
            alpha = ds.alpha.values.astype('float64')[()]
            Machtor = ds.Machtor.values.astype('float64')[()]
            gammaE = ds.gammaE.values.astype('float64')[()]
            normni = ds.normni.values.astype('float64')[()]
            Zi = ds.Zi.values.astype('float64')[()]
            ne = ds.ne.values.astype('float64')[()]
            Te = ds.Te.values.astype('float64')[()]
            Ane = ds.Ane.values.astype('float64')[()]
            Ate = ds.Ate.values.astype('float64')[()]
            Autor = ds.Autor.values.astype('float64')[()]
            pfe_SI = ds.pfe_SI.values.astype('float64')[()]
            # not present in QLK
            # vfe_SI = ds.vfe_SI.values.astype('float64')
            efe_SI = ds.efe_SI.values.astype('float64')[()]
            Ai = ds.Ai.values.astype('float64')[()]
            Ani = ds.Ani.values.astype('float64')[()]
            Ti = ds.Ti.values.astype('float64')[()]
            Te = ds.Te.values.astype('float64')[()]
            Ati = ds.Ati.values.astype('float64')[()]
            pfi_SI = ds.pfi_SI.values.astype('float64')[()]
            vfi_SI = ds.vfi_SI.values.astype('float64')[()]
            efi_SI = ds.efi_SI.values.astype('float64')[()]
            kthetarhos = ds.kthetarhos.values.astype('float64')[()]
            gam_GB = ds.gam_GB.values.astype('float64')[()]
            ome_GB = ds.ome_GB.values.astype('float64')[()]
            rmodewidth = ds.rmodewidth.values.astype('float64')[()]
            imodewidth = ds.imodewidth.values.astype('float64')[()]
            rmodeshift = ds.rmodeshift.values.astype('float64')[()]
            imodeshift = ds.imodeshift.values.astype('float64')[()]
            distan = ds.distan.values.astype('float64')[()]


            # # initialise constants and ratios
            QLK = qlk2ids_opt.constantsQLK(Ro, Bo, Te, Ai0_array[0])
            IMAS = qlk2ids_opt.constantsIMAS(Ro, Bo, Te, ne)
            RATIOS = qlk2ids_opt.constantsRATIO(QLK, IMAS)


            # # version
            code.version = ds.QLK_CLOSEST_RELEASE
            # # parameters
            # # TODO: ids_to_check.code.parameters = ?

            # # ids_to_check.normalizing_quantities #

            normalizing_quantities: gkids.InputNormalizing = gkids.InputNormalizing(
                # # t_e
                t_e=IMAS.Tref,
                # # n_e
                n_e=IMAS.nref,
                # r
                r=np.float64(IMAS.Rref),
                # b_field_tor
                b_field_tor=np.float64(IMAS.Bref),
            )

            # ids_to_check.flux_surface #
            # r_minor_norm
            flux_surface.r_minor_norm = qlk2ids_opt.r_minor_norm(Rmin, x, Ro)
            # q
            flux_surface.q = qlk2ids_opt.q_IMAS(q)
            # magnetic_shear_r_minor
            flux_surface.magnetic_shear_r_minor = qlk2ids_opt.magnetic_shear_r_minor(smag)
            # pressure_gradient_norm
            flux_surface.pressure_gradient_norm = qlk2ids_opt.pressure_gradient_norm(alpha, q, RATIOS.Rrat, RATIOS.Brat)

            # dgeometric_axis_r_dr_minor
            flux_surface.dgeometric_axis_r_dr_minor = qlk2ids_opt.dgeometric_axis_r_dr_minor(alpha)

            species_all = gkids.InputSpeciesGlobal(
                # velocity_tor_norm
                velocity_tor_norm=qlk2ids_opt.velocity_tor_norm(Machtor, QLK.cref, IMAS.vth_ref, RATIOS.Rrat),
                # shearing_rate_norm
                shearing_rate_norm=qlk2ids_opt.shearing_rate_norm(gammaE, QLK.cref, IMAS.vth_ref, RATIOS.Rrat),
                # zeff
                zeff=qlk2ids_opt.zeff(normni, Zi, RATIOS.qrat)
            )

            # ids_to_check.species #
            # electron first
            electron_temp = gkids.Species(
                # charge_norm
                charge_norm=qlk2ids_opt.charge_norm(np.float64(-1), RATIOS.qrat),
                # mass_norm
                mass_norm=qlk2ids_opt.mass_norm(QLK.me / QLK.mref, RATIOS.mrat),
                # density_norm
                density_norm=qlk2ids_opt.density_norm_e(ne),
                # temperature_norm
                temperature_norm=qlk2ids_opt.temperature_norm(Te, Te),
                # density_log_gradient_norm
                density_log_gradient_norm=qlk2ids_opt.density_log_gradient(Ane, RATIOS.Rrat),
                # temperature_log_gradient_norm
                temperature_log_gradient_norm=qlk2ids_opt.temperature_log_gradient(Ate, RATIOS.Rrat),
                # velocity_tor_gradient_norm
                velocity_tor_gradient_norm=qlk2ids_opt.velocity_tor_gradient_norm(Autor, QLK.cref, IMAS.vth_ref,
                                                                      RATIOS.Rrat),
            )
            # electron flux
            rho_star = IMAS.rho_ref / IMAS.Rref

            electron_fluxes = gkids.Fluxes(
                particles_phi_potential=qlk2ids_opt.normalise_integrated_particle_flux(pfe_SI, ne, IMAS.vth_ref, rho_star),
                momentum_tor_parallel_phi_potential=np.float64(0),
                momentum_tor_perpendicular_phi_potential=np.float64(0),
                energy_phi_potential=qlk2ids_opt.normalise_integrated_energy_flux(efe_SI, ne, IMAS.vth_ref, rho_star,
                                                                      IMAS.Tref)
            )


            # ions
            # precalculate arrays to access with loop over nions
            Charge_Norm = qlk2ids_opt.charge_norm(Zi, RATIOS.qrat)
            Mass_norm = qlk2ids_opt.mass_norm(Ai, RATIOS.mrat)
            dens_norm = qlk2ids_opt.density_norm_i(normni)
            dens_grad = qlk2ids_opt.density_log_gradient(Ani, RATIOS.Rrat)
            temperature_norm_i = qlk2ids_opt.temperature_norm(Ti, Te)

            temp_grad_i = qlk2ids_opt.temperature_log_gradient(Ati, RATIOS.Rrat)

            ni = ne * normni
            ion_particle_fluxes = qlk2ids_opt.normalise_integrated_particle_flux(pfi_SI, ni, IMAS.vth_ref, rho_star)
            ion_para_momentum_fluxes = qlk2ids_opt.normalise_integrated_momentum_flux(vfi_SI, ni, IMAS.vth_ref, rho_star, IMAS.mref,
                                                                          IMAS.Rref)
            ion_perp_momentum_fluxes = 0
            ion_energy_fluxes = qlk2ids_opt.normalise_integrated_energy_flux(efi_SI, ni, IMAS.vth_ref, rho_star, IMAS.Tref)

            # colliosionality #

            species: tuple[gkids.Species] = (electron_temp,) + tuple([gkids.Species(
                # charge_norm
                charge_norm=Charge_Norm[nion],
                # mass_norm
                mass_norm=Mass_norm[nion],
                # density_norm
                density_norm=dens_norm[nion],
                # temperature_norm
                temperature_norm=temperature_norm_i[nion],
                # density_log_gradient_norm
                density_log_gradient_norm=dens_grad[nion],
                # temperature_log_gradient_norm
                temperature_log_gradient_norm=temp_grad_i[nion],
                # velocity_tor_gradient_norm
                velocity_tor_gradient_norm=electron_temp.velocity_tor_gradient_norm
            ) for nion in range(nnions)])


            # electron + ion fluxes
            fluxes_integrated_norm = (electron_fluxes,) + tuple([
                gkids.Fluxes(
                    particles_phi_potential=ion_particle_fluxes[nion],
                    momentum_tor_parallel_phi_potential=ion_para_momentum_fluxes[nion],
                    momentum_tor_perpendicular_phi_potential=ion_perp_momentum_fluxes,
                    energy_phi_potential=ion_energy_fluxes[nion],
                ) for nion in range(nnions)
            ])

            coll_norm = qlk2ids_opt.collisionality_norm(IMAS.Rref,
                                            IMAS.vth_ref,
                                            ne,
                                            normni,
                                            Zi,
                                            RATIOS.qrat,
                                            Te,
                                            IMAS.vth_e)
            collisions = np.zeros((nnions + 1, nnions + 1))
            for nion in range(nnions):
                collisions[0, nion + 1] = coll_norm[nion]

            collisions = gkids.Collisions(collisionality_norm=collisions)

            # ids_to_check.wavevector #
            # calculate binormal for the whole set
            binormal_component_norms = qlk2ids_opt.binormal_component_norm(RATIOS.rho_rat, kthetarhos)
            # calculate growth rate and frequency for whole set
            growth_rate_norms = qlk2ids_opt.growth_rate_norm(gam_GB, Rmin, IMAS.Rref, RATIOS.vth_rat)
            frequency_norms = qlk2ids_opt.frequency_norm(ome_GB, Rmin, IMAS.Rref, RATIOS.vth_rat)
            # iterate over each wavevector
            recip_rho_star = IMAS.Rref / IMAS.rho_ref
            modewidth = np.complex128(rmodewidth + 1j * imodewidth)
            modeshift = np.complex128(rmodeshift + 1j * imodeshift)

            wavevector = []
            valid_profile = True
            for dimn in range(ndimn):
                # eigenmode
                d = distan[dimn]
                # compute theta values
                stdev = rmodewidth[dimn] / d
                if stdev > np.pi:
                    # poloidal_angle = np.linspace(-4 * stdev, 4 * stdev, 151)
                    poloidal_angle = np.linspace(-(1.5 * np.pi / stdev), (1.5 * np.pi / stdev), 151)
                else:
                    poloidal_angle = np.linspace(-np.pi, np.pi, 151)

                # potential calculation
                try:
                    phi_potential_perturbed_norm = qlk2ids_opt.phi_potential_perturbed_norm_linear(poloidal_angle,
                                                                                       modewidth[dimn],
                                                                                       modeshift[dimn],
                                                                                       d,
                                                                                       recip_rho_star,
                                                                                       IMAS.qref,
                                                                                       IMAS.Tref,
                                                                                       1)
                except Exception as e:
                    print(
                        f"numpy exception raised for profile {ds.dimx.values} of file {filepath} with ({d}, {modewidth[dimn].real} ) as \n {e}")
                    valid_profile = False
                    break

                # generate default wavevector class
                wavevector_temp = gkids.Wavevector(
                    # radial_component_norm
                    radial_component_norm=qlk2ids_opt.radial_component_norm(),
                    # binormal_component_norm (select from previously calculated)
                    binormal_component_norm=binormal_component_norms[dimn],
                    # iterate over the number of solutions
                    eigenmode=[gkids.Eigenmode(poloidal_angle=poloidal_angle,
                                               poloidal_turns=3,
                                               initial_value_run=1,
                                               growth_rate_norm=growth_rate_norms[dimn, numsol],
                                               frequency_norm=frequency_norms[dimn, numsol],
                                               phi_potential_perturbed_norm=phi_potential_perturbed_norm) for numsol in
                               range(nnumsols)]
                )

                wavevector.append(wavevector_temp)

            if valid_profile is False:
                continue
            # create the ids_to_check class instance
            ids = gkids.Gyrokinetics(
                ids_properties=ids_properties,
                model=model,
                flux_surface=flux_surface,
                species=species,
                collisions=collisions,
                species_all=species_all,
                code=code,
                fluxes_integrated_norm=fluxes_integrated_norm,
                wavevector=wavevector,
                normalizing_quantities=normalizing_quantities
            )
            idx_file += 1

            # yield the newly created ids_to_check
            yield base_dim_x[0], ds.dimx.values, ndimx, ids

    return base_dim_x[0][()], ds.dimx.values, ndimx


def main_loop(filepath: str = './edge10d_glued_01.nc.1'):
    print(f'Starting insertion from {filepath}')
    gkdb_client = GkdbClient(GKDB_CFG)
    idx = 0
    ids = None
    list_ids = []
    for result in convert_QLK_to_IDS(filepath):
        offset, index_x, tot_size, ids = result
        gkid, gkuuid = gkdb_client.add_new_ids(ids)

        list_ids.append((index_x, gkid, gkuuid))
        idx += 1
        if idx >= 30000:
            break
    gkdb_client.pool.close()
    return filepath, list_ids


if __name__ == "__main__":
    from multiprocessing import Pool
    import os
    import pickle
    base_path = r"D:\workdir\qlk_converter"

    nmax_file = 101
    first_file = 1
    pool_size = min(nmax_file, 25)

    global_time_thread_start = time.perf_counter()
    # xarray open_dataset is not thread safe and cannot be used with concurrent.futures, only multiprocessing
    global_result = []
    with Pool(pool_size) as pool:

        for result in pool.imap_unordered(main_loop,
                                          [os.path.join(base_path, "edge10d_glued_{0:02d}.nc.1".format(x)) for x in
                                           range(first_file, first_file + nmax_file)]):
            global_result.append(result)

    with open('list_entries_v3.pkl', 'wb') as f:
        pickle.dump(global_result, f)
    global_time_thread_end = time.perf_counter()
    print("global thread time : ", global_time_thread_end - global_time_thread_start)
