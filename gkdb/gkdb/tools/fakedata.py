from __future__ import annotations
from idspy_dictionaries import ids_gyrokinetics_local as gkids

from dataclasses import dataclass, asdict
import datetime
import numpy as np
from numpy import ndarray
from typing import Optional, Any
import numpy.random as rnd
import simplejson as js
import pprint as pp

GKDB_RNG = rnd.default_rng()
CODE_LIST = ['qualikiz', 'gkw', 'gene', 'tglf']
LIBRARY_LIST = ["fftw", "hdf5", "mpi", "netcdf"]


@dataclass(slots=True)
class RunParameters:
    nx: int
    ny: int
    nz: int
    nt: int
    n_angle_pol: int
    nvx: int
    nvy: int
    nvz: int
    nspecies: int
    nwavevector: int
    neigenmode: int
    ntags: int
    dx: float
    dy: float
    dz: float
    dvx: float
    dvy: float
    dvz: float
    nlibraries: int


#
# @dataclass(slots=True)
# class CodePartialConstant:
#     parameters: str
#     output_flag: int
#
#
# @dataclass(slots=True)
# class EntryTag:
#     tagid: str
#     categorie: str
#     description: str
#
#
# @dataclass(slots=True)
# class GkdbEntryTag:
#     category: str
#     tagid: str
#     description: str
#
#
# @dataclass(slots=True)
# class Collisions:
#     collisionality_norm: ndarray
#
#
# @dataclass(slots=True)
# class FluxSurface:
#     r_minor_norm: float
#     elongation: float
#     delongation_dr_minor_norm: float
#     dgeometric_axis_r_dr_minor: float
#     dgeometric_axis_z_dr_minor: float
#     q: float
#     magnetic_shear_r_minor: float
#     pressure_gradient_norm: float
#     ip_sign: float
#     b_field_tor_sign: float
#     shape_coefficients_c: ndarray
#     dc_dr_minor_norm: ndarray
#     shape_coefficients_s: ndarray
#     ds_dr_minor_norm: ndarray
#
#
# @dataclass(slots=True)
# class Fluxes:
#     particles_phi_potential: float
#     particles_a_field_parallel: float
#     particles_b_field_parallel: float
#     energy_phi_potential: float
#     energy_a_field_parallel: float
#     energy_b_field_parallel: float
#     momentum_tor_parallel_phi_potential: float
#     momentum_tor_parallel_a_field_parallel: float
#     momentum_tor_parallel_b_field_parallel: float
#     momentum_tor_perpendicular_phi_potential: float
#     momentum_tor_perpendicular_a_field_parallel: float
#     momentum_tor_perpendicular_b_field_parallel: float
#
#
# @dataclass(slots=True)
# class InputNormalizing:
#     t_e: float
#     n_e: float
#     r: float
#     b_field_tor: float
#
#
# @dataclass(slots=True)
# class InputSpeciesGlobal:
#     beta_reference: float
#     velocity_tor_norm: float
#     zeff: float
#     debye_length_reference: float
#     shearing_rate_norm: float
#
#
# @dataclass(slots=True)
# class Model:
#     include_centrifugal_effects: int
#     include_a_field_parallel: int
#     include_b_field_parallel: int
#     include_full_curvature_drift: int
#     collisions_pitch_only: int
#     collisions_momentum_conservation: int
#     collisions_energy_conservation: int
#     collisions_finite_larmor_radius: int
#     non_linear_run: int
#     time_interval_norm: ndarray
#
#
# @dataclass(slots=True)
# class Moments:
#     density: ndarray
#     density_gyroav: ndarray
#     j_parallel: ndarray
#     j_parallel_gyroav: ndarray
#     pressure_parallel: ndarray
#     pressure_parallel_gyroav: ndarray
#     pressure_perpendicular: ndarray
#     pressure_perpendicular_gyroav: ndarray
#     heat_flux_parallel: ndarray
#     heat_flux_parallel_gyroav: ndarray
#     v_parallel_energy_perpendicular: ndarray
#     v_parallel_energy_perpendicular_gyroav: ndarray
#     v_perpendicular_square_energy: ndarray
#     v_perpendicular_square_energy_gyroav: ndarray
#
#
# @dataclass(slots=True)
# class MomentsParticles:
#     density: ndarray
#     j_parallel: ndarray
#     pressure_parallel: ndarray
#     pressure_perpendicular: ndarray
#     heat_flux_parallel: ndarray
#     v_parallel_energy_perpendicular: ndarray
#     v_perpendicular_square_energy: ndarray
#
#
# @dataclass(slots=True)
# class Species:
#     charge_norm: float
#     mass_norm: float
#     density_norm: float
#     density_log_gradient_norm: float
#     temperature_norm: float
#     temperature_log_gradient_norm: float
#     velocity_tor_gradient_norm: float
#     potential_energy_norm: ndarray
#     potential_energy_gradient_norm: ndarray
#
# @dataclass(slots=True)
# class Library:
#     name: str
#     commit: str
#     version: str
#     repository: str
#     parameters: str
#
#
# @dataclass(slots=True)
# class Code:
#     name: str
#     commit: str
#     version: str
#     repository: str
#     parameters: str
#     output_flag: list[ndarray[(int,), int]]
#     library: list[Library]
#
# @dataclass(slots=True)
# class CodePartialConstant:
#     name: str
#     commit: str
#     version: str
#     repository: str
#     parameters: str
#
#
# @dataclass(slots=True)
# class FluxesMoments:
#     moments_norm_gyrocenter: Optional[Moments]
#     moments_norm_particle: Optional[MomentsParticles]
#     fluxes_norm_gyrocenter: Optional[Fluxes]
#     fluxes_norm_gyrocenter_rotating_frame: Optional[Fluxes]
#     fluxes_norm_particle: Optional[Fluxes]
#     fluxes_norm_particle_rotating_frame: Optional[Fluxes]
#
#
# @dataclass(slots=True)
# class Eigenmode:
#     poloidal_turns: int
#     growth_rate_norm: float
#     frequency_norm: float
#     growth_rate_tolerance: float
#     angle_pol: ndarray
#     phi_potential_perturbed_weight: ndarray
#     phi_potential_perturbed_parity: ndarray
#     a_field_parallel_perturbed_weight: ndarray
#     a_field_parallel_perturbed_parity: ndarray
#     b_field_parallel_perturbed_weight: ndarray
#     b_field_parallel_perturbed_parity: ndarray
#     poloidal_angle: ndarray
#     phi_potential_perturbed_norm: ndarray
#     a_field_parallel_perturbed_norm: ndarray
#     b_field_parallel_perturbed_norm: ndarray
#     time_norm: ndarray
#     fluxes_moments: list[FluxesMoments]
#     code: Optional[CodePartialConstant]
#     initial_value_run: int
#
#
# @dataclass(slots=True)
# class EigenmodeField:
#     poloidal_turns: int
#     growth_rate_norm: float
#     frequency_norm: float
#     growth_rate_tolerance: float
#     angle_pol: ndarray
#     phi_potential_perturbed_weight: ndarray
#     phi_potential_perturbed_parity: ndarray
#     a_field_parallel_perturbed_weight: ndarray
#     a_field_parallel_perturbed_parity: ndarray
#     b_field_parallel_perturbed_weight: ndarray
#     b_field_parallel_perturbed_parity: ndarray
#     poloidal_angle: ndarray
#     phi_potential_perturbed_norm: ndarray
#     a_field_parallel_perturbed_norm: ndarray
#     b_field_parallel_perturbed_norm: ndarray
#     time_norm: ndarray
#     fluxes_moments: list[FluxesMoments]
#     code: Optional[CodePartialConstant]
#     initial_value_run: int
#
# @dataclass(slots=True)
# class IdsProvenanceNode:
#     path: str
#     sources: Optional[list[str]]
#
#
# @dataclass(slots=True)
# class IdsProvenance:
#     node: list[IdsProvenanceNode]
#
#
# @dataclass(slots=True)
# class IdsProperties:
#     comment: str
#     homogeneous_time: int
#     provider: str
#     creation_date: str
#     provenance: Optional[IdsProvenance]
#
#
# @dataclass(slots=True)
# class Wavevector:
#     radial_component_norm: float
#     binormal_component_norm: float
#     eigenmode: list[Eigenmode]
#
#
# @dataclass(slots=True)
# class GyrokineticsLocal:
#     ids_properties: Optional[IdsProperties]
#     tag: Optional[list[EntryTag]]
#     normalizing_quantities: Optional[InputNormalizing]
#     flux_surface: Optional[FluxSurface]
#     model: Optional[Model]
#     species_all: Optional[InputSpeciesGlobal]
#     species: list[Species]
#     collisions: Optional[Collisions]
#     wavevector: list[Wavevector]
#     fluxes_integrated_norm: list[Fluxes]
#     code: Optional[Code]
#     time: list[ndarray[(int,), float]]
#

def _fake_gaussian(nx: int) -> np.array:
    x = np.linspace(-10, 10, num=nx)
    x0 = GKDB_RNG.uniform(-3, 3)
    b = GKDB_RNG.uniform(.4, 20)
    a = GKDB_RNG.uniform(.1, 10)
    return a * np.exp(-(x - x0) ** 2 / b)


def _fake_deriv_gaussian(nx: int):
    x = np.linspace(-10, 10, num=nx)
    x0 = GKDB_RNG.uniform(-3, 3)
    b = GKDB_RNG.uniform(.4, 20)
    a = GKDB_RNG.uniform(.1, 10)
    return -2 * a * (x - x0) * np.exp(-(x - x0) ** 2 / b) / b


def _fake_2d_array(ny: int, nt: int) -> np.array:
    funct_r = [_fake_gaussian, _fake_deriv_gaussian][GKDB_RNG.choice([0, 1])]
    funct_i = [_fake_gaussian, _fake_deriv_gaussian][GKDB_RNG.choice([0, 1])]
    array = funct_r(ny) + 1j * funct_i(ny)
    return np.vstack([array for _ in range(nt)])

def _fake_3d_array(nz:int, ny: int, nt: int) -> np.array:
    return np.ones(shape=(nz, ny, nt))

def generate_fake_ids_properties() -> gkids.IdsProperties:
    ipn = gkids.IdsProvenanceNode(
        path="gkdb://uuid",
        sources=["gkdb", ])

    ids_prov = gkids.IdsProvenance(node=[ipn, ])
    return gkids.IdsProperties(
        comment="useless",
        homogeneous_time=0,
        provider="me",
        creation_date=datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
        provenance=ids_prov)


def generate_fake_collision(nspecies: int) -> gkids.Collisions:
    return gkids.Collisions(collisionality_norm=GKDB_RNG.uniform(-1, 1,
                                                                 size=(nspecies, nspecies)))


def generate_fake_flux_surface() -> gkids.FluxSurface:
    return gkids.FluxSurface(
        r_minor_norm=GKDB_RNG.uniform(.1, 10),
        elongation=GKDB_RNG.uniform(.1, 1),
        delongation_dr_minor_norm=GKDB_RNG.uniform(.1, 1),
        dgeometric_axis_r_dr_minor=GKDB_RNG.uniform(.1, 1),
        dgeometric_axis_z_dr_minor=GKDB_RNG.uniform(.1, 1),
        q=GKDB_RNG.uniform(1, 10),
        magnetic_shear_r_minor=GKDB_RNG.uniform(.1, 1),
        pressure_gradient_norm=GKDB_RNG.uniform(1, 100),
        ip_sign=GKDB_RNG.uniform(-1, 1),
        b_field_tor_sign=GKDB_RNG.uniform(-1, 1),
        shape_coefficients_c=GKDB_RNG.uniform(.1, 1, size=(4,)),
        dc_dr_minor_norm=GKDB_RNG.uniform(.1, 1, size=(4,)),
        shape_coefficients_s=GKDB_RNG.uniform(.1, 1, size=(4,)),
        ds_dr_minor_norm=GKDB_RNG.uniform(.1, 1, size=(4,)),
    )


def generate_fake_model() -> gkids.Model:
    return gkids.Model(
        include_centrifugal_effects=GKDB_RNG.choice([True, False]),
        include_a_field_parallel=GKDB_RNG.choice([True, False]),
        include_b_field_parallel=GKDB_RNG.choice([True, False]),
        include_full_curvature_drift=GKDB_RNG.choice([True, False]),
        include_coriolis_drift=GKDB_RNG.choice([True, False]),
        collisions_pitch_only=GKDB_RNG.choice([True, False]),
        collisions_momentum_conservation=GKDB_RNG.choice([True, False]),
        collisions_energy_conservation=GKDB_RNG.choice([True, False]),
        collisions_finite_larmor_radius=GKDB_RNG.choice([True, False]),
        adiabatic_electrons=GKDB_RNG.choice([True, False]),
    )


def generate_fake_input_species() -> gkids.InputSpeciesGlobal:
    return gkids.InputSpeciesGlobal(
        beta_reference=GKDB_RNG.uniform(.1, 10),
        velocity_tor_norm=GKDB_RNG.uniform(.1, 10),
       # zeff=GKDB_RNG.uniform(.1, 10),
        debye_length_norm=GKDB_RNG.uniform(.1, 10),
        shearing_rate_norm=GKDB_RNG.uniform(.1, 10),
        angle_pol = np.linspace(0, np.pi, 10),

    )


def generate_fake_input_normalizing() -> gkids.InputNormalizing:
    return gkids.InputNormalizing(
        t_e=GKDB_RNG.uniform(1, 42),
        n_e=GKDB_RNG.uniform(1, 42),
        r=GKDB_RNG.uniform(.1, 10),
        b_field_tor=GKDB_RNG.uniform(.1, 10),
    )


def generate_fake_code(fake_params: RunParameters) -> gkids.Code:
    return gkids.Code(
        name=GKDB_RNG.choice(CODE_LIST),
        commit="".join([str(x) for x in GKDB_RNG.choice(10, 10)]),
        version=".".join([str(x) for x in GKDB_RNG.choice(10, 3)]),
        repository="git://iter/repository",
        parameters=js.dumps(asdict(fake_params)),
        output_flag=[GKDB_RNG.choice([0, 1], p=[0.95, 0.05]), ],
        library=[generate_fake_lib() for _ in range(fake_params.nlibraries)]
    )


def generate_fake_code_partial_constant(fake_params: RunParameters) -> gkids.CodePartialConstant:
    return gkids.CodePartialConstant(
        output_flag=GKDB_RNG.choice([0, 1], p=[0.95, 0.05]),
        parameters=js.dumps(asdict(fake_params)),
    )


def generate_fake_lib() -> gkids.Library:
    return gkids.Library(
        name=GKDB_RNG.choice(LIBRARY_LIST),
        commit="".join([str(x) for x in GKDB_RNG.choice(10, 10)]),
        version=".".join([str(x) for x in GKDB_RNG.choice(10, 3)]),
        repository="git://iter/repository",
        parameters=js.dumps({"a": 4, "b": 2, "c": 42})
    )


def generate_fake_fluxes(n_species) -> gkids.Fluxes:
    return gkids.Fluxes(
        particles_phi_potential=GKDB_RNG.uniform(-10, 10, size=n_species),
        particles_a_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        particles_b_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        energy_phi_potential=GKDB_RNG.uniform(-10, 10, size=n_species),
        energy_a_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        energy_b_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        momentum_tor_parallel_phi_potential=GKDB_RNG.uniform(-10, 10,
                                                             size=n_species),
        momentum_tor_parallel_a_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        momentum_tor_parallel_b_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        momentum_tor_perpendicular_phi_potential=GKDB_RNG.uniform(-10, 10, size=n_species),
        momentum_tor_perpendicular_a_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
        momentum_tor_perpendicular_b_field_parallel=GKDB_RNG.uniform(-10, 10, size=n_species),
    )


def generate_fake_moments_linear(nspecies, ny, nt) -> gkids.MomentsLinear:
    return gkids.MomentsLinear(
        density=_fake_3d_array( nspecies,ny, nt),
        j_parallel=_fake_3d_array(nspecies, ny, nt),
        pressure_parallel=_fake_3d_array(nspecies, ny, nt),
        pressure_perpendicular=_fake_3d_array(nspecies, ny, nt),
        heat_flux_parallel=_fake_3d_array(nspecies, ny, nt),
        v_parallel_energy_perpendicular=_fake_3d_array(nspecies, ny, nt),
        v_perpendicular_square_energy=_fake_3d_array(nspecies, ny, nt),
    )

def generate_fake_fluxes_nl(listdims:list|tuple, fluxids:Any) -> Any:
    return fluxids(
        particles_phi_potential=np.ones(shape=listdims),
        particles_a_field_parallel=np.ones(shape=listdims),
        particles_b_field_parallel=np.ones(shape=listdims),
        energy_phi_potential=np.ones(shape=listdims),
        energy_a_field_parallel=np.ones(shape=listdims),
        energy_b_field_parallel=np.ones(shape=listdims),
        momentum_tor_parallel_phi_potential=np.ones(shape=listdims),
        momentum_tor_parallel_a_field_parallel=np.ones(shape=listdims),
        momentum_tor_parallel_b_field_parallel=np.ones(shape=listdims),
        momentum_tor_perpendicular_phi_potential=np.ones(shape=listdims),
        momentum_tor_perpendicular_a_field_parallel=np.ones(shape=listdims),
        momentum_tor_perpendicular_b_field_parallel=np.ones(shape=listdims),
    )

def generate_fake_fields_nl(listdims:list|tuple, fieldids:Any) -> Any:
    return fieldids(
        phi_potential_perturbed_norm=np.ones(shape=listdims),
        a_field_parallel_perturbed_norm=np.ones(shape=listdims),
        b_field_parallel_perturbed_norm=np.ones(shape=listdims),
    )

# def generate_fake_moments_particle(ny, nt) -> gkids.MomentsParticles:
#     return gkids.MomentsParticles(
#         density=_fake_2d_array(ny, nt),
#         j_parallel=_fake_2d_array(ny, nt),
#         pressure_parallel=_fake_2d_array(ny, nt),
#         pressure_perpendicular=_fake_2d_array(ny, nt),
#         heat_flux_parallel=_fake_2d_array(ny, nt),
#         v_parallel_energy_perpendicular=_fake_2d_array(ny, nt),
#         v_perpendicular_square_energy=_fake_2d_array(ny, nt),
#     )


# def generate_fake_fluxes_moments(ny, nt) -> gkids.FluxesMoments:
#     return gkids.FluxesMoments(
#         moments_norm_gyrocenter=generate_fake_moments(ny, nt),
#         moments_norm_particle=generate_fake_moments_particle(ny, nt),
#         fluxes_norm_gyrocenter=generate_fake_flux(),
#         fluxes_norm_gyrocenter_rotating_frame=generate_fake_flux(),
#         fluxes_norm_particle=generate_fake_flux(),
#         fluxes_norm_particle_rotating_frame=generate_fake_flux(),
#     )


def generate_fake_species() -> gkids.Species:
    return gkids.Species(
        charge_norm=GKDB_RNG.uniform(-10, 10),
        mass_norm=GKDB_RNG.uniform(.1, 10),
        density_norm=GKDB_RNG.uniform(.1, 10),
        density_log_gradient_norm=GKDB_RNG.uniform(.1, 10),
        temperature_norm=GKDB_RNG.uniform(.1, 10),
        temperature_log_gradient_norm=GKDB_RNG.uniform(.1, 10),
        velocity_tor_gradient_norm=GKDB_RNG.uniform(.1, 10),
        potential_energy_gradient_norm=GKDB_RNG.uniform(.1, 1, size=32),
        potential_energy_norm=GKDB_RNG.uniform(.1, 1, size=32),
    )


def generate_fake_eigenmode_fields(fake_data: RunParameters) -> gkids.EigenmodeFields:
    return gkids.EigenmodeFields(
        phi_potential_perturbed_weight=GKDB_RNG.uniform(0, 2,
                                                        size=fake_data.nt),
        phi_potential_perturbed_parity=GKDB_RNG.uniform(0, 2,
                                                        size=fake_data.nt),
        a_field_parallel_perturbed_weight=GKDB_RNG.uniform(0, 2,
                                                           size=fake_data.nt),
        a_field_parallel_perturbed_parity=GKDB_RNG.uniform(0, 2,
                                                           size=fake_data.nt),
        b_field_parallel_perturbed_weight=GKDB_RNG.uniform(0, 2,
                                                           size=fake_data.nt),
        b_field_parallel_perturbed_parity=GKDB_RNG.uniform(0, 2,
                                                           size=fake_data.nt),
        phi_potential_perturbed_norm=GKDB_RNG.uniform(0, 2,
                                                      size=(fake_data.n_angle_pol, fake_data.nt)),
        a_field_parallel_perturbed_norm=GKDB_RNG.uniform(0, 2,
                                                         size=(fake_data.n_angle_pol, fake_data.nt))
    )


def generate_fake_eigenmode(fake_data: RunParameters) -> gkids.Eigenmode:
    return gkids.Eigenmode(
        poloidal_turns=GKDB_RNG.choice(5),
        growth_rate_norm=GKDB_RNG.uniform(-2, 2),
        frequency_norm=GKDB_RNG.uniform(0, 100),
        growth_rate_tolerance=GKDB_RNG.uniform(0, 2),
        angle_pol=GKDB_RNG.uniform(0, 2,
                                   size=fake_data.n_angle_pol),
        time_norm=np.arange(fake_data.nt),
        fields=generate_fake_eigenmode_fields(fake_data),
        code=generate_fake_code_partial_constant(fake_data),
        initial_value_run=GKDB_RNG.choice([True, False]),
        moments_norm_gyrocenter=generate_fake_moments_linear(fake_data.nspecies, fake_data.n_angle_pol, fake_data.nt),
        moments_norm_particle=generate_fake_moments_linear(fake_data.nspecies, fake_data.n_angle_pol, fake_data.nt),
        moments_norm_gyrocenter_bessel_0=generate_fake_moments_linear(fake_data.nspecies, fake_data.n_angle_pol, fake_data.nt),
        moments_norm_gyrocenter_bessel_1=generate_fake_moments_linear(fake_data.nspecies, fake_data.n_angle_pol, fake_data.nt),
        linear_weights=generate_fake_fluxes(fake_data.nspecies),
        linear_weights_rotating_frame=generate_fake_fluxes(fake_data.nspecies),

    )

def generate_fake_non_linear(fake_data:RunParameters)->gkids.GyrokineticsNonLinear:
    return gkids.GyrokineticsNonLinear(
        binormal_wavevector_norm=GKDB_RNG.uniform(-2, 2, size=fake_data.ny),
        radial_wavevector_norm=GKDB_RNG.uniform(-2, 2, size=fake_data.nz),
        angle_pol = GKDB_RNG.uniform(1, 3, size=fake_data.n_angle_pol),
        time_norm=np.arange(fake_data.nt),
        time_interval_norm=np.arange(fake_data.nt),
        quasi_linear = rnd.choice([True, False]),
        code = generate_fake_code_partial_constant(fake_data),
        fluxes_5d = generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,
                                             fake_data.nz, fake_data.n_angle_pol, fake_data.nt ),
                                            gkids.FluxesNl5D),
        fluxes_4d = generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,
                                             fake_data.nz, fake_data.n_angle_pol, ),
                                            gkids.FluxesNl4D),
        fluxes_3d = generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,
                                             fake_data.nz, ),
                                            gkids.FluxesNl3D),
        fluxes_2d_k_x_sum = generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,),
                                            gkids.FluxesNl2DSumKx),
        fluxes_2d_k_x_k_y_sum = generate_fake_fluxes_nl((fake_data.nspecies,fake_data.nt,),
                                            gkids.FluxesNl2DSumKxKy),
        fluxes_1d = generate_fake_fluxes_nl((fake_data.nspecies,), gkids.FluxesNl1D),
        fields_4d=generate_fake_fields_nl((fake_data.ny,
                                             fake_data.nz, fake_data.n_angle_pol, fake_data.nt), gkids.GyrokineticsFieldsNl4D),
        fields_intensity_3d=generate_fake_fields_nl((fake_data.ny,
                                             fake_data.nz, fake_data.n_angle_pol), gkids.GyrokineticsFieldsNl3D),
        fields_intensity_2d_surface_average=generate_fake_fields_nl((fake_data.nz,
                                             fake_data.nt, ),  gkids.GyrokineticsFieldsNl2DFsAverage),
        fields_zonal_2d=generate_fake_fields_nl((fake_data.ny,fake_data.nz,),
                                                gkids.GyrokineticsFieldsNl2DKy0),
        fields_intensity_1d=generate_fake_fields_nl((fake_data.ny,),
                                                    gkids.GyrokineticsFieldsNl1D),
        fluxes_5d_rotating_frame=generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,
                                             fake_data.nz, fake_data.n_angle_pol, fake_data.nt ),
                                            gkids.FluxesNl5D),
        fluxes_4d_rotating_frame=generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,
                                             fake_data.nz, fake_data.n_angle_pol, ),
                                            gkids.FluxesNl4D),
        fluxes_3d_rotating_frame=generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,
                                             fake_data.nz, ),
                                            gkids.FluxesNl3D),
        fluxes_2d_k_x_sum_rotating_frame=generate_fake_fluxes_nl((fake_data.nspecies,fake_data.ny,),
                                            gkids.FluxesNl2DSumKx),
        fluxes_2d_k_x_k_y_sum_rotating_frame=generate_fake_fluxes_nl((fake_data.nspecies,fake_data.nt,),
                                            gkids.FluxesNl2DSumKxKy),
        fluxes_1d_rotating_frame=generate_fake_fluxes_nl((fake_data.nspecies,),gkids.FluxesNl1D),
    )

def generate_fake_wavevector(fake_data) -> gkids.Wavevector:
    return gkids.Wavevector(
        radial_wavevector_norm=GKDB_RNG.uniform(0, 10),
        binormal_wavevector_norm=GKDB_RNG.uniform(0, 10),
        eigenmode=[generate_fake_eigenmode(fake_data) for _ in range(fake_data.neigenmode)]
    )


def generate_fake_code_partial(fake_params) -> gkids.CodePartialConstant:
    return gkids.CodePartialConstant(parameters=js.dumps(asdict(fake_params)),
                                     output_flag=GKDB_RNG.choice([0, 1], p=[0.95, 0.05])
                                     )


def generate_fake_params(max_size: int = 16) -> RunParameters:
    return RunParameters(nx=rnd.randint(low=1, high=max_size),
                         ny=rnd.randint(low=1, high=max_size),
                         nz=rnd.randint(low=1, high=max_size),
                         nt=rnd.randint(low=1, high=max_size),
                         nvx=rnd.randint(low=1, high=max_size),
                         nvy=rnd.randint(low=1, high=max_size),
                         nvz=rnd.randint(low=1, high=max_size),
                         nspecies=rnd.randint(low=1, high=16),
                         nwavevector=rnd.randint(low=1, high=16),
                         neigenmode=rnd.randint(low=1, high=16),
                         ntags=rnd.randint(low=0, high=10),
                         dx=rnd.uniform(1e-6, 10),
                         dy=rnd.uniform(1e-6, 10),
                         dz=rnd.uniform(1e-6, 10),
                         dvx=rnd.uniform(1e-6, 10),
                         dvy=rnd.uniform(1e-6, 10),
                         dvz=rnd.uniform(1e-6, 10),
                         nlibraries=rnd.randint(low=0, high=4),
                         n_angle_pol=rnd.randint(low=1, high=16),
                         )


def generate_fake_tag() -> EntryTag:
    alphabet = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    np_alphabet = np.array(alphabet, dtype="|S1")
    np_codes = str(b"".join((rnd.choice(np_alphabet, 12),)))
    return EntryTag(
        tagid="".join(("#", *[str(x) for x in rnd.randint(0, 9, size=8)])),
        categorie=rnd.choice(CODE_LIST),
        description=np_codes
    )


def generate_rnd_ids_gk(fake_params: RunParameters | None = None) -> gkids.GyrokineticsLocal:
    if fake_params is None:
        fake_params = generate_fake_params()

    return gkids.GyrokineticsLocal(
    #    ids_properties=generate_fake_ids_properties(),
#        tag=[generate_fake_tag() for _ in range(fake_params.ntags)],
        normalizing_quantities=generate_fake_input_normalizing(),
        flux_surface=generate_fake_flux_surface(),
        model=generate_fake_model(),
        species_all=generate_fake_input_species(),
        species=[generate_fake_species() for _ in range(fake_params.nspecies)],
        collisions=generate_fake_collision(fake_params.nspecies),
        linear=gkids.GyrokineticsLinear(wavevector=[generate_fake_wavevector(fake_params) for _ in range(fake_params.nwavevector)]),
       # fluxes_integrated_norm=[generate_fake_fluxes(fake_params.nspecies) for _ in range(fake_params.nspecies)],
        code=generate_fake_code(fake_params),
        non_linear = generate_fake_non_linear(fake_params),
     #   time=[0.1, 0.2],
    )


if __name__ == "__main__":
    from mem_size import total_size

    # print(generate_fake_params().to_dict())
    gkids = generate_rnd_ids_gk()
    print(np.round(total_size(gkids) / (1024 ** 2)))
