import os
from dataclasses import dataclass, field
from functools import partial

PROD_SCHEMA_NAME = "gkdb_production"
DEV_SCHEMA_NAME = "gkdb_development"


@dataclass
class DBConfig:
    user: str = field(default_factory=partial(os.environ.get, 'GKDB_USER', 'FASTER'))
    password: str = field(default_factory=partial(os.environ.get,
                                                  'GKDB_PWD', 'FASTER'))
    host: str = field(default_factory=partial(os.environ.get,
                                              'GKDB_HOST', 'localhost'))
    port: str = field(default_factory=partial(os.environ.get,
                                              'GKDB_PORT', '5432'))
    database: str = field(default_factory=partial(os.environ.get,
                                                  'GKDB_DB_NAME', 'gkdb'))
    schema: str = field(default_factory=partial(os.environ.get,
                                                'GKDB_SCHEMA', 'gkdb_development'))
    # do we want to store moments into DB? default false
    #write_moments: bool = field(default=False)

    def make_connection_string(self):
        return f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}"
