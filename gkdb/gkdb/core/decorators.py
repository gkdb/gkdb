from __future__ import annotations

from psycopg import sql
from gkdb.config import DBConfig

from functools import wraps


GKDB_CFG = DBConfig()

def check_column_exists(cursor, parent_key_name: str, parent_table: str, schema: str) -> bool:
    # Check if parent_key_name exists in the table
    key_check_query = sql.SQL("""
    SELECT column_name FROM information_schema.columns WHERE 
    table_schema = {} AND 
    table_name = {} AND 
    column_name = {};
    """).format(sql.Literal(schema), sql.Literal(parent_table), sql.Literal(parent_key_name))

    cursor.execute(key_check_query)
    key_exists = cursor.fetchone()
    if not key_exists:
        raise AttributeError(f"{parent_key_name} does not exist in {schema}.{parent_table}")
    return True


def check_foreign_key_exists(cursor, foreign_key_values: int | list | tuple,
                             parent_key_name: str, parent_table: str, schema: str) -> bool:
    # check FK exists
    check_column_exists(cursor, parent_key_name, parent_table, schema)

    # Construct the SELECT sql_query to check for foreign key existence
    if not isinstance(foreign_key_values, (list, tuple)):
        foreign_key_values = tuple((foreign_key_values,))
    elif isinstance(foreign_key_values[0], tuple|list):
        foreign_key_values = foreign_key_values[0]

    select_query = sql.SQL("""
    SELECT {} FROM {}.{} WHERE {} IN ({});
    """).format(sql.Identifier(parent_key_name),
                sql.Identifier(schema),
                sql.Identifier(parent_table),
                sql.Identifier(parent_key_name),
                sql.SQL(",").join([sql.Literal(str(k)) for k in foreign_key_values])
                )

    # Execute the SELECT sql_query with the foreign key values as a tuple
    cursor.execute(select_query)

    # Fetch the primary key values that exist in the parent table
    existing_keys = set(row[0] for row in cursor.fetchall())
    # Check if all foreign keys exist in the parent table
    for foreign_key in foreign_key_values:
        if isinstance(foreign_key, str):
            key_as_int = int(foreign_key)
        else:
            key_as_int = foreign_key
        if key_as_int not in existing_keys:
            return False

    return True


def foreign_key_exists(parent_table: str, schema = GKDB_CFG.schema):
    def actual_decorator(function):
        @wraps(function)
        def wrapper(cursor, foreign_key_values: int | list | tuple,  *args, **kwargs):
            # Here you would call the check if key exists function
            parent_key_name = "id"
            # if isinstance(foreign_key_values, int):
            #     fk_to_check = foreign_key_values
            # elif isinstance(foreign_key_values[0], tuple|list):
            #     fk_to_check = foreign_key_values[0][0]
            # else:
            #     fk_to_check = foreign_key_values[0]
            if not check_foreign_key_exists(cursor, foreign_key_values, parent_key_name, parent_table, schema):
                raise ValueError(f"Foreign keys {foreign_key_values} is missing in the table {schema}.{parent_table}")

            # If everything is fine, call the decorated function
            result = function(cursor, foreign_key_values,  *args, **kwargs)
            return result

        return wrapper

    return actual_decorator

