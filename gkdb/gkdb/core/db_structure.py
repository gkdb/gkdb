
COLUMNS_FLUXES_TABLE = (
    "frame",
    "ftype",
    "uuid",
    "particles_phi_potential",
    "particles_a_field_parallel",
    "particles_b_field_parallel",
    "energy_phi_potential",
    "energy_a_field_parallel",
    "energy_b_field_parallel",
    "momentum_tor_parallel_phi_potential",
    "momentum_tor_parallel_a_field_parallel",
    "momentum_tor_parallel_b_field_parallel",
    "momentum_tor_perpendicular_phi_potential",
    "momentum_tor_perpendicular_a_field_parallel",
    "momentum_tor_perpendicular_b_field_parallel"
)

COLUMNS_MOMENTS_TABLE = (
    "mtype",
    "uuid",
    "norm",
    "density",
    "j_parallel",
    "pressure_parallel",
    "pressure_perpendicular",
    "heat_flux_parallel",
    "v_parallel_energy_perpendicular",
    "v_perpendicular_square_energy"
)

COLUMNS_EIGENMODE_TABLE = (
"wavevector_id",
"poloidal_turns",
"growth_rate_norm",
"frequency_norm",
"growth_rate_tolerance",
"phi_potential_perturbed_weight",
"phi_potential_perturbed_parity",
"a_field_parallel_perturbed_weight",
"a_field_parallel_perturbed_parity",
"b_field_parallel_perturbed_weight",
"b_field_parallel_perturbed_parity",
"poloidal_angle",
"phi_potential_perturbed_norm",
"a_field_parallel_perturbed_norm",
"b_field_parallel_perturbed_norm",
"time_norm",
"initial_value_run"
)