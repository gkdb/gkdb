from idspy_toolkit.exceptions import IdsBaseException

class IdsDimensionError(IdsBaseException):
    pass
class IdsForbiddenValue(IdsBaseException):
    pass
class IdsEmptyEntriesError(IdsBaseException):
    pass


class IdsMissingEntriesError(IdsBaseException):
    pass


class IdsWrongFormatError(IdsBaseException):
    pass


class IdsNotValid(IdsBaseException):
    """ exception raised when an IDS is not considered as valid """
    pass

