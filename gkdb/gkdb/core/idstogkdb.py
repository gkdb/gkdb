from __future__ import annotations

import copy
import psycopg.sql as sql
from gkdb.core.utils import dataclass_to_list, dict2pgjsonb
from gkdb.core.internal_dataclasses import ExtendedFluxes, ExtendedMoments


def __query_from_list(k: list, v: list, schema: str, table: str) -> sql.Composed:
    if isinstance(v[0], (tuple, list)):
        assert len(k) == len(v[0]), f"list size mismatch {len(k)} == {len(v[0])}"
        query = sql.SQL("""INSERT INTO {}.{} ({}) VALUES {}
            RETURNING id;
        """).format(
            sql.Identifier(schema),
            sql.Identifier(table),
            sql.SQL(",").join(k),
            sql.SQL(",").join([sql.SQL('({})').format(sql.SQL(",").join(i)) for i in v])
        )
    else:
        assert len(k) == len(v), "list size mismatch"
        query = sql.SQL("INSERT INTO {}.{} ({}) VALUES ({}) RETURNING id;").format(
            sql.Identifier(schema),
            sql.Identifier(table),
            sql.SQL(",").join(k),
            sql.SQL(",").join(v),
        )
    return query


def query_insert_gyrokinetic_table(schema_name: str = 'public') -> sql.Composed:
    request = sql.SQL("INSERT into {}.{} (time) VALUES ({}) RETURNING (id, uuid)").format(
        sql.Identifier(schema_name),
        sql.Identifier("gyrokinetics_table"),
        sql.Literal(0.0))
    return request


def query_insert_simple_ids(ids_tag, table: str, schema: str = "public",
                            fk_name: list | tuple | str = None,
                            fk_id: list | tuple | int = None) -> sql.Composed:
    list_keys, list_vals = dataclass_to_list(ids_tag)

    if fk_id is not None:
        if isinstance(fk_name, (list, tuple)):
            assert len(fk_name) == len(fk_id)
            list_keys.extend([sql.Identifier(x) for x in fk_name])
            list_vals.extend([sql.Literal(x) for x in fk_id])
        else:
            list_keys.append(sql.Identifier(fk_name))
            list_vals.append(sql.Literal(fk_id))
    return __query_from_list(list_keys, list_vals, schema, table)


def query_insert_species(ids_species, gk_id: int | None = None, schema_name: str = 'public'):
    table = "species_table"
    return query_insert_simple_ids(ids_species, table, schema_name, "gk_id", gk_id)


def query_insert_library(ids_lib, ck_id: int | None = None, schema: str = 'public'):
    table = "library_table"
    return query_insert_simple_ids(ids_lib, table, schema, "code_id", ck_id)


def query_insert_tag(ids_tag, schema_name: str = 'public'):
    table = "tag_table"
    return query_insert_simple_ids(ids_tag, table, schema_name)


def query_tag_link_gkid(tag_id, gk_id, schema: str = "public") -> sql.Composed:
    table = "ids_tags_table"
    query = sql.SQL("INSERT INTO {}.{} (gk_id, tag_id) VALUES ({}, {});").format(
        sql.Identifier(schema), sql.Identifier(table), sql.Literal(gk_id), sql.Literal(tag_id)
    )
    return query


def query_insert_code(ids_code, gk_id, schema: str = "public") -> sql.Composed:
    table = "code_table"
    return query_insert_simple_ids(ids_code, table, schema, "gk_id", gk_id)


def query_insert_eigenmode(ids_eig, wv_id, schema: str = "public") -> sql.Composed:
    table = "eigenmode_table"

    return query_insert_simple_ids(ids_eig, table, schema, "wavevector_id", wv_id)


def query_insert_code_partial(ids_code, gk_id: list | tuple,
                              schema: str = "public") -> sql.Composed:
    eig_id, code_id = gk_id[0][0], gk_id[1][0]
   # ids_code.parameters = dict2pgjsonb(ids_code.parameters)
    table = "code_partial_constant_table"
    return query_insert_simple_ids(ids_code, table, schema,
                                   ("eigenmode_id", "code_id"), (eig_id, code_id))


def query_insert_model(ids_model, gk_id: int | None = None,
                       schema_name: str = 'public'):
    table = "model_table"
    return query_insert_simple_ids(ids_model, table, schema_name, "gk_id", gk_id)


def query_insert_flux_surface(ids_flux_surface, gk_id: int | None = None,
                              schema_name: str = 'public'):
    table = "flux_surface_table"
    return query_insert_simple_ids(ids_flux_surface, table, schema_name, "gk_id", gk_id)


def query_insert_wavevector(ids_wavevector, gk_id: int | None = None,
                            schema_name: str = 'public'):
    table = "wavevector_table"
    return query_insert_simple_ids(ids_wavevector, table, schema_name, "gk_id", gk_id)


def query_insert_input_species(ids_inputspecies, gk_id: int | None = None,
                               schema_name: str = 'public'):
    table = "species_all_table"
    return query_insert_simple_ids(ids_inputspecies, table, schema_name, "gk_id", gk_id)


def query_insert_input_normalizing(ids_inputnorm, gk_id: int | None = None,
                                   schema_name: str = 'public'):
    table = "input_normalizing_table"
    return query_insert_simple_ids(ids_inputnorm, table, schema_name, "gk_id", gk_id)


def query_insert_ids_properties(ids_prop, gk_id: int | None = None,
                                schema_name: str = 'public'):
    table = "ids_properties_table"
    modified_ids = copy.deepcopy(ids_prop)
    if getattr(modified_ids, 'provenance', None) is not None:
        modified_ids.provenance = modified_ids.provenance.node
    return query_insert_simple_ids(modified_ids, table, schema_name, "gk_id", gk_id)


def query_insert_ids_provenance_node(ids_provenance_node, prop_id: int | None = None,
                                     schema_name: str = 'public'):
    table = "ids_provenance_node_table"
    if ids_provenance_node is None:
        return None
    return query_insert_simple_ids(ids_provenance_node, table, schema_name, "ids_properties_id", prop_id)


def query_insert_fluxes(ids_flux_ex: ExtendedFluxes, gk_id: int, eigenmode_id: int | None = None,
                        specie_id: int | None = None,
                        schema: str = "public") -> sql.Composed:
    table = "fluxes_table"

    if eigenmode_id is not None:
        return query_insert_simple_ids(ids_flux_ex, table, schema,
                                       ("gk_id", "eigenmode_id", "species_id"),
                                       (gk_id, eigenmode_id, specie_id)
                                       )
    else:
        return query_insert_simple_ids(ids_flux_ex, table, schema,
                                       "gk_id",
                                       gk_id,
                                       )


def query_insert_moments(ids_moments_ex: ExtendedMoments, gk_id: int, eigenmode_id: int,
                         specie_id: int,
                         schema: str = "public") -> sql.Composed:
    table = "moments_table"

    return query_insert_simple_ids(ids_moments_ex, table, schema,
                                   ("gk_id", "eigenmode_id", "species_id"),
                                   (gk_id, eigenmode_id, specie_id)
                                   )
