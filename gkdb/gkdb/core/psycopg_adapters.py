
import struct
from typing import Callable, cast, Union, Tuple, TypeAlias, Any
from enum import IntEnum
from psycopg.abc import AdaptContext
from psycopg.adapt import Loader, Dumper
from psycopg.types.composite import CompositeInfo, register_composite
import numpy as np
from ast import literal_eval


# directly imported from psycopg3.2
# numpy support should be added to next psycopg release, this part might be removed later
PackFloat: TypeAlias = Callable[[float], bytes]
UnpackFloat: TypeAlias = Callable[[bytes], Tuple[float]]

pack_float4 = cast(PackFloat, struct.Struct("!f").pack)
pack_float8 = cast(PackFloat, struct.Struct("!d").pack)

unpack_float4 = cast(UnpackFloat, struct.Struct("!f").unpack)
unpack_float8 = cast(UnpackFloat, struct.Struct("!d").unpack)

# An object implementing the buffer protocol
Buffer: TypeAlias = Union[bytes, bytearray, memoryview]


def pack_float4_bug_304(x: float) -> bytes:
    raise ValueError(
        "cannot dump Float4: Python affected by bug #304. Note that the psycopg-c"
        " and psycopg-binary packages are not affected by this issue."
        " See https://github.com/psycopg/psycopg/issues/304"
    )


# If issue #304 is detected, raise an error instead of dumping wrong data.
if struct.Struct("!f").pack(1.0) != bytes.fromhex("3f800000"):
    pack_float4 = pack_float4_bug_304


class Format(IntEnum):
    """
    Enum representing the format of a sql_query argument or return value.

    These values are only the ones managed by the libpq. `~psycopg` may also
    support automatically-chosen values: see `psycopg.adapt.PyFormat`.
    """

    __module__ = "psycopg.pq"

    TEXT = 0
    """Text parameter."""
    BINARY = 1
    """Binary parameter."""


FLOAT4_OID = 700
FLOAT8_OID = 701
BOOL_OID = 16

class _SpecialValuesDumper(Dumper):
    _special: dict[bytes, bytes] = {}

    def dump(self, obj: Any) -> bytes:
        return str(obj).encode()

    def quote(self, obj: Any) -> bytes:
        value = self.dump(obj)

        if value in self._special:
            return self._special[value]

        return value if obj >= 0 else b" " + value


class FloatDumper(_SpecialValuesDumper):
    oid = FLOAT8_OID

    _special = {
        b"inf": b"'Infinity'::float8",
        b"-inf": b"'-Infinity'::float8",
        b"nan": b"'NaN'::float8",
    }


class Float4Dumper(FloatDumper):
    oid = FLOAT4_OID


class FloatBinaryDumper(Dumper):
    format = Format.BINARY
    oid = FLOAT8_OID

    def dump(self, obj: float) -> bytes:
        return pack_float8(obj)


class Float4BinaryDumper(FloatBinaryDumper):
    oid = FLOAT4_OID

    def dump(self, obj: float) -> bytes:
        return pack_float4(obj)


class FloatLoader(Loader):
    def load(self, data: Buffer) -> float:
        # it supports bytes directly
        return float(data)


class Float4BinaryLoader(Loader):
    format = Format.BINARY

    def load(self, data: Buffer) -> float:
        return unpack_float4(data)[0]


class Float8BinaryLoader(Loader):
    format = Format.BINARY

    def load(self, data: Buffer) -> float:
        return unpack_float8(data)[0]


def register_default_adapters_float_numpy(context: AdaptContext) -> None:
    adapters = context.adapters
    adapters.register_dumper("numpy.float32", Float4Dumper)
    adapters.register_dumper("numpy.float64", FloatDumper)

    adapters.register_dumper("numpy.float32", Float4BinaryDumper)
    adapters.register_dumper("numpy.float64", FloatBinaryDumper)


# bool type dumper
class BoolDumper(Dumper):
    oid = BOOL_OID

    def dump(self, obj: bool) -> bytes:
        return b"t" if obj else b"f"

    def quote(self, obj: bool) -> bytes:
        return b"true" if obj else b"false"


class BoolBinaryDumper(Dumper):
    format = Format.BINARY
    oid = BOOL_OID

    def dump(self, obj: bool) -> bytes:
        return b"\x01" if obj else b"\x00"


class BoolLoader(Loader):
    def load(self, data: Buffer) -> bool:
        return data == b"t"


class BoolBinaryLoader(Loader):
    format = Format.BINARY

    def load(self, data: Buffer) -> bool:
        return data != b"\x00"


def register_default_adapters_bool(context: AdaptContext) -> None:
    adapters = context.adapters
    adapters.register_dumper(bool, BoolDumper)
    adapters.register_dumper(bool, BoolBinaryDumper)
    adapters.register_loader("bool", BoolLoader)
    adapters.register_loader("bool", BoolBinaryLoader)

# adapter to support the composite complex type
# must be used as parameter of the connection pool
class ComplexLoader(Loader):
    """

    The ComplexLoader class is a subclass of the Loader class from the psycopg module. It is used to load complex number data from a given input.

    Attributes:

    - None

    Methods:

    - load(data):
        This method takes in a data parameter which represents the complex number data to be loaded. The data should be passed as a string representation of a complex number. This method converts the string data into a complex number object using the literal_eval function from the ast module. It returns the loaded complex number.

    """
    def load(self, data):
        cplx = complex(*literal_eval(bytes(data).decode("utf8")))
        return np.complex64(cplx)


class ComplexDumper(Dumper):
    """
    The `ComplexDumper` class is a subclass of `Dumper` and provides functionality to convert a complex number to a string representation.

    Methods:
        - dump(elem: complex) -> bytes:
            This method takes a complex number as input and returns its string representation encoded as UTF-8 bytes.

    Usage:
        dumper = ComplexDumper()
        complex_number = complex(3, 4)
        complex_str = dumper.dump(complex_number)
        print(complex_str.decode("utf-8"))

    Output:
        (3, 4)
    """
    def dump(self, elem: complex):
        complex_str = str(tuple((np.float32(elem.real), np.float32(elem.imag),)))
        return complex_str.encode("utf-8")


def complex_type_register(conn):
    """
    Registers a complex type in the PostgreSQL database.

    Parameters:
    - cur: The connection to the PostgreSQL database.

    Returns:
    - cur: The same connection object after registering the complex type.
    """
    # complex type must me present in the DB for this to work
    info_composite_complex = CompositeInfo.fetch(conn, "complex")
    if not info_composite_complex:
        raise ValueError("The complex type is not registered.")
    register_composite(info_composite_complex, conn)
    conn.adapters.register_dumper(complex, ComplexDumper)
    conn.adapters.register_dumper("numpy.complex64", ComplexDumper)
    conn.adapters.register_loader(info_composite_complex.oid, ComplexLoader)
    register_default_adapters_float_numpy(conn)
    register_default_adapters_bool(conn)
    return conn
