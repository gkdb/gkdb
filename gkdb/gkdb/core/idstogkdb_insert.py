from __future__ import annotations

# for line_profiler, might be removed later
import builtins

import psycopg.errors

try:
    profile = builtins.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func):
        return func

from typing import Type, Any, Optional
from dataclasses import dataclass
import psycopg
import numpy as np
from psycopg_pool import ConnectionPool
from psycopg import sql
from idspy_dictionaries import ids_gyrokinetics_local as gkids

from gkdb.config import DBConfig
import gkdb.core.idstogkdb_queries as ids2gkdb
from gkdb.core.utils import dict2pgjsonb, int2bool, list2pgbytea, shorten_imas_default, compressed_nparray, \
    list_from_ids
from gkdb.core.psycopg_adapters import complex_type_register
from gkdb.core.utils import format_value_pg
from gkdb.core.constants_gkdb import (COLUMNS_EIGENMODE_TABLE, QUERY_COPY_LINEAR_WEIGHTS,
                                      COLUMNS_SPECIES_TABLE, COLUMNS_FLUXES_TABLE)
from gkdb.core.internal_dataclasses import CodeVersion, CODE_TYPE_LIST, CodeMin
from dataclasses import fields
from gkdb.core.exceptions import IdsNotValid, IdsEmptyEntriesError
import gkdb.ids_checks.gyrokinetics_ids_check as gk_checks
from gkdb.core.internal_dataclasses import NonLinearExtended

GKDB_CFG = DBConfig()
GKDB_CFG.write_moments = True


def _compress_with_type(value: Any):
    if isinstance(value, np.ndarray):
        return compressed_nparray(value)
    elif isinstance(value, dict):
        return dict2pgjsonb(value)
    else:
        return shorten_imas_default(value)


def _insert_collisions(cursor, fk_list: tuple[int] | list[int],
                       ids_collisions: gkids.Collisions, ) -> bool | None:
    """

    _insert_collisions(cur, fk_list: tuple[int | tuple[int]] | None, ids_collisions) -> list | None

    Inserts collision data into the database.

    Parameters:
    - cur : Connection
        A psycopg connection object.
    - fk_list : tuple[int | tuple[int]] | None
        A tuple representing foreign keys for the collision data. The first element is the foreign key for the gyrokinetics
        table, and the second element is a tuple of species identifiers.
    - ids_collisions : ExtendedCollisions
        An object representing the collision data to be inserted.

    Returns:
    - True if the insertion was successful. False otherwise

    Raises:
    - AttributeError
        1. If the collision table is not linked to a gyrokinetics table.
        2. If there is a length mismatch between the number of species and the collisionality_norm data.

    """

    def __pgcopy_collisions(cursor, flat_coll_array):
        with cursor.copy(
                sql.SQL("COPY {}.{} ({}, {}, {}) FROM  STDIN").format(
                    sql.Identifier(GKDB_CFG.schema),
                    sql.Identifier("collisions_table"),
                    *[sql.Identifier(x) for x in ("species_one", "species_two", "collisionality_norm")])
        ) as copy:
            for record in flat_coll_array:
                copy.write_row(record)

    id_species = fk_list[0]
    if id_species is None:
        raise IdsNotValid("cannot create a collision table without associated list of species")
    # first gk second species
    # if gk_id_fk is None:
    #     raise AttributeError("collision table must be link to a gyrokinetc table")

    n_species = len(id_species)
    if not gk_checks.check_collisions(ids_collisions, n_species=n_species):
        raise IdsNotValid("collisionality ids is not valid")
    # flatten it
    if isinstance(ids_collisions.collisionality_norm, (list, tuple)):
        collisionality = np.array(ids_collisions.collisionality_norm, dtype=np.float32)
    else:
        collisionality = ids_collisions.collisionality_norm

    flatten_collisionality = []
    for idx_s1 in range(len(id_species)):
        for idx_s2 in range(len(id_species)):
            flatten_collisionality.append((id_species[idx_s1],
                                           id_species[idx_s2], collisionality[idx_s2, idx_s1]))
    __pgcopy_collisions(cursor, flatten_collisionality)
    return True


def _insert_flux_surface(conn,
                         fk_list: tuple[int | tuple[int]] | None,
                         ids_fluxsurface) -> int | None:
    """

    Inserts a flux surface into the database.

    Parameters:
    - cur: psycopg_pool.ConnectionPool
        The connection pool to the database.
    - fk_list: tuple[int | tuple[int]] | None
        The foreign key list.
    - ids_fluxsurface: ids_gyrokinetics.IdsFluxSurface
        The flux surface object to be inserted.

    Returns:
    - int | None
        The ID of the inserted flux surface, or None if the insertion fails.

    """
    gk_id_fk = fk_list[0]

    if not gk_checks.check_flux_surface(ids_fluxsurface):
        raise IdsNotValid("Flux surface are not valid in current IDS.")

    ids_fluxsurface.shape_coefficients_c = list2pgbytea(ids_fluxsurface.shape_coefficients_c)
    ids_fluxsurface.dc_dr_minor_norm = list2pgbytea(ids_fluxsurface.dc_dr_minor_norm)
    ids_fluxsurface.shape_coefficients_s = list2pgbytea(ids_fluxsurface.shape_coefficients_s)
    ids_fluxsurface.ds_dr_minor_norm = list2pgbytea(ids_fluxsurface.ds_dr_minor_norm)
    ids_fluxsurface.ip_sign = float(ids_fluxsurface.ip_sign)
    ids_fluxsurface.b_field_tor_sign = float(ids_fluxsurface.b_field_tor_sign)
    query = ids2gkdb.query_insert_flux_surface(ids_fluxsurface, gk_id_fk, GKDB_CFG.schema)
    cur = conn.execute(query)
    return cur.fetchone()[0]


def _insert_code(conn,
                 fk_list: tuple[int | tuple[int]] | None,
                 ids_code: gkids.Code) -> int | None:
    """
    Inserts a code and its associated libraries into the database.

    Parameters:
        conn: The database connection.
        fk_list: The foreign key list.
        ids_code: The code to be inserted.

    Returns:
        The ID of the inserted code or None if the insertion fails.
    """
    if not gk_checks.check_code(ids_code):
        raise IdsNotValid("current code ids is not valid and cannot be inserted into the GKDB database.")
    code_id = None

    gk_id_fk = fk_list[0]
    # convert the IMAS code IDS to a GKDB "IDS like" code
    # libraries will be extracted and put in other instances of the CodeVersion dataclass
    new_code = CodeVersion(description=ids_code.description,
                           name=ids_code.name,
                           release_version=ids_code.version,
                           commit=ids_code.commit,
                           repository=ids_code.repository,
                           #compilation_arguments=None,
                           type=CODE_TYPE_LIST['code'])

    # code parameters are now in the linear/nonlinear ids_to_check
    code_parameters = dict2pgjsonb(ids_code.parameters)

    # insert code in DB and get back the associated id to put as an FK in the code instance
    query = ids2gkdb.query_insert_code_version(new_code, GKDB_CFG.schema)
    cur = conn.execute(query)
    code_version_id_fk = cur.fetchone()[0]

    # insert into the code table with an FK to the main IDS and an FK to the code_version entity
    query = ids2gkdb.query_insert_code(None, gk_id_fk,
                                       code_version_id_fk, GKDB_CFG.schema)
    cur = conn.execute(query)
    code_id = cur.fetchone()[0]
    # add libraries linked to the code
    if ids_code.library is not None:
        if isinstance(ids_code.library, (list, tuple)) is False:
            ids_list = [ids_code.library, ]
        else:
            ids_list = ids_code.library

        for lib in ids_list:
            new_lib_param = CodeMin(parameters=dict2pgjsonb(lib.parameters))
            new_library = CodeVersion(description=lib.description,
                                      name=lib.name,
                                      release_version=lib.version,
                                      commit=lib.commit,
                                      repository=lib.repository,
                                      #compilation_arguments=None,
                                      type=CODE_TYPE_LIST['library'])

            lib.parameters = dict2pgjsonb(lib.parameters)

            # insert code in DB and get back the associated id to put as an FK in the code instance
            query = ids2gkdb.query_insert_code_version(new_library, GKDB_CFG.schema)
            cur = conn.execute(query)
            lib_version_id_fk = cur.fetchone()[0]

            query = ids2gkdb.query_insert_library(new_lib_param,
                                                  lib_version_id_fk,
                                                  code_id, schema=GKDB_CFG.schema)
            conn.execute(query)

    return code_id


def __non_linear_to_pg_format(ids_non_linear: gkids.GyrokineticsNonLinear, ) -> NonLinearExtended:
    return NonLinearExtended(

        radial_wavevector_max=None,
        binormal_wavevector_max=None,
        radial_wavevector_interval=None,
        binormal_wavevector_interval=None,
        angle_pol_interval_per_turn=None,
        time_norm_max=None,
        code_parameters=None,
        quasi_linear=None,
        fluxes_1d = None,
        fluxes_2d_k_x_sum = None,
        fluxes_2d_k_x_k_y_sum = None,
        fields_intensity_1d=None,
        fields_zonal_2d = None,
        fields_intensity_2d_surface_average = None,
        fluxes_1d_rotating_frame=None,
        fluxes_2d_k_x_sum_rotating_frame=None,
        fluxes_2d_k_x_k_y_sum_rotating_frame=None,


    )


def _insert_non_linear(cur, fk_list: tuple[int | tuple[int]],
                       ids_non_linear: gkids.GyrokineticsNonLinear,
                       ids_species: tuple[gkids.Species] | None,
                       uuid_gkid) -> bool | None:

    def __copy_nonlinear_fluxes_1d(linear_weights_input, frame, gk_id_fk, list_species, id_non_linear):
        list_non_linear_fluxes_1d = list_from_ids(linear_weights_input)
        if list_non_linear_fluxes_1d is None:
            raise IdsEmptyEntriesError("linear weights are empty, nothing to insert in DB")
        assert len([1 for _ in fields(list_non_linear_fluxes_1d[0])]) \
               == len(COLUMNS_FLUXES_TABLE), \
            "Linear weights number of columns does not match with associated imas ids"
        assert len(list_non_linear_fluxes_1d) == len(cleaned_id_species), (f"size mismatch between "
                                                                     f"non linear fluxes 1D {len(list_non_linear_fluxes_1d)} "
                                                                     f"and number of species {len(cleaned_id_species)}!")
        for species_id, linear_weights in zip(list_species,
                                              list_non_linear_fluxes_1d):
            copy.write_row(
                (gk_id_fk,
                 species_id,
                 id_non_linear,
                 frame,
                 *[(getattr(linear_weights, name, None)) for name in
                   COLUMNS_FLUXES_TABLE]))


    cleaned_id_species = tuple([x for x in ids_species if x])
    if not gk_checks.check_non_linear_structure(ids_non_linear, len(cleaned_id_species)):
        raise IdsNotValid("non linear ids is not valid, cannot insert non linear part into the GKDB database.")

    # need gk as first and code as second
    gk_id_fk = fk_list[0][0]
    code_id = fk_list[1][0]

    if code_id is None:
        raise AttributeError("current IDS does not have a root code associated, skipping insertion.")

    if gk_id_fk is None:
        raise AttributeError("current IDS does not have a root IDS associated, skipping insertion.")

    list_fluxes_1d = list_from_ids(ids_non_linear.fluxes_1d)
    return True


def _insert_linear(cur, fk_list: tuple[int | tuple[int]],
                   ids_linear: gkids.GyrokineticsLinear,
                   ids_species: tuple | None,
                   uuid_gkid) -> bool | None:
    cleaned_id_species = tuple([x for x in ids_species if x])
    if not gk_checks.check_linear_structure(ids_linear, len(cleaned_id_species)):
        raise IdsNotValid("wavevector is not valid, cannot insert wavevector into the GKDB database.")

    _insert_wavevector(cur, fk_list, ids_linear.wavevector, ids_species, uuid_gkid)
    return True


def _insert_wavevector(cur, fk_list: tuple[int | tuple[int]],
                       ids_wavevector: list | tuple | None,
                       ids_species: tuple | None,
                       uuid_gkid) -> bool | None:
    """

    This method, _insert_wavevector, is used to insert wavevector information into the gyrokinetics database (gkdb).

    Parameters:
    - cur: A psycopg connection object used to connect to the database.
    - fk_list: A tuple containing two elements. The first element is an integer representing the foreign key ID for the gyrokinetics table. The second element is an integer representing the foreign key ID for the code table.
    - ids_wavevector: A list or tuple containing wavevector objects or None.

    Returns:
    - A tuple containing an integer and a dictionary or None.
      - The integer represents the status of the insertion process. It can be 1 if the insertion is successful or -1 if there is an error.
      - The dictionary contains the IDs of the inserted wavevectors and their corresponding eigenmodes.

    """

    # need gk as first and code as second
    gk_id_fk = fk_list[0][0]
    code_id = fk_list[1][0]

    if code_id is None:
        raise AttributeError("current IDS does not have a root code associated")

    if not isinstance(ids_wavevector, (list, tuple)):
        print(f"a list of wavevector must be used as argument not a {type(ids_wavevector)}")
        return -1, {}
    cleaned_id_species = tuple([x for x in ids_species if x])

    for wavevector in ids_wavevector:
        if not gk_checks.check_wavevector(wavevector, len(cleaned_id_species)):
            raise IdsNotValid("wavevector is not valid, cannot insert wavevector into the GKDB database.")
    id_wavevector = []
    # dict containing, for each wavevector_id, a list of all the corresponding eigenmodes

    # generate a list of wavevector to insert
    wv_list = []
    for wv in ids_wavevector:
        wv_list.append([gk_id_fk, wv.radial_wavevector_norm, wv.binormal_wavevector_norm])
    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", "radial_wavevector_norm", "binormal_wavevector_norm")],
        wv_list,
        GKDB_CFG.schema,
        "linear_table")
    id_wavevector = cur.execute(query)
    id_wavevector = [int(x[0]) for x in id_wavevector]

    # generate corresponding eigenmodes
    eigenvector_list_flat = []

    for wv_id, wv in zip(id_wavevector, ids_wavevector):
        for eig in wv.eigenmode:

            eig.initial_value_run = int2bool(eig.initial_value_run)
            print(eig.initial_value_run)
            if getattr(eig, 'code', None) is not None:
                code_param = dict2pgjsonb(eig.code.parameters)
            else:
                code_param = dict2pgjsonb({})
            is_eig_valid, has_linear, has_linear_rot = gk_checks.check_eigenmode(eig)
            if not is_eig_valid:
                raise IdsNotValid("eigenmode in current IDS is not valid")
            eigenvector_list_flat.append(
                (gk_id_fk,
                 wv_id,
                 code_param,
                 *[format_value_pg(getattr(eig, name, None)) for name in COLUMNS_EIGENMODE_TABLE]
                 )
            )
    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", "linear_id",
                                     "code_parameters", *COLUMNS_EIGENMODE_TABLE)],
        eigenvector_list_flat,
        GKDB_CFG.schema,
        "eigenmode_table")

    id_eigenmode = cur.execute(query)
    id_eigenmode = [int(x[0]) for x in id_eigenmode]

    def __copy_linear_weights(linear_weights_input, frame, gk_id_fk, list_species, id_eig):
        list_linear_weights = list_from_ids(linear_weights_input)
        if list_linear_weights is None:
            raise IdsEmptyEntriesError("linear weights are empty, nothing to insert in DB")
        assert len([1 for _ in fields(list_linear_weights[0])]) \
               == len(COLUMNS_FLUXES_TABLE), \
            (f"Linear weights number of columns does not match with "
             f"associated imas ids {len([1 for _ in fields(list_linear_weights[0])]), len(COLUMNS_FLUXES_TABLE)}")
        assert len(list_linear_weights) == len(cleaned_id_species), (f"size mismatch between "
                                                                     f"linear weights {len(list_linear_weights)} "
                                                                     f"and number of species {len(cleaned_id_species)}!")
        for species_id, linear_weights in zip(list_species,
                                              list_linear_weights):
            copy.write_row(
                (gk_id_fk,
                 species_id,
                 id_eig,
                 frame,
                 *[(getattr(linear_weights, name, None)) for name in
                   COLUMNS_FLUXES_TABLE]))

    # copy linear weights
    try:
        with cur.copy(QUERY_COPY_LINEAR_WEIGHTS) as copy:
            idx_eig = 0
            for wv in ids_wavevector:
                for idx_eig, eig in enumerate(wv.eigenmode):
                    try:
                        if has_linear:
                            # linear_weights are arrays of fields fluxes with number of species as size
                            # has to convert it to a list of dataclass having fluxes as members
                            __copy_linear_weights(eig.linear_weights, "laboratory", gk_id_fk, cleaned_id_species,
                                                  id_eigenmode[idx_eig])
                        if has_linear_rot:
                            __copy_linear_weights(eig.linear_weights_rotating_frame, "rotating",
                                                  gk_id_fk,
                                                  cleaned_id_species,
                                                  id_eigenmode[idx_eig])
                    except IdsEmptyEntriesError:
                        continue
    except psycopg.errors.CheckViolation as e:
        print(e)
        raise IdsNotValid(f"current ids with uuid {uuid_gkid} has inconsistency in linear weights, skipped")

    # idx_eig = 0
    # with cur.copy(
    #         sql.SQL("COPY {}.{} ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}) FROM  STDIN").format(
    #             sql.Identifier(GKDB_CFG.schema),
    #             sql.Identifier("moments_table"),
    #             *[sql.Identifier(x) for x in ("gk_id", "species_id", "eigenmode_id", *columns_wv_moments)])
    # ) as copy:
    #     for wv in ids_wavevector:
    #         for eig in wv.eigenmode:
    #             if eig.fluxes_moments is not None:
    #                 for specie_id, fluxmoment in zip(cleaned_id_species,
    #                                                  eig.fluxes_moments):
    #                     list_moments, list_flux = __fluxes_moment_as_list(fluxmoment, uuid_gkid)
    #                     for idx in (0, 1, 2):
    #                         copy.write_row(
    #                             (gk_id_fk,
    #                              specie_id,
    #                              id_eigenmode[idx_eig],
    #                              list_moments[idx].mtype,
    #                              list_moments[idx].uuid,
    #                              list_moments[idx].norm,
    #                              *[compressed_nparray(getattr(list_moments[idx], name, None)) for name in
    #                                columns_wv_moments[3:]]))
    #             idx_eig += 1

    return True


@profile
def _insert_model(conn, fk_list: tuple[int | tuple[int]] | None, ids_model) -> int | None:
    """
    Inserts a model into the database.

    Parameters:
    - cur: psycopg.PoolConnection - The connection to the database.
    - fk_list: tuple[int | tuple[int]] | None - The foreign key list.
    - ids_model: Any - The model to be inserted.

    Returns:
    - int | None - The ID of the inserted model or None if the model is None.

    """
    gk_id_fk = fk_list[0]
    if not gk_checks.check_model(ids_model):
        raise IdsNotValid("current species is not valid and cannot be inserted into GKDB")

    ids_model.include_centrifugal_effects = int2bool(ids_model.include_centrifugal_effects)
    ids_model.include_a_field_parallel = int2bool(ids_model.include_a_field_parallel)
    ids_model.include_b_field_parallel = int2bool(ids_model.include_b_field_parallel)
    ids_model.include_full_curvature_drift = int2bool(ids_model.include_full_curvature_drift)
    ids_model.collisions_pitch_only = int2bool(ids_model.collisions_pitch_only)
    ids_model.collisions_momentum_conservation = int2bool(ids_model.collisions_momentum_conservation)
    ids_model.collisions_energy_conservation = int2bool(ids_model.collisions_energy_conservation)
    ids_model.collisions_finite_larmor_radius = int2bool(ids_model.collisions_finite_larmor_radius)
    ids_model.adiabatic_electrons = int2bool(ids_model.adiabatic_electrons)
    ids_model.include_coriolis_drift = int2bool(ids_model.include_coriolis_drift)

    query = ids2gkdb.query_insert_model(ids_model, gk_id_fk, GKDB_CFG.schema)
    return conn.execute(query).fetchone()[0]


def _insert_species(cur, fk_list: tuple[int | tuple[int]] | None,
                    ids_species: list) -> tuple[bool, Optional[tuple[int]]]:
    """
    Inserts species into the gyrokinetics table.

    Parameters:
        cursor (ConnectionPool): The connection pool to the database.
        fk_list (tuple[int | tuple[int]] | None): The foreign key list.
        ids_species (list): The list of species to insert.

    Returns:
        tuple[int, list[int] | None]: A tuple containing the result code and the list of inserted species IDs or None.

    Raises:
        Exception: If ids_species is not a list or tuple.

    Example usage:
        cur = ConnectionPool()
        fk_list = (1, (2, 3))
        ids_species = [1, 2, 3]
        result = _insert_species(cur, fk_list, ids_species)
    """
    gk_id_fk = fk_list[0]
    if ids_species is None:
        return False, None

    if not isinstance(ids_species, (list, tuple)):
        raise AttributeError(f"ids_species must be a list or tuple not a {type(ids_species)}")

    flatten_species = []
    for s in ids_species:
        if not gk_checks.check_species(s):
            raise IdsNotValid("current species is not valid and cannot be inserted into GKDB")
        assert len(s.potential_energy_norm) == len(s.potential_energy_gradient_norm)
        flatten_species.append(
            (gk_id_fk, *[_compress_with_type(getattr(s, name, None)) for name in COLUMNS_SPECIES_TABLE]))

    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", *COLUMNS_SPECIES_TABLE)],
        flatten_species,
        GKDB_CFG.schema,
        "species_table")
    id_species = cur.execute(query)
    id_species = tuple([int(x[0]) for x in id_species])
    assert len(id_species) == len(ids_species), "some species were not added in the DB"
    return True, id_species or None
