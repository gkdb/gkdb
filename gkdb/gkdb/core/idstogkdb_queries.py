from __future__ import annotations

import copy
import psycopg.sql as sql
from gkdb.core.utils import dataclass_to_list, dict2pgjsonb
from gkdb.core.internal_dataclasses import ModifiedFluxes1D, ModifiedFluxes2D, ExtendedMomentsLinear, CodeMin


def __query_from_list(k: list, v: list, schema: str, table: str) -> sql.Composed:
    if isinstance(v[0], (tuple, list)):
        assert len(k) == len(v[0]), f"list size mismatch {len(k)} == {len(v[0])}"
        query = sql.SQL("""INSERT INTO {}.{} ({}) VALUES {}
            RETURNING id;
        """).format(
            sql.Identifier(schema),
            sql.Identifier(table),
            sql.SQL(",").join(k),
            sql.SQL(",").join([sql.SQL('({})').format(sql.SQL(",").join(i)) for i in v])
        )
    else:
        assert len(k) == len(v), "list size mismatch"
        query = sql.SQL("INSERT INTO {}.{} ({}) VALUES ({}) RETURNING id;").format(
            sql.Identifier(schema),
            sql.Identifier(table),
            sql.SQL(",").join(k),
            sql.SQL(",").join(v),
        )
    return query


def __query_from_list_with_conflict(k: list, v: list, k_conflict: list | None,
                                    schema: str, table: str) -> sql.Composed:
    assert len(k) == len(v), "list size mismatch"

    positions_values = [k.index(item) for item in k_conflict]
    # Create a list of conditions for the WHERE clause
    conditions = []
    for name, value in zip(k_conflict, positions_values):
        conditions.append(sql.SQL("{} = {}").format(name,
                                                    v[value]))

    # Join the conditions using AND
    where_clause = sql.SQL(" AND ").join(conditions)

    query = sql.SQL("WITH inserted_id AS ("
                    "INSERT INTO {schema}.{table} ({columns}) VALUES ({values})"
                    " ON CONFLICT({conflicts}) DO NOTHING RETURNING id) "
                    " SELECT id FROM inserted_id "
                    " UNION ALL "
                    " SELECT id FROM {schema}.{table} WHERE {cond} ;").format(
        schema=sql.Identifier(schema),
        table=sql.Identifier(table),
        columns=sql.SQL(",").join(k),
        values=sql.SQL(",").join(v),
        conflicts=sql.SQL(",").join(k_conflict),
        cond=where_clause,
    )
    return query


def query_insert_gyrokinetics_local_table(schema_name: str = 'public') -> sql.Composed:
    request = sql.SQL("INSERT into {}.{} DEFAULT VALUES RETURNING (id, uuid)").format(
        sql.Identifier(schema_name),
        sql.Identifier("gyrokinetics_local_table"),
        sql.Literal(0.0))
    return request


def query_insert_simple_ids(ids_tag, table: str, schema: str = "public",
                            fk_name: list | tuple | str = None,
                            fk_id: list | tuple | int = None) -> sql.Composed:
    list_keys, list_vals = dataclass_to_list(ids_tag)

    if fk_id is not None:
        if isinstance(fk_name, (list, tuple)):
            assert len(fk_name) == len(fk_id)
            if None in fk_id:
                raise ValueError("a foreign key cannot be None")
            list_keys.extend([sql.Identifier(x) for x in fk_name])
            list_vals.extend([sql.Literal(x) for x in fk_id])
        else:
            list_keys.append(sql.Identifier(fk_name))
            list_vals.append(sql.Literal(fk_id))
    return __query_from_list(list_keys, list_vals, schema, table)


def query_insert_simple_ids_with_conflicts(ids_tag,
                                           conflict_list: list | tuple,
                                           table: str,
                                           schema: str = "public",
                                           fk_name: list | tuple | str = None,
                                           fk_id: list | tuple | int = None) -> sql.Composed:
    list_keys, list_vals = dataclass_to_list(ids_tag)
    list_conflicts = [sql.Identifier(k) for k in conflict_list]

    if fk_id is not None:
        if isinstance(fk_name, (list, tuple)):
            assert len(fk_name) == len(fk_id)
            list_keys.extend([sql.Identifier(x) for x in fk_name])
            list_vals.extend([sql.Literal(x) for x in fk_id])
        else:
            list_keys.append(sql.Identifier(fk_name))
            list_vals.append(sql.Literal(fk_id))
    return __query_from_list_with_conflict(list_keys, list_vals, list_conflicts, schema, table)


def query_insert_species(ids_species, gk_id: int | None = None, schema_name: str = 'public'):
    table = "species_table"
    if gk_id is None:
        raise AssertionError("the foreign key to the gyrokinetic table is required")
    return query_insert_simple_ids(ids_species, table, schema_name, "gk_id", gk_id)


def query_insert_library(ids_lib: CodeMin,
                         version_id: int,
                         ck_id: int | None = None,
                         schema: str = 'public'):
    table = "library_table"
    return query_insert_simple_ids(ids_lib, table, schema,
                                   ("version_id", "code_id"),
                                   (version_id, ck_id)
                                   )


def query_insert_tag(ids_tag, schema_name: str = 'public'):
    table = "tag_table"
    return query_insert_simple_ids(ids_tag, table, schema_name)


def query_tag_link_gkid(tag_id, gk_id, schema: str = "public") -> sql.Composed:
    table = "ids_tags_table"
    query = sql.SQL("INSERT INTO {}.{} (gk_id, tag_id) VALUES ({}, {});").format(
        sql.Identifier(schema), sql.Identifier(table), sql.Literal(gk_id), sql.Literal(tag_id)
    )
    return query


def query_insert_code(ids_code: CodeMin,
                      gk_id: int, code_version_id: int,
                      schema: str = "public") -> sql.Composed:
    table = "code_table"
    if gk_id is None:
        raise AssertionError("the foreign key to the gyrokinetic table is required")
    if code_version_id is None:
        raise AssertionError("the foreign key to the code_version table is required")

    return query_insert_simple_ids(ids_code, table, schema,
                                   ("gk_id", "version_id"),
                                   (gk_id, code_version_id))


def query_insert_code_version(ids_code, schema: str = "public") -> sql.Composed:
    table = "code_version_table"
    unique_list = ('name', 'commit', "release_version")
    return query_insert_simple_ids_with_conflicts(ids_code, unique_list,
                                                  table, schema)


def query_insert_eigenmode(ids_eig, wv_id, schema: str = "public") -> sql.Composed:
    table = "eigenmode_table"
    if not wv_id:
        raise ValueError("wv_id is required and cannot be None")
    return query_insert_simple_ids(ids_eig, table, schema, "wavevector_id", wv_id)


def query_insert_code_partial(ids_code, gk_id: list | tuple,
                              schema: str = "public") -> sql.Composed:
    eig_id, code_id = gk_id[0][0], gk_id[1][0]
    # ids_code.parameters = dict2pgjsonb(ids_code.parameters)
    table = "code_partial_constant_table"
    return query_insert_simple_ids(ids_code, table, schema,
                                   ("eigenmode_id", "code_id"), (eig_id, code_id))


def query_insert_model(ids_model, gk_id: int | None = None,
                       schema_name: str = 'public'):
    table = "model_table"
    return query_insert_simple_ids(ids_model, table, schema_name, "gk_id", gk_id)


def query_insert_flux_surface(ids_flux_surface, gk_id: int | None = None,
                              schema_name: str = 'public'):
    table = "flux_surface_table"
    return query_insert_simple_ids(ids_flux_surface, table, schema_name, "gk_id", gk_id)


def query_insert_wavevector(ids_wavevector, gk_id: int | None = None,
                            schema_name: str = 'public'):
    table = "wavevector_table"
    return query_insert_simple_ids(ids_wavevector, table, schema_name, "gk_id", gk_id)


def query_insert_input_species(ids_inputspecies, gk_id: int | None = None,
                               schema_name: str = 'public'):
    table = "input_species_global_table"
    return query_insert_simple_ids(ids_inputspecies, table, schema_name, "gk_id", gk_id)


def query_insert_input_normalizing(ids_inputnorm, gk_id: int | None = None,
                                   schema_name: str = 'public'):
    table = "input_normalizing_table"
    return query_insert_simple_ids(ids_inputnorm, table, schema_name, "gk_id", gk_id)


def query_insert_fluxes_non_linear_1d(ids_flux_ex: ModifiedFluxes1D, gk_id: int,
                                      non_linear_run_id: int,
                                      species_id: int,
                                      schema: str = "public") -> sql.Composed:
    table = "fluxes_1d_table"

    return query_insert_simple_ids(ids_flux_ex, table, schema,
                                   ("gk_id", "non_linear_run_id", "species_id"),
                                   (gk_id, non_linear_run_id, species_id),
                                   )


def query_insert_fluxes_non_linear_2d(ids_flux_ex: ModifiedFluxes2D, gk_id: int,
                                      non_linear_run_id: int,
                                      specie_id: int,
                                      schema: str = "public") -> sql.Composed:
    table = "fluxes_2d_table"

    return query_insert_simple_ids(ids_flux_ex, table, schema,
                                   ("gk_id", "non_linear_run_id", "species_id"),
                                   (gk_id, non_linear_run_id, specie_id),
                                   )


def query_insert_moments(ids_moments_ex: ExtendedMomentsLinear, gk_id: int, eigenmode_id: int,
                         specie_id: int,
                         schema: str = "public") -> sql.Composed:
    table = "moments_table"

    return query_insert_simple_ids(ids_moments_ex, table, schema,
                                   ("gk_id", "eigenmode_id", "species_id"),
                                   (gk_id, eigenmode_id, specie_id)
                                   )
