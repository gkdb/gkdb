from sqlalchemy import create_engine, Integer, String, DateTime, Text, ForeignKey, \
    Float, Boolean, inspect, Table, Column, SmallInteger, text, BigInteger, REAL, Date, \
    Index
from sqlalchemy.orm import relationship, sessionmaker, column_property
from datetime import datetime
from sqlalchemy import DateTime, String, Text, UniqueConstraint, CheckConstraint
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.sql import func
from typing import Optional
from sqlalchemy.dialects.postgresql import UUID as pgUUID
from sqlalchemy.dialects.postgresql import ARRAY, JSONB, BYTEA
from sqlalchemy.sql.expression import and_, or_, not_
from dataclasses import dataclass
import gkdb.core.constants as constants
import os

import sqlalchemy as sa

from sqlalchemy.types import UserDefinedType


# rajouter le type pour les moments et flux pour savoir ce que c'est


class PGComplexWrapper(UserDefinedType):
    """
    This class implements a custom SQLAlchemy UserDefinedType for storage and retrieval of complex numbers in a PostgreSQL database.

    Attributes:
        N/A

    Methods:
        - get_col_spec: Returns the column specification for the custom complex type.
        - bind_processor: Returns a processor function that converts a complex number to a tuple of its real and imaginary parts.
        - result_processor: Returns a processor function that converts a tuple of real and imaginary parts to a complex number.

    Example usage:
        # Creating a table with a column of type PGComplexWrapper
        class MyTable(Base):
            __tablename__ = 'my_table'
            id = Column(Integer, primary_key=True)
            complex_number = Column(PGComplexWrapper)

        # Inserting a complex number into the table
        session.add(MyTable(complex_number=3+4j))
        session.commit()

        # Retrieving the complex number from the table
        my_instance = session.sql_query(MyTable).first()
        print(my_instance.complex_number)  # Output: (3+4j)
    """

    def get_col_spec(self):
        return "complex"

    def bind_processor(self, dialect):
        def process(value):
            if value is not None:
                return (value.real, value.imag)
            return None

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is not None:
                return complex(*(map(float, value[1:-1].split(','))))
            return None

        return process


class PGTupleWrapper(UserDefinedType):
    def get_col_spec(self):
        return "complex"

    def bind_processor(self, dialect):
        def process(value):
            if value is not None:
                return (value[0], value[1])
            return None

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is not None:
                return tuple(*(map(float, value[1:-1].split(','))))
            return None

        return process


metadata_obj = sa.MetaData(schema="gkdb_development")


class IdsBaseSchema(DeclarativeBase):
    metadata = metadata_obj


@dataclass
class Complex:
    r: float
    i: float


# class Complex(IdsBaseSchema):
#     __tablename__ = 'complex'
#     id = sa.Column(Integer, primary_key=True)
#     balance = sa.Column(
#         CompositeType(
#             'complex_type',
#             [
#                 sa.Column('r', REAL),
#                 sa.Column('i', Integer)
#             ]
#         )
#     )

GK_ROOT_TABLE_ID = "gyrokinetics_local_table.id"

ids_tags_table = Table(
    "ids_tags_table",
    IdsBaseSchema.metadata,
    Column("gk_id", BigInteger, ForeignKey(GK_ROOT_TABLE_ID), primary_key=True),
    Column("tag_id", Integer, ForeignKey("tag_table.id"), primary_key=True),
)

ids_author_table = Table(
    "ids_authors_table",
    IdsBaseSchema.metadata,
    Column("gk_id", BigInteger, ForeignKey(GK_ROOT_TABLE_ID), primary_key=True),
    Column("author_id", Integer, ForeignKey("author_table.id"), primary_key=True),
)


class Tag(IdsBaseSchema):
    __tablename__ = 'tag_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    tagid: Mapped[str] = mapped_column(String(32), nullable=False)
    categorie: Mapped[str] = mapped_column(String(32),
                                           CheckConstraint(
                                               "categorie IN ('{}')".format("', '".join(constants.TAG_CATEGORIES))),
                                           nullable=False, default="other",
                                           index=True, )
    description: Mapped[Optional[str]] = mapped_column(Text, nullable=True)

    gyrokinetics: Mapped[list["GyrokineticsLocal"]] = relationship(
        secondary=ids_tags_table, back_populates="tag"
    )

    __table_args__ = (
        UniqueConstraint('tagid', 'categorie'),
    )


class Restrictions(IdsBaseSchema):
    __tablename__ = 'restrictions_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)

    # restriction_type : private, internal
    restriction_type: Mapped[str] = mapped_column(default="private", nullable=False)
    has_restrictions: Mapped[bool] = mapped_column(default=False, nullable=False)
    time_limited: Mapped[bool] = mapped_column(default=False, nullable=False)
    end_time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), server_default=func.now()
    )
    gyrokinetics: Mapped[list["GyrokineticsLocal"]] = relationship(
        secondary=ids_tags_table, back_populates="tag"
    )


class Author(IdsBaseSchema):
    __tablename__ = 'author_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    has_ids: Mapped[bool] = mapped_column(default=False, nullable=False)
    firstname: Mapped[str] = mapped_column(default=None, nullable=False)
    lastname: Mapped[str] = mapped_column(default=None, nullable=False)
    email: Mapped[str] = mapped_column(default=None, nullable=False)
    institute: Mapped[str] = mapped_column(default=None, nullable=True)
    is_public: Mapped[bool] = mapped_column(default=False, nullable=False)
    creation_date: Mapped[datetime] = mapped_column(
        DateTime(timezone=False), server_default=func.now()
    )

    __table_args__ = (
        UniqueConstraint('firstname', 'lastname', 'email'),
    )


class IdsAuthorRevisionHistory(IdsBaseSchema):
    __tablename__ = 'ids_revision_history_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    author_id: Mapped[int] = mapped_column(ForeignKey("author_table.id"),
                                           nullable=False, index=True)
    simulation_details_id: Mapped[int] = mapped_column(ForeignKey("ids_simulation_details_table.id"),
                                                       nullable=False, index=True)

    is_deletion: Mapped[bool] = mapped_column(default=False, nullable=False)
    is_update: Mapped[bool] = mapped_column(default=False, nullable=False)
    is_creation: Mapped[bool] = mapped_column(default=True, nullable=False)
    revision_date: Mapped[datetime] = mapped_column(
        DateTime(timezone=False), server_default=func.now()
    )

    __table_args__ = (
        CheckConstraint(
            or_(
                and_(is_update.is_(True), not_(is_deletion), not_(is_creation)),
                and_(is_deletion.is_(True), not_(is_update), not_(is_creation)),
                and_(is_creation.is_(True), not_(is_update), not_(is_deletion))
            ),
            name='only_one_condition_true'
        ),
    )


class IdsSimulationDetails(IdsBaseSchema):
    __tablename__ = 'ids_simulation_details_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)

    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)

    # copy of the associated IDS uuid
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, index=True)
    has_file_storage: Mapped[bool] = mapped_column(default=False, nullable=False)
    number_species: Mapped[int] = mapped_column(default=None, nullable=False)
    number_wavevectors_lin: Mapped[int] = mapped_column(default=None, nullable=True)
    number_wavevectors_non_lin: Mapped[int] = mapped_column(default=None, nullable=True)
    is_collisionless: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fluxes_1d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fluxes_2d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fluxes_3d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fluxes_4d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fluxes_5d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_moments_1d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_moments_2d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_moments_3d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_moments_4d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_moments_5d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fields_1d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fields_2d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fields_3d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fields_4d: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_fields_5d: Mapped[bool] = mapped_column(default=False, nullable=False)
    input_hash_full: Mapped[str] = mapped_column(default="", nullable=True)
    input_hash_without_parameters: Mapped[str] = mapped_column(default="",
                                                               nullable=True)
    has_linear: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_quasi_linear: Mapped[bool] = mapped_column(default=False, nullable=False)
    has_non_linear: Mapped[bool] = mapped_column(default=False, nullable=False)
    __table_args__ = (
        CheckConstraint(
            "uuid IS NOT NULL OR has_file_storage IS NOT NULL OR number_species IS NOT NULL OR "
            " has_fluxes_1d IS NOT NULL OR "
            "has_fluxes_2d IS NOT NULL OR has_fluxes_3d IS NOT NULL OR "
            "has_fluxes_4d IS NOT NULL OR has_fluxes_5d IS NOT NULL OR "
            "has_moments_1d IS NOT NULL OR has_moments_2d IS NOT NULL OR "
            "has_moments_3d IS NOT NULL OR has_moments_4d IS NOT NULL OR "
            "has_moments_5d IS NOT NULL OR input_hash_full IS NOT NULL OR "
            "input_hash_without_parameters IS NOT NULL OR has_linear IS NOT NULL OR "
            "has_quasi_linear IS NOT NULL OR has_non_linear IS NOT NULL",
            name='at_least_one_field_not_null'
        ),
    )


class DatabaseStats(IdsBaseSchema):
    __tablename__ = 'database_statistics_table'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    number_runs: Mapped[BigInteger] = mapped_column(BigInteger, nullable=False, unique=True)
    linear_runs: Mapped[BigInteger] = mapped_column(BigInteger, nullable=False, unique=True)
    quasi_linear_runs: Mapped[BigInteger] = mapped_column(BigInteger, nullable=False, unique=True)
    non_linear_runs: Mapped[BigInteger] = mapped_column(BigInteger, nullable=False, unique=True)
    cpu_time_str: Mapped[str] = mapped_column(String(256), nullable=False, unique=True)
    cpu_time: Mapped[BigInteger] = mapped_column(BigInteger, unique=True)
    storage: Mapped[int] = mapped_column(nullable=False, unique=True)
    storage_str: Mapped[str] = mapped_column(String(16), nullable=False, unique=True)

    __table_args__ = (
        CheckConstraint(
            "number_runs IS NOT NULL OR linear_runs IS NOT NULL OR quasi_linear_runs IS NOT NULL OR "
            "non_linear_runs IS NOT NULL OR cpu_time_str IS NOT NULL OR cpu_time IS NOT NULL OR "
            "storage IS NOT NULL OR storage_str IS NOT NULL",
            name='at_least_one_field_not_null'
        ),
    )


class FileStorage(IdsBaseSchema):
    __tablename__ = 'file_storage_table'

    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    # TODO: define if 1 or multiple files and if multiple, how to split
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="file_storage")
    hash: Mapped[str] = mapped_column(String(128), nullable=False, unique=True)
    filename: Mapped[str] = mapped_column(String(256), nullable=False, unique=False)
    filetype: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)
    storage: Mapped[str] = mapped_column(String(512), nullable=False, default="local")
    plugin_id: Mapped[Optional[BigInteger]] = mapped_column(BigInteger, ForeignKey('code_version_table.id'),
                                                            nullable=True, unique=False)
    ids_version: Mapped[str] = mapped_column(String(256), nullable=False, )
    __table_args__ = (
        UniqueConstraint('hash', 'storage'),
    )


# class CodePartialConstant(IdsBaseSchema):
#     __tablename__ = 'code_partial_constant_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     eigenmode_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('eigenmode_table.id'),
#                                                      nullable=False)
#     gyrokineticsnonlinear_id: Mapped[BigInteger] = mapped_column(BigInteger,
#                                                                  ForeignKey('gyrokinetics_non_linear_table.id'),
#                                                                  nullable=False)
#     code_id: Mapped[int] = mapped_column(ForeignKey('code_table.id'), nullable=False)
#     eigenmode: Mapped["Eigenmode"] = relationship(back_populates="code")
#     gyrokineticsnonlinear: Mapped["GyrokineticsNonLinear"] = relationship(back_populates="code")
#     parameters: Mapped[dict] = mapped_column(JSONB, nullable=False, unique=False)
#     output_flag: Mapped[int] = mapped_column(Integer,
#                                              nullable=True, )


class EigenmodeFields(IdsBaseSchema):
    __tablename__ = "eigenmode_fields_table"
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    eigenmode_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('eigenmode_table.id'),
                                                     nullable=False)
    eigenmode: Mapped["Eigenmode"] = relationship(back_populates="code")

    phi_potential_perturbed_weight = mapped_column(BYTEA, nullable=False, unique=False)
    phi_potential_perturbed_parity = mapped_column(BYTEA, nullable=False, unique=False)
    a_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=False, unique=False)
    a_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=False, unique=False)
    b_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=False, unique=False)
    b_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=False, unique=False)
    phi_potential_perturbed_norm = mapped_column(BYTEA, nullable=False, unique=False)
    a_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=False, unique=False)
    b_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=False, unique=False)


class Collisions(IdsBaseSchema):
    __tablename__ = 'collisions_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    # fk removed, not very usefull nor making sense to ask for collision without species
    # gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
    #                                          nullable=False, index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="collisions")
    collisionality_norm: Mapped[float] = mapped_column(REAL, nullable=False, unique=False)
    species_one: Mapped[int] = mapped_column(ForeignKey('species_table.id'), nullable=False)
    species_two: Mapped[int] = mapped_column(ForeignKey('species_table.id'), nullable=False)


# class EigenmodeFields(IdsBaseSchema):
#     __tablename__ = "eigenmode_fields"
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'),
#                                               nullable=False, index=True)
#     eigenmode_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('eigenmode_table.id'),
#                                                      nullable=False, index=True)
#
#     phi_potential_perturbed_weight = mapped_column(BYTEA, nullable=False, unique=False)
#     phi_potential_perturbed_parity = mapped_column(BYTEA, nullable=False, unique=False)
#     a_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=False, unique=False)
#     a_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=False, unique=False)
#     b_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=False, unique=False)
#     b_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=False, unique=False)
#     phi_potential_perturbed_norm = mapped_column(BYTEA, nullable=False, unique=False)
#     a_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=False, unique=False)
#     b_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=False, unique=False)
#

class GyrokineticsFields(IdsBaseSchema):
    __tablename__ = "fields_table"

    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    # 1d, intens_1d, zonal_2d, surf_average, intens_2d
    field_type: Mapped[str] = mapped_column(String(32), nullable=False, unique=False)

    # number of dimensions, 1 to 5? 6?
    field_dim: Mapped[int] = mapped_column(Integer, nullable=False, unique=False)
    field_value = mapped_column(BYTEA, nullable=False, unique=False)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    non_linear_run_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('non_linear_table.id'),
                                                          nullable=False, index=True)

    phi_potential_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)
    a_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)
    b_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)


# class GyrokineticsMoments(IdsBaseSchema):
#     __tablename__ = "moments_table"
#
#     id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
#     # linear nonlinear quasilinear
#     moment_type: Mapped[str] = mapped_column(String(32), nullable=False, unique=False)
#     # enum a_field, b_field etc
#     moment_name: Mapped[str] = mapped_column(String(32), nullable=False, unique=False)
#     # number of dimensions, 1 to 5? 6?
#     moment_dim: Mapped[int] = mapped_column(Integer, nullable=False, unique=False)
#     moment_value = mapped_column(BYTEA, nullable=False, unique=False)
#     gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
#                                               nullable=False, index=True)
#     non_linear_run_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('non_linear_table.id'),
#                                               nullable=False, index=True)

class FluxSurface(IdsBaseSchema):
    __tablename__ = 'flux_surface_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="flux_surface")

    r_minor_norm: Mapped[float] = mapped_column(REAL, nullable=False,
                                                default=constants.IMAS_DEFAULT_FLOAT)
    elongation: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    delongation_dr_minor_norm: Mapped[float] = mapped_column(REAL,
                                                             nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    dgeometric_axis_r_dr_minor: Mapped[float] = mapped_column(REAL,
                                                              nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    dgeometric_axis_z_dr_minor: Mapped[float] = mapped_column(REAL,
                                                              nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    q: Mapped[float] = mapped_column(REAL, nullable=False,
                                     default=constants.IMAS_DEFAULT_FLOAT)
    magnetic_shear_r_minor: Mapped[float] = mapped_column(REAL,
                                                          nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    pressure_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                          nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    ip_sign: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    b_field_tor_sign: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    shape_coefficients_c = mapped_column(BYTEA, nullable=False, unique=False)
    dc_dr_minor_norm = mapped_column(BYTEA,
                                     nullable=False, unique=False)
    shape_coefficients_s = mapped_column(BYTEA,
                                         nullable=False, unique=False)
    ds_dr_minor_norm = mapped_column(BYTEA,
                                     nullable=False, unique=False)


# array : flux par espece
# tous doivent avoir le meme nombre d'especes
# coherence indice espece
class NonLinearFluxes1Dspecies(IdsBaseSchema):
    __tablename__ = 'fluxes_1d_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    species_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
                                            nullable=True, index=True)

    non_linear_run_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('non_linear_table.id'),
                                                          nullable=False, index=True)

    # can be rotating or laboratory
    frame: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)
    # can be integrated, linear_weight, nonlinear
    # ftype: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)
    # copy of the associated IDS uuid
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, index=True)

    particles_phi_potential: Mapped[float] = mapped_column(REAL, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    particles_a_field_parallel: Mapped[float] = mapped_column(REAL,
                                                              nullable=False,
                                                              default=constants.IMAS_DEFAULT_FLOAT)
    particles_b_field_parallel: Mapped[float] = mapped_column(REAL,
                                                              nullable=False,
                                                              default=constants.IMAS_DEFAULT_FLOAT)
    energy_phi_potential: Mapped[float] = mapped_column(REAL, nullable=False,
                                                        default=constants.IMAS_DEFAULT_FLOAT)
    energy_a_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    energy_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_phi_potential: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                       default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_a_field_parallel: Mapped[float] = mapped_column(REAL,
                                                                          nullable=True,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_phi_potential: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                            default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_a_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)


class NonLinearFluxes2Dspecies(IdsBaseSchema):
    __tablename__ = 'fluxes_2d_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    species_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
                                            nullable=True, index=True)

    non_linear_run_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('non_linear_table.id'),
                                                          nullable=False, index=True)

    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="fluxes_species_2d")

    # can be sum2dkx, sum2dkxky
    ftype: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)
    # coordinate 2 of the IMAS shape of the array, coordinate 1 being the species
    coord2: Mapped[int] = mapped_column(nullable=False)

    # copy of the associated IDS uuid
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, index=True)

    particles_phi_potential = mapped_column(BYTEA, nullable=False,
                                            default=constants.IMAS_DEFAULT_FLOAT)
    particles_a_field_parallel = mapped_column(BYTEA,
                                               nullable=False,
                                               default=constants.IMAS_DEFAULT_FLOAT)
    particles_b_field_parallel = mapped_column(BYTEA,
                                               nullable=False,
                                               default=constants.IMAS_DEFAULT_FLOAT)
    energy_phi_potential: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                        default=constants.IMAS_DEFAULT_FLOAT)
    energy_a_field_parallel: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    energy_b_field_parallel: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_phi_potential: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                                       default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_a_field_parallel: Mapped[float] = mapped_column(BYTEA,
                                                                          nullable=False,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_b_field_parallel: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_phi_potential: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                                            default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_a_field_parallel: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_b_field_parallel: Mapped[float] = mapped_column(BYTEA, nullable=False,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)


# array : flux par espece
# tous doivent avoir le meme nombre d'especes
# coherence indice espece
class LinearWeights(IdsBaseSchema):
    __tablename__ = 'linear_weights_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    species_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
                                            nullable=True, index=True)

    eigenmode_id: Mapped[BigInteger] = mapped_column(ForeignKey('eigenmode_table.id'),
                                                     nullable=True, index=True)

    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="linear_weights")

    # can be rotating or laboratory
    frame: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)

    # copy of the associated IDS uuid
    # uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
    #                                  nullable=False, index=True)

    particles_phi_potential: Mapped[float] = mapped_column(REAL, nullable=True,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    particles_a_field_parallel: Mapped[float] = mapped_column(REAL,
                                                              nullable=True,
                                                              default=constants.IMAS_DEFAULT_FLOAT)
    particles_b_field_parallel: Mapped[float] = mapped_column(REAL,
                                                              nullable=True,
                                                              default=constants.IMAS_DEFAULT_FLOAT)
    energy_phi_potential: Mapped[float] = mapped_column(REAL, nullable=True,
                                                        default=constants.IMAS_DEFAULT_FLOAT)
    energy_a_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    energy_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_phi_potential: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                       default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_a_field_parallel: Mapped[float] = mapped_column(REAL,
                                                                          nullable=True,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_phi_potential: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                            default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_a_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=True,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)
    __table_args__ = (
        CheckConstraint(
            "particles_phi_potential IS NOT NULL OR particles_a_field_parallel IS NOT NULL OR particles_b_field_parallel IS NOT NULL OR "
            "energy_phi_potential IS NOT NULL OR energy_a_field_parallel IS NOT NULL OR energy_b_field_parallel IS NOT NULL OR "
            "momentum_tor_parallel_phi_potential IS NOT NULL OR momentum_tor_parallel_a_field_parallel IS NOT NULL OR momentum_tor_parallel_b_field_parallel IS NOT NULL OR "
            "momentum_tor_perpendicular_phi_potential IS NOT NULL OR momentum_tor_perpendicular_a_field_parallel IS NOT NULL OR momentum_tor_perpendicular_b_field_parallel IS NOT NULL",
            name='at_least_one_weight_not_null'
        ),
    )


class InputNormalizing(IdsBaseSchema):
    __tablename__ = 'input_normalizing_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
                                              index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="normalizing_quantities")

    t_e: Mapped[float] = mapped_column(REAL, nullable=True, default=constants.IMAS_DEFAULT_FLOAT)
    n_e: Mapped[float] = mapped_column(REAL, nullable=True, default=constants.IMAS_DEFAULT_FLOAT)
    r: Mapped[float] = mapped_column(REAL, nullable=True, default=constants.IMAS_DEFAULT_FLOAT)
    b_field_tor: Mapped[float] = mapped_column(REAL, nullable=True, default=constants.IMAS_DEFAULT_FLOAT)
    __table_args__ = (
        CheckConstraint(
            "t_e IS NOT NULL OR n_e IS NOT NULL OR r IS NOT NULL OR "
            "b_field_tor IS NOT NULL",
            name='at_least_one_quantity_not_null'
        ),
    )


class InputSpeciesGlobal(IdsBaseSchema):
    __tablename__ = 'input_species_global_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
                                              index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="species_all")

    beta_reference: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    velocity_tor_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    debye_length_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    shearing_rate_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    angle_pol = mapped_column(BYTEA, nullable=False, unique=False)


class Model(IdsBaseSchema):
    __tablename__ = 'model_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
                                              index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="model")

    include_coriolis_drift: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                         default=constants.IMAS_DEFAULT_INT)
    include_centrifugal_effects: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                              default=constants.IMAS_DEFAULT_INT)
    include_a_field_parallel: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    include_b_field_parallel: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    include_full_curvature_drift: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                               default=constants.IMAS_DEFAULT_INT)
    collisions_pitch_only: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                        default=constants.IMAS_DEFAULT_INT)
    collisions_momentum_conservation: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                                   default=constants.IMAS_DEFAULT_INT)
    collisions_energy_conservation: Mapped[bool] = mapped_column(Boolean,
                                                                 nullable=False,
                                                                 default=constants.IMAS_DEFAULT_INT)
    collisions_finite_larmor_radius: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                                  default=constants.IMAS_DEFAULT_INT)
    adiabatic_electrons: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                      default=constants.IMAS_DEFAULT_INT)
    # non_linear_run: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    # time_interval_norm = mapped_column(BYTEA, nullable=True,
    #                                    unique=False)


#
# class Moments(IdsBaseSchema):
#     __tablename__ = 'moments_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     fluxesmoments_id: Mapped[int] = mapped_column(ForeignKey('fluxes_moments_table.id'), nullable=False)
#
#     density = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                             nullable=False, unique=False)
#     density_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                    nullable=False, unique=False)
#     j_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                nullable=False, unique=False)
#     j_parallel_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                       nullable=False, unique=False)
#     pressure_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                       nullable=False, unique=False)
#     pressure_parallel_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                              nullable=False, unique=False)
#     pressure_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                            nullable=False, unique=False)
#     pressure_perpendicular_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                   nullable=False, unique=False)
#     heat_flux_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                        nullable=False, unique=False)
#     heat_flux_parallel_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                               nullable=False, unique=False)
#     v_parallel_energy_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                     nullable=False, unique=False)
#     v_parallel_energy_perpendicular_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                            nullable=False, unique=False)
#     v_perpendicular_square_energy = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                   nullable=False, unique=False)
#     v_perpendicular_square_energy_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                          nullable=False, unique=False)


# class MomentsParticles(IdsBaseSchema):
#     __tablename__ = 'moments_particles_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     fluxesmoments_id: Mapped[int] = mapped_column(ForeignKey('fluxes_moments_table.id'), nullable=False)
#
#     density = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     j_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     pressure_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     pressure_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     heat_flux_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     v_parallel_energy_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     v_perpendicular_square_energy = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#
# class FluxesMoments(IdsBaseSchema):
#     __tablename__ = 'fluxes_moments_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False)
#     eigenmode_id: Mapped[BigInteger] = mapped_column(ForeignKey('eigenmode_table.id'),
#                                               nullable=False)
#     specie_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
#                                            nullable=False)
#
#     eigenmode: Mapped["Eigenmode"] = relationship(back_populates="fluxes_moments")
#
#     moments_particle_id: Mapped[int] = mapped_column(ForeignKey('moments_table.id'),
#                                                      nullable=False)
#
#     moments_id: Mapped[int] = mapped_column(ForeignKey('moments_table.id'),
#                                             nullable=False)
#
#     moments_gyroav_id: Mapped[int] = mapped_column(ForeignKey('moments_table.id'),
#                                                    nullable=False)
#     # moments_norm_gyrocenter_id: Optional[Moments] = Mapped[int] = mapped_column(ForeignKey('wavevector.id'), nullable=False)
#     # moments_norm_particle_id: Optional[MomentsParticles] = relationship()
#     # fluxes_norm_gyrocenter_id: Optional[Fluxes] = relationship()
#     # fluxes_norm_gyrocenter_rotating_frame_id: Optional[Fluxes] = relationship()
#     # fluxes_norm_particle_id: Optional[Fluxes] = relationship()
#     # fluxes_norm_particle_rotating_frame_id: Optional[Fluxes] = relationship()
#     moments_norm_gyrocenter_id: Mapped[Optional[int]] = mapped_column(ForeignKey('moments_table.id'), nullable=True)
#     moments_norm_particle_id: Mapped[Optional[int]] = mapped_column(ForeignKey('moments_table.id'),
#                                                                     nullable=True)
#     fluxes_norm_gyrocenter_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'), nullable=True)
#     fluxes_norm_gyrocenter_rotating_frame_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'),
#                                                                                     nullable=True)
#     fluxes_norm_particle_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'), nullable=True)
#     fluxes_norm_particle_rotating_frame_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'),
#                                                                                   nullable=True)


class MomentsLinear(IdsBaseSchema):
    __tablename__ = 'moments_linear_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False)

    species_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
                                            nullable=True, index=True)
    eigenmode_id: Mapped[BigInteger] = mapped_column(ForeignKey('eigenmode_table.id'),
                                                     nullable=True, index=True)

    mtype: Mapped[str] = mapped_column(String(16), default="default")
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, index=True)
    norm: Mapped[bool] = mapped_column(Boolean,
                                       nullable=True, index=False, default=False)
    # fluxesmoments_id: Mapped[int] = mapped_column(ForeignKey('fluxes_moments_table.id'),
    #                                              nullable=False)

    density = mapped_column(BYTEA, nullable=False, unique=False)
    j_parallel = mapped_column(BYTEA, nullable=False, unique=False)
    pressure_parallel = mapped_column(BYTEA, nullable=False, unique=False)
    pressure_perpendicular = mapped_column(BYTEA, nullable=False, unique=False)
    heat_flux_parallel = mapped_column(BYTEA, nullable=False, unique=False)
    v_parallel_energy_perpendicular = mapped_column(BYTEA, nullable=False, unique=False)
    v_perpendicular_square_energy = mapped_column(BYTEA, nullable=False, unique=False)


class Species(IdsBaseSchema):
    __tablename__ = 'species_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
                                              index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="species")

    charge_norm: Mapped[float] = mapped_column(REAL, nullable=False,
                                               default=constants.IMAS_DEFAULT_FLOAT)
    mass_norm: Mapped[float] = mapped_column(REAL,
                                             nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    density_norm: Mapped[float] = mapped_column(REAL,
                                                nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    density_log_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                             nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    temperature_norm: Mapped[float] = mapped_column(REAL,
                                                    nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    temperature_log_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                                 nullable=False,
                                                                 default=constants.IMAS_DEFAULT_FLOAT)
    velocity_tor_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                              nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    potential_energy_norm = mapped_column(BYTEA, nullable=False, unique=False)

    potential_energy_gradient_norm = mapped_column(BYTEA, nullable=False, unique=False)


# class linked to ids_to_check properties removed (deprecated)
# class IdsProvenanceNode(IdsBaseSchema):
#     __tablename__ = 'ids_provenance_node_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     #  ids_provenance_id: Mapped[int] = mapped_column(ForeignKey("ids_provenance_table.id"))
#     path: Mapped[str] = mapped_column(String(512), nullable=False)
#     sources: Mapped[list[str]] = mapped_column(ARRAY(String(256), dimensions=1), nullable=False, unique=False)
#     ids_properties_id: Mapped[int] = mapped_column(ForeignKey("ids_properties_table.id"))


# class IdsProvenance(IdsBaseSchema):
#     # node: list[IdsProvenanceNode]
#     __tablename__ = 'ids_provenance_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     ids_properties_id: Mapped[int] = mapped_column(ForeignKey("ids_properties_table.id"))
#     ids_provenance_node: Mapped[list[IdsProvenanceNode]] = relationship()

# class IdsProperties(IdsBaseSchema):
#     __tablename__ = 'ids_properties_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
#                                               index=True)
#     gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="ids_properties")
#
#     comment: Mapped[str] = mapped_column(Text, nullable=False)
#     name: Mapped[str] = mapped_column(Text, nullable=True)
#     homogeneous_time: Mapped[int] = mapped_column(Integer, nullable=False, default=constants.IMAS_DEFAULT_INT)
#     provider: Mapped[str] = mapped_column(String(512), nullable=False)
#     creation_date: Mapped[str] = mapped_column(String(512), nullable=False)
#     provenance: Mapped[Optional[list[IdsProvenanceNode]]] = relationship()
#

class CodeVersion(IdsBaseSchema):
    __tablename__ = 'code_version_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    description: Mapped[str] = mapped_column(String(512), nullable=True, default=constants.IMAS_DEFAULT_STR)
    name: Mapped[str] = mapped_column(String(64), nullable=False, index=False)
    commit: Mapped[str] = mapped_column(String(512), nullable=True, default=constants.IMAS_DEFAULT_STR)
    release_version: Mapped[str] = mapped_column(String(16), nullable=True, default=constants.IMAS_DEFAULT_STR)
    repository: Mapped[str] = mapped_column(String(512), nullable=True, default=constants.IMAS_DEFAULT_STR)
    #compilation_arguments: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)

    # 0 for code, 1 for library, 2 for script, 3 for plugin
    type: Mapped[int] = mapped_column(nullable=False)
    # Index on (name, release_version)
    __table_args__ = (
        Index('idx_name_commit', 'name', 'commit'),
        UniqueConstraint('name', 'commit', 'release_version', name='uq_name_release_version_lower'),
    )


class Library(IdsBaseSchema):
    __tablename__ = 'library_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    code_id: Mapped[int] = mapped_column(ForeignKey("code_table.id"))
    version_id: Mapped[int] = mapped_column(ForeignKey("code_version_table.id"))
    parameters: Mapped[dict] = mapped_column(JSONB, nullable=True)


class Code(IdsBaseSchema):
    __tablename__ = 'code_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False,
                                              index=False)

    version_id: Mapped[int] = mapped_column(ForeignKey("code_version_table.id"))
    library_id: Mapped[int] = mapped_column(ForeignKey("library_table.id"), nullable=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="code")
    version: Mapped[CodeVersion] = relationship(back_populates="code")
    #not used any more in this table, see non_linear_table and eigenmode_table
    #    parameters: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)
    library: Mapped[Optional[list[Library]]] = relationship(back_populates="code")


# class CodePartial(IdsBaseSchema):
#     __tablename__ = 'code_partial_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     name: Mapped[str] = mapped_column(String(128), nullable=False, index=True)
#     commit: Mapped[str] = mapped_column(String(512), nullable=False)
#     version: Mapped[str] = mapped_column(String(32), nullable=False)
#     repository: Mapped[str] = mapped_column(String(256), nullable=False)
#     parameters: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)


# class GyrokineticsEigenmodeFields(IdsBaseSchema):
#     __tablename__ = "gyrokinetics_eigenmode_fields"
#
#     phi_potential_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
#     phi_potential_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
#     a_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
#     a_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
#     b_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
#     b_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
#     phi_potential_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)
#     a_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)
#     b_field_parallel_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)

class Eigenmode(IdsBaseSchema):
    __tablename__ = 'eigenmode_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    linear_id: Mapped[BigInteger] = mapped_column(ForeignKey('linear_table.id'),
                                                  nullable=False, index=True)
    gk_id: Mapped[BigInteger] = mapped_column(ForeignKey(GK_ROOT_TABLE_ID),
                                              nullable=False, index=True)
    # wavevector: Mapped["Wavevector"] = relationship(back_populates="eigenmode")
    code_parameters: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)

    poloidal_turns: Mapped[int] = mapped_column(Integer, nullable=False, default=constants.IMAS_DEFAULT_INT)
    growth_rate_norm: Mapped[float] = mapped_column(REAL, nullable=True, index=True)
    frequency_norm: Mapped[float] = mapped_column(REAL, nullable=True, index=True)
    growth_rate_tolerance: Mapped[float] = mapped_column(REAL, nullable=True, default=constants.IMAS_DEFAULT_FLOAT)
    angle_pol = mapped_column(BYTEA, nullable=True, unique=False)
    time_norm = mapped_column(BYTEA, nullable=True, unique=False)
    initial_value_run: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    #
    #
    # phi_potential_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
    # phi_potential_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
    # a_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
    # a_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
    # b_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
    # b_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
    # poloidal_angle = mapped_column(BYTEA, nullable=True, unique=False)
    # phi_potential_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)
    # a_field_parallel_perturbed_norm = mapped_column(BYTEA,
    #                                                 nullable=True, unique=False)
    # b_field_parallel_perturbed_norm = mapped_column(BYTEA,
    #                                                 nullable=True, unique=False)


# class GyrokineticsLinear(IdsBaseSchema):
#     __tablename__ = "gyrokinetics_linear"
# not used in db since there is just wavevectors in


class GyrokineticsNonLinear(IdsBaseSchema):
    __tablename__ = "non_linear_table"

    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)

    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
                                              index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="non_linear")

    binormal_wavevector_norm = mapped_column(BYTEA, nullable=True, unique=False)
    radial_wavevector_norm = mapped_column(BYTEA, nullable=True, unique=False)

    binormal_wavevector_max: Mapped[float] = mapped_column(REAL, nullable=True, unique=False)
    radial_wavevector_max: Mapped[float] = mapped_column(REAL, nullable=True, unique=False)
    binormal_wavevector_interval: Mapped[float] = mapped_column(REAL, nullable=True, unique=False)
    radial_wavevector_interval: Mapped[float] = mapped_column(REAL, nullable=True, unique=False)

    angle_pol = mapped_column(BYTEA, nullable=True, unique=False)
    time_norm = mapped_column(BYTEA, nullable=True, unique=False)
    time_norm_max: Mapped[float] = mapped_column(REAL, nullable=True, unique=False)
    time_interval_norm = mapped_column(BYTEA, nullable=True, unique=False)
    quasi_linear: Mapped[bool] = mapped_column(nullable=False, default=False)
    code_parameters: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)


class Wavevector(IdsBaseSchema):
    __tablename__ = 'linear_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey(GK_ROOT_TABLE_ID), nullable=False,
                                              index=True)
    gyrokinetics: Mapped["GyrokineticsLocal"] = relationship(back_populates="wavevector")
    eigenmode: Mapped[Optional[list[Eigenmode]]] = relationship(back_populates="wavevector")

    radial_wavevector_norm: Mapped[float] = mapped_column(REAL,
                                                          nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    binormal_wavevector_norm: Mapped[float] = mapped_column(REAL, nullable=False,
                                                            default=constants.IMAS_DEFAULT_FLOAT)


class GyrokineticsLocal(IdsBaseSchema):
    __tablename__ = 'gyrokinetics_local_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, server_default=text("uuid_generate_v7()"), index=True)

    #    ids_properties: Mapped[Optional[IdsProperties]] = relationship(back_populates="gyrokinetics")

    code: Mapped[Optional[Code]] = relationship(back_populates="gyrokinetics")
    collisions: Mapped[Collisions] = relationship(back_populates="gyrokinetics")
    species: Mapped[list[Species]] = relationship(back_populates="gyrokinetics")
    species_all: Mapped[Optional[InputSpeciesGlobal]] = relationship(back_populates="gyrokinetics")
    model: Mapped[Optional[Model]] = relationship(back_populates="gyrokinetics")

    linear: Mapped[Optional[Wavevector]] = relationship(back_populates="gyrokinetics")
    non_linear: Mapped[Optional[GyrokineticsNonLinear]] = relationship(back_populates="gyrokinetics")
    flux_surface: Mapped[Optional[FluxSurface]] = relationship(back_populates="gyrokinetics")
    normalizing_quantities: Mapped[Optional[InputNormalizing]] = relationship(back_populates="gyrokinetics")

    file_storage: Mapped[Optional[list[FileStorage]]] = relationship(back_populates="gyrokinetics")
    #    ids_properties: Mapped[Optional[IdsProperties]] = relationship(back_populates="gyrokinetics")

    tag: Mapped[Optional[list[Tag]]] = relationship(
        secondary=ids_tags_table, back_populates="gyrokinetics"
    )

    __table_args__ = (
        UniqueConstraint('uuid'),
        #   Index('ix_ids_properties_id', 'id', unique=True),
    )


class COMPLEX(sa.sql.sqltypes.TypeEngine):
    __visit_name__ = "complex"

    def __init__(self, x=None, y=None):
        # e.g. 'PointZ'
        self.x = x
        # e.g. '4326'
        self.y = y


sa.dialects.postgresql.base.ischema_names['complex'] = COMPLEX

def create_gkdb_database():
    user = os.getenv('GKDB_DB_ROOT', 'FASTER')
    password = os.getenv('GKDB_DB_ROOTPWD', 'FASTER')
    host = os.getenv('GKDB_DB_HOST', 'localhost')
    port = os.getenv('GKDB_DB_PORT', '5432')
    database = os.getenv('GKDB_NAME', 'gkdb')
    db_schema = "gkdb_development"
    engine = create_engine(f"postgresql+psycopg://{user}:{password}@{host}:{port}/{database}", echo=False)

    # have to do this in case where DB is not declared in the public schema but in any other schema
    @sa.event.listens_for(engine, "connect")
    def set_schema(dbapi_connection, connection_record):
        cursor = dbapi_connection.cursor()
        cursor.execute(f"SET search_path TO {db_schema}")
        cursor.close()


    with engine.connect() as con:
        try:
            con.execute(text(f"set schema '{db_schema}';CREATE EXTENSION pg_uuidv7;"))
        except sa.exc.ProgrammingError:
            con.rollback()
            con.commit()
        try:
            con.execute(text(  #f"set schema '{db_schema}';"
                """
DO $$ BEGIN
    IF to_regtype('complex') IS NULL THEN
        CREATE TYPE  complex AS (
            r float4,
            i float4
        );
    END IF;
END $$;
    """))
        except sa.exc.ProgrammingError as e:
            print(e)
            con.rollback()
            con.commit()
        con.commit()
        try:
            con.execute(text("""CREATE OR REPLACE FUNCTION get_table_counts_and_size(p_schema text)
RETURNS TABLE(table_name text, row_count bigint, table_size text) AS $$
DECLARE 
    _tbl text; 
BEGIN 
    FOR _tbl IN 
      SELECT tablename  
      FROM   pg_tables 
      WHERE  schemaname = p_schema
    LOOP
        RETURN QUERY EXECUTE format(
            'SELECT %L::text, count(*), pg_size_pretty(pg_total_relation_size(%L::regclass)) FROM %I.%I', 
            _tbl, _tbl, p_schema, _tbl);
    END LOOP;  
END;
$$ LANGUAGE PLPGSQL;
"""))
        except sa.exc.ProgrammingError as e:
            print(e)
            con.rollback()
            con.commit()
        con.commit()
    IdsBaseSchema.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)


if __name__ == "__main__":
    create_gkdb_database()

    # inspector = inspect(engine)
    # table_names = inspector.get_table_names()
    # for table_name in table_names:
    #     print(f"Table:{table_name}")
    #     column_items = inspector.get_columns(table_name)
    #     print('\t'.join(n for n in column_items[0]))
    #     for c in column_items:
    #         assert len(c) == len(column_items[0])
    #         print('\t'.join(str(c[n]) for n in c))
    # print(table_names)
    # print("end of script")
