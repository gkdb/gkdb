from psycopg import sql
from gkdb.config import DBConfig

COLUMNS_FLUXES_TABLE = (
#    "frame",
    "particles_phi_potential",
    "particles_a_field_parallel",
    "particles_b_field_parallel",
    "energy_phi_potential",
    "energy_a_field_parallel",
    "energy_b_field_parallel",
    "momentum_tor_parallel_phi_potential",
    "momentum_tor_parallel_a_field_parallel",
    "momentum_tor_parallel_b_field_parallel",
    "momentum_tor_perpendicular_phi_potential",
    "momentum_tor_perpendicular_a_field_parallel",
    "momentum_tor_perpendicular_b_field_parallel"
)

COLUMNS_MOMENTS_TABLE = (
    "mtype",
    "uuid",
    "norm",
    "density",
    "j_parallel",
    "pressure_parallel",
    "pressure_perpendicular",
    "heat_flux_parallel",
    "v_parallel_energy_perpendicular",
    "v_perpendicular_square_energy"
)

COLUMNS_LINEAR_WEIGHTS = (
    "particles_phi_potential",
    "particles_phi_potential",
    "j_parallel",
    "pressure_parallel",
    "pressure_perpendicular",
    "heat_flux_parallel",
    "v_parallel_energy_perpendicular",
    "v_perpendicular_square_energy"
)


COLUMNS_EIGENMODE_TABLE = (
    "poloidal_turns",
    "growth_rate_norm",
    "frequency_norm",
    "growth_rate_tolerance",
    "angle_pol",
    "time_norm",
    "initial_value_run",
)

COLUMNS_WAVEVECTOR_TABLE = (
    "poloidal_turns",
    "growth_rate_norm",
    "frequency_norm",
    "growth_rate_tolerance",
    "phi_potential_perturbed_weight",
    "phi_potential_perturbed_parity",
    "a_field_parallel_perturbed_weight",
    "a_field_parallel_perturbed_parity",
    "b_field_parallel_perturbed_weight",
    "b_field_parallel_perturbed_parity",
    "poloidal_angle",
    "phi_potential_perturbed_norm",
    "a_field_parallel_perturbed_norm",
    "b_field_parallel_perturbed_norm",
    "time_norm",
    "initial_value_run",
)

COLUMNS_SPECIES_TABLE = (
        "charge_norm",
        "mass_norm",
        "density_norm",
        "density_log_gradient_norm",
        "temperature_norm",
        "temperature_log_gradient_norm",
        "velocity_tor_gradient_norm",
        "potential_energy_norm",
        "potential_energy_gradient_norm",
    )

GKDB_CFG = DBConfig()
GKDB_CFG.write_moments = True


_copy_arg_string = "{}, {}, {}, {}, "+"{}, "*len(COLUMNS_FLUXES_TABLE)
QUERY_COPY_LINEAR_WEIGHTS = sql.SQL(
    "COPY {}.{} ("+_copy_arg_string[:-2]+") FROM STDIN").format(
    sql.Identifier(GKDB_CFG.schema),
    sql.Identifier("linear_weights_table"),
    *[sql.Identifier(x) for x in ("gk_id", "species_id", "eigenmode_id", "frame",
                                  *COLUMNS_FLUXES_TABLE)])