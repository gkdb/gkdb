from __future__ import annotations

from dataclasses import dataclass, Field, field
import idspy_dictionaries.ids_gyrokinetics_local as gkids
from typing import Optional
from psycopg.types.json import Jsonb
import numpy as np

"""
Class defined here are for internal used only, do not use them for IDS
"""

CODE_TYPE_LIST = {'code': 0, 'library': 1, 'script': 2, 'plugin': 3}
CODE_NAME_LIST = ['qualikiz', 'tglf', 'gene', 'cgyro', 'gkw']


@dataclass(slots=True)
class ModifiedFluxes1D:
    frame: str = field(default="")
    uuid: str = field(default="")

    particles_phi_potential: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    particles_a_field_parallel: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    particles_b_field_parallel: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    energy_phi_potential: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    energy_a_field_parallel: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    energy_b_field_parallel: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    momentum_tor_parallel_phi_potential: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    momentum_tor_parallel_a_field_parallel: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    momentum_tor_parallel_b_field_parallel: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    momentum_tor_perpendicular_phi_potential: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    momentum_tor_perpendicular_a_field_parallel: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    momentum_tor_perpendicular_b_field_parallel: Optional[np.array] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )


@dataclass(slots=True)
class ModifiedFluxes2D(gkids.Fluxes):
    frame: str = field(default="")
    uuid: str = field(default="")
    fsize: int = field(default=None)

    particles_phi_potential: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm', },
            "field_type": np.array
        }
    )
    particles_a_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm', },
            "field_type": np.array
        }
    )
    particles_b_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    energy_phi_potential: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_2D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    energy_a_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_2D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    energy_b_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_2D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    momentum_tor_parallel_phi_potential: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_2D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    momentum_tor_parallel_a_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    momentum_tor_parallel_b_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    momentum_tor_perpendicular_phi_potential: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    momentum_tor_perpendicular_a_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_1D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm'},
            "field_type": np.array
        }
    )
    momentum_tor_perpendicular_b_field_parallel: Optional[np.array] = field(
        default_factory=lambda: np.zeros(shape=(0,) * 1, dtype=float),
        metadata={
            "imas_type": "FLT_2D",
            "ndims": 1,
            "coordinates": {'coordinate2': '../../time_norm', },
            "field_type": np.array
        }
    )


@dataclass(slots=True)
class ExtendedMomentsLinear(gkids.MomentsLinear):
    uuid: str = field(default="")
    mtype: str = field(default="")
    norm: bool = field(default=False)


@dataclass(slots=True)
class CodeVersion:
    class Meta:
        name = "gkdb_code_version"
        is_root_ids = False
        is_imas_ids = False

    description: Optional[str] = field(default="")
    name: str = field(default="")
    commit: Optional[str] = field(default="")
    release_version: str = field(default="")
    repository: Optional[str] = field(default="")
    #compilation_arguments: Optional[dict | str | None] = field(default_factory={})
    # 0 for code, 1 for library, 2 for script, 3 for plugin
    type: int = field(default=-1)

    def __post_init__(self):

        if self.type not in CODE_TYPE_LIST.values():
            raise ValueError(f"type is {self.type} "
                             f"and must be in {CODE_TYPE_LIST.values()}")
        if self.name.strip().lower() == "":
            raise ValueError(f"name has to be specified")

        if self.type == CODE_TYPE_LIST['code']:
            self.name = self.name.lower().strip()
            if self.name.lower() not in CODE_NAME_LIST:
                raise ValueError(f"name {self.name} does not correspond to a code indicated in {CODE_NAME_LIST}")

@dataclass(slots=True)
class CodeMin:
    class Meta:
        name = "gkdb_code_min"
        is_root_ids = False
        is_imas_ids = False

    parameters: Jsonb | None = field(default=None)


@dataclass(slots=True)
class NonLinearExtended(gkids.GyrokineticsNonLinear):
    class Meta:
        name = "gkdb_non_linear_extended"
        is_root_ids = False
        is_imas_ids = False

    radial_wavevector_max: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    binormal_wavevector_max: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )

    radial_wavevector_interval: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    binormal_wavevector_interval: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )
    angle_pol_interval_per_turn: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )

    time_norm_max: Optional[float] = field(
        default=9e+40,
        metadata={
            "imas_type": "FLT_0D",
            "field_type": float
        }
    )

    code_parameters: Optional[dict] = field(
        default_factory=lambda: {},
        metadata={
            "imas_type": "str",
            "field_type": dict
        }
    )

    def __post_init__(self):
        if not isinstance(self.code_parameters, dict):
            raise ValueError(f"code_parameters is {type(self.code_parameters)} "
                             f"and must be a dict")
