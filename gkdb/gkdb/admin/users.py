#!/usr/bin/python3
# -*- coding: utf-8 -*-

# import the psycopg  database adapter for PostgreSQL
from psycopg import connect, sql
from psycopg.errors import DuplicateObject, DuplicateSchema, DuplicateDatabase

DB_NAME = "gkdb"
PROD_SCHEMA_NAME = "gkdb_production"
DEV_SCHEMA_NAME = "gkdb_development"
MAX_CONNECTIONS = 10


class PGConnector:
    """
    class used to manage postgre users
    """
    equivalence_dict: dict = {'r': 'Select, Connect', 'w': 'Insert, Update',
                              'a': 'all', 'e': 'execute', 'u': 'update', 'd': 'delete',
                              'i': 'insert', 'c': 'create'}

    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def revoke_public_role(self):
        """
        Revoke public role permissions on the schema 'public' and the database.

        Revoke the `CREATE` permission on the 'public' schema from the `PUBLIC` role.
        Revoke all permissions on the database from the `PUBLIC` role.

        :return: True if the revocation was successful, False otherwise.
        """
        self.cursor.execute(sql.SQL(
            "REVOKE CREATE ON SCHEMA public FROM PUBLIC"
        ))
        with self.conn.transaction():
            self.cursor.execute(sql.SQL(
                "REVOKE ALL ON DATABASE {} FROM PUBLIC;").format(sql.Identifier(DB_NAME)))
        return True

    def add_user(self, username: str, password, role: str) -> bool:
        """
        :param username: The username of the user to be added.
        :param password: The password for the user.
        :param role: The role to be granted to the user.
        :return: True if the user was successfully added, False if the user already exists.
        """
        try:
            self.cursor.execute(sql.SQL(
                "CREATE USER {} WITH PASSWORD {} CONNECTION LIMIT {}"
            ).format(sql.Identifier(username), sql.Literal(password), sql.Literal(MAX_CONNECTIONS)))
            with self.conn.transaction():
                self.cursor.execute(sql.SQL(
                    "GRANT {} TO {};").format(sql.Identifier(role), sql.Identifier(username)))
            return True
        except DuplicateObject:
            return False

    def get_user_roles(self, username: str) -> tuple:
        """
        get all the roles of a given user
        Args:
            username:

        Returns: tuple containing all the user roles

        """
        roles = self.cursor.execute(sql.SQL(
            "SELECT rolname FROM pg_roles WHERE pg_has_role( {}, oid, 'member') ORDER BY 1;"
        ).format(sql.Literal(username))).fetchall()
        return tuple(x[0] for x in roles)

    def get_users_with_roles(self) -> tuple:
        """
        Get all users and their associated roles.

        Returns:
            tuple: A tuple containing all roles for each user.

        """
        roles = self.cursor.execute(sql.SQL(
            'SELECT r.rolname as username,r1.rolname as "role" FROM pg_catalog.pg_roles r '
            'JOIN pg_catalog.pg_auth_members m ON (m.member = r.oid) '
            'JOIN pg_roles r1 ON (m.roleid=r1.oid) WHERE r.rolcanlogin ORDER BY 1;'
        )).fetchall()
        return tuple(roles)

    def create_role(self, role, schema: (tuple, list, str) = None) -> bool:
        """
        default roles : admin, readonly, readwrite, readwritedel, webservice
        Returns:

        """
        if isinstance(schema, str):
            schema = (schema,)
        try:
            self.cursor.execute(sql.SQL(
                "CREATE ROLE {}"
            ).format(sql.Identifier(role)))
            with self.conn.transaction():
                self.cursor.execute(sql.SQL(
                    "GRANT CONNECT ON DATABASE {} TO {};").format(sql.Identifier(DB_NAME), sql.Identifier(role)))
                for sch in schema:
                    self.cursor.execute(sql.SQL(
                        "ALTER DEFAULT PRIVILEGES IN SCHEMA {} GRANT SELECT ON TABLES TO {};").format(
                        sql.Identifier(sch),
                        sql.Identifier(role)))
                    self.cursor.execute(sql.SQL(
                        "ALTER DEFAULT PRIVILEGES IN SCHEMA {} GRANT USAGE ON SEQUENCES TO {};").format(
                        sql.Identifier(sch),
                        sql.Identifier(role)))
                    self.cursor.execute(sql.SQL(
                        "ALTER DEFAULT PRIVILEGES IN SCHEMA {} GRANT EXECUTE ON FUNCTIONS TO {};").format(
                        sql.Identifier(sch),
                        sql.Identifier(role)))

            return True
        except DuplicateObject:
            return False

    def create_schema(self, schema: str) -> bool:
        """
        Create a new database schema.

        :param schema: The name of the schema to be created.
        :type schema: str
        :return: True if the schema was created successfully, False otherwise.
        :rtype: bool
        """
        try:
            self.cursor.execute(sql.SQL(
                "CREATE SCHEMA {}"
            ).format(sql.Identifier(schema)))
            return True
        except DuplicateSchema:
            return False

    def create_database(self, dbname: str) -> bool:
        """
        Create a new database.

        :param dbname: Name of the database to be created
        :type dbname: str
        :return: True if the database is created successfully, False if a database with the same name already exists
        :rtype: bool
        """
        try:
            self.cursor.execute(sql.SQL(
                "CREATE DATABASE {}"
            ).format(sql.Identifier(dbname)))
            return True
        except DuplicateDatabase:
            return False

    def update_role_privileges(self, role, schema, privileges: str):
        """
        :param role: The role for which the privileges need to be updated.
        :param schema: The schema on which the default privileges need to be set.
        :param privileges: The privileges to be granted. Only a single privilege can be granted at once.

        :return: None

        Raises:
        - AssertionError: If more than one privilege is provided.
        - KeyError: If an unknown privilege is provided.

        Example usage:
            pg_connector = PGConnector()
            pg_connector.update_role_privileges('role_name', 'schema', 'SELECT')
        """
        assert len(privileges) == 1, "cannot add multiple privileges at once"
        new_privileges = PGConnector.equivalence_dict.get(privileges, None)
        if new_privileges is None:
            raise KeyError(f'unknown privilege {privileges}')
        with self.conn.transaction():
            self.cursor.execute(sql.SQL("ALTER DEFAULT PRIVILEGES IN SCHEMA {} GRANT {} ON TABLES TO {};")
            .format(
                sql.Identifier(schema),
                sql.SQL(new_privileges.strip()),
                sql.Identifier(role)
            )
            )


if __name__ == "__main__":
    # 1 connect to the DB
    # declare a new PostgreSQL connection object
    from gkdb.config import DBConfig
    cfg = DBConfig()
    conn = connect(
        dbname="postgres",
        user=cfg.user,
        host=cfg.host,
        port=cfg.port,
        password=cfg.password,
        autocommit=True
    )

    # # 2 Initialize a DBConnector and create the DB
    manager = PGConnector(conn)
    manager.create_database(cfg.database)
    conn.close()

    # 2 Initialize a DBConnector and log to the DB
    conn = connect(
        dbname=cfg.database,
        user=cfg.user,
        host=cfg.host,
        port=cfg.port,
        password=cfg.password,
        autocommit=True
    )
    manager = PGConnector(conn)

    # 3 remove too permissive default roles
    manager.revoke_public_role()

    # 5 create the new schemes
    manager.create_schema(PROD_SCHEMA_NAME)
    manager.create_schema(DEV_SCHEMA_NAME)

    # 4 create the new roles : readonly, readwrite, readwritedelete, webservice and admin
    manager.create_role('readonly', (DEV_SCHEMA_NAME, PROD_SCHEMA_NAME))
    manager.create_role('readwrite', DEV_SCHEMA_NAME)
    manager.create_role('webservice', DEV_SCHEMA_NAME)
    manager.create_role('readwritedelete', DEV_SCHEMA_NAME)

    # update privileges from readwrite and readwritedelete
    manager.update_role_privileges('readwrite', DEV_SCHEMA_NAME, 'w')
    manager.update_role_privileges('readwritedelete', DEV_SCHEMA_NAME, 'w')
    manager.update_role_privileges('readwritedelete', DEV_SCHEMA_NAME, 'd')

    # # 7 and now we can create users
    # manager.add_user("myuser", "mypassword", "readonly")
    #
    # print(manager.get_users_with_roles())
    # print(manager.get_user_roles('myuser'))
