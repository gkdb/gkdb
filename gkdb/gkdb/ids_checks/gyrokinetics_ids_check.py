from __future__ import annotations

from gkdb.ids_checks.coordinates import Coordinates
from typing import Any
import logging
import numpy as np
import dataclasses
import idspy_dictionaries.ids_gyrokinetics_local as gkids
from idspy_toolkit.exceptions import IdsRequiredMissing, IdsForbiddenValue, IdsDimensionError, IdsNotValid
from gkdb.ids_checks.unit_checks import ids_check_pipeline

LOGGER = logging.getLogger(__name__)


def linear_coordinates(number_species: int, number_angles: int, number_time: int) -> dict:
    if None in (number_species, number_angles, number_time):
        raise AssertionError("None is not allowed as dimension.")
    dict_coordinates = {'Fluxes': (Coordinates(number_species),),
                        'MomentsLinear': (Coordinates(number_species, number_angles, number_time),),
                        'EigenmodeFields': (Coordinates(number_time), Coordinates(number_angles, number_time)),
                        }
    return dict_coordinates


def non_linear_coordinates(number_species, number_angles, number_times,
                           number_binormal_wavevectors, number_radial_wavevectors) -> dict:
    if None in (number_species, number_angles, number_times, number_binormal_wavevectors,
                number_radial_wavevectors):
        raise IdsDimensionError("None is not allowed as dimension.")
    if 0 in (number_species, number_angles, number_binormal_wavevectors,
             number_radial_wavevectors):
        raise IdsDimensionError(f"A dimension of 0 is not allowed for output fields."
                                f" dimensions for (species, angle_pol, time, binormal_wavevectors, "
                                f"radial_wavevectors) "
                                f"are {(number_species, number_angles, number_times, number_binormal_wavevectors, number_radial_wavevectors)}")
    dict_coordinates = {'GyrokineticsFieldsNl1D': (Coordinates(number_binormal_wavevectors),),
                        'GyrokineticsFieldsNl2DKy0': (Coordinates(number_binormal_wavevectors,
                                                                  number_times),),
                        'GyrokineticsFieldsNl2DFsAverage': (Coordinates(number_binormal_wavevectors,
                                                                        number_radial_wavevectors),),
                        'GyrokineticsFieldsNl3D': (Coordinates(number_binormal_wavevectors,
                                                               number_radial_wavevectors, number_angles),),
                        'GyrokineticsFieldsNl4D': (
                            Coordinates(number_binormal_wavevectors, number_radial_wavevectors,
                                        number_angles, number_times),),
                        'FluxesNl1D': (Coordinates(number_species),),
                        'FluxesNl2DSumKxKy': (Coordinates(number_species, number_times),),
                        'FluxesNl2DSumKx': (Coordinates(number_species, number_binormal_wavevectors),),
                        'FluxesNl3D': (Coordinates(number_species, number_binormal_wavevectors,
                                                   number_radial_wavevectors),),
                        'FluxesNl4D': (
                            Coordinates(number_species, number_binormal_wavevectors,
                                        number_radial_wavevectors,
                                        number_angles, ),),
                        'FluxesNl5D': (
                            Coordinates(number_species, number_binormal_wavevectors,
                                        number_radial_wavevectors,
                                        number_angles, number_times),),
                        }
    return dict_coordinates


def check_flux_surface(ids: gkids.idspy_gyrokinetics_local) -> bool:
    if ids_check_pipeline(ids, require_all=True) is False:
        return False

    if len(ids.shape_coefficients_c) != len(ids.dc_dr_minor_norm):
        LOGGER.warning("ids_to_check shape_coefficients_c is not equal to dc_dr_minor_norm")
        return False
    if len(ids.shape_coefficients_s) != len(ids.ds_dr_minor_norm):
        LOGGER.warning("ids_to_check shape_coefficients_s is not equal to ds_dr_minor_norm")
        return False
    return True


def check_wavevector(wv: gkids.Wavevector, length_species: int) -> bool:
    if not ids_check_pipeline(wv, require_all=False, required_members=("eigenmode",)):
        return False
    for eig in wv.eigenmode:
        if not ids_check_pipeline(eig, require_all=False):
            return False
        length_angle = len(eig.angle_pol)
        length_time = len(eig.time_norm)
        coordinates_constraints = linear_coordinates(length_species,
                                                     length_angle, length_time)
        for f in dataclasses.fields(eig):
            current_item = getattr(eig, f.name)
            # no fields to check at eigenmode level just for childrens
            if dataclasses.is_dataclass(current_item):
                current_ids = current_item.__class__.__name__
                current_constraints = coordinates_constraints.get(current_ids, None)
                if current_constraints is None:
                    continue
                for ff in dataclasses.fields(current_item):
                    current_subitem = getattr(current_item, ff.name)
                    if isinstance(current_subitem, np.ndarray):
                        if True not in (x.is_right_shape(current_subitem) for x in current_constraints):
                            raise IdsDimensionError(
                                f"{ff.name} of {f.name} has a wrong shape {current_subitem.shape}, it should be in "
                                f"{[x.shape for x in current_constraints]}")
    return True


def check_eigenmode(ids: gkids.Eigenmode) -> tuple[bool, bool, bool]:
    if ids_check_pipeline(ids, require_all=False) is False:
        return False, False, False
    if getattr(ids, "linear_weights", None) is not None:
        has_weight = ids_check_pipeline(ids.linear_weights, require_all=False,
                                        required_members=["particles_phi_potential",
                                                          "particles_a_field_parallel",
                                                          "particles_b_field_parallel",
                                                          "energy_phi_potential",
                                                          "energy_a_field_parallel",
                                                          "energy_b_field_parallel"])
    else:
        has_weight = False

    if getattr(ids, "linear_weights_rotating_frame", None) is not None:
        has_weight_rotating = ids_check_pipeline(ids.linear_weights_rotating_frame, require_all=False,
                                                 required_members=["particles_phi_potential",
                                                                   "particles_a_field_parallel",
                                                                   "particles_b_field_parallel",
                                                                   "energy_phi_potential",
                                                                   "energy_a_field_parallel",
                                                                   "energy_b_field_parallel"])
    else:
        has_weight_rotating = False
    return True, has_weight, has_weight_rotating


def check_linear_structure(ids: gkids.GyrokineticsLinear, length_species: int) -> tuple[bool, bool | None, bool | None]:
    if not ids_check_pipeline(ids, require_all=False, required_members=("wavevector",)):
        return False, None, None

    if length_species < 1:
        raise IdsForbiddenValue(f"Length species [{length_species}] cannot be less than 1 .")
    # check it has a non empty linear part and a non empty list of wavevectors
    has_weight = None
    has_weight_rotating = None
    for wv in ids.wavevector:
        if not check_wavevector(wv, length_species):
            return False, None, None
        for eig in wv.eigenmode:
            valid, ret_weight, ret_weight_rotating = check_eigenmode(eig)
            if not valid:
                return False, None, None

            if has_weight is None:
                has_weight = ret_weight
            elif ret_weight != has_weight:
                raise IdsNotValid("A valid IDS must have linear_weight for all eigenmodes or None of them.")

            if has_weight_rotating is None:
                has_weight_rotating = ret_weight_rotating
            elif ret_weight_rotating != has_weight_rotating:
                raise IdsNotValid(
                    "A valid IDS must have linear_weights_rotating_frame for all eigenmodes or None of them.")

    return True, has_weight, has_weight_rotating


def check_non_linear_structure(non_linear_ids: gkids.GyrokineticsNonLinear,
                               length_species: int) -> bool:
    if not ids_check_pipeline(non_linear_ids, require_all=False,
                              required_members=["code", "quasi_linear",
                                                "binormal_wavevector_norm", "radial_wavevector_norm", ]):
        return False

    if not ids_check_pipeline(non_linear_ids.code, require_all=False, ):
        raise IdsDimensionError(
            f"member code [{non_linear_ids.code}] of current ids_to_check appears to not be valid .")

    if length_species < 1:
        raise IdsForbiddenValue(f"Length species [{length_species}] cannot be less than 1 .")

    # consistency for non linear simulations
    length_angle = len(non_linear_ids.angle_pol)
    length_time = len(non_linear_ids.time_norm)
    length_nonlinear_binormal = len(non_linear_ids.binormal_wavevector_norm)
    length_nonlinear_radial = len(non_linear_ids.radial_wavevector_norm)

    coordinates_constraints = non_linear_coordinates(length_species, length_angle, length_time,
                                                     length_nonlinear_binormal, length_nonlinear_radial)
    for f in dataclasses.fields(non_linear_ids):

        current_item = getattr(non_linear_ids, f.name)
        # no fields to check at eigenmode level just for childrens
        if dataclasses.is_dataclass(current_item):
            current_ids = current_item.__class__.__name__
            current_constraints = coordinates_constraints.get(current_ids, None)
            if current_constraints is None:
                continue
            for ff in dataclasses.fields(current_item):
                current_subitem = getattr(current_item, ff.name)
                if isinstance(current_subitem, (list, tuple, np.ndarray)):
                    if not current_constraints[0].is_right_shape(current_subitem):
                        raise IdsDimensionError(
                            f"{ff.name} of {f.name} has a wrong shape {current_subitem.shape}, it should be {current_constraints[0].shape}")
    return True


def check_species(ids: gkids.Species) -> bool:
    return ids_check_pipeline(ids, require_all=True)


def check_model(ids: gkids.Model) -> bool:
    return ids_check_pipeline(ids, require_all=True)


def check_code(ids: gkids.Code) -> bool:
    return ids_check_pipeline(ids, require_all=False,
                              required_members=("name", "commit"))


def check_collisions(ids: gkids.Collisions, n_species: int) -> bool:
    if n_species == 0:
        raise IdsRequiredMissing("An IDS without any species is not valid.")

    is_valid = ids_check_pipeline(ids, require_all=True, )
    if not is_valid:
        return False

    if ids.collisionality_norm.shape != (n_species, n_species):
        raise IdsDimensionError(
            f"Collisionality norm has shape {ids.collisionality_norm.shape} "
            f"and it should have {(n_species, n_species)} as shape.")
    return True


def check_input_normalizing(ids: gkids.InputNormalizing):
    return ids_check_pipeline(ids, require_all=False)


def check_input_global(ids: gkids.InputSpeciesGlobal):
    return ids_check_pipeline(ids, require_all=True)


def check_gyrokinetics_ids(ids: gkids.GyrokineticsLocal) -> (bool, bool | None,
                                                             bool | None):
    if not ids_check_pipeline(ids, required_members=("species", "model", "code",
                                                     "species_all", "normalizing_quantities",
                                                     "flux_surface")):
        return False, None, None

    if not check_input_global(ids.species_all):
        return False, None, None

    if not check_input_normalizing(ids.normalizing_quantities):
        return False, None, None

    if not check_flux_surface(ids.flux_surface):
        return False, None, None

    if not check_model(ids.model):
        return False, None, None

    if not check_code(ids.code):
        return False, None, None

    for species in ids.species:
        if not check_species(species):
            return False, None, None

    nspecies = len(ids.species)
    if not check_collisions(ids.collisionality_norm, n_species=nspecies):
        return False, None, None

    has_linear, has_linear_weight, has_linear_weight_rotating = check_linear_structure(ids.linear, nspecies)
    has_non_linear = check_non_linear_structure(ids.non_linear, nspecies)
    return True, has_linear, has_non_linear
