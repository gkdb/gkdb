from __future__ import annotations

import dataclasses
import numpy as np
from typing import Any
import logging

from idspy_toolkit import is_default_imas_value
from idspy_toolkit.exceptions import  IdsTypeError, IdsRequiredMissing
from gkdb.core.constants import IMAS_DEFAULT_INT, IMAS_DEFAULT_FLOAT, \
    IMAS_DEFAULT_CPLX, IMAS_DEFAULT_STR, IMAS_DEFAULT_LIST, IMAS_DEFAULT_ARRAY, COLUMNS_TO_IGNORE

LOGGER = logging.getLogger()


def is_ids_valid_type(ids: any) -> bool:
    return dataclasses.is_dataclass(ids)


def check_no_none(ids: Any) -> bool:
    """
        Check if any attribute of the provided object contains None values.

        Parameters
        ----------
        :param ids : The object to check for None values.
        :type ids: Any

        :returns:  True if no attribute contains None values, False otherwise.
        :rtype: bool

        :raises : IdsTypeError: If the provided object is not a valid type for the check.

        Notes
        -----
        This function iterates over the attributes of the provided object. If any attribute
        contains None values, it logs an error message and returns False. Otherwise, it
        returns True.

        """

    if not is_ids_valid_type(ids):
        raise IdsTypeError(msg=f"current value is not an ids but an {type(ids)}",
                           logger=LOGGER)

    for f in dataclasses.fields(ids):
        if f.name in COLUMNS_TO_IGNORE:
            continue


        current_value = getattr(ids, f.name)

        # value is None and object is  a builtin type
        if (current_value is None) and (f.type.__module__ != "typing"):
            logging.error(
                f"ids member {f.name} of ids {ids.__class__.__name__} is None,"
                f" it should be a default IMAS value ")
            return False

        if isinstance(current_value, (np.ndarray, list, tuple, set)):
            if None in current_value:
                logging.error(
                    f"ids member {f.name} of ids {ids.__class__.__name__} has null value in,"
                    f" it should be a default IMAS value")
                return False

        # # check if current member is a dataclass
        # if is_ids_valid_type(current_value):
        #     continue
        #
        # if is_default_imas_value(ids, f.name):
        #     continue


    return True


def check_array_size(ids: Any, ids_member_name: str,
                     expected_shape: int | list | tuple) -> bool:
    """
       Check if the size of an array attribute of the provided object matches the expected shape.

       Parameters
       ----------
       ids : Any
           The object containing the array attribute to check.
       ids_member_name : str
           The name of the array attribute.
       expected_shape : int or list or tuple
           The expected shape of the array attribute. Can be an integer for a 1D array
           or a list/tuple specifying the shape for arrays of higher dimensions.

       Returns
       -------
       bool
           True if the size of the array attribute matches the expected shape, False otherwise.

       Notes
       -----
       This function checks if the specified attribute of the provided object is an array-like
       object (e.g., list, tuple, set, or numpy.ndarray) and then compares its shape with the
       expected shape. If the shape matches, it returns True; otherwise, it logs an error message
       and returns False.

       Raises
       ------
       ValueError
           If `expected_shape` is not a valid shape specification.

       Examples
       --------
       >>> # Example usage:
       >>> class ExampleIds:
       ...     def __init__(self, data):
       ...         self.data = data
       ...
       >>> ids_instance = ExampleIds([1, 2, 3, 4, 5])
       >>> check_array_size(ids_instance, 'data', 5)
       True
       >>> check_array_size(ids_instance, 'data', 3)
       False
       >>> check_array_size(ids_instance, 'data', (5,))
       True
       >>> check_array_size(ids_instance, 'data', (3,))
       False
       """
    is_ids_valid_type(ids)
    current_value = getattr(ids, ids_member_name)

    if not isinstance(current_value, (np.ndarray, list, tuple, set)):
        logging.error(
            f"ids member {ids_member_name} of ids {ids.__class__.__name__} must be a list or equivalent,"
            f" not an {type(current_value)}")
        return False

    current_value_as_array = np.asarray(current_value)
    if isinstance(expected_shape, int):
        if len(current_value_as_array.shape) != 1:
            logging.error(f"ids member {ids_member_name} of ids {ids.__class__.__name__}"
                          f" must be a 1D array,"
                          f" not an array of shape {current_value_as_array.shape}")
            return False
        return len(current_value) == expected_shape
    else:

        if current_value_as_array.shape != expected_shape:
            logging.error(
                f"ids member {ids_member_name} of ids {ids.__class__.__name__} has a shape"
                f" of {current_value_as_array.shape} and not  {expected_shape}")
            return False

    return True



def get_not_default_ids_members(ids_to_check: Any) -> tuple:
    """
        Retrieve non-null members from the provided 'ids' object.

        Parameters
        ----------
        ids_to_check : Any
            The 'ids' object to retrieve non-null members from.

        Returns
        -------
        tuple
            A tuple containing the names of non-null members.
            members being ids are considered as not default if present

        Notes
        -----
        This function iterates over the members of the provided 'ids' object and retrieves
        the names of those members that do not have default IMAS values.
        If a member is a nested dataclass or is in the list of columns to ignore, it is skipped.
        """
    number_members = 1
    number_default = 0
    non_null_ids = []
    for member in dataclasses.fields(ids_to_check):
        if member.name in COLUMNS_TO_IGNORE:
            continue

        if dataclasses.is_dataclass(getattr(ids_to_check, member.name)):
            non_null_ids.append(member.name)
        elif is_default_imas_value(ids_to_check, member.name):
            number_default += 1
        else:
            non_null_ids.append(member.name)
        number_members = number_members + 1

    # all members contain default imas values
    if number_members == number_default:
        LOGGER.warning(f"ids {ids_to_check.__class__.__name__} contains only IMAS default values")
    return tuple(non_null_ids)


def contains_all_required(required_list: list | tuple, given_list: list | tuple):
    """
    Check if all elements in the required list are present in the given list.

    Parameters
    ----------
    required_list : list
        The list containing elements that are required to be present.
    given_list : list
        The list to check for the presence of required elements.

    Returns
    -------
    tuple
        A tuple containing two elements:
        - A boolean indicating whether all required elements are present in the given list.
        - A list of elements from the required list that are missing in the given list.
    """
    missing_elements = [value for value in required_list if value not in given_list]
    return len(missing_elements) == 0, missing_elements


def ids_check_pipeline(ids: Any, require_all: bool = True,
                       required_members: list | tuple | None = None) -> bool:
    """
        Perform a series of checks on the provided 'ids' object.

        Parameters
        ----------
        ids : Any
            The object to be checked.
        require_all : bool
            are all members of the current ids object required or not? Default is True.
        required_members : list or None, optional
            A list of required members that must be present in 'ids'. If None, no members are required. (Default is None)

        Returns
        -------
        bool
            True if all checks pass, False otherwise.

        Raises
        ------
        IdsTypeError
            If 'ids' is not a valid type or if required members are missing.

        Notes
        -----
        This function performs a pipeline of checks on the provided 'ids' object:
        1. Checks if 'ids' is a valid type.
        2. Ensures 'ids' has no None/Null values.
        3. Checks that 'ids' does not contain only IMAS default values.
        4. Optionally, checks if 'ids' contains all required members.

        """
    if not is_ids_valid_type(ids):
        raise IdsTypeError(f"ids must be a valid ids object not an {type(ids)}", LOGGER, )

    if require_all and (required_members is not None):
        raise AttributeError("'require_all' and 'required_members' are mutually exclusive.")

    if require_all:
        required_members = tuple([f.name for f in dataclasses.fields(ids) if f.name not in COLUMNS_TO_IGNORE])
    else:
        if required_members is not None:
            if not isinstance(required_members, (list, set, tuple)):
                raise AttributeError(f"required_members must be a list, set, or tuple, not {type(required_members)}")

    # check ids has no None/Null in it
    if check_no_none(ids) is False:
        raise IdsTypeError(f"current ids {ids.__class__.__name__} has None values in")

    # check that ids does not contain only IMAS default values
    non_empty_members = get_not_default_ids_members(ids)
    if len(non_empty_members) == 0:
        LOGGER.warning(f"ids {ids.__class__.__name__} contains only IMAS default values")
        return False

    if required_members is not None:
        has_all, list_missing = contains_all_required(required_members, non_empty_members)
        if has_all is False:
            raise IdsRequiredMissing(f"In ids {ids.__class__.__name__}, "
                                     f"missing required members : [{list_missing}]",
                                     LOGGER, )

    return True
