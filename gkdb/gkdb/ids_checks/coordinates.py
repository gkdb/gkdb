from __future__ import annotations
from dataclasses import dataclass, field
import numpy as np

from idspy_dictionaries import ids_gyrokinetics_local as gkids

COORDINATES_TRANSLATION = {
    '1...N': -1,
    '1...2': 2,
    "/species": "/GyrokineticsLocal/species",
    '../../species_all/angle_pol': None,
    '../shape_coefficients_c': None,
    '../shape_coefficients_s': None,
    '../../../../../species': "/GyrokineticsLocal/species",
    '../../angle_pol': None,
    '../../time_norm': None,
    '../../binormal_wavevector_norm': None,
    '../../radial_wavevector_norm': None,
    '../../species': "/GyrokineticsLocal/species",
}




@dataclass(slots=True)
class Coordinates:
    coord1: int | None = None
    coord2: int | None = None
    coord3: int | None = None
    coord4: int | None = None
    coord5: int | None = None
    coord6: int | None = None
    ndims: int = field(init=False)
    maxdim: int = field(init=False, default=10)

    def __post_init__(self):
        self.ndims = 0
        for idx in range(self.maxdim, 0, -1):
            if hasattr(self, f"coord{idx}"):
                current_val = getattr(self, f"coord{idx}", None)
                if current_val is not None:
                    self.ndims = idx
                    break

    def is_right_shape(self, item: list | set | tuple) -> bool:
        if hasattr(item, "shape"):
            return self.shape == item.shape
        try:
            return self.shape == (len(item),)
        except TypeError:
            raise AttributeError(f"item is not a list or a list-like object but a {type(item)}")

    @property
    def shape(self):
        return tuple([x for x in [getattr(self, f"coord{idx}")
                                  for idx in range(1, self.maxdim)
                                  if hasattr(self, f"coord{idx}")]
                      if x is not None])
