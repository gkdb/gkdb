from sqlalchemy import create_engine, Integer, String, DateTime, Text, ForeignKey, \
    Float, Boolean, inspect, Table, Column, SmallInteger, text, BigInteger, REAL
from sqlalchemy.orm import relationship, sessionmaker
from datetime import datetime
from sqlalchemy import DateTime, String, Text, UniqueConstraint, CheckConstraint
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from typing import Optional
from sqlalchemy.dialects.postgresql import UUID as pgUUID
from sqlalchemy.dialects.postgresql import ARRAY, JSONB, BYTEA
from dataclasses import dataclass
import constants
import os

import sqlalchemy as sa

from sqlalchemy.types import UserDefinedType


# rajouter le type pour les moments et flux pour savoir ce que c'est


class PGComplexWrapper(UserDefinedType):
    """
    This class implements a custom SQLAlchemy UserDefinedType for storage and retrieval of complex numbers in a PostgreSQL database.

    Attributes:
        N/A

    Methods:
        - get_col_spec: Returns the column specification for the custom complex type.
        - bind_processor: Returns a processor function that converts a complex number to a tuple of its real and imaginary parts.
        - result_processor: Returns a processor function that converts a tuple of real and imaginary parts to a complex number.

    Example usage:
        # Creating a table with a column of type PGComplexWrapper
        class MyTable(Base):
            __tablename__ = 'my_table'
            id = Column(Integer, primary_key=True)
            complex_number = Column(PGComplexWrapper)

        # Inserting a complex number into the table
        session.add(MyTable(complex_number=3+4j))
        session.commit()

        # Retrieving the complex number from the table
        my_instance = session.sql_query(MyTable).first()
        print(my_instance.complex_number)  # Output: (3+4j)
    """

    def get_col_spec(self):
        return "complex"

    def bind_processor(self, dialect):
        def process(value):
            if value is not None:
                return (value.real, value.imag)
            return None

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is not None:
                return complex(*(map(float, value[1:-1].split(','))))
            return None

        return process


class PGTupleWrapper(UserDefinedType):
    def get_col_spec(self):
        return "complex"

    def bind_processor(self, dialect):
        def process(value):
            if value is not None:
                return (value[0], value[1])
            return None

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is not None:
                return tuple(*(map(float, value[1:-1].split(','))))
            return None

        return process


metadata_obj = sa.MetaData(schema="gkdb_development")


class IdsBaseClass(DeclarativeBase):
    metadata = metadata_obj


@dataclass
class Complex:
    r: float
    i: float


# class Complex(IdsBaseClass):
#     __tablename__ = 'complex'
#     id = sa.Column(Integer, primary_key=True)
#     balance = sa.Column(
#         CompositeType(
#             'complex_type',
#             [
#                 sa.Column('r', REAL),
#                 sa.Column('i', Integer)
#             ]
#         )
#     )


ids_tags_table = Table(
    "ids_tags_table",
    IdsBaseClass.metadata,
    Column("gk_id", BigInteger, ForeignKey("gyrokinetics_table.id"), primary_key=True),
    Column("tag_id", Integer, ForeignKey("tag_table.id"), primary_key=True),
)


class Tag(IdsBaseClass):
    __tablename__ = 'tag_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    tagid: Mapped[str] = mapped_column(String(32), nullable=False)
    categorie: Mapped[str] = mapped_column(String(32),
                                           CheckConstraint(
                                               "categorie IN ('{}')".format("', '".join(constants.TAG_CATEGORIES))),
                                           nullable=False, default="other",
                                           index=True, )
    description: Mapped[Optional[str]] = mapped_column(Text, nullable=True)

    gyrokinetics: Mapped[list["Gyrokinetics"]] = relationship(
        secondary=ids_tags_table, back_populates="tag"
    )

    __table_args__ = (
        UniqueConstraint('tagid', 'categorie'),
    )


class FileEntry(IdsBaseClass):
    __tablename__ = 'file_entry_table'

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    # TODO: define if 1 or multiple files and if multiple, how to split
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'),
                                              nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="file_entry")
    hash: Mapped[str] = mapped_column(String(64), nullable=False, unique=True)
    filename: Mapped[str] = mapped_column(String(256), nullable=False, unique=True)
    storage: Mapped[str] = mapped_column(String(256), nullable=False, default="local")

    __table_args__ = (
        UniqueConstraint('hash', 'storage'),
    )


# class CodePartialConstant(IdsBaseClass):
#     __tablename__ = 'code_partial_constant_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     eigenmode_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('eigenmode_table.id'),
#                                               nullable=False)
#     code_id: Mapped[int] = mapped_column(ForeignKey('code_table.id'), nullable=False)
#     eigenmode: Mapped["Eigenmode"] = relationship(back_populates="code")
#     parameters: Mapped[dict] = mapped_column(JSONB, nullable=False, unique=False)
#     output_flag: Mapped[int] = mapped_column(Integer,
#                                                    nullable=True, )
#

class Collisions(IdsBaseClass):
    __tablename__ = 'collisions_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'),
                                              nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="collisions")
    collisionality_norm: Mapped[float] = mapped_column(REAL, nullable=False, unique=False)
    specie_one: Mapped[int] = mapped_column(ForeignKey('species_table.id'), nullable=False)
    specie_two: Mapped[int] = mapped_column(ForeignKey('species_table.id'), nullable=False)


class FluxSurface(IdsBaseClass):
    __tablename__ = 'flux_surface_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'),
                                              nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="flux_surface")

    r_minor_norm: Mapped[float] = mapped_column(REAL, nullable=False,
                                                default=constants.IMAS_DEFAULT_FLOAT)
    elongation: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    delongation_dr_minor_norm: Mapped[float] = mapped_column(REAL,
                                                             nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    dgeometric_axis_r_dr_minor: Mapped[float] = mapped_column(REAL,
                                                              nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    dgeometric_axis_z_dr_minor: Mapped[float] = mapped_column(REAL,
                                                              nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    q: Mapped[float] = mapped_column(REAL, nullable=False,
                                     default=constants.IMAS_DEFAULT_FLOAT)
    magnetic_shear_r_minor: Mapped[float] = mapped_column(REAL,
                                                          nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    pressure_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                          nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    ip_sign: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    b_field_tor_sign: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    shape_coefficients_c = mapped_column(BYTEA, nullable=False, unique=False)
    dc_dr_minor_norm = mapped_column(BYTEA,
                                     nullable=False, unique=False)
    shape_coefficients_s = mapped_column(BYTEA,
                                         nullable=False, unique=False)
    ds_dr_minor_norm = mapped_column(BYTEA,
                                     nullable=False, unique=False)


class Fluxes(IdsBaseClass):
    __tablename__ = 'fluxes_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'),
                                              nullable=False, index=True)
    species_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
                                            nullable=True, index=True)
    eigenmode_id: Mapped[BigInteger] = mapped_column(ForeignKey('eigenmode_table.id'),
                                              nullable=True, index=True)

    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="fluxes_integrated_norm")

    # can be particle or laboratory
    frame: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)
    # can be integrated, linear_weight, nonlinear
    ftype: Mapped[str] = mapped_column(String(16), nullable=False, unique=False)
    # copy of the associated IDS uuid
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, index=True)

    particles_phi_potential: Mapped[float] = mapped_column(REAL, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    particles_a_field_parallel: Mapped[float] = mapped_column(REAL,
                                                              nullable=False,
                                                              default=constants.IMAS_DEFAULT_FLOAT)
    particles_b_field_parallel: Mapped[float] = mapped_column(REAL,
                                                              nullable=False,
                                                              default=constants.IMAS_DEFAULT_FLOAT)
    energy_phi_potential: Mapped[float] = mapped_column(REAL, nullable=False,
                                                        default=constants.IMAS_DEFAULT_FLOAT)
    energy_a_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    energy_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                           default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_phi_potential: Mapped[float] = mapped_column(REAL, nullable=False,
                                                                       default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_a_field_parallel: Mapped[float] = mapped_column(REAL,
                                                                          nullable=False,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_parallel_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                                          default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_phi_potential: Mapped[float] = mapped_column(REAL, nullable=False,
                                                                            default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_a_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)
    momentum_tor_perpendicular_b_field_parallel: Mapped[float] = mapped_column(REAL, nullable=False,
                                                                               default=constants.IMAS_DEFAULT_FLOAT)


class InputNormalizing(IdsBaseClass):
    __tablename__ = 'input_normalizing_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="normalizing_quantities")

    t_e: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    n_e: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    r: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    b_field_tor: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)


class SpeciesAll(IdsBaseClass):
    __tablename__ = 'species_all_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="species_all")

    beta_reference: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    velocity_tor_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    zeff: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    debye_length_reference: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    shearing_rate_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)


class Model(IdsBaseClass):
    __tablename__ = 'model_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="model")

    include_centrifugal_effects: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                              default=constants.IMAS_DEFAULT_INT)
    include_a_field_parallel: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    include_b_field_parallel: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    include_full_curvature_drift: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                               default=constants.IMAS_DEFAULT_INT)
    collisions_pitch_only: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                        default=constants.IMAS_DEFAULT_INT)
    collisions_momentum_conservation: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                                   default=constants.IMAS_DEFAULT_INT)
    collisions_energy_conservation: Mapped[bool] = mapped_column(Boolean,
                                                                 nullable=False,
                                                                 default=constants.IMAS_DEFAULT_INT)
    collisions_finite_larmor_radius: Mapped[bool] = mapped_column(Boolean, nullable=False,
                                                                  default=constants.IMAS_DEFAULT_INT)
    non_linear_run: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)
    time_interval_norm = mapped_column(BYTEA, nullable=True,
                                       unique=False)


#
# class Moments(IdsBaseClass):
#     __tablename__ = 'moments_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     fluxesmoments_id: Mapped[int] = mapped_column(ForeignKey('fluxes_moments_table.id'), nullable=False)
#
#     density = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                             nullable=False, unique=False)
#     density_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                    nullable=False, unique=False)
#     j_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                nullable=False, unique=False)
#     j_parallel_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                       nullable=False, unique=False)
#     pressure_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                       nullable=False, unique=False)
#     pressure_parallel_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                              nullable=False, unique=False)
#     pressure_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                            nullable=False, unique=False)
#     pressure_perpendicular_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                   nullable=False, unique=False)
#     heat_flux_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                        nullable=False, unique=False)
#     heat_flux_parallel_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                               nullable=False, unique=False)
#     v_parallel_energy_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                     nullable=False, unique=False)
#     v_parallel_energy_perpendicular_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                            nullable=False, unique=False)
#     v_perpendicular_square_energy = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                   nullable=False, unique=False)
#     v_perpendicular_square_energy_gyroav = mapped_column(ARRAY(PGComplexWrapper, dimensions=2),
#                                                          nullable=False, unique=False)


# class MomentsParticles(IdsBaseClass):
#     __tablename__ = 'moments_particles_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     fluxesmoments_id: Mapped[int] = mapped_column(ForeignKey('fluxes_moments_table.id'), nullable=False)
#
#     density = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     j_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     pressure_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     pressure_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     heat_flux_parallel = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     v_parallel_energy_perpendicular = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#     v_perpendicular_square_energy = mapped_column(ARRAY(PGComplexWrapper, dimensions=2), nullable=False, unique=False)
#
# class FluxesMoments(IdsBaseClass):
#     __tablename__ = 'fluxes_moments_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False)
#     eigenmode_id: Mapped[BigInteger] = mapped_column(ForeignKey('eigenmode_table.id'),
#                                               nullable=False)
#     specie_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
#                                            nullable=False)
#
#     eigenmode: Mapped["Eigenmode"] = relationship(back_populates="fluxes_moments")
#
#     moments_particle_id: Mapped[int] = mapped_column(ForeignKey('moments_table.id'),
#                                                      nullable=False)
#
#     moments_id: Mapped[int] = mapped_column(ForeignKey('moments_table.id'),
#                                             nullable=False)
#
#     moments_gyroav_id: Mapped[int] = mapped_column(ForeignKey('moments_table.id'),
#                                                    nullable=False)
#     # moments_norm_gyrocenter_id: Optional[Moments] = Mapped[int] = mapped_column(ForeignKey('wavevector.id'), nullable=False)
#     # moments_norm_particle_id: Optional[MomentsParticles] = relationship()
#     # fluxes_norm_gyrocenter_id: Optional[Fluxes] = relationship()
#     # fluxes_norm_gyrocenter_rotating_frame_id: Optional[Fluxes] = relationship()
#     # fluxes_norm_particle_id: Optional[Fluxes] = relationship()
#     # fluxes_norm_particle_rotating_frame_id: Optional[Fluxes] = relationship()
#     moments_norm_gyrocenter_id: Mapped[Optional[int]] = mapped_column(ForeignKey('moments_table.id'), nullable=True)
#     moments_norm_particle_id: Mapped[Optional[int]] = mapped_column(ForeignKey('moments_table.id'),
#                                                                     nullable=True)
#     fluxes_norm_gyrocenter_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'), nullable=True)
#     fluxes_norm_gyrocenter_rotating_frame_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'),
#                                                                                     nullable=True)
#     fluxes_norm_particle_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'), nullable=True)
#     fluxes_norm_particle_rotating_frame_id: Mapped[Optional[int]] = mapped_column(ForeignKey('fluxes_table.id'),
#                                                                                   nullable=True)


class Moments(IdsBaseClass):
    __tablename__ = 'moments_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)

    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False)

    species_id: Mapped[int] = mapped_column(ForeignKey('species_table.id'),
                                            nullable=True, index=True)
    eigenmode_id: Mapped[BigInteger] = mapped_column(ForeignKey('eigenmode_table.id'),
                                              nullable=True, index=True)

    mtype: Mapped[str] = mapped_column(String(16), default="default")
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, index=True)
    norm: Mapped[bool] = mapped_column(Boolean,
                                      nullable=True, index=False, default = False)
    # fluxesmoments_id: Mapped[int] = mapped_column(ForeignKey('fluxes_moments_table.id'),
    #                                              nullable=False)

    density = mapped_column(BYTEA, nullable=False, unique=False)
    j_parallel = mapped_column(BYTEA, nullable=False, unique=False)
    pressure_parallel = mapped_column(BYTEA, nullable=False, unique=False)
    pressure_perpendicular = mapped_column(BYTEA, nullable=False, unique=False)
    heat_flux_parallel = mapped_column(BYTEA, nullable=False, unique=False)
    v_parallel_energy_perpendicular = mapped_column(BYTEA, nullable=False, unique=False)
    v_perpendicular_square_energy = mapped_column(BYTEA, nullable=False, unique=False)


class Species(IdsBaseClass):
    __tablename__ = 'species_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="species")

    charge_norm: Mapped[float] = mapped_column(REAL, nullable=False,
                                               default=constants.IMAS_DEFAULT_FLOAT)
    mass_norm: Mapped[float] = mapped_column(REAL,
                                             nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    density_norm: Mapped[float] = mapped_column(REAL,
                                                nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    density_log_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                             nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    temperature_norm: Mapped[float] = mapped_column(REAL,
                                                    nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    temperature_log_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                                 nullable=False,
                                                                 default=constants.IMAS_DEFAULT_FLOAT)
    velocity_tor_gradient_norm: Mapped[float] = mapped_column(REAL,
                                                              nullable=False, default=constants.IMAS_DEFAULT_FLOAT)


class IdsProvenanceNode(IdsBaseClass):
    __tablename__ = 'ids_provenance_node_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    #  ids_provenance_id: Mapped[int] = mapped_column(ForeignKey("ids_provenance_table.id"))
    path: Mapped[str] = mapped_column(String(512), nullable=False)
    sources: Mapped[list[str]] = mapped_column(ARRAY(String(256), dimensions=1), nullable=False, unique=False)
    ids_properties_id: Mapped[int] = mapped_column(ForeignKey("ids_properties_table.id"))


# class IdsProvenance(IdsBaseClass):
#     # node: list[IdsProvenanceNode]
#     __tablename__ = 'ids_provenance_table'
#     id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
#     ids_properties_id: Mapped[int] = mapped_column(ForeignKey("ids_properties_table.id"))
#     ids_provenance_node: Mapped[list[IdsProvenanceNode]] = relationship()


class IdsProperties(IdsBaseClass):
    __tablename__ = 'ids_properties_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="ids_properties")

    comment: Mapped[str] = mapped_column(Text, nullable=False)
    homogeneous_time: Mapped[int] = mapped_column(Integer, nullable=False, default=constants.IMAS_DEFAULT_INT)
    provider: Mapped[str] = mapped_column(String(512), nullable=False)
    creation_date: Mapped[str] = mapped_column(String(512), nullable=False)
    # provenance: Mapped[Optional[IdsProvenance]] = relationship()
    provenance: Mapped[Optional[list[IdsProvenanceNode]]] = relationship()


class Library(IdsBaseClass):
    __tablename__ = 'library_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    code_id: Mapped[int] = mapped_column(ForeignKey("code_table.id"))
    code: Mapped["Code"] = relationship(back_populates="library")

    name: Mapped[str] = mapped_column(String(64), nullable=False)
    commit: Mapped[str] = mapped_column(String(512), nullable=False)
    version: Mapped[str] = mapped_column(String(32), nullable=False)
    repository: Mapped[str] = mapped_column(String(256), nullable=False)
    parameters: Mapped[dict] = mapped_column(JSONB, nullable=False)


class Code(IdsBaseClass):
    __tablename__ = 'code_table'
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="code")

    name: Mapped[str] = mapped_column(String(64), nullable=False, index=True)
    commit: Mapped[str] = mapped_column(String(512), nullable=False)
    version: Mapped[str] = mapped_column(String(32), nullable=False)
    repository: Mapped[str] = mapped_column(String(256), nullable=False)
    parameters: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)
    output_flag: Mapped[list[int]] = mapped_column(BYTEA, nullable=False)
    library: Mapped[Optional[list[Library]]] = relationship(back_populates="code")


class Eigenmode(IdsBaseClass):
    __tablename__ = 'eigenmode_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    wavevector_id: Mapped[BigInteger] = mapped_column(ForeignKey('wavevector_table.id'),
                                                      nullable=False, index=True)
    gk_id: Mapped[BigInteger] = mapped_column(ForeignKey('gyrokinetics_table.id'),
                                              nullable=False, index=True)
    wavevector: Mapped["Wavevector"] = relationship(back_populates="eigenmode")

    fluxes_moments: Mapped[list["FluxesMoments"]] = relationship(back_populates="eigenmode")
    code_parameters: Mapped[dict] = mapped_column(JSONB, nullable=True, unique=False)

    poloidal_turns: Mapped[int] = mapped_column(Integer, nullable=False, default=constants.IMAS_DEFAULT_INT)
    growth_rate_norm: Mapped[float] = mapped_column(REAL, nullable=True,  index=True)
    frequency_norm: Mapped[float] = mapped_column(REAL, nullable=True,  index=True)
    growth_rate_tolerance: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    phi_potential_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
    phi_potential_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
    a_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
    a_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
    b_field_parallel_perturbed_weight = mapped_column(BYTEA, nullable=True, unique=False)
    b_field_parallel_perturbed_parity = mapped_column(BYTEA, nullable=True, unique=False)
    poloidal_angle = mapped_column(BYTEA, nullable=True, unique=False)
    phi_potential_perturbed_norm = mapped_column(BYTEA, nullable=True, unique=False)
    a_field_parallel_perturbed_norm = mapped_column(BYTEA,
                                                    nullable=True, unique=False)
    b_field_parallel_perturbed_norm = mapped_column(BYTEA,
                                                    nullable=True, unique=False)
    time_norm = mapped_column(BYTEA, nullable=True, unique=False)

    initial_value_run: Mapped[bool] = mapped_column(Boolean, nullable=False, default=constants.IMAS_DEFAULT_INT)


class Wavevector(IdsBaseClass):
    __tablename__ = 'wavevector_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    gk_id: Mapped[BigInteger] = mapped_column(BigInteger, ForeignKey('gyrokinetics_table.id'), nullable=False, index=True)
    gyrokinetics: Mapped["Gyrokinetics"] = relationship(back_populates="wavevector")
    eigenmode: Mapped[Optional[list[Eigenmode]]] = relationship(back_populates="wavevector")

    radial_component_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)
    binormal_component_norm: Mapped[float] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)


class Gyrokinetics(IdsBaseClass):
    __tablename__ = 'gyrokinetics_table'
    id: Mapped[BigInteger] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    uuid: Mapped[str] = mapped_column(pgUUID(as_uuid=True),
                                      nullable=False, server_default=text("uuid_generate_v7()"), index=True)
    time: Mapped[Optional[float]] = mapped_column(REAL, nullable=False, default=constants.IMAS_DEFAULT_FLOAT)

    ids_properties: Mapped[Optional[IdsProperties]] = relationship(back_populates="gyrokinetics")
    normalizing_quantities: Mapped[Optional[InputNormalizing]] = relationship(back_populates="gyrokinetics")
    fluxes_integrated_norm: Mapped[list[Fluxes]] = relationship(back_populates="gyrokinetics")

    collisions: Mapped[Collisions] = relationship(back_populates="gyrokinetics")
    species: Mapped[list[Species]] = relationship(back_populates="gyrokinetics")
    wavevector: Mapped[Optional[list[Wavevector]]] = relationship(back_populates="gyrokinetics")
    code: Mapped[Optional[Code]] = relationship(back_populates="gyrokinetics")
    file_entry: Mapped[Optional[list[FileEntry]]] = relationship(back_populates="gyrokinetics")
    flux_surface: Mapped[Optional[FluxSurface]] = relationship(back_populates="gyrokinetics")
    model: Mapped[Optional[Model]] = relationship(back_populates="gyrokinetics")
    species_all: Mapped[Optional[SpeciesAll]] = relationship(back_populates="gyrokinetics")
    tag: Mapped[Optional[list[Tag]]] = relationship(
        secondary=ids_tags_table, back_populates="gyrokinetics"
    )

    __table_args__ = (
        UniqueConstraint('uuid'),
        #   Index('ix_ids_properties_id', 'id', unique=True),
    )


class COMPLEX(sa.sql.sqltypes.TypeEngine):
    __visit_name__ = "complex"

    def __init__(self, x=None, y=None):
        # e.g. 'PointZ'
        self.x = x
        # e.g. '4326'
        self.y = y


sa.dialects.postgresql.base.ischema_names['complex'] = COMPLEX

if __name__ == "__main__":
    user = os.getenv('GKDB_DB_ROOT', 'faster')
    password = os.getenv('GKDB_DB_ROOTPWD', 'faster')
    host = os.getenv('GKDB_DB_HOST', 'localhost')
    port = os.getenv('GKDB_DB_PORT', '5432')
    database = os.getenv('GKDB_NAME', 'gkdb')

    engine = create_engine(f"postgresql+psycopg://{user}:{password}@{host}:{port}/{database}", echo=True)


    # have to do this in case where DB is not declared in the public schema but in any other schema
    @sa.event.listens_for(engine, "connect")
    def set_schema(dbapi_connection, connection_record):
        cursor = dbapi_connection.cursor()
        cursor.execute("SET search_path TO gkdb_development")
        cursor.close()


    with engine.connect() as con:
        try:
            con.execute(text("set schema 'gkdb_development';CREATE EXTENSION pg_uuidv7;"))
        except sa.exc.ProgrammingError:
            con.rollback()
            con.commit()
        try:
            con.execute(text("set schema 'gkdb_development';"
                             """
    DO $$ BEGIN 
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'complex') THEN
        CREATE TYPE complex AS (
            r real,
            i real
        );
    END IF;
    CREATE TABLE dummy_table (dummy_column complex);
    DROP TABLE dummy_table;
    END $$;
    """))
        except sa.exc.ProgrammingError as e:
            print(e)
            con.rollback()
            con.commit()
        con.commit()
        try:
            con.execute(text("""CREATE OR REPLACE FUNCTION get_table_counts_and_size(p_schema text)
RETURNS TABLE(table_name text, row_count bigint, table_size text) AS $$
DECLARE 
    _tbl text; 
BEGIN 
    FOR _tbl IN 
      SELECT tablename  
      FROM   pg_tables 
      WHERE  schemaname = p_schema
    LOOP
        RETURN QUERY EXECUTE format(
            'SELECT %L::text, count(*), pg_size_pretty(pg_total_relation_size(%L::regclass)) FROM %I.%I', 
            _tbl, _tbl, p_schema, _tbl);
    END LOOP;  
END;
$$ LANGUAGE PLPGSQL;
"""))
        except sa.exc.ProgrammingError as e:
            print(e)
            con.rollback()
            con.commit()
        con.commit()
    IdsBaseClass.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)

    inspector = inspect(engine)
    table_names = inspector.get_table_names()
    print([(k,
            list(getattr(IdsProperties, k).__annotations__.items()))
           for k, v in IdsProperties.__annotations__.items()])
    for table_name in table_names:
        print(f"Table:{table_name}")
        column_items = inspector.get_columns(table_name)
        print('\t'.join(n for n in column_items[0]))
        for c in column_items:
            assert len(c) == len(column_items[0])
            print('\t'.join(str(c[n]) for n in c))
    print(table_names)
    print("end of script")
