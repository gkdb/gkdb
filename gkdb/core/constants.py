# -*- coding: utf-8 -*-
# @author : Guillaume
# @date :  24/01/2023
# @version : 1.0.0
# base on gkdb/ids_check
#
#
# Changelog :
#
#
#
from typing import Tuple, List, Dict
from numpy import array

ELECTRON_NORM: Dict = {'mass_norm': 2.724437108e-4,
                       'temperature_norm': 1,
                       'density_norm': 1}

ALLOWED_CODES: tuple[str, str, str, str] = ('gkw', 'gene', 'test', 'qualikiz', 'tglf')

INPUT_FIELDS: tuple[str, str, str, str, str, str, str, str] = (
    'ids_properties',
    'model',
    'species_all',
    'model',
    'flux_surface',
    'wavevector',
    'species',
    'collisions',
)

TAG_CATEGORIES: tuple[str, str, str, str, str, str, str, str, str, str] = (
    "simulation", "experiment", "doi", "conference", "tutorial", "ML_training", "ML_set",
    "other", "reference_case", "zenodo", "equivalents"
)
TAG_CATEGORIES += ALLOWED_CODES

electron_mandatory = ELECTRON_NORM

IMAS_DEFAULT_INT = 999999999
IMAS_DEFAULT_FLOAT = 9e40
IMAS_DEFAULT_CPLX = complex(9e40, -9e40)
IMAS_DEFAULT_STR = ""
IMAS_DEFAULT_LIST = []
IMAS_DEFAULT_ARRAY = array([])