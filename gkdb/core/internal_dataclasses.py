from dataclasses import dataclass, Field, field
import idspy_dictionaries.ids_gyrokinetics as gkids

"""
Class defined here are for internal used only, do not use them for IDS
"""


@dataclass(slots=True)
class ExtendedFluxes(gkids.Fluxes):
    frame: str = field(default="")
    uuid: str = field(default="")
    ftype: str = field(default="")


@dataclass(slots=True)
class ExtendedMoments(gkids.MomentsParticles):
    uuid: str = field(default="")
    mtype: str = field(default="")
    norm: bool = field(default=False)
