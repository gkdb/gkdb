from __future__ import annotations

from numpy import complex64, float32

def _cplx_decode(val)->complex64:
    return complex64(complex(val))

def _cplx_encode(val:complex64|complex)->tuple:
    return float32(val.real), float32(val.imag)

async def init_connection(conn):
    await conn.set_type_codec(
            'complex', encoder=_cplx_encode, decoder=_cplx_decode,
            schema='pg_catalog', format='text'
        )