from __future__ import annotations

from dataclasses import dataclass, is_dataclass, fields
from typing import Any, Type
import simplejson as js
import xmltodict
import psycopg.sql as sql
import numpy as np
from gkdb.core.constants import IMAS_DEFAULT_INT
from psycopg.types.json import Jsonb
# for line_profiler, might be removed later
import builtins
import zstandard

ZSTD_COMPRESSOR = zstandard.ZstdCompressor(level=16)

try:
    profile = builtins.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func):
        return func


def shorten_imas_default(value: Any) -> Any:
    if isinstance(value, (float, np.float64)):
        if value >= 9e40:
            value = np.float32(9e30)
    return value

def compressed_nparray(array:np.ndarray|None)->str|None:
    if array is None:
        return None
    return ZSTD_COMPRESSOR.compress(array.flatten().tobytes(order="C"))


def dataclass_to_list(invar: Type[dataclass] | dict, as_sql_query: bool = True) -> AttributeError | tuple[
    list[str | Any], list[Any]]:
    """
    Converts a dataclass or dictionary to a list of keys and values.

    :param invar: Input variable (dataclass or dictionary)
    :type invar: object
    :return: Tuple containing two lists: key list and value list
    :rtype: tuple
    :raises AttributeError: If the input variable is not a dataclass or dictionary

    Example usage:
        dataclass1 = DataClassField(key='value')
        keys, values = dataclass_to_list(dataclass1)
        print(keys)  # ['key']
        print(values)  # ['value']

        dataclass2 = DataClassField(key='value', another_key='another_value')
        keys, values = dataclass_to_list(dataclass2)
        print(keys)  # ['key', 'another_key']
        print(values)  # ['value', 'another_value']

        dict1 = {'key': 'value'}
        keys, values = dataclass_to_list(dict1)
        print(keys)  # ['key']
        print(values)  # ['value']

        dict2 = {'key': 'value', 'another_key': 'another_value'}
        keys, values = dataclass_to_list(dict2)
        print(keys)  # ['key', 'another_key']
        print(values)  # ['value', 'another_value']
    """
    key_list = []
    val_list = []
    if not is_dataclass(invar):
        if isinstance(invar, dict):
            for k, v in invar.items():
                if as_sql_query is True:
                    key_list.append(sql.Identifier(k))
                    val_list.append(sql.Literal(v))
                else:
                    key_list.append(k)
                    val_list.append(v)
        else:
            return AttributeError(f"var {invar} is not a dataclass, type [{type(invar)}]")
    else:
        for f in fields(invar):
            # if field is a dataclass, it has to be put in another table
            current_item = shorten_imas_default(getattr(invar, f.name))
            if is_dataclass(current_item):
                continue
            # if field is a list it means it's a list of dataclass and the DB relationship will be different
            if isinstance(current_item, (list, tuple)):
                if len(current_item) > 0:
                    if is_dataclass(current_item[0]):
                        continue
                    else:
                        current_item = list2pgbytea(current_item)
                else:
                    continue
            elif isinstance(current_item, np.ndarray):
                current_item = list2pgbytea(current_item)
            elif isinstance(current_item, np.bool_):
                current_item = bool(current_item)
            elif isinstance(current_item, np.int32):
                current_item = int(current_item)
            if as_sql_query is True:
                key_list.append(sql.Identifier(f.name))
                val_list.append(sql.Literal(current_item))
            else:
                key_list.append(f.name)
                val_list.append(current_item)
    return key_list, val_list


@profile
def format_value_pg(current_item: Any) -> Any:
    if isinstance(current_item, (list, tuple)):
        if len(current_item) > 0:
            if is_dataclass(current_item[0]):
                raise AttributeError("format_value function cannot handle dataclass item")
            else:
                return list2pgbytea(current_item)
        else:
            return []
    elif isinstance(current_item, np.ndarray):
        return list2pgbytea(current_item)
    elif isinstance(current_item, np.bool_):
        return bool(current_item)
    elif isinstance(current_item, np.int32):
        return int(current_item)
    return shorten_imas_default(current_item)


def dict2pgjsonb(instring: str | dict | Jsonb) -> Jsonb:
    """
    Convert a dictionary or XML string to JSON string.

    Args:
        instring (str | dict): The input XML string or dictionary.

    Returns:
        str: The JSON representation of the input.

    Raises:
        None

    Examples:
        >>> dict2pgjsonb('<person><name>John</name><age>30</age></person>')
        '{"person": {"name": "John", "age": "30"}}'

        >>> dict2pgjsonb({'person': {'name': 'John', 'age': '30'}})
        '{"person": {"name": "John", "age": "30"}}'
    """
    return_string = None
    if isinstance(instring, Jsonb):
        return instring
    if isinstance(instring, dict):
        return_string = Jsonb(instring)
    elif isinstance(instring, str):
        instring = instring.strip().strip("\n").strip()
        # might be xml string
        if len(instring) == 0:
            return Jsonb({})
        if instring[0] == "<":
            return_string = Jsonb(xmltodict.parse(instring))
        elif instring[0] == "{":
            return_string = Jsonb(js.loads(instring))
    if return_string is None:
        raise AttributeError(f"argument of type {type(instring)} cannot be converted to a jsonb variable : {instring}")
    return return_string


def int2bool(invalue: int | None) -> bool:
    """
    Converts an integer value to a boolean value.

    Parameters:
        invalue (int | None): The integer value to be converted. If None, False is returned.

    Returns:
        bool: The resulting boolean value.

    Example:
        >>> int2bool(0)
        False
        >>> int2bool(1)
        True
        >>> int2bool(None)
        False
    """
    bool_value = invalue or False
    if invalue:
        if invalue in (0, IMAS_DEFAULT_INT):
            bool_value = False
        else:
            bool_value = True
    return bool_value


@profile
def list2pglist(array: list | tuple | np.array) -> list | None:
    """
    Converts a given array to a PostgreSQL list.

    Parameters:
        array (list | tuple | np.array): The input array to be converted.

    Returns:
        list | None: The PostgreSQL list representation of the input array. Returns None if conversion fails.

    Example:
        array = [1, 2, 3]
        result = list2pglist(array)
        -> [1, 2, 3]
    """
    array_ = None
    is_complex = np.iscomplexobj(array)
    try:
        if isinstance(array[0], str):
            return array
        if is_complex is False:
            if all(np.issubdtype(type(x), int) for x in array):
                if isinstance(array, (list, tuple)):
                    array_: list[int] = np.array(array, dtype=int).tolist()
                    assert isinstance(array_[0], np.int32) is False
                elif isinstance(array, np.ndarray):
                    # had to do this to convert np.int32 type to basic python int type
                    array_: list[int] = array.astype(int, casting='same_kind').tolist()
                    assert isinstance(array_[0], np.int32) is False
            else:
                if isinstance(array, (list, tuple)):
                    array_: list[np.float32] = np.array(array, dtype=np.float32).tolist()
                elif isinstance(array, np.ndarray):
                    array_: list[np.float32] = array.astype(np.float32, casting='same_kind').tolist()
        else:
            if isinstance(array, (list, tuple)):
                array_ = np.array(array, dtype=np.complex64).tolist()
            elif isinstance(array, np.ndarray):
                array_ = array.astype(np.complex64).tolist()
    except TypeError:
        array_ = None
    return array_


def list2pgbytea(array: list | tuple | np.array) -> str | None:
    """
    Converts a given array to a PostgreSQL list.

    Parameters:
        array (list | tuple | np.array): The input array to be converted.

    Returns:
        list | None: The PostgreSQL list representation of the input array. Returns None if conversion fails.

    Example:
        array = [1, 2, 3]
        result = list2pglist(array)
        -> [1, 2, 3]
    """
    array_ = None
    is_complex = np.iscomplexobj(array)

    try:
        if isinstance(array[0], str):
            return array
        if is_complex is False:
            if all(np.issubdtype(type(x), int) for x in array):
                if isinstance(array, (list, tuple)):
                    array_: str = compressed_nparray(np.array(array, dtype=int))
                    # assert isinstance(array_[0], np.int32) is False
                elif isinstance(array, np.ndarray):
                    # had to do this to convert np.int32 type to basic python int type
                    array_: str = compressed_nparray(array.astype(int, casting='same_kind'))
                    # assert isinstance(array_[0], np.int32) is False
            else:
                if isinstance(array, (list, tuple)):
                    array_: str = compressed_nparray(np.array(array, dtype=np.float32))
                elif isinstance(array, np.ndarray):
                    array_: str = compressed_nparray(array.astype(np.float32, casting='same_kind'))
        else:
            if isinstance(array, (list, tuple)):
                array_ = compressed_nparray(np.array(array, dtype=np.complex64))
            elif isinstance(array, np.ndarray):
                try:
                    # ugly patch to avoid a strange underflow exception casted
                    array_ = array.flatten()
                    array_.dtype = np.float64
                    array_[np.abs(array_) < np.finfo(np.float32).tiny] = 0
                    array_ = array_.astype(np.float32, casting="unsafe", copy=False)
                    array_.dtype = np.complex64
                    array_ = compressed_nparray(array_)
                except FloatingPointError:
                    print("exception raised on ", array.dtype, type(array),
                          type(array_), array_.dtype)
                    print(array_[np.abs(array_)<np.finfo(np.float32).tiny])
                    raise
    except TypeError:
        array_ = None
    return array_
