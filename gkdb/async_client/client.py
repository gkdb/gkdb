from __future__ import annotations

import asyncpg
import asyncio
from asyncpg.exceptions import DiskFullError, UndefinedColumnError
import pandas as pd
import numpy as np
from gkdb.config import DBConfig
import time
from idspy_dictionaries import ids_gyrokinetics as gkids
import dataclasses
from typing import Any
from pprint import pprint

import zstandard
import gzip

GKDB_CFG = DBConfig()

ZSTD_COMPRESS = zstandard.ZstdCompressor(level=16)
ZSTD_DECOMPRESS = zstandard.ZstdDecompressor()


def dict_to_ids(dict_entry: dict, ids_class: Any, pk: str = 'id') -> tuple[Any, int | str]:
    args_ids = {k.name: dict_entry.get(k.name, None) for k in dataclasses.fields(ids_class)
                if ('_id' not in k.name) and (k.name != pk)}
    new_ids = ids_class(**args_ids)
    return (new_ids, dict_entry.get(pk, None))


def get_dataclass_field_default(cls: Any, field_name: str):
    """
    Retrieve the default value for a field in a dataclass.

    :param cls: The dataclass from which to retrieve the default value.
    :type cls: class
    :param field_name: The name of the field for which to retrieve the default value.
    :type field_name: str
    :return: Default value of the specified dataclass field.
    :raises ValueError: If `cls` is not a dataclass or `field_name` is not a field of the dataclass.

    Usage::

        @dataclass
        class Example:
            field1: int = 10
            field2: str = "hello"

        print(get_dataclass_field_default(Example, 'field1'))  # prints: 10
        print(get_dataclass_field_default(Example, 'field2'))  # prints: hello
    """
    if dataclasses.is_dataclass(cls):
        dataclass_field: dataclasses.Field = cls.__dataclass_fields__.get(field_name)
        if dataclass_field:
            if dataclass_field.default is not dataclasses.MISSING:
                return dataclass_field.default
            else:
                return None
    raise ValueError(f"{cls} is not a dataclass or {field_name} is not a field of {cls}")


def ids_info_from_dict(gkdb_dict: dict) -> gkids.Gyrokinetics:
    def _create_ids_from_dict(db_dict, ids_type):
        dict_inputs = {}
        for entries in dataclasses.fields(ids_type):
            dict_inputs.update({entries.name: db_dict.get(entries.name,
                                                          get_dataclass_field_default(ids_type,
                                                                                      entries.name)
                                                          )
                                }
                               )
        return dict_to_ids(dict_inputs, ids_type)[0]

    ids_model = _create_ids_from_dict(gkdb_dict, gkids.Model)
    ids_code = _create_ids_from_dict(gkdb_dict, gkids.Code)
    ids_flux_surfaces = _create_ids_from_dict(gkdb_dict, gkids.FluxSurface)
    ids_species_all = _create_ids_from_dict(gkdb_dict, gkids.InputSpeciesGlobal)
    ids_quantities = _create_ids_from_dict(gkdb_dict, gkids.InputNormalizing)
    ids_properties = _create_ids_from_dict(gkdb_dict, gkids.IdsProperties)

    if ids_flux_surfaces is not None:
        if ids_flux_surfaces.shape_coefficients_c is not None:
            ids_flux_surfaces.shape_coefficients_c = np.frombuffer(ZSTD_DECOMPRESS.decompress(
                ids_flux_surfaces.shape_coefficients_c), dtype=np.float32)

        if ids_flux_surfaces.dc_dr_minor_norm is not None:
            ids_flux_surfaces.dc_dr_minor_norm = np.frombuffer(ZSTD_DECOMPRESS.decompress(
                ids_flux_surfaces.dc_dr_minor_norm), dtype=np.float32)

        if ids_flux_surfaces.shape_coefficients_s is not None:
            ids_flux_surfaces.shape_coefficients_s = np.frombuffer(ZSTD_DECOMPRESS.decompress(
                ids_flux_surfaces.shape_coefficients_s), dtype=np.float32)

        if ids_flux_surfaces.ds_dr_minor_norm is not None:
            ids_flux_surfaces.ds_dr_minor_norm = np.frombuffer(ZSTD_DECOMPRESS.decompress(
                ids_flux_surfaces.ds_dr_minor_norm), dtype=np.float32)

    return gkids.Gyrokinetics(species_all=ids_species_all,
                              code=ids_code,
                              flux_surface=ids_flux_surfaces,
                              model=ids_model,
                              normalizing_quantities=ids_quantities,
                              time=gkdb_dict.get('time', []),
                              ids_properties=ids_properties
                              )


class AsyncGkdbClient:
    global_query_scheme_with_moments = """
SELECT gk.*, cpc.*, code.*, coll.*, filestorage.*, fs.*, ids.*, inputs.*, model.*, specall.*, species.*, wavevector.*, eig.*, moments.*, fluxes.* 
FROM {schema_name}.gyrokinetics_table AS gk 
INNER JOIN {schema_name}.code_table AS code on code.gk_id=gk.id
INNER JOIN {schema_name}.collisions_table AS coll on coll.gk_id=gk.id
LEFT OUTER JOIN {schema_name}.file_entry_table AS filestorage on filestorage.gk_id=gk.id
INNER JOIN {schema_name}.flux_surface_table AS fs on fs.gk_id=gk.id
INNER JOIN {schema_name}.ids_properties_table AS ids on ids.gk_id=gk.id
INNER JOIN {schema_name}.input_normalizing_table AS inputs on inputs.gk_id=gk.id
INNER JOIN {schema_name}.model_table AS model on model.gk_id=gk.id
INNER JOIN {schema_name}.species_all_table AS specall on specall.gk_id=gk.id
INNER JOIN {schema_name}.species_table AS species on species.gk_id=gk.id
INNER JOIN {schema_name}.wavevector_table AS wavevector on wavevector.gk_id=gk.id
INNER JOIN {schema_name}.eigenmode_table AS eig on eig.wavevector_id=wavevector.id
INNER JOIN {schema_name}.code_partial_constant_table AS cpc on cpc.eigenmode_id=eig.id
INNER JOIN {schema_name}.ids_provenance_node_table AS provenance on provenance.ids_properties_id=ids.id
INNER JOIN {schema_name}.moments_table AS moments on (moments.eigenmode_id=eig.id AND moments.species_id=species.id)
INNER JOIN {schema_name}.fluxes_table AS fluxes on (fluxes.gk_id=gk.id AND fluxes.species_id=species.id)
WHERE {condition};
"""
    global_query_scheme = """
    SELECT gk.*, cpc.*, code.*, coll.*, filestorage.*, fs.*, ids.*, inputs.*, model.*, specall.*, species.*, wavevector.*, eig.*, fluxes.* 
    FROM {schema_name}.gyrokinetics_table AS gk 
    INNER JOIN {schema_name}.code_table AS code on code.gk_id=gk.id
    INNER JOIN {schema_name}.collisions_table AS coll on coll.gk_id=gk.id
    LEFT OUTER JOIN {schema_name}.file_entry_table AS filestorage on filestorage.gk_id=gk.id
    INNER JOIN {schema_name}.flux_surface_table AS fs on fs.gk_id=gk.id
    INNER JOIN {schema_name}.ids_properties_table AS ids on ids.gk_id=gk.id
    INNER JOIN {schema_name}.input_normalizing_table AS inputs on inputs.gk_id=gk.id
    INNER JOIN {schema_name}.model_table AS model on model.gk_id=gk.id
    INNER JOIN {schema_name}.species_all_table AS specall on specall.gk_id=gk.id
    INNER JOIN {schema_name}.species_table AS species on species.gk_id=gk.id
    INNER JOIN {schema_name}.wavevector_table AS wavevector on wavevector.gk_id=gk.id
    INNER JOIN {schema_name}.eigenmode_table AS eig on eig.wavevector_id=wavevector.id
    INNER JOIN {schema_name}.code_partial_constant_table AS cpc on cpc.eigenmode_id=eig.id
    INNER JOIN {schema_name}.ids_provenance_node_table AS provenance on provenance.ids_properties_id=ids.id
    INNER JOIN {schema_name}.fluxes_table AS fluxes on (fluxes.gk_id=gk.id AND fluxes.species_id=species.id)
    WHERE {condition} {iterator};
    """

    growth_rate_query_scheme = """
    SELECT gk.*, code.*, model.*, species.*, wavevector.*, eig.* 
    FROM {schema_name}.gyrokinetics_table AS gk 
    INNER JOIN {schema_name}.code_table AS code on code.gk_id=gk.id
    INNER JOIN {schema_name}.model_table AS model on model.gk_id=gk.id
    INNER JOIN {schema_name}.species_table AS species on species.gk_id=gk.id
    INNER JOIN {schema_name}.wavevector_table AS wavevector on wavevector.gk_id=gk.id
    INNER JOIN {schema_name}.eigenmode_table AS eig on eig.wavevector_id=wavevector.id
    WHERE {condition} {iterator};
    """

    async def _pool_conn(self, query_str, query_args=None, verbose: bool = False):
        if verbose:
            print(query_str, query_args, flush=True)
        async with self.pool.acquire() as connection:
            if query_args is None:
                record = await connection.fetch(query_str)
            else:
                record = await connection.fetch(query_str, *query_args)
        return record

    def __init__(self, user, password, database, host) -> None:
        self.user = user
        self.password = password
        self.database = database
        self.host = host
        self.pool = None

    async def connect(self) -> None:
        self.pool = await asyncpg.create_pool(
            user=self.user,
            password=self.password,
            database=self.database,
            host=self.host,
            # server_settings = {'search_path': GKDB_CFG.schema}
        )

    async def close(self) -> None:
        await self.pool.close()

    async def count(self, table_name: str, schema_name: str = "public") -> int:
        async with self.pool.acquire() as connection:
            result = await connection.fetchval(
                f'SELECT COUNT(*) FROM {schema_name}.{table_name};'
            )
            return result

    async def get_db_stats(self, schema_name: str = "public") -> pd.DataFrame:
        request_stat = f"SELECT * FROM {schema_name}.get_table_counts_and_size($1::text);"
        try:
            record = await self._pool_conn(request_stat, (schema_name,))
            return pd.DataFrame([dict(r.items()) for r in record])
        except UndefinedColumnError as e:
            print(e)
            return pd.DataFrame(columns=['table_name', 'row_count', 'table_size'])

    async def get_omega_bins(self, schema_name: str = 'public'):
        request = f"""SELECT 
         CONCAT(FLOOR(frequency_norm / 10) * 10, ' - ', FLOOR(frequency_norm / 10) * 10 + 9)
        AS frequency,
        COUNT(id) AS number
    FROM 
        {schema_name}.eigenmode_table
    GROUP BY 
        frequency
    ORDER BY 
        frequency;"""
        record = await self._pool_conn(request)
        return pd.DataFrame([dict(r.items()) for r in record])

    async def get_gamma_bins(self, schema_name: str = 'public'):
        request = f"""SELECT 
    CASE 
        WHEN growth_rate_norm < 1e-2 THEN 'Gamma_<_1e-2'
        WHEN growth_rate_norm = 0 THEN 'Gamma_0'
        ELSE CONCAT(FLOOR(growth_rate_norm / 10) * 10, ' - ', FLOOR(growth_rate_norm / 10) * 10 + 9)
    END AS growth_rate,
    COUNT(id) AS number
FROM 
    {schema_name}.eigenmode_table
GROUP BY 
    growth_rate
ORDER BY 
    growth_rate;"""
        record = await self._pool_conn(request)
        # async with self.pool.acquire() as connection:
        #     record = await connection.fetch(request)
        return pd.DataFrame([dict(r.items()) for r in record])

    async def stats_db(self, schema_name: str = "public") -> pd.DataFrame:
        request = f"""
        SELECT code.name, model.non_linear_run, COUNT(*)
FROM {schema_name}.gyrokinetics_table gk
INNER JOIN {schema_name}.code_table code ON gk.id = code.gk_id
INNER JOIN {schema_name}.model_table model ON gk.id = model.gk_id
GROUP BY code.name, model.non_linear_run;
        """

        request = f"""SELECT code.name, model.non_linear_run,
 COUNT(DISTINCT code.id) AS total_number
FROM (
    SELECT code.gk_id, code.id, code.name 
    FROM  {schema_name}.code_table AS code
    ORDER BY "gk_id"
) AS code 
INNER JOIN (
    SELECT model.id, model.gk_id, model.non_linear_run
    FROM {schema_name}.model_table AS model
    ORDER BY "gk_id"
) AS model 
ON code.gk_id= model.gk_id
GROUP BY code.name, model.non_linear_run;"""
        record = await self._pool_conn(request)
        # async with self.pool.acquire() as connection:
        #     record = await connection.fetch(request)
        return pd.DataFrame([dict(r.items()) for r in record])

    async def multiple_select(self, schema_name: str = "public"):
        limit = 2
        offset = 0

        request = f"""WITH filtered_gk_ids AS (
            SELECT gk.id as gkid
            FROM {schema_name}.gyrokinetics_table AS gk 
            INNER JOIN {schema_name}.eigenmode_table AS eig 
            ON gk.id = eig.gk_id
            WHERE eig.growth_rate_norm >= 0.8999999761581421 
            AND eig.growth_rate_norm <= 1.100000023841858 
            ORDER BY gk.id ASC LIMIT {limit} OFFSET {offset}
        ),
        wavevector_ids AS (
            SELECT id
            FROM {schema_name}.wavevector_table as wv
            WHERE id IN (SELECT id FROM filtered_gk_ids)
        ),
        code_ids AS (
            SELECT code.name
            FROM {schema_name}.code_table as code
            WHERE id IN (SELECT id FROM filtered_gk_ids)
        )
        SELECT 
            array_agg(DISTINCT w.id) as wavevector_ids,
            array_agg(DISTINCT c.name) as gyrokinetics_names
        FROM
            wavevector_ids AS w,
            code_ids AS c;"""
        async with self.pool.acquire() as connection:
            record = await connection.fetch(request)

        return record

    async def find_by_uuid(self, uuid: str, schema_name: str = "public"):
        # first get all unique elements for an IDS
        request_single_elements = f"""SELECT gk.id as ids_id, gk.uuid, gk.time,  code.*, filestorage.*, fs.*, ids.*, inputs.*, model.*, specall.*
FROM {schema_name}.gyrokinetics_table AS gk 
INNER JOIN {schema_name}.code_table AS code on code.gk_id=gk.id
LEFT OUTER JOIN {schema_name}.file_entry_table AS filestorage on filestorage.gk_id=gk.id
INNER JOIN {schema_name}.flux_surface_table AS fs on fs.gk_id=gk.id
LEFT OUTER JOIN {schema_name}.ids_properties_table AS ids on ids.gk_id=gk.id
INNER JOIN {schema_name}.input_normalizing_table AS inputs on inputs.gk_id=gk.id
INNER JOIN {schema_name}.model_table AS model on model.gk_id=gk.id
INNER JOIN {schema_name}.species_all_table AS specall on specall.gk_id=gk.id
WHERE gk.uuid='{uuid}'"""

        records = await self._pool_conn(request_single_elements)
        if len(records) == 0:
            return None
        assert len(records) == 1, \
            "more than one entry match the given uuid and it should not be possible, please contact GKDB group"
        record_as_dict = dict(records[0].items())
        gk_id = record_as_dict.get("ids_id", None)
        assert str(record_as_dict.get("uuid",
                                      None)) == uuid, f"obtained uuid is [{record_as_dict.get('uuid', None)}] and should be [{uuid}]"
        if gk_id is None:
            raise ValueError(f"no entry match uuid {uuid}")

        # then since we now have the associated gk.id, we can get all the other elements :
        # 1- species
        sub_query_species = f"""
                SELECT species.id as species_id, species.charge_norm, species.mass_norm, species.density_norm, species.density_log_gradient_norm, species.temperature_norm, species.temperature_log_gradient_norm, species.velocity_tor_gradient_norm 
                FROM {schema_name}.gyrokinetics_table AS gk
                INNER JOIN  {schema_name}.species_table AS species on species.gk_id=gk.id
                WHERE gk.id={gk_id}
                ORDER BY species_id ASC
                """
        records_species = await self._pool_conn(sub_query_species)
        species_as_dict = {
            k: v for record in records_species
            for v, k in [dict_to_ids(dict(record.items()),
                                     gkids.Species,
                                     pk='species_id')]
        }
        species_list = sorted(tuple([k for k in species_as_dict.keys()]))
        species_list_map = {v: idx for idx, v in enumerate(species_list)}
        # 2- collisions
        sub_query_collisions = f"""
                SELECT coll.id as coll_id, coll.specie_one as s1, coll.specie_two as s2, coll.collisionality_norm as collisionality
                FROM {schema_name}.gyrokinetics_table AS gk
                INNER JOIN  {schema_name}.collisions_table AS coll on coll.gk_id=gk.id
                WHERE gk.id={gk_id}
                ORDER BY coll_id, s1, s2 ASC
                """
        records_collisions = await self._pool_conn(sub_query_collisions)
        list_collisionality = [((species_list_map.get(record['s1'], None), species_list_map.get(record['s2'], None)), record['collisionality']) for record in records_collisions]
        list_species_collisions = set([x for t in list_collisionality for x in t[0]])
        assert list_species_collisions - set([species_list_map.get(k) for k in species_as_dict.keys()]) == set(), (f"missing specie {list_species_collisions - set(species_list)}")

        # 3- wavevectors
        sub_query_wv = f"""
        SELECT  wavevector.id as wv_id, wavevector.radial_component_norm as radial_component_norm, wavevector.binormal_component_norm as binormal_component_norm  
        FROM {schema_name}.gyrokinetics_table AS gk 
        INNER JOIN {schema_name}.wavevector_table AS wavevector on wavevector.gk_id=gk.id
        WHERE gk.id={gk_id}
        ORDER BY wv_id ASC
        """
        records_wv = await self._pool_conn(sub_query_wv)
        wv_as_dict = {
            k: v for record in records_wv
            for v, k in [dict_to_ids(dict(record.items()),
                                     gkids.Wavevector,
                                     pk='wv_id')]
        }

        # 4 - eigenmodes
        sub_query_eig = f"""
        SELECT  eig.*
        FROM {schema_name}.gyrokinetics_table AS gk 
        INNER JOIN {schema_name}.eigenmode_table AS eig on eig.gk_id=gk.id
        WHERE gk.id={gk_id}
        ORDER BY id ASC
        """
        records_eig = await self._pool_conn(sub_query_eig)
        records_eig = [dict(r.items()) for r in records_eig]
        list_eig_id = []
        for idx in range(len(records_eig)):
            records_eig[idx]['poloidal_angle'] = np.frombuffer(ZSTD_DECOMPRESS.decompress(
                records_eig[idx]['poloidal_angle']), dtype=np.float32)
            records_eig[idx]['phi_potential_perturbed_norm'] = np.frombuffer(ZSTD_DECOMPRESS.decompress(
                records_eig[idx]['phi_potential_perturbed_norm']), dtype=np.complex64)
            records_eig[idx], associated_id = dict_to_ids(records_eig[idx],
                                                          gkids.Eigenmode,
                                                          pk='wavevector_id')
            list_eig_id.append((associated_id, idx))

        for wv_id in wv_as_dict.keys():
            wv_as_dict[wv_id].eigenmode = []
            for eig_wv_id in list_eig_id:
                if eig_wv_id[0] == wv_id:
                    wv_as_dict[wv_id].eigenmode.append(records_eig[eig_wv_id[1]])

        # 5 - fluxes
        sub_query_fluxes = f"""
        SELECT  fluxes.*
        FROM {schema_name}.fluxes_table AS fluxes 
        WHERE fluxes.eigenmode_id IN (
        SELECT  eig.id
        FROM  {schema_name}.eigenmode_table AS eig WHERE eig.gk_id={gk_id}
        ORDER BY eig.id ASC
        );
        """
        records_fluxes_eig = await self._pool_conn(sub_query_fluxes)
        records_fluxes_eig = [dict(r.items()) for r in records_fluxes_eig]

        # 6 - integrated fluxes
        sub_query_fluxes = f"""
        SELECT  fluxes.*
        FROM {schema_name}.fluxes_table AS fluxes 
        WHERE fluxes.gk_id={gk_id} and fluxes.ftype='integrated'
        ORDER BY fluxes.species_id ASC
        ;
        """
        records_fluxes_int = await self._pool_conn(sub_query_fluxes)
        records_fluxes_int = [dict(r.items()) for r in records_fluxes_int]
        integrated_fluxes_as_dict = {
            k: v for record in records_fluxes_int
            for v, k in [dict_to_ids(dict(record.items()),
                                     gkids.Fluxes,
                                     pk='species_id')]
        }
        assert set(sorted([k for k in integrated_fluxes_as_dict.keys()])) - set(sorted(species_list)) == set()

        obtained_ids = ids_info_from_dict(record_as_dict)
        obtained_ids.collisions = list_collisionality
        obtained_ids.species = tuple([species_as_dict[k] for k in species_list])
        obtained_ids.fluxes_integrated_norm = tuple([integrated_fluxes_as_dict[k] for k in species_list])
        obtained_ids.wavevector = tuple([v for v in wv_as_dict.values()])

        # 6 - moments
        # print("moments not extracted from GKDB for now, stay tuned")
        return obtained_ids

    async def find_by_growthrate(self, gamma_min: float, gamma_max: float | None = None,
                                 schema_name: str = "public", limit: int = 10) -> tuple:
        gamma_min = np.float32(gamma_min)
        condition = f"eig.growth_rate_norm>={gamma_min}"
        if gamma_max is not None:
            gamma_max = np.float32(gamma_max)
            condition += f" and eig.growth_rate_norm<={gamma_max}"

        base_query = f"""SELECT gk.uuid as uuid, wv.radial_component_norm as kx, wv.binormal_component_norm as ky,  eig.growth_rate_norm as gamma, eig.frequency_norm as omega, code.name as codename
FROM {schema_name}.eigenmode_table as eig 
INNER JOIN {schema_name}.wavevector_table AS wv ON wv.gk_id=eig.gk_id
INNER JOIN {schema_name}.gyrokinetics_table AS gk ON gk.id=eig.gk_id
INNER JOIN  {schema_name}.code_table AS code on code.gk_id=eig.gk_id
WHERE eig.wavevector_id= wv.id and {condition}
ORDER BY wv.gk_id, wv.id LIMIT {limit}; 
"""

        try:
            records = await self._pool_conn(base_query)
        except DiskFullError:
            print("Not enough Shared Memory for PostgreSQL to handle that request ")
            return pd.DataFrame()

        return pd.DataFrame([dict(r.items()) for r in records])

    async def find_by_fluxes_ratio(self, qei_min: float, qei_max: float | None = None, schema_name: str = "public",
                                   limit: int = 10) -> pd.DataFrame:
        qei_min = np.float32(qei_min)
        condition = f"Qei>={qei_min}"
        if qei_max is not None:
            qei_max = np.float32(qei_max)
            condition += f" and Qei<={qei_max}"

        request_qei = f"""WITH species AS (
  WITH positive AS (
    SELECT gk_id, max(density_norm) as max_density
    FROM {schema_name}.species_table
    WHERE charge_norm > 0
    GROUP BY gk_id
  ),
  negative AS (
    SELECT gk_id, max(density_norm) as max_density
    FROM {schema_name}.species_table
    WHERE charge_norm < 0
    GROUP BY gk_id
  )
  SELECT id, gk_id, charge_norm
  FROM {schema_name}.species_table
  WHERE ((charge_norm < 0 AND density_norm = (SELECT max_density FROM negative WHERE gk_id = species_table.gk_id))
    OR (charge_norm > 0 AND density_norm = (SELECT max_density FROM positive WHERE gk_id = species_table.gk_id)))
),
positive_flux AS (
  SELECT species.gk_id, fluxes.energy_phi_potential
  FROM {schema_name}.fluxes_table as fluxes, species
  WHERE species_id = species.id AND fluxes.ftype='integrated' AND species.charge_norm > 0
),
negative_flux AS (
  SELECT species.gk_id, fluxes.energy_phi_potential
  FROM {schema_name}.fluxes_table as fluxes, species
  WHERE species_id = species.id AND fluxes.ftype='integrated' AND species.charge_norm < 0
),
q_values AS (
  SELECT positive_flux.gk_id, (negative_flux.energy_phi_potential/positive_flux.energy_phi_potential) AS Qei
  FROM positive_flux, negative_flux
  WHERE positive_flux.energy_phi_potential>0 and positive_flux.gk_id = negative_flux.gk_id
)
SELECT  gk.uuid,  Qei
FROM q_values
INNER JOIN {schema_name}.gyrokinetics_table as gk on gk.id=gk_id
WHERE {condition}
ORDER BY Qei, gk_id LIMIT {limit};"""
        record = await self._pool_conn(request_qei)
        return pd.DataFrame([dict(r.items()) for r in record])


# usage

async def main():
    db = AsyncGkdbClient(GKDB_CFG.user, GKDB_CFG.password,
                         GKDB_CFG.database,
                         GKDB_CFG.host)
    await db.connect()
    count = await db.count('gyrokinetics_table', GKDB_CFG.schema)
    print(f"Number of records: {count}")
    start = time.monotonic()
    records = await db.find_by_uuid("018a48ed-60f4-7e34-bff4-25da778e61c5", GKDB_CFG.schema)
    print("query time uuid", time.monotonic() - start)
    print(records.collisions)
    await db.close()


if __name__ == '__main__':
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        pass
