from __future__ import annotations

from dataclasses import dataclass, asdict
import datetime
import numpy as np
from numpy import ndarray
from typing import Optional
import numpy.random as rnd
import simplejson as js
import pprint as pp

GKDB_RNG = rnd.default_rng()
CODE_LIST = ['qualikiz', 'gkw', 'gene', 'tglf']
LIBRARY_LIST = ["fftw", "hdf5", "mpi", "netcdf"]


@dataclass(slots=True)
class RunParameters:
    nx: int
    ny: int
    nz: int
    nt: int
    nvx: int
    nvy: int
    nvz: int
    nspecies: int
    nwavevector: int
    neigenmode: int
    ntags: int
    dx: float
    dy: float
    dz: float
    dvx: float
    dvy: float
    dvz: float
    nlibraries: int


@dataclass(slots=True)
class CodePartialConstant:
    parameters: str
    output_flag: int


@dataclass(slots=True)
class EntryTag:
    tagid: str
    categorie: str
    description: str


@dataclass(slots=True)
class GkdbEntryTag:
    category: str
    tagid: str
    description: str


@dataclass(slots=True)
class Collisions:
    collisionality_norm: ndarray


@dataclass(slots=True)
class FluxSurface:
    r_minor_norm: float
    elongation: float
    delongation_dr_minor_norm: float
    dgeometric_axis_r_dr_minor: float
    dgeometric_axis_z_dr_minor: float
    q: float
    magnetic_shear_r_minor: float
    pressure_gradient_norm: float
    ip_sign: float
    b_field_tor_sign: float
    shape_coefficients_c: ndarray
    dc_dr_minor_norm: ndarray
    shape_coefficients_s: ndarray
    ds_dr_minor_norm: ndarray


@dataclass(slots=True)
class Fluxes:
    particles_phi_potential: float
    particles_a_field_parallel: float
    particles_b_field_parallel: float
    energy_phi_potential: float
    energy_a_field_parallel: float
    energy_b_field_parallel: float
    momentum_tor_parallel_phi_potential: float
    momentum_tor_parallel_a_field_parallel: float
    momentum_tor_parallel_b_field_parallel: float
    momentum_tor_perpendicular_phi_potential: float
    momentum_tor_perpendicular_a_field_parallel: float
    momentum_tor_perpendicular_b_field_parallel: float


@dataclass(slots=True)
class InputNormalizing:
    t_e: float
    n_e: float
    r: float
    b_field_tor: float


@dataclass(slots=True)
class InputSpeciesGlobal:
    beta_reference: float
    velocity_tor_norm: float
    zeff: float
    debye_length_reference: float
    shearing_rate_norm: float


@dataclass(slots=True)
class Model:
    include_centrifugal_effects: int
    include_a_field_parallel: int
    include_b_field_parallel: int
    include_full_curvature_drift: int
    collisions_pitch_only: int
    collisions_momentum_conservation: int
    collisions_energy_conservation: int
    collisions_finite_larmor_radius: int
    non_linear_run: int
    time_interval_norm: ndarray


@dataclass(slots=True)
class Moments:
    density: ndarray
    density_gyroav: ndarray
    j_parallel: ndarray
    j_parallel_gyroav: ndarray
    pressure_parallel: ndarray
    pressure_parallel_gyroav: ndarray
    pressure_perpendicular: ndarray
    pressure_perpendicular_gyroav: ndarray
    heat_flux_parallel: ndarray
    heat_flux_parallel_gyroav: ndarray
    v_parallel_energy_perpendicular: ndarray
    v_parallel_energy_perpendicular_gyroav: ndarray
    v_perpendicular_square_energy: ndarray
    v_perpendicular_square_energy_gyroav: ndarray


@dataclass(slots=True)
class MomentsParticles:
    density: ndarray
    j_parallel: ndarray
    pressure_parallel: ndarray
    pressure_perpendicular: ndarray
    heat_flux_parallel: ndarray
    v_parallel_energy_perpendicular: ndarray
    v_perpendicular_square_energy: ndarray


@dataclass(slots=True)
class Species:
    charge_norm: float
    mass_norm: float
    density_norm: float
    density_log_gradient_norm: float
    temperature_norm: float
    temperature_log_gradient_norm: float
    velocity_tor_gradient_norm: float


@dataclass(slots=True)
class Library:
    name: str
    commit: str
    version: str
    repository: str
    parameters: str


@dataclass(slots=True)
class Code:
    name: str
    commit: str
    version: str
    repository: str
    parameters: str
    output_flag: list[ndarray[(int,), int]]
    library: list[Library]


@dataclass(slots=True)
class FluxesMoments:
    moments_norm_gyrocenter: Optional[Moments]
    moments_norm_particle: Optional[MomentsParticles]
    fluxes_norm_gyrocenter: Optional[Fluxes]
    fluxes_norm_gyrocenter_rotating_frame: Optional[Fluxes]
    fluxes_norm_particle: Optional[Fluxes]
    fluxes_norm_particle_rotating_frame: Optional[Fluxes]


@dataclass(slots=True)
class Eigenmode:
    poloidal_turns: int
    growth_rate_norm: float
    frequency_norm: float
    growth_rate_tolerance: float
    phi_potential_perturbed_weight: ndarray
    phi_potential_perturbed_parity: ndarray
    a_field_parallel_perturbed_weight: ndarray
    a_field_parallel_perturbed_parity: ndarray
    b_field_parallel_perturbed_weight: ndarray
    b_field_parallel_perturbed_parity: ndarray
    poloidal_angle: ndarray
    phi_potential_perturbed_norm: ndarray
    a_field_parallel_perturbed_norm: ndarray
    b_field_parallel_perturbed_norm: ndarray
    time_norm: ndarray
    fluxes_moments: list[FluxesMoments]
    code: Optional[CodePartialConstant]
    initial_value_run: int


@dataclass(slots=True)
class IdsProvenanceNode:
    path: str
    sources: Optional[list[str]]


@dataclass(slots=True)
class IdsProvenance:
    node: list[IdsProvenanceNode]


@dataclass(slots=True)
class IdsProperties:
    comment: str
    homogeneous_time: int
    provider: str
    creation_date: str
    provenance: Optional[IdsProvenance]


@dataclass(slots=True)
class Wavevector:
    radial_component_norm: float
    binormal_component_norm: float
    eigenmode: list[Eigenmode]


@dataclass(slots=True)
class Gyrokinetics:
    ids_properties: Optional[IdsProperties]
    tag: Optional[list[EntryTag]]
    normalizing_quantities: Optional[InputNormalizing]
    flux_surface: Optional[FluxSurface]
    model: Optional[Model]
    species_all: Optional[InputSpeciesGlobal]
    species: list[Species]
    collisions: Optional[Collisions]
    wavevector: list[Wavevector]
    fluxes_integrated_norm: list[Fluxes]
    code: Optional[Code]
    time: list[ndarray[(int,), float]]


def _fake_gaussian(nx: int) -> np.array:
    x = np.linspace(-10, 10, num=nx)
    x0 = GKDB_RNG.uniform(-3, 3)
    b = GKDB_RNG.uniform(.4, 20)
    a = GKDB_RNG.uniform(.1, 10)
    return a * np.exp(-(x - x0) ** 2 / b)


def _fake_deriv_gaussian(nx: int):
    x = np.linspace(-10, 10, num=nx)
    x0 = GKDB_RNG.uniform(-3, 3)
    b = GKDB_RNG.uniform(.4, 20)
    a = GKDB_RNG.uniform(.1, 10)
    return -2 * a * (x - x0) * np.exp(-(x - x0) ** 2 / b) / b


def _fake_2d_array(ny: int, nt: int) -> np.array:
    funct_r = [_fake_gaussian, _fake_deriv_gaussian][GKDB_RNG.choice([0, 1])]
    funct_i = [_fake_gaussian, _fake_deriv_gaussian][GKDB_RNG.choice([0, 1])]
    array = funct_r(ny) + 1j * funct_i(ny)
    return np.vstack([array for _ in range(nt)])


def generate_fake_ids_properties() -> IdsProperties:
    ipn = IdsProvenanceNode(
        path="gkdb://uuid",
        sources=["gkdb", ])

    ids_prov = IdsProvenance(node=[ipn, ])
    return IdsProperties(
        comment="useless",
        homogeneous_time=0,
        provider="me",
        creation_date=datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
        provenance=ids_prov)


def generate_fake_collision(nspecies: int) -> Collisions:
    return Collisions(GKDB_RNG.uniform(-1, 1, size=(nspecies, nspecies)))


def generate_fake_flux_surface() -> FluxSurface:
    return FluxSurface(
        r_minor_norm=GKDB_RNG.uniform(.1, 10),
        elongation=GKDB_RNG.uniform(.1, 1),
        delongation_dr_minor_norm=GKDB_RNG.uniform(.1, 1),
        dgeometric_axis_r_dr_minor=GKDB_RNG.uniform(.1, 1),
        dgeometric_axis_z_dr_minor=GKDB_RNG.uniform(.1, 1),
        q=GKDB_RNG.uniform(1, 10),
        magnetic_shear_r_minor=GKDB_RNG.uniform(.1, 1),
        pressure_gradient_norm=GKDB_RNG.uniform(1, 100),
        ip_sign=GKDB_RNG.uniform(-1, 1),
        b_field_tor_sign=GKDB_RNG.uniform(-1, 1),
        shape_coefficients_c=GKDB_RNG.uniform(.1, 1, size=4),
        dc_dr_minor_norm=GKDB_RNG.uniform(.1, 1, size=4),
        shape_coefficients_s=GKDB_RNG.uniform(.1, 1, size=4),
        ds_dr_minor_norm=GKDB_RNG.uniform(.1, 1, size=4),
    )


def generate_fake_model() -> Model:
    return Model(
        include_centrifugal_effects=GKDB_RNG.choice([True, False]),
        include_a_field_parallel=GKDB_RNG.choice([True, False]),
        include_b_field_parallel=GKDB_RNG.choice([True, False]),
        include_full_curvature_drift=GKDB_RNG.choice([True, False]),
        collisions_pitch_only=GKDB_RNG.choice([True, False]),
        collisions_momentum_conservation=GKDB_RNG.choice([True, False]),
        collisions_energy_conservation=GKDB_RNG.choice([True, False]),
        collisions_finite_larmor_radius=GKDB_RNG.choice([True, False]),
        non_linear_run=GKDB_RNG.choice([True, False]),
        time_interval_norm=GKDB_RNG.uniform(.1, 10, size=2),
    )


def generate_fake_input_species() -> InputSpeciesGlobal:
    return InputSpeciesGlobal(
        beta_reference=GKDB_RNG.uniform(.1, 10),
        velocity_tor_norm=GKDB_RNG.uniform(.1, 10),
        zeff=GKDB_RNG.uniform(.1, 10),
        debye_length_reference=GKDB_RNG.uniform(.1, 10),
        shearing_rate_norm=GKDB_RNG.uniform(.1, 10),
    )


def generate_fake_input_normalizing() -> InputNormalizing:
    return InputNormalizing(
        t_e=GKDB_RNG.uniform(1, 42),
        n_e=GKDB_RNG.uniform(1, 42),
        r=GKDB_RNG.uniform(.1, 10),
        b_field_tor=GKDB_RNG.uniform(.1, 10),
    )


def generate_fake_code(fake_params: RunParameters) -> Code:
    return Code(
        name=GKDB_RNG.choice(CODE_LIST),
        commit="".join([str(x) for x in GKDB_RNG.choice(10, 10)]),
        version=".".join([str(x) for x in GKDB_RNG.choice(10, 3)]),
        repository="git://iter/repository",
        parameters=js.dumps(asdict(fake_params)),
        output_flag=[GKDB_RNG.choice([0, 1], p=[0.95, 0.05]), ],
        library=[generate_fake_lib() for _ in range(fake_params.nlibraries)]
    )


def generate_fake_lib() -> Library:
    return Library(
        name=GKDB_RNG.choice(LIBRARY_LIST),
        commit="".join([str(x) for x in GKDB_RNG.choice(10, 10)]),
        version=".".join([str(x) for x in GKDB_RNG.choice(10, 3)]),
        repository="git://iter/repository",
        parameters=js.dumps({"a": 4, "b": 2, "c": 42})
    )


def generate_fake_flux() -> Fluxes:
    return Fluxes(
        particles_phi_potential=GKDB_RNG.uniform(-10, 10),
        particles_a_field_parallel=GKDB_RNG.uniform(-10, 10),
        particles_b_field_parallel=GKDB_RNG.uniform(-10, 10),
        energy_phi_potential=GKDB_RNG.uniform(-10, 10),
        energy_a_field_parallel=GKDB_RNG.uniform(-10, 10),
        energy_b_field_parallel=GKDB_RNG.uniform(-10, 10),
        momentum_tor_parallel_phi_potential=GKDB_RNG.uniform(-10, 10),
        momentum_tor_parallel_a_field_parallel=GKDB_RNG.uniform(-10, 10),
        momentum_tor_parallel_b_field_parallel=GKDB_RNG.uniform(-10, 10),
        momentum_tor_perpendicular_phi_potential=GKDB_RNG.uniform(-10, 10),
        momentum_tor_perpendicular_a_field_parallel=GKDB_RNG.uniform(-10, 10),
        momentum_tor_perpendicular_b_field_parallel=GKDB_RNG.uniform(-10, 10),
    )


def generate_fake_moments(ny, nt) -> Moments:
    return Moments(
        density=_fake_2d_array(ny, nt),
        density_gyroav=_fake_2d_array(ny, nt),
        j_parallel=_fake_2d_array(ny, nt),
        j_parallel_gyroav=_fake_2d_array(ny, nt),
        pressure_parallel=_fake_2d_array(ny, nt),
        pressure_parallel_gyroav=_fake_2d_array(ny, nt),
        pressure_perpendicular=_fake_2d_array(ny, nt),
        pressure_perpendicular_gyroav=_fake_2d_array(ny, nt),
        heat_flux_parallel=_fake_2d_array(ny, nt),
        heat_flux_parallel_gyroav=_fake_2d_array(ny, nt),
        v_parallel_energy_perpendicular=_fake_2d_array(ny, nt),
        v_parallel_energy_perpendicular_gyroav=_fake_2d_array(ny, nt),
        v_perpendicular_square_energy=_fake_2d_array(ny, nt),
        v_perpendicular_square_energy_gyroav=_fake_2d_array(ny, nt),
    )


def generate_fake_moments_particle(ny, nt) -> MomentsParticles:
    return MomentsParticles(
        density=_fake_2d_array(ny, nt),
        j_parallel=_fake_2d_array(ny, nt),
        pressure_parallel=_fake_2d_array(ny, nt),
        pressure_perpendicular=_fake_2d_array(ny, nt),
        heat_flux_parallel=_fake_2d_array(ny, nt),
        v_parallel_energy_perpendicular=_fake_2d_array(ny, nt),
        v_perpendicular_square_energy=_fake_2d_array(ny, nt),
    )


def generate_fake_fluxes_moments(ny, nt) -> FluxesMoments:
    return FluxesMoments(
        moments_norm_gyrocenter=generate_fake_moments(ny, nt),
        moments_norm_particle=generate_fake_moments_particle(ny, nt),
        fluxes_norm_gyrocenter=generate_fake_flux(),
        fluxes_norm_gyrocenter_rotating_frame=generate_fake_flux(),
        fluxes_norm_particle=generate_fake_flux(),
        fluxes_norm_particle_rotating_frame=generate_fake_flux(),
    )


def generate_fake_specie() -> Species:
    return Species(
        charge_norm=GKDB_RNG.uniform(-10, 10),
        mass_norm=GKDB_RNG.uniform(.1, 10),
        density_norm=GKDB_RNG.uniform(.1, 10),
        density_log_gradient_norm=GKDB_RNG.uniform(.1, 10),
        temperature_norm=GKDB_RNG.uniform(.1, 10),
        temperature_log_gradient_norm=GKDB_RNG.uniform(.1, 10),
        velocity_tor_gradient_norm=GKDB_RNG.uniform(.1, 10),
    )


def generate_fake_eigenmode(fake_data: RunParameters) -> Eigenmode:
    return Eigenmode(
        poloidal_turns=GKDB_RNG.choice(5),
        growth_rate_norm=GKDB_RNG.uniform(-2, 2),
        frequency_norm=GKDB_RNG.uniform(0, 100),
        growth_rate_tolerance=GKDB_RNG.uniform(0, 2),
        phi_potential_perturbed_weight=GKDB_RNG.uniform(0, 1,
                                                        size=fake_data.nt),
        phi_potential_perturbed_parity=GKDB_RNG.uniform(0, 2 * np.pi,
                                                        size=fake_data.nt),
        a_field_parallel_perturbed_weight=GKDB_RNG.uniform(0, 1,
                                                           size=fake_data.nt),
        a_field_parallel_perturbed_parity=GKDB_RNG.uniform(0, 2 * np.pi,
                                                           size=fake_data.nt),
        b_field_parallel_perturbed_weight=GKDB_RNG.uniform(0, 1,
                                                           size=fake_data.nt),
        b_field_parallel_perturbed_parity=GKDB_RNG.uniform(0, 2 * np.pi,
                                                           size=fake_data.nt),
        poloidal_angle=np.arange(0, fake_data.ny) * fake_data.dy,
        phi_potential_perturbed_norm=_fake_2d_array(fake_data.ny, fake_data.nt),
        a_field_parallel_perturbed_norm=_fake_2d_array(fake_data.ny, fake_data.nt),
        b_field_parallel_perturbed_norm=_fake_2d_array(fake_data.ny, fake_data.nt),
        time_norm=np.arange(fake_data.nt),
        fluxes_moments=[generate_fake_fluxes_moments(fake_data.ny, fake_data.nt) for _ in range(fake_data.nspecies)],
        code=generate_fake_code_partial(fake_data),
        initial_value_run=GKDB_RNG.choice([True, False])
    )


def generate_fake_wavevector(fake_data) -> Wavevector:
    return Wavevector(
        radial_component_norm=GKDB_RNG.uniform(0, 10),
        binormal_component_norm=GKDB_RNG.uniform(0, 10),
        eigenmode=[generate_fake_eigenmode(fake_data) for _ in range(fake_data.neigenmode)]
    )


def generate_fake_code_partial(fake_params) -> CodePartialConstant:
    return CodePartialConstant(parameters=js.dumps(asdict(fake_params)),
                               output_flag=GKDB_RNG.choice([0, 1], p=[0.95, 0.05])
                               )


def generate_fake_params(max_size: int = 16) -> RunParameters:
    return RunParameters(nx=rnd.randint(low=1, high=max_size),
                         ny=rnd.randint(low=1, high=max_size),
                         nz=rnd.randint(low=1, high=max_size),
                         nt=rnd.randint(low=1, high=max_size),
                         nvx=rnd.randint(low=1, high=max_size),
                         nvy=rnd.randint(low=1, high=max_size),
                         nvz=rnd.randint(low=1, high=max_size),
                         nspecies=rnd.randint(low=1, high=16),
                         nwavevector=rnd.randint(low=1, high=16),
                         neigenmode=rnd.randint(low=1, high=16),
                         ntags=rnd.randint(low=0, high=10),
                         dx=rnd.uniform(1e-6, 10),
                         dy=rnd.uniform(1e-6, 10),
                         dz=rnd.uniform(1e-6, 10),
                         dvx=rnd.uniform(1e-6, 10),
                         dvy=rnd.uniform(1e-6, 10),
                         dvz=rnd.uniform(1e-6, 10),
                         nlibraries=rnd.randint(low=0, high=4),
                         )

def generate_fake_tag() -> EntryTag:
    alphabet = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    np_alphabet = np.array(alphabet, dtype="|S1")
    np_codes = str(b"".join((rnd.choice(np_alphabet, 12),)))
    return EntryTag(
        tagid="".join(("#", *[str(x) for x in rnd.randint(0,9,size=8)])),
        categorie=rnd.choice(CODE_LIST),
        description=np_codes
    )
def generate_rnd_ids_gk(fake_params: RunParameters | None = None) -> Gyrokinetics:
    if fake_params is None:
        fake_params = generate_fake_params()

    return Gyrokinetics(
        ids_properties=generate_fake_ids_properties(),
        tag=[generate_fake_tag() for _ in range(fake_params.ntags)],
        normalizing_quantities=generate_fake_input_normalizing(),
        flux_surface=generate_fake_flux_surface(),
        model=generate_fake_model(),
        species_all=generate_fake_input_species(),
        species=[generate_fake_specie() for _ in range(fake_params.nspecies)],
        collisions=generate_fake_collision(fake_params.nspecies),
        wavevector=[generate_fake_wavevector(fake_params) for _ in range(fake_params.nwavevector)],
        fluxes_integrated_norm=[generate_fake_flux() for _ in range(fake_params.nspecies)],
        code=generate_fake_code(fake_params),
        time=[0.1, 0.2],
    )


if __name__ == "__main__":
    from mem_size import total_size

    # print(generate_fake_params().to_dict())
    gkids = generate_rnd_ids_gk()
    print(np.round(total_size(gkids) / (1024 ** 2)))
