import xarray as xr
import scipy.constants
import numpy as np
import inspect

np.seterr(all="raise", )

# set certain constants from file
constants_from_file = {}
with open('constants.txt', 'r') as f:
    for line in f:
        l = line.strip().split('=')
        constants_from_file[l[0]] = np.float64(l[1])


def return_values(ds_entry):
    """
    Check the dimension of the dataset variable and return in appropriate form.
    :param ds_entry: xarray dataset variable
    :return: values of the dataset as float or np array
    """
    # if float or int , return float
    if isinstance(ds_entry, (float, int)):
        return float(ds_entry)
    # if xarray dataset
    elif isinstance(ds_entry, xr.core.dataarray.DataArray):
        # if 0D array, return value
        if ds_entry.shape == ():
            # print('we got a single value in a 1D array')
            return float(ds_entry.item(0))
            # if 0D array, return value
        elif ds_entry.size == 1:
            # print('we got a single value in an n-D array')
            return float(ds_entry.item(0))
        # if n-D array, return numpy array of values
        else:
            # print('we got some kind of array')
            return ds_entry.values


def split_xarray(array):
    """
    splits an xarray along its last dimension into n separate arrays, where n is the size of the last dimension
    :param array: xarray file to split
    :return: list of n arrays with 1 less dimension than the original array
    """
    # get size of last dim
    last_dim_size = array.shape[-1]

    # create a list to hold the split arrays
    split_arrays = []

    # loop over the last dimension and select a single value for each split array
    for i in range(last_dim_size):
        split_arr = array.isel(**{array.dims[-1]: i})
        split_arrays.append(split_arr)

    return split_arrays


def count_unique(xarray, ret=False, make_set=False):
    flattened = np.ravel(xarray.values)
    if make_set is False:
        unique_set = {flattened[0]}
        for n, val in enumerate(flattened):
            if val not in unique_set:
                unique_set.add(val)
        print(f'{len(unique_set)} unique values')
        if ret is False:
            for val in sorted(unique_set):
                print(val)
        else:
            return sorted(unique_set)
    else:
        unique_set = {}
        for n, val in enumerate(flattened):
            if val not in unique_set:
                unique_set[val] = 1
            else:
                unique_set[val] += 1
        print(f'{len(unique_set)} unique values')
        if ret is False:
            ii_sum = 0
            for val in sorted(unique_set):
                ii = unique_set[val] / 39936
                print(val, ii)
                ii_sum += ii
            print(f'Num: {ii_sum}')
        else:
            return sorted(unique_set)


class constantsQLK():
    """
    Initialise a ratio constants class with QLK:IMAS ratios for use in the conversion functions.
    :param ds: optional parameter required for conversion routine. If not present only the default values from QLK will be returned.
    """
    qref = np.float64(constants_from_file['qref_qlk'])  # C, qlk charge
    mref = np.float64(constants_from_file['mref_qlk'])  # kg, qlk proton mass
    me = np.float64(constants_from_file['me_qlk'])  # kg, qlk electron mass
    Tref = np.float64(constants_from_file['Tref_qlk'])  # eV
    nref = np.float64(constants_from_file['nref_qlk'])  # m^-3
    cref = np.sqrt(
        np.float64(constants_from_file['Tref_qlk']) * np.float64(constants_from_file['qref_qlk']) / np.float64(
            constants_from_file['mref_qlk']))

    def __init__(self, Ro, Bo, Te, Ai0):
        self.Rref = np.float64(Ro)  # m
        self.Bref = np.float64(Bo)  # T
        # self.cref = np.sqrt(self.Tref * self.qref / self.mref)
        # mass of first ion species
        # self.mi0 = np.float64(ds.isel(nions=0).Ai.values) * self.mref
        self.Ai0 = Ai0
        self.mi0 = self.Ai0 * self.mref
        self.vth_i0 = np.sqrt(np.float64(Te) * self.Tref * self.qref / self.mi0)
        self.rho_i0 = self.mi0 * self.vth_i0 / (self.qref * self.Bref)

    def print_constants(self):

        print('\nQLK Constants:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


class constantsIMAS:
    """
    Initialise an IMAS constants class from scipy.constants for use in the conversion functions

    :param ds: optional parameter required for conversion routine. If not present only the default values from scipy will be returned.
    """
    qref = np.float64(scipy.constants.physical_constants['electron volt'][0])  # C
    mref = np.float64(scipy.constants.physical_constants['deuteron mass'][0])  # kg
    me = np.float64(scipy.constants.physical_constants['electron mass'][0])  # kg

    def __init__(self, Ro, Bo, Te, ne):
        # return eV value (given in keV)
        self.Tref = Te * constants_from_file['Tref_qlk']
        # return m^-3 value (given in 1e19 m^-3)
        self.nref = np.float64(ne) * constants_from_file['nref_qlk']
        self.Rref = Ro
        self.Bref = Bo
        qTe = self.Tref * self.qref
        self.vth_ref = np.sqrt(np.float64(2) * qTe / self.mref)
        self.rho_ref = self.mref * self.vth_ref / (self.qref * self.Bref)
        self.vth_e = np.sqrt(np.float64(2) * qTe / self.me)

    def print_constants(self):

        print('\nIMAS Constants:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


class constantsRATIO:
    """
    Initialise a ratio constants class with QLK:IMAS ratios for use in the conversion functions
    :param QLK: the QLK constants
    :param IMAS: the IMAS constants
    """
    Trat = np.float64(1)

    def __init__(self, QLK, IMAS):
        self.qrat = QLK.qref / IMAS.qref
        self.mrat = QLK.mref / IMAS.mref
        self.nrat = QLK.nref / IMAS.nref
        self.Rrat = QLK.Rref / IMAS.Rref
        self.Brat = QLK.Bref / IMAS.Bref
        self.vth_rat = QLK.vth_i0 / IMAS.vth_ref
        self.rho_rat = QLK.rho_i0 / IMAS.rho_ref
        # rho_rat 2 seems more consistent in outputting a single values, avoiding errors due to cancelling
        self.rho_rat2 = np.sqrt(QLK.mi0 / (2 * IMAS.mref * self.qrat * self.Brat))

    def print_constants(self):

        print('\nConstant Ratios:')
        # members of an object
        for i in inspect.getmembers(self):

            # to remove private and protected
            # functions
            if not i[0].startswith('_'):

                # To remove other methods that
                # doesnot start with a underscore
                if not inspect.ismethod(i[1]):
                    var = i[1]
                    if type(var) == float:
                        print(f'{i[0]}: {var}, {type(var)}')
                    elif var.shape == ():
                        print(f'{i[0]}: {var}, {type(var)}')
                    else:
                        print(f'{i[0]} shape: {var.shape}')


def r_minor_norm(Rmin, x, Ro):
    """
    Calculates the IMAS normalised minor radius r_minor_norm given the following qualikiz inputs:
    :param Rmin: array or float of  QLK minor radius of LCS (ds.Rmin.values)
    :param x: array or float of  QLK normalised minor radius of flux surface (ds.x.values)
    :param Ro: array or float of QLK major radius (ds.Ro.values)

    :returns: array or float of IMAS minor radius normalised to major radius
    """
    return Rmin * x / Ro


def b_field_tor_sign():
    """
    Calculates the sign of the IMAS toroidal field direction:
    Always returns 1 for QLK
    """
    return 1


def ip_sign():
    """
    Calculates the sign of the IMAS toroidal current direction:
    Always returns 1 for QLK
    """
    return 1


def q_IMAS(q):
    """
    Calculates the IMAS safety factor:
    :param q: float or array of the qlk safety factor (ds.q.values)
    :return: float or array of the IMAS normalised safety factor
    """
    return np.float64(q)


def magnetic_shear_r_minor(smag):
    """
    Calculates the IMAS magnetic shear given the following qualikiz inputs:
    :param smag: array or float of QLK magnetic shear (ds.smag.values)

    :return: IMAS normalised magnetic shear
    """
    return np.float64(smag)


def pressure_gradient_norm(alpha, q, Rrat, Brat):
    """
    Calculates the IMAS normalised pressure gradient (pressure_gradient_norm) given the following qualikiz inputs:
    :param alpha: array or float of QLK MHD alpha (ds.alpha.values)
    :param q: array or float of  QLK safety factor (ds.q.values)
    :param Rrat: float of QLK:IMAS ratio of reference lengths
    :param Rrat: float of QLK:IMAS ratio of reference magnetic fields

    :returns: array or float of IMAS normalised pressure gradient
    """

    pres_grad_norm = alpha * Brat ** 2 / q ** 2 / Rrat
    return pres_grad_norm


def dgeometric_axis_r_dr_minor(alpha):
    """
    Calculates the IMAS derivative of R_0 wrt. r at r=r_0 given the following qualikiz inputs:
    :param alpha: array or float of QLK MHD alpha (ds.alpha.values)

    :return: array or float of IMAS derivative of R_0 wrt. r
    """
    return np.float64(-alpha)


def dgeometric_axis_z_dr_minor():
    """
    Returns the IMAS derivative of z wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.float64(0)


def elongation():
    """
    Returns the IMAS elongation which is always 1 for QLK

    :return: always 1 for QLK
    """
    return np.float64(1)


def delongation_dr_minor_norm():
    """
    Calculates the IMAS derivative of elongation wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return np.float64(0)


def shape_coefficients_c():
    """
    Calculates the IMAS MXH cosine shape coefficients which are always 0 for QLK

    :return: always 0 for QLK
    """
    return (np.float64(0),)


def shape_coefficients_s():
    """
    Calculates the IMAS MXH sine shape coefficients which are always 0 for QLK

    :return: always 0 for QLK
    """
    return (np.float64(0),)


def dc_dr_minor_norm():
    """
    Calculates the IMAS derivative of IMAS MXH cosine shape coefficients wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return (np.float64(0),)


def ds_dr_minor_norm():
    """
    Calculates the IMAS derivative of IMAS MXH sine shape coefficients wrt. r at r=r_0 which is always 0 for QLK

    :return: always 0 for QLK
    """
    return (np.float64(0),)


def charge_norm(Zi_QLK, q_rat):
    """
    Calculates the IMAS normalised charge of an ion species given the following inputs:
    Zi_QLK: array of the ion species charge number from QLK
    q_rat: float of the ratio of QLK:IMAS reference charge (ratioclass.qrat)
    """
    return Zi_QLK * q_rat


def mass_norm(Ai_QLK, m_rat):
    """
    Calculates the IMAS normalised charge of a species given the following inputs:
    Ai_QLK: ds.Ai, the species mass number from QLK
    m_rat: the ratio of QLK:IMAS reference masses (ratioclass.mrat)
    """
    return Ai_QLK * m_rat


def density_norm_e(ne_QLK):
    """
    Calculates the normalised IMAS electron density. Should always be 1
    :param ne_QLK: float or array of the electron density from QLK (ds.ne.values)
    :return: same dimension array as ne, filled with 1s
    """
    if np.isscalar(ne_QLK):
        return np.float64(1)
    else:
        return np.ones_like(ne_QLK, np.float64)


def density_norm_i(normni_QLK):
    """
    Calculates the normalised IMAS ion density. Should always be the values from ds.normni
    but needs to be split into one value for each ion species
    :param normni_QLK: array or float of QLK normalised ion densities ds.normni.values
    :return: input
    """
    return normni_QLK


def density_log_gradient(Ans_QLK, R_rat):
    """
    Calculates the IMAS normalised charge of a species given the following inputs:
    :param Ans_QLK: float or array of the species normalised logarithmic temperature gradient from QLK (ds.Ani.values / ds.Ane.values)
    :param L_rat: float of the ratio of QLK:IMAS reference masses (RATIOS.Rrat)

    :return: IMAS normalised logarithmic temperature gradient
    """
    Ans = Ans_QLK / R_rat
    return Ans


def temperature_norm(Ts_QLK, Te_QLK):
    """
    Calculates the IMAS normalised temperature of a species given the following inputs:
    Ts_QLK: float or array of (ds.Te.values or ds.Ti.values) the species temperature from QLK
    Te_QLK: float of (ds.Te.values) the electron temperature from QLK
    :return: normalised temperature, Ts / Te
    """
    T_norm = Ts_QLK / Te_QLK
    return T_norm


def temperature_log_gradient(Ats_QLK, R_rat):
    """
    Calculates the IMAS normalised temperature gradient of a species given the following inputs:
    Ans_QLK: ds.Ati/ds.Ate the species normalised temperature gradient from QLK
    R_rat: the ratio of QLK:IMAS reference lengths (ratioclass.Lrat)

    strictly speaking, should be Ans_QLK/L_rat but since L_rat is 1 (for now) it just saves processing time
    """
    return Ats_QLK / R_rat


def velocity_tor_norm(Machtor, cref_QLK, vth_ref_IMAS, Rrat):
    """
    Calculates the IMAS normalised toroidal veloctiy given the following qualikiz inputs:

    :param Machtor: array or float of toroidal mach number from QLK files (ds.Machtor.values)
    :param cref_QLK: float of normalising velocity for QLK normalisations (QLK.cref)
    :param vth_ref_IMAS: float of IMAS reference thermal velocity (IMAS.vth_ref)
    :param Rrat: float of ratio of QLK to IMAS major radius (RATIOS.Rrat)
    :return: IMAS normalised toroidal velocity
    """
    vel_tor_norm = Machtor * cref_QLK / (vth_ref_IMAS * Rrat)
    return vel_tor_norm


def velocity_tor_gradient_norm(Autor_QLK, cref_QLK, vth_ref_IMAS, Rrat_ratio):
    """
    Calculates the IMAS normalised toroidal veloctiy gradient (or parallel flow shear) given the following qualikiz inputs:
    :param Autor_QLK: array or float of the toroidal velocity gradient from QLK files (ds.Autor.values)
    :param cref_QLK: float of normalising velocity for QLK normalisations  (QLK.cref)
    :param vth_ref_IMAS: float of IMAS reference thermal velocity (IMAS.vth_ref)
    :param Rrat:  float of ratio of QLK to IMAS major radius (RATIOS.Rrat)
    :return: IMAS normalised toroidal velocity gradient
    """
    vel_tor_gradient_norm = cref_QLK * Autor_QLK / (vth_ref_IMAS * Rrat_ratio ** 2)
    return vel_tor_gradient_norm


def shearing_rate_norm(gammE_QLK, cref_QLK, vth_ref_IMAS, Rrat):
    """
    Calculates the IMAS normalised perpendicular flow shear given the following qualikiz inputs:

    :param gammE_QLK: array or float of (ds.gammaE.values) the QLK gammaE data
    :param cref_QLK:  array or float of (QLK.cref) QLK reference velocity for normalising velocities
    :param vth_ref_IMAS: array or float of (IMAS.vth_ref) IMAS thermal reference velocity
    :param Rrat: array or float of (RATIOS.Rrat) ratio of QLK to IMAS R (=1)
    :return: IMAS normalised perpendicular flow shear
    """
    shear_rate_norm = gammE_QLK * cref_QLK / (vth_ref_IMAS * Rrat)
    return shear_rate_norm


def beta_reference():
    """
    beta reference is set to 0 for electrostatic codes like QLk
    """
    return np.float64(0)


def debye_length_reference():
    """
    debye length is assumed to be 0 for simplifications to hold
    """
    return np.float64(0)


def radial_component_norm():
    """
    radial wavevector is set to 0
    """
    return np.float64(0)


def binormal_component_norm(rho_rat, kthetarho_QLK):
    """
    Calculates the IMAS binormal wavevector given the following inputs:

    :param kthetarho_QLK: float or array of the wavenumber from QLK (ds.kthetarhos.values)
    :param rho_rat: float of (RATIOS.rho_rat) the ratio of larmor radii of QLK to IMAS :return: IMAS binormal wavevector
    """
    if np.isscalar(rho_rat):
        kthetas = kthetarho_QLK / rho_rat
    else:
        kthetas = np.array([kthetarho_QLK / rho for rho in rho_rat])
    return kthetas


def collisionality_norm(IMAS_Rref, IMAS_vth_ref, QLK_n_e, QLK_n_b, QLK_Z_b, q_rat, QLK_t_e, IMAS_vth_a):
    """
    Calculates the IMAS normalised collisionality (only valid for electrons onto ions for QLK?). Splits the ion density
    array and calculates the collisionality for each ion type separately before summing. Uses the following inputs:
    IMAS_Rref: IMAS reference length (IMAS.Rref)
    IMAS_vth_ref: IMAS reference thermal velocity (IMAS.vth_ref)
    QLK_n_e: float or array of  qualikiz electron density in units of 1e19 (ds.ne.values)
    QLK_n_b: float or array of normalised qualikiz density of target ion (SI), (ds.normni.values)
    QLK_Z_b: float or array of charge number of target ion (ds.Zi.values)
    q_rat: float of QLK:IMAS refernce charge ratio
    QLK_t_e: float or array of QLK electron temperature (ds.Te.values)
    IMAS_vth_a: thermal velocity of incident particles i.e. electrons (IMAS.vth_e)

    returns: sum of the collisionality arrays for each ion
    """

    def coulomblog(QLK_ne, QLK_Te):
        coulog = np.float64(15.2 - 0.5 * np.log(QLK_ne / 10) + np.log(QLK_Te))
        return coulog

    # convert qualikiz normalised ion density to SI
    n_b = QLK_n_b * QLK_n_e * np.float64(1e19)
    Z_a = np.float64(-1) * q_rat  # incident particles are hardcoded to electrons in QLK
    Z_b = QLK_Z_b * q_rat
    e = scipy.constants.physical_constants['electron volt'][0]
    e_0 = scipy.constants.physical_constants['vacuum electric permittivity'][0]
    m_a = scipy.constants.physical_constants['electron mass'][0]  # incident particles are hardcoded to electrons in QLK
    coulomblog = coulomblog(QLK_n_e, QLK_t_e)
    collated_constants = Z_a ** 2 * e ** 4 / (4 * np.pi * e_0 ** 2 * m_a ** 2)
    collisionality = collated_constants * IMAS_Rref * n_b * (Z_b ** 2) * coulomblog / (IMAS_vth_ref * (IMAS_vth_a ** 3))
    return collisionality


def include_centrifugal_effects(rot_flag_QLK):
    """
    return true or false flag depending on QLK settings, taking in as input:
    rot_flag_QLK: (ds.rot_flag) the rot_flag value in QLK parameters

    :returns: 0

    note: QLK pythontools cannot scan over rotflag so the value is usually given as a float. I don't anticipate
    this being a problem for this reason, but it might need to be adjusted in the future?
    """
    rot_flag = return_values(rot_flag_QLK)

    cent_effect = np.float64(0)
    return cent_effect


def collisions_pitch_only():
    """return true for QLK"""
    return True


def temperature_converter(Temp, convert_to='joules'):
    """
    converts a temperature in eV or joules to the other
    :param Temp: Temperature or temperature array to convert
    :param convert_to: units to convert to (assumes the other as input) either 'joule'/'joules'/'J' (default) or 'eV'/'electron volts'/'electron volt' case insensitive.
    :return: temperature/array converted into the new unit
    """
    convert_to_filtered = convert_to.lower()
    T_in = return_values(Temp)

    if convert_to_filtered in ['joules', 'j', 'joule']:
        # convert from eV to J
        T_out = T_in * scipy.constants.physical_constants['electron volt'][0]
        return T_out
    elif convert_to_filtered in ['ev', 'electron volt', 'electron volts', 'electronvolt', 'electronvolts']:
        # convert from J to eV
        T_out = T_in / scipy.constants.physical_constants['electron volt'][0]
        return T_out
    else:
        raise Exception(f'conversion to unit: {convert_to} not recognised')


def extend_xarray(array, target):
    """
    extends the input array to match the dimensions of the target array
    :param array: some xarray to extend
    :param target: the array with the wanted shape
    :return: the input array, with values duplicated to match the size of the target array
    """
    if not isinstance(array, xr.core.dataarray.DataArray):
        raise Exception("Input array is not an xarray")
    if not isinstance(target, xr.core.dataarray.DataArray):
        raise Exception("Target array is not an xarray")
    array_broadcast = array.broadcast_like(target)
    return array_broadcast


def growth_rate_norm(gamma_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised growth rate of a mode given the following inputs:
    gamma_GB_QLK: float or array of (ds.gam_GB.values)  GyroBohm normalised growth rate from QLK
    Rmin_QLK: float or array of (ds.Rmin.values) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    gamma_IMAS = gamma_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK
    return gamma_IMAS


def growth_rate_norm_single(gamma_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised growth rate of a mode given the following inputs:
    gamma_GB_QLK: (ds.gam_GB) GB growth rate from QLK
    Rmin_QLK: (ds.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    return gamma_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK


def growth_rate_norm_split(gamma_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised growth rates of a mode given the following inputs:
    gamma_GB_QLK: (ds.gam_GB) GB growth rate from QLK
    Rmin_QLK: (QLK.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    :returns: an array of length numsols, one for each of the growth rates of the mode
    """
    gamma_GB = return_values(gamma_GB_QLK)
    Rref = return_values(Rref_IMAS)
    Rmin = return_values(Rmin_QLK)
    vth_rat = return_values(vth_rat_RATIO)
    # check if vth_rat is an array and needs to be extended to gamma_GB
    if isinstance(vth_rat, xr.core.dataarray.DataArray):
        vth_rat_ext = extend_xarray(vth_rat, gamma_GB)
    else:
        vth_rat_ext = vth_rat
    gamma_IMAS = gamma_GB * Rref * vth_rat_ext / Rmin
    gamma_list = split_xarray(gamma_IMAS)
    return gamma_list


def frequency_norm(omega_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    omega_GB_QLK: float or array of (ds.ome_GB.values) GyroBohm normalised frequency from QLK
    Rmin_QLK: (ds.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    return omega_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK


def frequency_norm_single(gamma_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    omega_GB_QLK: GB mode frequency from QLK
    Rmin_QLK: (QLK.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    """
    return gamma_GB_QLK * Rref_IMAS * vth_rat_RATIO / Rmin_QLK


def frequency_norm_split(ome_GB_QLK, Rmin_QLK, Rref_IMAS, vth_rat_RATIO):
    """
    Calculates the IMAS normalised frequency of a mode given the following inputs:
    ome_GB_QLK: (ds.ome_GB) GB growth rate from QLK
    Rmin_QLK: (QLK.Rmin) QLK minor radius of LCS
    Rref_IMAS: (IMAS.Rref) IMAS reference length for the normalisation
    vth_rat_RATIO: (RATIOS.vth_rat) ratio of QLK:IMAS thermal velocities
    :returns: an array of length numsols, one for each of the frequencies of the mode
    """
    ome_GB = return_values(ome_GB_QLK)
    Rref = return_values(Rref_IMAS)
    Rmin = return_values(Rmin_QLK)
    vth_rat = return_values(vth_rat_RATIO)
    # check if vth_rat is an array and needs to be extended to gamma_GB
    if isinstance(vth_rat, xr.core.dataarray.DataArray):
        vth_rat_ext = extend_xarray(vth_rat, ome_GB)
    else:
        vth_rat_ext = vth_rat
    gamma_IMAS = ome_GB * Rref * vth_rat_ext / Rmin
    ome_list = split_xarray(gamma_IMAS)
    return ome_list


def poloidal_angle():
    return np.linspace(-3 * np.pi, 3 * np.pi, 150)


def phi_potential_perturbed_norm_nonlinear():
    """
    Calculates the IMAS normalised perturbed potential for a nonlinear simulation given the following inputs:
    TODO: uneeded for our purposes, need for QLK.nc generic file conversion? not complete in doc yet
    """
    return


def phi_potential_perturbed_norm_linear(theta, modewidth, modeshift, d, recip_rho_star, q_ref, T_ref, phi_0=1):
    """
    Calculates the IMAS normalised perturbed potential for a linear simulation given the following inputs:

    :param theta: theta array over which to integrate/calculate potential
    :param modewidth: modewidth from QLK. (Complex number made up of ds.imodewidth and ds.rmodewidth)
    :param modeshift: modeshift from QLK. (Complex number made up of ds.imodeshift and ds.rmodeshift)
    :param d: TODO: not sure what this parameter represents (ds.distan)
    :param phi_0: normalising factor, wont affect the result
    :param recip_rho_star: reciprocal of rho_star, the normalised IMAS reference larmor radius
    :param q_ref: reference IMAS q
    :param T_ref: reference IMAS Temperature
    :return: the normalised IMAS potential over the theta grid
    """

    # calculate the factor for phiN
    pre_fact = recip_rho_star * q_ref / T_ref
    # calculate the QLK potential
    # stph_factor coefficient not used in derivation due to normalisations rendering it unecessary
    stph_factor = np.sqrt(2 * np.pi) * modewidth * phi_0
    # exp_factor = modewidth^2/(2*d^2)
    exp_factor = modewidth / d
    exp_factor = .5 * exp_factor
    theta2 = theta ** 2
    exp_term = -(theta2 * exp_factor)
    if np.abs(modeshift) > 1e-8:
        phi = np.exp(exp_term) * np.exp(theta * (1j * modeshift / d))
    else:
        phi = np.exp(exp_term)

    # normalise according to the prefactor
    phi_N = pre_fact * phi

    # find maximum and index for exp(ialpha)
    max_index = np.argmax(np.abs(phi_N))
    phi_hat_N_max = np.abs(phi_N)[max_index]
    # calculate normalisation factor
    if phi_N[max_index] == 0:
        # in that case e_ialpha = 0
        phi_Nf = np.zeros_like(phi_N)
    else:
        # integrating to find Af
        # abs square of phi_N to integrate
        phi_N_abs_sq = np.abs(phi_N) ** 2
        Af = np.sqrt(1 / (2 * np.pi) * np.trapz(phi_N_abs_sq, theta))
        e_ialpha = phi_hat_N_max / phi_N[max_index]
        phi_Nf = phi_N * e_ialpha / Af

    return phi_Nf


def zeff(nis, Zis, qrat):
    '''
    Calculates the effective ion charge given floats or arrays of ion densities, charge and the charge ratio of QLK to IMAS
    :param nis: array or float of ion densities (ds.normni.values)
    :param Zis: array or float of ion charges (ds.Zi.values)
    :param qrat: float of the QLK to IMAS reference charges (RATIOS.qrat)

    :return: array or float of the effective ion charge
    TODO: check this
    '''
    if np.isscalar(Zis):
        return Zis
    else:
        denom = 0
        num = 0
        for i, _ in enumerate(nis):
            denom += nis[i] * Zis[i]
            num += nis[i] * Zis[i] ** 2
        return num / denom


def normalise_integrated_particle_flux(particle_flux, n_s, v_thref, rho_star):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param particle_flux: float or array of QLK particle flux (ds.pfe_SI.values or ds.pfi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :return: IMAS normalised particle flux
    '''

    denom = n_s * np.float64(1e19) * v_thref * rho_star ** 2
    particle_flux_norm = np.divide(particle_flux, denom, out=np.zeros_like(denom),
                                   where=denom != 0)
    return particle_flux_norm[()]


def normalise_integrated_energy_flux(energy_flux, n_s, v_thref, rho_star, T_ref):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param energy_flux: float or array of QLK energy flux (ds.efe_SI.values or ds.efi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param T_ref: float of IMAS reference temperature (IMAS.Tref)
    :return: IMAS normalised energy flux
    '''
    denom = n_s * np.float64(1e19) * T_ref * v_thref ** 2 * rho_star ** 2
    energy_flux_norm = np.divide(energy_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return energy_flux_norm[()]


def normalise_integrated_momentum_flux(momentum_flux, n_s, v_thref, rho_star, m_ref, L_ref):
    '''
    Normalises a QLK particle flux to IMAS standard.
    :param momentum_flux: float or array of QLK momentum flux (ds.vfe_SI.values or ds.vfi_SI.values)
    :param n_s: float or array of particle density (ds.ne.values or ds.normni.values * ds.ne.values) in units of 1e19
    :param v_thref: float of IMAS v_thref
    :param rho_star: float of IMAS rhostar (reference larmor radius normalised to reactor Lref)
    :param m_ref: float of IMAS reference mass (IMAS.mref)
    :param L_ref: float of IMAS reference major radius (IMAS.Rref)
    :return: IMAS normalised energy flux
    '''
    denom = n_s * np.float64(1e19) * m_ref * L_ref * v_thref ** 2 * rho_star ** 2
    # conditon for dealing with 0 density species, sets outputs of division to 0 if denom = 0
    momentum_flux_norm = np.divide(momentum_flux, denom, out=np.zeros_like(denom), where=denom != 0)
    return momentum_flux_norm[()]

