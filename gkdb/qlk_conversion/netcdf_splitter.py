import xarray as xr
import numpy as np

filepath = f'./glued_netcdf.nc.1'
splitted_file = './edge10d_glued_{0:02d}.nc.1'

full_ds64 = xr.open_dataset(filepath, engine='netcdf4')

dim_x = full_ds64.dimx.values
size_x = len(dim_x)
split_number = 100
chunck = size_x//split_number

offset = 0
idx = 0
while True : 
    print(f"creating split [{idx+1}]/{split_number}")
    ds_buffer = full_ds64.isel(dimx=full_ds64['dimx'].isin(range(offset,min(offset+chunck, size_x))))
    ds_buffer.to_netcdf(splitted_file.format(idx+1))
    if offset+chunck>size_x:
        break
    offset += chunck
    idx += 1