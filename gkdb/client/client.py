from __future__ import annotations

# for line_profiler, might be removed later
import builtins

try:
    profile = builtins.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func):
        return func

from typing import Type, Any, Optional

import psycopg
import numpy as np
from psycopg_pool import ConnectionPool
from psycopg import sql
from idspy_dictionaries import ids_gyrokinetics

from gkdb.config import DBConfig
import gkdb.core.idstogkdb as ids2gkdb
from gkdb.core.utils import dict2pgjsonb, int2bool, list2pgbytea, shorten_imas_default, compressed_nparray
from gkdb.core.internal_dataclasses import ExtendedFluxes, ExtendedMoments
from gkdb.core.psycopg_adapters import complex_type_register
from gkdb.core.utils import format_value_pg

READ_WRITE_DELETE_ROLE = "readwritedelete"
READ_WRITE_ROLE = "readwrite"
READ_ONLY_ROLE = "readonly"

GKDB_CFG = DBConfig()
GKDB_CFG.write_moments = True

COLUMNS_FLUXES_TABLE = (
    "frame",
    "ftype",
    "uuid",
    "particles_phi_potential",
    "particles_a_field_parallel",
    "particles_b_field_parallel",
    "energy_phi_potential",
    "energy_a_field_parallel",
    "energy_b_field_parallel",
    "momentum_tor_parallel_phi_potential",
    "momentum_tor_parallel_a_field_parallel",
    "momentum_tor_parallel_b_field_parallel",
    "momentum_tor_perpendicular_phi_potential",
    "momentum_tor_perpendicular_a_field_parallel",
    "momentum_tor_perpendicular_b_field_parallel"
)
COLUMNS_MOMENTS_TABLE = (
    "mtype",
    "uuid",
    "norm",
    "density",
    "j_parallel",
    "pressure_parallel",
    "pressure_perpendicular",
    "heat_flux_parallel",
    "v_parallel_energy_perpendicular",
    "v_perpendicular_square_energy"
)

QUERY_COPY_FLUXES = sql.SQL(
    "COPY {}.{} ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}) FROM  STDIN").format(
    sql.Identifier(GKDB_CFG.schema),
    sql.Identifier("fluxes_table"),
    *[sql.Identifier(x) for x in ("gk_id", "species_id", "eigenmode_id",
                                  *COLUMNS_FLUXES_TABLE)])


# @foreign_key_exists("gyrokinetics_table")
@profile
def _insert_model(conn, fk_list: tuple[int | tuple[int]] | None, ids_model) -> int | None:
    """
    Inserts a model into the database.

    Parameters:
    - cur: psycopg.PoolConnection - The connection to the database.
    - fk_list: tuple[int | tuple[int]] | None - The foreign key list.
    - ids_model: Any - The model to be inserted.

    Returns:
    - int | None - The ID of the inserted model or None if the model is None.

    """
    gk_id_fk = fk_list[0]
    if ids_model is None:
        return None

    #    ids_model.energy_conservation = int2bool(ids_model.energy_conservation)
    ids_model.include_centrifugal_effects = int2bool(ids_model.include_centrifugal_effects)
    ids_model.include_a_field_parallel = int2bool(ids_model.include_a_field_parallel)
    ids_model.include_b_field_parallel = int2bool(ids_model.include_b_field_parallel)
    ids_model.include_full_curvature_drift = int2bool(ids_model.include_full_curvature_drift)
    ids_model.collisions_pitch_only = int2bool(ids_model.collisions_pitch_only)
    ids_model.collisions_momentum_conservation = int2bool(ids_model.collisions_momentum_conservation)
    ids_model.collisions_energy_conservation = int2bool(ids_model.collisions_energy_conservation)
    ids_model.collisions_finite_larmor_radius = int2bool(ids_model.collisions_finite_larmor_radius)
    ids_model.non_linear_run = int2bool(ids_model.non_linear_run)
    ids_model.time_interval_norm = list2pgbytea(ids_model.time_interval_norm)
    query = ids2gkdb.query_insert_model(ids_model, gk_id_fk, GKDB_CFG.schema)
    return conn.execute(query).fetchone()[0]


def _insert_integrated_fluxes(cur, fk_list: tuple[int | tuple[int]],
                              ids_fluxes: list | tuple | None,
                              id_species: tuple[int],
                              uuid_gkid) -> bool | None:
    gk_id_fk = fk_list[0][0]
    if ids_fluxes is None:
        return None

    assert len(id_species) == len(ids_fluxes)
    int_fluxes_flat = []

    for specie_id, integrated_flux in zip(id_species, ids_fluxes):
        current_fluxes = _fluxes_as_pgflux(integrated_flux, uuid_gkid, "integrated", "laboratory")

        int_fluxes_flat.append(
            (
                gk_id_fk, specie_id,
                *[getattr(current_fluxes, name) for name in
                  COLUMNS_FLUXES_TABLE]
            )
        )

    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", "species_id",
                                     *COLUMNS_FLUXES_TABLE)],
        int_fluxes_flat,
        GKDB_CFG.schema,
        "fluxes_table")
    cur.execute(query)
    return True


# TODO : multiple FK check
@profile
def _insert_wavevector(cur, fk_list: tuple[int | tuple[int]],
                       ids_wavevector: list | tuple | None,
                       ids_species: tuple | None,
                       uuid_gkid) -> bool | None:
    """

    This method, _insert_wavevector, is used to insert wavevector information into the gyrokinetics database (gkdb).

    Parameters:
    - cur: A psycopg connection object used to connect to the database.
    - fk_list: A tuple containing two elements. The first element is an integer representing the foreign key ID for the gyrokinetics table. The second element is an integer representing the foreign key ID for the code table.
    - ids_wavevector: A list or tuple containing wavevector objects or None.

    Returns:
    - A tuple containing an integer and a dictionary or None.
      - The integer represents the status of the insertion process. It can be 1 if the insertion is successful or -1 if there is an error.
      - The dictionary contains the IDs of the inserted wavevectors and their corresponding eigenmodes.

    """

    # need gk as first and code as second
    gk_id_fk = fk_list[0][0]
    code_id = fk_list[1][0]
    if ids_wavevector is None:
        return None

    if code_id is None:
        raise AttributeError("current IDS does not have a root code associated")

    if not isinstance(ids_wavevector, (list, tuple)):
        print(f"a list of wavevector must be used as argument not a {type(ids_wavevector)}")
        return -1, {}
    cleaned_id_species = tuple([x for x in ids_species if x])

    id_wavevector = []
    # dict containing, for each wavevector_id, a list of all the corresponding eigenmodes

    # generate a list of wavevector to insert
    wv_list = []
    for wv in ids_wavevector:
        wv_list.append([gk_id_fk, wv.radial_component_norm, wv.binormal_component_norm])
    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", "radial_component_norm", "binormal_component_norm")],
        wv_list,
        GKDB_CFG.schema,
        "wavevector_table")
    id_wavevector = cur.execute(query)
    id_wavevector = [int(x[0]) for x in id_wavevector]

    eigenvector_list_flat = []
    column_list = (
        "poloidal_turns",
        "growth_rate_norm",
        "frequency_norm",
        "growth_rate_tolerance",
        "phi_potential_perturbed_weight",
        "phi_potential_perturbed_parity",
        "a_field_parallel_perturbed_weight",
        "a_field_parallel_perturbed_parity",
        "b_field_parallel_perturbed_weight",
        "b_field_parallel_perturbed_parity",
        "poloidal_angle",
        "phi_potential_perturbed_norm",
        "a_field_parallel_perturbed_norm",
        "b_field_parallel_perturbed_norm",
        "time_norm",
        "initial_value_run",
    )
    for wv_id, wv in zip(id_wavevector, ids_wavevector):
        for eig in wv.eigenmode:
            eig.initial_value_run = int2bool(eig.initial_value_run)
            if getattr(eig, 'code', None) is not None:
                code_param = dict2pgjsonb(eig.code.parameters)
            else:
                code_param = dict2pgjsonb({})
            eigenvector_list_flat.append(
                (gk_id_fk,
                 wv_id,
                 code_param,
                 *[format_value_pg(getattr(eig, name, None)) for name in column_list]
                 )
            )
    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", "wavevector_id", "code_parameters", *column_list)],
        eigenvector_list_flat,
        GKDB_CFG.schema,
        "eigenmode_table")
    id_eigenmode = cur.execute(query)
    id_eigenmode = [int(x[0]) for x in id_eigenmode]

    # idx_eig = 0
    # code_partial_columns = (
    #     "eigenmode_id",
    #     "code_id",
    #     "parameters",
    #     "output_flag"
    # )
    # code_eig_flat = []
    # for wv_id, wv in zip(id_wavevector, ids_wavevector):
    #     for eig in wv.eigenmode:
    #         if getattr(eig, 'code', None) is not None:
    #             eig.code.parameters = dict2pgjsonb(eig.code.parameters)
    #             eig.code.output_flag = int(eig.code.output_flag)
    #             code_eig_flat.append((id_eigenmode[idx_eig],
    #                                   code_id,
    #                                   eig.code.parameters,
    #                                   eig.code.output_flag))
    #         else :
    #             code_eig_flat.append((id_eigenmode[idx_eig],
    #                                   code_id,
    #                                   dict2pgjsonb(""),
    #                                   1))
    #
    #         idx_eig += 1
    # query = ids2gkdb.__query_from_list(
    #     [sql.Identifier(x) for x in code_partial_columns],
    #     code_eig_flat,
    #     GKDB_CFG.schema,
    #     "code_partial_constant_table")
    # cur.execute(query)
    # # id_code_eig = [int(x[0]) for x in id_code_eig]
    # TODO : check consistency

    columns_wv_flux = (
        "frame",
        "ftype",
        "uuid",
        "particles_phi_potential",
        "particles_a_field_parallel",
        "particles_b_field_parallel",
        "energy_phi_potential",
        "energy_a_field_parallel",
        "energy_b_field_parallel",
        "momentum_tor_parallel_phi_potential",
        "momentum_tor_parallel_a_field_parallel",
        "momentum_tor_parallel_b_field_parallel",
        "momentum_tor_perpendicular_phi_potential",
        "momentum_tor_perpendicular_a_field_parallel",
        "momentum_tor_perpendicular_b_field_parallel"
    )
    columns_wv_moments = (
        "mtype",
        "uuid",
        "norm",
        "density",
        "j_parallel",
        "pressure_parallel",
        "pressure_perpendicular",
        "heat_flux_parallel",
        "v_parallel_energy_perpendicular",
        "v_perpendicular_square_energy"
    )

    # with cur.copy(
    #         sql.SQL(
    #             "COPY {}.{} ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}) FROM  STDIN").format(
    #             sql.Identifier(GKDB_CFG.schema),
    #             sql.Identifier("fluxes_table"),
    #             *[sql.Identifier(x) for x in ("gk_id", "species_id", "eigenmode_id",
    #                                           *columns_wv_flux)])
    with cur.copy(QUERY_COPY_FLUXES) as copy:
        idx_eig = 0
        for wv in ids_wavevector:
            for eig in wv.eigenmode:
                if eig.fluxes_moments is not None:
                    if len(eig.fluxes_moments) > 0:
                        assert len(cleaned_id_species) == len(
                            eig.fluxes_moments), f"{len(cleaned_id_species)} vs {len(eig.fluxes_moments)}"
                        for specie_id, fluxmoment in zip(cleaned_id_species,
                                                         eig.fluxes_moments):
                            list_moments, list_flux = __fluxes_moment_as_list(fluxmoment, uuid_gkid)
                            for idx in (0, 1, 2):
                                copy.write_row(
                                    (gk_id_fk,
                                     specie_id,
                                     id_eigenmode[idx_eig],
                                     *[(getattr(list_flux[idx], name, None)) for name in
                                       columns_wv_flux]))
                idx_eig += 1

    idx_eig = 0
    with cur.copy(
            sql.SQL("COPY {}.{} ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}) FROM  STDIN").format(
                sql.Identifier(GKDB_CFG.schema),
                sql.Identifier("moments_table"),
                *[sql.Identifier(x) for x in ("gk_id", "species_id", "eigenmode_id", *columns_wv_moments)])
    ) as copy:
        for wv in ids_wavevector:
            for eig in wv.eigenmode:
                if eig.fluxes_moments is not None:
                    for specie_id, fluxmoment in zip(cleaned_id_species,
                                                     eig.fluxes_moments):
                        list_moments, list_flux = __fluxes_moment_as_list(fluxmoment, uuid_gkid)
                        for idx in (0, 1, 2):
                            copy.write_row(
                                (gk_id_fk,
                                 specie_id,
                                 id_eigenmode[idx_eig],
                                 list_moments[idx].mtype,
                                 list_moments[idx].uuid,
                                 list_moments[idx].norm,
                                 *[compressed_nparray(getattr(list_moments[idx], name, None)) for name in
                                   columns_wv_moments[3:]]))
                idx_eig += 1

    return True


# @foreign_key_exists("gyrokinetics_table")
# @profile
def _insert_wavevector_old(conn, fk_list: tuple[int | tuple[int]],
                           ids_wavevector: list | tuple | None,
                           ids_species: tuple | None,
                           uuid_gkid) -> tuple[int, dict | None]:
    """

    This method, _insert_wavevector, is used to insert wavevector information into the gyrokinetics database (gkdb).

    Parameters:
    - cur: A psycopg connection object used to connect to the database.
    - fk_list: A tuple containing two elements. The first element is an integer representing the foreign key ID for the gyrokinetics table. The second element is an integer representing the foreign key ID for the code table.
    - ids_wavevector: A list or tuple containing wavevector objects or None.

    Returns:
    - A tuple containing an integer and a dictionary or None.
      - The integer represents the status of the insertion process. It can be 1 if the insertion is successful or -1 if there is an error.
      - The dictionary contains the IDs of the inserted wavevectors and their corresponding eigenmodes.

    """

    # need gk as first and code as second
    gk_id_fk = fk_list[0][0]
    code_id = fk_list[1][0]
    if ids_wavevector is None:
        return None
    if not isinstance(ids_wavevector, (list, tuple)):
        print(f"a list of wavevector must be used as argument not a {type(ids_wavevector)}")
        return -1, {}
    cleaned_id_species = tuple([x for x in ids_species if x])

    id_wavevector = []
    # dict containing, for each wavevector_id, a list of all the corresponding eigenmodes
    wv_eig_id = {}
    for wv in ids_wavevector:
        query = ids2gkdb.query_insert_wavevector(wv, gk_id_fk, GKDB_CFG.schema)
        cur = conn.execute(query)
        wv_id = cur.fetchone()[0]
        id_wavevector.append(wv_id)
        id_eig = []
        for eig in wv.eigenmode:
            query = ids2gkdb.query_insert_eigenmode(eig, wv_id, GKDB_CFG.schema)
            cur = conn.execute(query)
            eig_id = cur.fetchone()[0]
            id_eig.append(eig_id)
            if eig.code is not None:
                eig.code.parameters = dict2pgjsonb(eig.code.parameters)
                eig.code.output_flag = int(eig.code.output_flag)
                if code_id is None:
                    raise AttributeError("current IDS does not have a root code associated")
                query = ids2gkdb.query_insert_code_partial(eig.code, ((eig_id,), (code_id,)),
                                                           GKDB_CFG.schema)
                conn.execute(query)

            if eig.fluxes_moments is not None:
                assert len(cleaned_id_species) == len(
                    eig.fluxes_moments), f"{len(cleaned_id_species)} vs {len(eig.fluxes_moments)}"
                for specie_id, fluxmoment in zip(cleaned_id_species, eig.fluxes_moments):
                    _insert_fluxes_moment(conn,
                                          ((gk_id_fk,), (eig_id,), (specie_id,)),
                                          fluxmoment,
                                          uuid_gkid
                                          )
        wv_eig_id.update({wv_id: id_eig})
    return 1, wv_eig_id or None


# @foreign_key_exists("gyrokinetics_table")
def _insert_code(conn, fk_list: tuple[int | tuple[int]] | None, ids_code) -> int | None:
    """
    Inserts a code and its associated libraries into the database.

    Parameters:
        conn: The database connection.
        fk_list: The foreign key list.
        ids_code: The code to be inserted.

    Returns:
        The ID of the inserted code or None if the insertion fails.
    """
    gk_id_fk = fk_list[0]
    code_id = None
    if ids_code is not None:
        ids_code.parameters = dict2pgjsonb(ids_code.parameters)
        query = ids2gkdb.query_insert_code(ids_code, gk_id_fk, GKDB_CFG.schema)
        cur = conn.execute(query)
        code_id = cur.fetchone()[0]
        # add libraries linked to the code
        if ids_code.library is not None:
            if isinstance(ids_code.library, (list, tuple)) is True:
                for lib in ids_code.library:
                    lib.parameters = dict2pgjsonb(lib.parameters)
                    query = ids2gkdb.query_insert_library(lib,
                                                          code_id, schema=GKDB_CFG.schema)
                    conn.execute(query)
            else:
                ids_code.library.parameters = dict2pgjsonb(ids_code.library.parameters)
                query = ids2gkdb.query_insert_library(ids_code.library,
                                                      code_id, schema=GKDB_CFG.schema)
                conn.execute(query)
    return code_id


# @foreign_key_exists("gyrokinetics_table")
def _insert_tag(cursor, fk_list: tuple[int | tuple[int]] | None, ids_tag) -> tuple[int, list[int] | None]:
    gk_id_fk = fk_list[0]
    tag_id = None
    ret_val = 0
    if ids_tag is None:
        return ret_val, None
    if not isinstance(ids_tag, (list, tuple)):
        ids_tag = (ids_tag,)
    tag_id_list = []
    ret_val = 1
    for t in ids_tag:
        tag_id: int | None = None
        query = ids2gkdb.query_insert_tag(t, GKDB_CFG.schema)
        try:
            cur = cursor.execute(query)
        except psycopg.errors.CheckViolation:
            print(f"tag {t} uses an invalid category : {t.categorie}")
            ret_val = -1
            continue
        except psycopg.errors.UniqueViolation:
            cursor.connection.rollback()
            tag_id_loc = cursor.execute(
                sql.SQL("""SELECT id FROM {}.{} as t WHERE t.tagid={} AND t.categorie={}""").format(
                    sql.Identifier(GKDB_CFG.schema),
                    sql.Identifier("tag_table"),
                    sql.Literal(t.tagid),
                    sql.Literal(t.categorie)
                )
            ).fetchone()

            assert tag_id_loc is not None, "tag_id should not be None"
            tag_id = tag_id_loc[0]
        tag_id = tag_id or cur.fetchone()[0]
        try:
            query_link = ids2gkdb.query_tag_link_gkid(tag_id, gk_id_fk,
                                                      GKDB_CFG.schema)
            cursor.execute(query_link)
        except psycopg.errors.UniqueViolation:
            pass
        tag_id_list.append(tag_id)
    return ret_val, tag_id_list or None


# @foreign_key_exists("gyrokinetics_table")
def _insert_ids_properties(conn, fk_list: tuple[int | tuple[int]] | None, ids_properties) -> int | None:
    gk_id_fk = fk_list[0]
    idsprop_id = None
    if ids_properties is not None:
        query = ids2gkdb.query_insert_ids_properties(ids_properties,
                                                     gk_id_fk, GKDB_CFG.schema)
        cur = conn.execute(query)
        idsprop_id = cur.fetchone()[0]
        if getattr(ids_properties, 'provenance', None) is not None:
            for n in ids_properties.provenance.node:
                query = ids2gkdb.query_insert_ids_provenance_node(n, idsprop_id, GKDB_CFG.schema)
                conn.execute(query)
    return idsprop_id


# @foreign_key_exists("gyrokinetics_table")
def _insert_collisions(cursor, fk_list: tuple[int | tuple[int]] | None, ids_collisions, ) -> bool | None:
    """

    _insert_collisions(cur, fk_list: tuple[int | tuple[int]] | None, ids_collisions) -> list | None

    Inserts collision data into the database.

    Parameters:
    - cur : Connection
        A psycopg connection object.
    - fk_list : tuple[int | tuple[int]] | None
        A tuple representing foreign keys for the collision data. The first element is the foreign key for the gyrokinetics
        table, and the second element is a tuple of species identifiers.
    - ids_collisions : ExtendedCollisions
        An object representing the collision data to be inserted.

    Returns:
    - list | None
        A list of IDs generated for the inserted collision data.

    Raises:
    - AttributeError
        1. If the collision table is not linked to a gyrokinetics table.
        2. If there is a length mismatch between the number of species and the collisionality_norm data.

    """

    def __pgcopy_collisions(cursor, flat_coll_array):
        with cursor.copy(
                sql.SQL("COPY {}.{} ({}, {}, {},{}) FROM  STDIN").format(
                    sql.Identifier(GKDB_CFG.schema),
                    sql.Identifier("collisions_table"),
                    *[sql.Identifier(x) for x in ("gk_id", "specie_one", "specie_two", "collisionality_norm")])
        ) as copy:
            for record in flat_coll_array:
                copy.write_row(record)

    gk_id_fk = fk_list[0][0]
    id_species = fk_list[1]
    # first gk second species
    if gk_id_fk is None:
        raise AttributeError("collision table must be link to a gyrokinetc table")

    # check array size match
    if len(ids_collisions.collisionality_norm) * len(ids_collisions.collisionality_norm[0]) != len(id_species) ** 2:
        raise AttributeError(f'length mismatch between number of species {len(id_species)} '
                             f'and collisionality_norm {len(ids_collisions.collisionality_norm)}')
    # flatten it
    if isinstance(ids_collisions.collisionality_norm, (list, tuple)):
        collisionality = np.array(ids_collisions.collisionality_norm, dtype=np.float32)
    else:
        collisionality = ids_collisions.collisionality_norm

    flatten_collisionality = []
    for idx_s1 in range(len(id_species)):
        for idx_s2 in range(len(id_species)):
            flatten_collisionality.append((gk_id_fk,
                                           id_species[idx_s1],
                                           id_species[idx_s2], collisionality[idx_s2, idx_s1]))
    # insert many (old way)
    # sql_query = ids2gkdb.__query_from_list(
    #     [sql.Identifier(x) for x in ("gk_id", "specie_one", "specie_two", "collisionality_norm")],
    #     flatten_collisionality,
    #     GKDB_CFG.schema,
    #     "collisions_table")
    # id_list = cur.execute(sql_query)
    __pgcopy_collisions(cursor, flatten_collisionality)
    return True


# return [x[0] for x in id_list.fetchall()]


# @foreign_key_exists("gyrokinetics_table")
def _insert_flux_surface(conn,
                         fk_list: tuple[int | tuple[int]] | None,
                         ids_fluxsurface) -> int | None:
    """

    Inserts a flux surface into the database.

    Parameters:
    - cur: psycopg_pool.ConnectionPool
        The connection pool to the database.
    - fk_list: tuple[int | tuple[int]] | None
        The foreign key list.
    - ids_fluxsurface: ids_gyrokinetics.IdsFluxSurface
        The flux surface object to be inserted.

    Returns:
    - int | None
        The ID of the inserted flux surface, or None if the insertion fails.

    """
    gk_id_fk = fk_list[0]

    if ids_fluxsurface is None:
        return None

    ids_fluxsurface.shape_coefficients_c = list2pgbytea(ids_fluxsurface.shape_coefficients_c)
    ids_fluxsurface.dc_dr_minor_norm = list2pgbytea(ids_fluxsurface.dc_dr_minor_norm)
    ids_fluxsurface.shape_coefficients_s = list2pgbytea(ids_fluxsurface.shape_coefficients_s)
    ids_fluxsurface.ds_dr_minor_norm = list2pgbytea(ids_fluxsurface.ds_dr_minor_norm)
    ids_fluxsurface.ip_sign = float(ids_fluxsurface.ip_sign)
    ids_fluxsurface.b_field_tor_sign = float(ids_fluxsurface.b_field_tor_sign)
    query = ids2gkdb.query_insert_flux_surface(ids_fluxsurface, gk_id_fk, GKDB_CFG.schema)
    cur = conn.execute(query)
    return cur.fetchone()[0]


# @foreign_key_exists("gyrokinetics_table")
@profile
def _insert_species(cur, fk_list: tuple[int | tuple[int]] | None,
                    ids_species: list) -> tuple[int, list[int] | None]:
    """
    Inserts species into the gyrokinetics table.

    Parameters:
        cursor (ConnectionPool): The connection pool to the database.
        fk_list (tuple[int | tuple[int]] | None): The foreign key list.
        ids_species (list): The list of species to insert.

    Returns:
        tuple[int, list[int] | None]: A tuple containing the result code and the list of inserted species IDs or None.

    Raises:
        Exception: If ids_species is not a list or tuple.

    Example usage:
        cur = ConnectionPool()
        fk_list = (1, (2, 3))
        ids_species = [1, 2, 3]
        result = _insert_species(cur, fk_list, ids_species)
    """
    gk_id_fk = fk_list[0]
    if ids_species is None:
        return 0, None

    if not isinstance(ids_species, (list, tuple)):
        print(f"ids_species must be a list or tuple not a {type(ids_species)}")
        return -1, []

    column_species = (
        "charge_norm",
        "mass_norm",
        "density_norm",
        "density_log_gradient_norm",
        "temperature_norm",
        "temperature_log_gradient_norm",
        "velocity_tor_gradient_norm"
    )

    flatten_species = []
    for s in ids_species:
        flatten_species.append((gk_id_fk, *[np.float32(getattr(s, name, None)) for name in column_species]))

    query = ids2gkdb.__query_from_list(
        [sql.Identifier(x) for x in ("gk_id", *column_species)],
        flatten_species,
        GKDB_CFG.schema,
        "species_table")
    id_species = cur.execute(query)
    id_species = [int(x[0]) for x in id_species]
    assert len(id_species) == len(ids_species), "some species were not added in the DB"
    return 1, id_species or None


# @foreign_key_exists("gyrokinetics_table")
def _insert_fluxes(conn, fk_list: tuple[int | tuple[int]],
                   ids_fluxes, uuid, ftype: str, frame: str) -> int | None:
    """

    Inserts fluxes data into the database.

    Parameters:
    - cur: The database connection object.
    - fk_list: A tuple containing foreign key values or tuples of foreign key values.
    The first element corresponds to the gyrokinetics table foreign key.
    If there are additional elements, they correspond to the eigenmode_id and specie_id foreign keys respectively.
    - ids_fluxes: An object containing fluxes data.
    - uuid: The UUID of the fluxes data.
    - ftype: The type of the fluxes data.
    - frame: The frame of the fluxes data.

    Returns:
    - The ID of the inserted fluxes data if the insertion was successful.
    - None if ids_fluxes is None or the insertion failed.

    Example usage:
    cur = ConnectionPool().getconn()
    fk_list = (1, (2, 3))
    ids_fluxes = ExtendedFluxes(...)
    uuid = "12345"
    ftype = "fluxes"
    frame = "global"

    result = _insert_fluxes(cur, fk_list, ids_fluxes, uuid, ftype, frame)
    """
    gk_id_fk = fk_list[0][0]
    if ids_fluxes is None:
        return None
    if len(fk_list) > 1:
        eigenmode_id_fk = fk_list[1][0]
        specie_id_fk = fk_list[2][0]
    else:
        eigenmode_id_fk = specie_id_fk = None

    ids_flux_ex = ExtendedFluxes(
        particles_phi_potential=shorten_imas_default(ids_fluxes.particles_phi_potential),
        particles_a_field_parallel=shorten_imas_default(ids_fluxes.particles_a_field_parallel),
        particles_b_field_parallel=shorten_imas_default(ids_fluxes.particles_b_field_parallel),
        energy_phi_potential=shorten_imas_default(ids_fluxes.energy_phi_potential),
        energy_a_field_parallel=shorten_imas_default(ids_fluxes.energy_a_field_parallel),
        energy_b_field_parallel=shorten_imas_default(ids_fluxes.energy_b_field_parallel),
        momentum_tor_parallel_phi_potential=shorten_imas_default(ids_fluxes.momentum_tor_parallel_phi_potential),
        momentum_tor_parallel_a_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_a_field_parallel),
        momentum_tor_parallel_b_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_b_field_parallel),
        momentum_tor_perpendicular_phi_potential=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_phi_potential),
        momentum_tor_perpendicular_a_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_a_field_parallel),
        momentum_tor_perpendicular_b_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_b_field_parallel),
        uuid=uuid,
        ftype=ftype,
        frame=frame
    )
    query = ids2gkdb.query_insert_fluxes(ids_flux_ex,
                                         gk_id_fk,
                                         eigenmode_id=eigenmode_id_fk,
                                         specie_id=specie_id_fk,
                                         schema=GKDB_CFG.schema)
    return conn.execute(query).fetchone()[0]


# @foreign_key_exists("gyrokinetics_table")
def _insert_moment(conn, fk_list: tuple[int | tuple[int]] | None,
                   ids_moment_ex: ExtendedMoments) -> int | None:
    gk_id_fk = fk_list[0][0]
    if ids_moment_ex is None:
        return None

    eigenmode_id_fk = fk_list[1][0]
    specie_id_fk = fk_list[2][0]

    query = ids2gkdb.query_insert_moments(ids_moment_ex,
                                          gk_id_fk,
                                          eigenmode_id=eigenmode_id_fk,
                                          specie_id=specie_id_fk,
                                          schema=GKDB_CFG.schema)
    return conn.execute(query).fetchone()[0]


def _fluxes_as_pgflux(
        ids_fluxes, uuid, ftype: str, frame: str) -> ExtendedFluxes:
    ids_flux_ex = ExtendedFluxes(
        particles_phi_potential=shorten_imas_default(ids_fluxes.particles_phi_potential),
        particles_a_field_parallel=shorten_imas_default(ids_fluxes.particles_a_field_parallel),
        particles_b_field_parallel=shorten_imas_default(ids_fluxes.particles_b_field_parallel),
        energy_phi_potential=shorten_imas_default(ids_fluxes.energy_phi_potential),
        energy_a_field_parallel=shorten_imas_default(ids_fluxes.energy_a_field_parallel),
        energy_b_field_parallel=shorten_imas_default(ids_fluxes.energy_b_field_parallel),
        momentum_tor_parallel_phi_potential=shorten_imas_default(ids_fluxes.momentum_tor_parallel_phi_potential),
        momentum_tor_parallel_a_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_a_field_parallel),
        momentum_tor_parallel_b_field_parallel=shorten_imas_default(ids_fluxes.momentum_tor_parallel_b_field_parallel),
        momentum_tor_perpendicular_phi_potential=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_phi_potential),
        momentum_tor_perpendicular_a_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_a_field_parallel),
        momentum_tor_perpendicular_b_field_parallel=shorten_imas_default(
            ids_fluxes.momentum_tor_perpendicular_b_field_parallel),
        uuid=uuid,
        ftype=ftype,
        frame=frame
    )
    return ids_flux_ex


def __fluxes_moment_as_list(
        ids_fluxes_moment, uuid, ) -> tuple[list, list]:
    list_moments = []
    if GKDB_CFG.write_moments is True:
        ids_moments_ex = ExtendedMoments(
            density=ids_fluxes_moment.moments_norm_particle.density,
            j_parallel=ids_fluxes_moment.moments_norm_particle.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_particle.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_particle.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_particle.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_particle.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_particle.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="particle",
            norm=True

        )

        list_moments.append(ids_moments_ex)

        ids_moments_ex = ExtendedMoments(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="default",
            norm=True
        )

        list_moments.append(ids_moments_ex)

        ids_moments_ex = ExtendedMoments(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density_gyroav,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel_gyroav,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel_gyroav,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular_gyroav,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel_gyroav,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular_gyroav,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy_gyroav,
            uuid=uuid,
            mtype="gyroav",
            norm=True
        )
        # new_id = _insert_moment(cursor, fk_list, ids_moments_ex)
        list_moments.append(ids_moments_ex)
    else:
        list_moments.append(*[None, None, None])

    list_fluxes = []

    list_fluxes.append(_fluxes_as_pgflux(ids_fluxes_moment.fluxes_norm_gyrocenter,
                                         uuid,
                                         "gyro",
                                         frame="lab"))
    list_fluxes.append(_fluxes_as_pgflux(ids_fluxes_moment.fluxes_norm_gyrocenter_rotating_frame,
                                         uuid,
                                         "particle",
                                         frame="lab"))
    list_fluxes.append(_fluxes_as_pgflux(ids_fluxes_moment.fluxes_norm_particle_rotating_frame,
                                         uuid,
                                         "particle",
                                         frame="rotating"))

    return list_moments, list_fluxes


# @foreign_key_exists("gyrokinetics_table")
def _insert_fluxes_moment(conn, fk_list: tuple[int | tuple[int]] | None,
                          ids_fluxes_moment, uuid, ) -> tuple[int | None]:
    """
    Inserts fluxes and moments data into the database.

    Parameters:
        conn (psycopg2.extensions.connection): The connection to the database.
        fk_list (tuple[int | tuple[int]] | None): The foreign key list.
        ids_fluxes_moment (Any): The fluxes and moments data.
        uuid (str): The UUID of the data.

    fk_list must follow the format ((gk_id,),(eig_id_0, eig_id_1,...),(specie_id_0, specie_id_1,...))
    Returns:
        tuple[int|None]: The IDs of the inserted moments and fluxes.
    """
    list_moments = []
    if GKDB_CFG.write_moments is True:
        ids_moments_ex = ExtendedMoments(
            density=ids_fluxes_moment.moments_norm_particle.density,
            j_parallel=ids_fluxes_moment.moments_norm_particle.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_particle.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_particle.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_particle.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_particle.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_particle.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="particle",
            norm=True

        )
        new_id = _insert_moment(conn, fk_list, ids_moments_ex)
        list_moments.append(new_id)

        ids_moments_ex = ExtendedMoments(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy,
            uuid=uuid,
            mtype="default",
            norm=True
        )
        new_id = _insert_moment(conn, fk_list, ids_moments_ex)
        list_moments.append(new_id)

        ids_moments_ex = ExtendedMoments(
            density=ids_fluxes_moment.moments_norm_gyrocenter.density_gyroav,
            j_parallel=ids_fluxes_moment.moments_norm_gyrocenter.j_parallel_gyroav,
            pressure_parallel=ids_fluxes_moment.moments_norm_gyrocenter.pressure_parallel_gyroav,
            pressure_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.pressure_perpendicular_gyroav,
            heat_flux_parallel=ids_fluxes_moment.moments_norm_gyrocenter.heat_flux_parallel_gyroav,
            v_parallel_energy_perpendicular=ids_fluxes_moment.moments_norm_gyrocenter.v_parallel_energy_perpendicular_gyroav,
            v_perpendicular_square_energy=ids_fluxes_moment.moments_norm_gyrocenter.v_perpendicular_square_energy_gyroav,
            uuid=uuid,
            mtype="gyroav",
            norm=True
        )
        new_id = _insert_moment(conn, fk_list, ids_moments_ex)
        list_moments.append(new_id)

    list_fluxes = []
    # fluxes_norm_gyrocenter: Optional[Fluxes]
    # fluxes_norm_gyrocenter_rotating_frame: Optional[Fluxes]
    # fluxes_norm_particle: Optional[Fluxes]
    # fluxes_norm_particle_rotating_frame: Optional[Fluxes]
    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_gyrocenter, uuid, "gyro", frame="lab")
    list_fluxes.append(new_id)

    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_gyrocenter_rotating_frame, uuid, "gyro",
                            frame="rotating")
    list_fluxes.append(new_id)

    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_particle, uuid, "particle", frame="lab")
    list_fluxes.append(new_id)

    new_id = _insert_fluxes(conn, fk_list, ids_fluxes_moment.fluxes_norm_particle_rotating_frame, uuid, "particle",
                            frame="rotating")
    list_fluxes.append(new_id)

    return tuple(list_moments), tuple(list_fluxes)


class GkdbClient:
    dbinfo = None
    pool: ConnectionPool = None
    has_read_permission: bool = False
    has_write_permission: bool = False
    has_del_permission: bool = False
    conninfo: Optional[str] = None

    def __establish_pool(self):
        self.pool = ConnectionPool(self.conninfo, configure=complex_type_register, name="pool_gkdb")

    def __init__(self, dbinfo):
        self.dbinfo = dbinfo

        self.conninfo = f"postgresql://{dbinfo.user}:{dbinfo.password}@{dbinfo.host}:{dbinfo.port}/{dbinfo.database}"
        self.__establish_pool()
        self._get_user_permissions()

    def _get_user_permissions(self):
        """
        Get user permissions.

        :return: A tuple containing the user's read, write, and delete permissions.
        :rtype: tuple

        """
        with self.pool.connection() as conn:
            permissions = conn.execute(
                r" SELECT rolname FROM pg_roles WHERE pg_has_role( CURRENT_USER, oid, 'member');").fetchall()
        permissions = tuple(x[0] for x in permissions)

        if READ_WRITE_DELETE_ROLE in permissions:
            self.has_read_permission = True
            self.has_write_permission = True
            self.has_del_permission = True
        elif READ_WRITE_ROLE in permissions:
            self.has_read_permission = True
            self.has_write_permission = True
            self.has_del_permission = False
        elif READ_ONLY_ROLE in permissions:
            self.has_read_permission = True
            self.has_write_permission = False
            self.has_del_permission = False
        elif "pg_write_all_data" in permissions:
            self.has_read_permission = True
            self.has_write_permission = True
            self.has_del_permission = True
        return self.has_read_permission, self.has_write_permission, self.has_del_permission

    def get_number_records(self):
        """Get the total number of records in the gyrokinetics table.

        Returns:
            int: The total number of records in the gyrokinetics table.

        Raises:
            psycopg.errors.UndefinedTable: If the gyrokinetics table does not exist in the database.

        Example:
            gkdb_client = GkdbClient()
            num_records = gkdb_client.get_number_records()
        """
        try:
            with self.pool.connection() as conn:
                records = conn.execute(
                    sql.SQL(r"SELECT count(uuid) FROM {}.gyrokinetics_table;").format(
                        sql.Identifier(self.dbinfo.schema))).fetchone()[0]

        except psycopg.errors.UndefinedTable:
            print("not a GKDB database")
            records = None
        return records

    def get_number_records_per_type(self):
        pass

    def get_number_records_per_code(self):
        pass

    @profile
    def add_new_ids(self, ids: Type[ids_gyrokinetics]) -> tuple[int, str | Any]:
        # TODO : check backpropagation
        if self.has_write_permission is False:
            raise PermissionError("Current User does not have write permission")

        def __insert_gyrokinetic_table() -> str:
            request = sql.SQL("INSERT into {}.{} (time) VALUES ({}) RETURNING (id, uuid)").format(
                sql.Identifier(self.dbinfo.schema),
                sql.Identifier("gyrokinetics_table"),
                sql.Literal(0.0))
            return request

        request_gk_table = __insert_gyrokinetic_table()
        with self.pool.connection() as conn:
            # get id, uuid for new IDS
            with conn.cursor() as cur:
                cur.execute(request_gk_table)
                gk_id, gk_uuid = cur.fetchone()[0]

            # with self.pool.connection() as cur:
            #     with cur.cursor() as cur:
            #         request = sql.SQL("SELECT id from {}.{}").format(
            #             sql.Identifier(self.db_info.schema),
            #             sql.Identifier("gyrokinetics_table"),
            #         )
            #         cur.execute(request)
            #
            # #   cur.commit()
            # print(f"adding ids with id {gk_id} and uuid [{gk_uuid}]")
            # with self.pool.connection() as cur:
            # ids_properties: Optional[IdsProperties]
            with conn.cursor() as cur:
                _insert_ids_properties(cur, (gk_id,), ids.ids_properties, )

                # tag: list[EntryTag]
                # TODO : debug when tag already exists
                # with cur.cursor() as cur:
                _insert_tag(cur, (gk_id,), ids.tag)

                # normalizing_quantities: Optional[InputNormalizing]
                if ids.normalizing_quantities is not None:
                    query = ids2gkdb.query_insert_input_normalizing(ids.normalizing_quantities, gk_id, GKDB_CFG.schema)
                    cur.execute(query)
                # flux_surface: Optional[FluxSurface]
                # with cur.cursor() as cur:
                _insert_flux_surface(cur, (gk_id,), ids.flux_surface)

                # model: Optional[Model]
                # with cur.cursor() as cur:
                _insert_model(cur, (gk_id,), ids.model)

                # species_all: Optional[InputSpeciesGlobal]
                if ids.species_all is not None:
                    query = ids2gkdb.query_insert_input_species(ids.species_all, gk_id, GKDB_CFG.schema)
                    cur.execute(query)

                # species: list[Species]
                # with cur.cursor() as cur:
                ret_species, id_species = _insert_species(cur, (gk_id,), ids.species)

                # collisions: Optional[Collisions]
                # with cur.cursor() as cur:

                _insert_collisions(cur, ((gk_id,), id_species), ids.collisions)
                # code: Optional[Code]
                # with cur.cursor() as cur:
                code_id = _insert_code(cur, (gk_id,), ids.code)

                # wavevector: list[Wavevector]
                # with cur.cursor() as cur:
                _insert_wavevector(cur, ((gk_id,), (code_id,)), ids.wavevector,
                                   id_species, gk_uuid)

                # time: list[ndarray[(int,), float]]

                # fluxes_integrated_norm: list[Fluxes]
                _insert_integrated_fluxes(cur, ((gk_id,),), ids.fluxes_integrated_norm, id_species, gk_uuid)

            conn.commit()

        return gk_id, gk_uuid

    def get_ids_by_uuid(self) -> str:
        pass


if __name__ == "__main__":
    test = GkdbClient(GKDB_CFG)
    print("number of records in the database : {0}".format(test.get_number_records()))
    from gkdb.tools.fakedata import generate_fake_params, generate_rnd_ids_gk
    #
    # import time
    #
    # start = time.perf_counter()
    # for _ in range(1000):
    #     new_params = generate_fake_params()
    #     new_ids = generate_rnd_ids_gk(new_params)
    #     gkid, gkuuid = test.add_new_ids(new_ids)
    #     # print(gkid, gkuuid)
    # print(time.perf_counter() - start)
    # print("number of records in the database : {0}".format(test.get_number_records()))
